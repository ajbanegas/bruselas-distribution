#!/usr/bin/python

import pybel, sys

def split_file(foo):
	for sd in pybel.readfile("mol2", foo):
		filename = sd.OBMol.GetTitle()
		filename = "VS-SF-" + filename + "-aligned.mol2"
		sd.write("mol2", filename, True)	# overwrite file


if (len(sys.argv) == 1):
	sys.exit(0)

files = sys.argv[1:]

for infile in files:
	split_file(infile)
