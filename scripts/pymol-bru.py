import __main__
__main__.pymol_argv = ['pymol', '-qc'] 		# Quiet and no GUI

import os, pymol, sys, json
pymol.finish_launching()

PROG_LISICA = 'LISICA'
PROG_OPTIPHARM = 'OPTIPHARM'
PROG_PHARMER = 'PHARMER'
PROG_SCREEN3D = 'SCREEN3D'
PROG_SHAFTS = 'SHAFTS'
PROG_WEGA = 'WEGA'

# Get the name of the program that generated the file
def get_menu(code):
	if 'LI' == code:
		return PROG_LISICA
	elif 'WG' == code:
		return PROG_WEGA
	elif 'S3' == code:
		return PROG_SCREEN3D
	elif 'OP' == code:
		return PROG_OPTIPHARM
	elif 'SF' == code:
		return PROG_SHAFTS
	elif 'PH' == code:
		return PROG_PHARMER

	return 'OTHERS'

# Make a menu for Screen3D
def group_S3(code, ligand, query, program, menu, visible, score):
	label_l = program + '_' + code + str(score)
	label_q = 'query_' + code
        group = 'ligands_' + code
	root = menu

        exists_l = os.path.isfile(ligand)
        if not exists_l:
                return

        pymol.cmd.load(ligand, label_l)
        pymol.cmd.load(query, label_q)

        pymol.cmd.group(group, label_q)
        pymol.cmd.group(group, label_l)
        pymol.cmd.group(root, group)

        # Enable or disable the molecules
        if visible == 1:
                pymol.cmd.enable(label_l)
                pymol.cmd.enable(label_q)
        	pymol.cmd.enable(group)
        else:
                pymol.cmd.disable(label_l)
                pymol.cmd.disable(label_q)
                pymol.cmd.disable(group)

# Make a menu for a program other than Screen3D
def group_other(code, ligand, query, program, menu, visible, score):
	label_l = program + '_' + code + str(score)
	label_q = 'query_' + program
        group = 'ligands_' + menu
	root = menu
	
	exists_l = os.path.isfile(ligand)
	if not exists_l:
		return

        pymol.cmd.load(ligand, label_l)
        pymol.cmd.load(query, label_q)

        pymol.cmd.group(group, label_l)
        pymol.cmd.group(root, group)
        pymol.cmd.group(root, label_q)

        # Enable or disable the molecule
        if visible == 1:
        	pymol.cmd.enable(label_l)
        else:
                pymol.cmd.disable(label_l)


# Load and process file
def load_file(item):
	program = item["program"]
        ligand = item["ligand"]
	query = item["query"]
	code = item["code"]
        score = item["score"]
        visible = int(item["visible"])

        # Get the menu entry where the molecule is to attached
        menu = get_menu(program)

        # Load the molecules and attach them to the menu
	label_l = program + '_' + code + '_' + str(score)
	root = program

	if menu == PROG_SCREEN3D:
		group_S3(code, ligand, query, program, menu, visible, score)
	else:
		group_other(code, ligand, query, program, menu, visible, score)

# Set the global settings
def set_settings():
	pymol.cmd.show_as('sticks','all')
        pymol.cmd.hide('sticks','elem h and (neighbor elem c)') # hide non polar hydrogens
        pymol.cmd.bg_color('black')
        pymol.cmd.zoom('all')

# ##### Main #####
if len(sys.argv) < 2:
	print "usage: pymol-bru-json.py <json file>"
	exit()

try:
	with open(sys.argv[1],'r') as json_file:
		# Load the full JSON definition
		data = json.load(json_file)

		# Build the session file
		id = data["id"]
		path=os.path.dirname(sys.argv[1])
		session_file = path + '/BRUSELAS-' + str(id) + ".pse"

		# Process all the files individually
		i = 0
		while i < len(data["data"]):
			item = data["data"][i]
			load_file(item)
			i += 1

	# General settings (background, visibility....)
	set_settings()

	# Save the session
	pymol.cmd.save(session_file)

except Exception as e:
	print e

pymol.cmd.quit()
