import json, sys

with open(sys.argv[1]) as file:
	data = json.load(file)

	features = {}
	model = []

	for p in data['points']:
		if not p['name'] in features:
			if p.get('radius'):
				p['radius'] = 1
			if p.get('vector'):
				p['vector'] = None
			p['enabled'] = True
			p['vector_on'] = 0
			p['minsize'] = ""
			p['maxsize'] = ""
		else:
			p['enabled'] = False
			p['vector'] = None
			p['vector_on'] = 0

		model.append(p)
		features[p['name']] = 1

with open(sys.argv[2],'w') as file:
	json.dump(data, file, indent=4)
