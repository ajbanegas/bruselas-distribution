#!/usr/bin/python

import pybel, sys

def get_ligand_name(foo_map, line):
	f = open(foo_map, "r")
	linelist = f.readlines()
	return linelist[line-1].strip()

def write_ligand(filename, source):
	output = open(filename, "w")
	output.write(source.write("mol2"))
	output.close()

def set_title(filename, title):
	foo = pybel.readfile("mol2", filename).next()
	foo.OBMol.SetTitle(title)
	write_ligand(filename, foo)

def split_file(foo, foo_map, prefix):
	index = 1
	for mol2 in pybel.readfile("mol2", foo):
		ligand = get_ligand_name(foo_map, index)
		filename = "VS-" + prefix + "-" + ligand + "_aligned.mol2"
		write_ligand(filename, mol2)
		set_title(filename, ligand)
		index = index + 1

# validar llamada
if (len(sys.argv) == 1):
	sys.exit(0)

infile = sys.argv[1]
dictionary = sys.argv[2]
prefix = sys.argv[3]

# dividir bloque en ficheros individuales
split_file(infile, dictionary, prefix)
