import sys
import numpy as np

def load(foo):
    rmsds = []
    with open(foo, 'r') as f:
        lines = f.readlines()
	rmsds = rmsds + map(lambda x: float(x.split('####')[0]), lines)

    return rmsds

def normalize(rmsd, valmin, valmax):
    value = 1.0 - ((rmsd - valmin)/(valmax - valmin))
    return max(0.0, value)

def calculate(foo, valmin, valmax):
    out = []
    with open(foo, 'r') as f:
	lines = f.readlines()
	out = map(lambda l: str(normalize(float(l.split('####')[0]), valmin, valmax)) + '####' + l.split('####')[1], lines)

    with open(foo, 'w') as f:
    	f.write(''.join(out))

def main(argv):
    rmsds = []
    for f in argv[1:]:
        rmsds = rmsds + load(f)

    lowest = 0.0 #np.min(rmsds)
    greatest = 1.5 #np.max(rmsds)

    for f in argv[1:]:
	calculate(f, lowest, greatest)

if __name__ == "__main__":
    main(sys.argv)

