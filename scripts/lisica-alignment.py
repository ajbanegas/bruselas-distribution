#!/usr/bin/python

import __main__
__main__.pymol_argv = ['pymol', '-qc'] 		# Quiet and no GUI

from shutil import copyfile

import os, pymol, re, sys, time
pymol.finish_launching()

# Carga un fichero mol2 y devuelve el objeto que lo contiene
def load_molecule(path):
	try:
		mpath = os.path.abspath(path)
		name = mpath.split('/')[-1].split('.')[0]
		name = "q_" + name + "_" + str(int(time.time()/100))
		pymol.cmd.load(mpath, name)
		return name
	except Exception as e:
		print("Error in load_molecule")
		print(e)

# Busca la primera linea de datos generados por LiSiCA
def find_data(lines):
	j = -1
	for line in lines:
		if "LiSiCA  RESULTS" in line:
			return j+5
		j = j+1
	return 0

# Lee la seccion "Results" generada por LiSiCA y rellena los arrays
def read_atoms(path, obj1, obj2):
	joins = []
	
	with open(path) as f:
		lines = f.readlines()

	# de la linea inicial hasta una en blanco	
	init = find_data(lines)
	if init > 0:
		for i in range(init, len(lines)):
			line = lines[i].strip()
			if len(line) == 0:
				break
			chunks = re.split(r'\t+', line)
			joins.append(obj2 + "////" + chunks[3])
			joins.append(obj1 + "////" + chunks[1])
	return joins

# Alinea las dos moleculas en base a los atomos proporcionados
def align_molecules(atoms):
	pymol.cmd.pair_fit(*atoms)

# Guarda el ligando alineado en el fichero indicado
def save_alignment(obj, foo):
	retries = 10
	while True and (retries >= 0):
		try:
			pymol.cmd.save(foo, obj)
		except Exception:
			retries = retries - 1
			continue
		break

	if retries < 0:
		return 0

	return 1

# Copia el fichero de origen en el destino
def copy_file(src, target):
	copyfile(src, target)

def doit(argv1, argv2, argv3):
	print(argv1 + " + " + argv2 + " -> " + argv3)

	# procesar parametros de entrada
	query = load_molecule(argv1)
	ligand = load_molecule(argv2)
	output = argv3
	# alinear y salvar
	res = read_atoms(argv2, query, ligand)
	if len(res) > 0:
		align_molecules(res)
		exit = save_alignment(ligand, output)
		if exit == 0:
			copy_file(argv2, output)
	else:
		copy_file(argv2, output)

# ##### Main #####
if len(sys.argv) < 4:
	print("usage: lisica-alignment.py <query.mol2> <lisica-generated.mol2> <aligment-output.mol2>")
	exit()

try:
	doit(sys.argv[1], sys.argv[2], sys.argv[3])

except Exception as e:
	print("ERROR: " + sys.argv[1] + " and " + sys.argv[2])
	print(e)

# salir
pymol.cmd.quit()

