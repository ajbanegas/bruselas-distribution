from os import listdir
import sys
import re
import os

# DEPRECATED, NOT USED AND NOT DOCUMENTED

rutaImagenesWEB="/srv/www/antonio/imagenesWeb/"
spath = sys.argv[1]  #path donde se buscaran los ficheros y se generaran las imagenes
opcion= sys.argv[2]  #opcion de docking

if opcion == "WG" or opcion == "wg":
	#dependiendo de si es wega con vina la extension y los ligandos se guardan en caarpetas diferentes
	opcion="WG"
	extension="sd"
	pathOri=rutaImagenesWEB+"ligandos/lig_"+extension+"/"
if opcion == "LI" or opcion == "li" or opcion == "S3" or opcion == "s3":
	opcion=opcion.upper()
	extension="mol2"
	pathOri=rutaImagenesWEB+"ligandos/lig_"+extension+"/"
if opcion== "AD" or opcion== "ad":
	opcion="AD"
	extension="pdbqt"
	pathOri=rutaImagenesWEB+"ligDocking/"

for lig in listdir(spath):
	if len(re.findall('\\b'+extension+'\\b', lig ) ) > 0: 
		if (opcion == "WG" and lig != "ligand."+extension) or (opcion == "LI" and lig != "ligand."+extension) or (opcion == "S3" and lig!= "ligand."+extension):
			output=lig.split('/')[-1].split('.')[0]
			output=spath+output.split('.')[0]+"P.png"
			ligOri=lig[10:]
			ligOri=ligOri[:ligOri.find("lig")+3]
			lig=spath+lig
			ligOri=pathOri+ligOri+"."+extension
			os.system("python "+rutaImagenesWEB+"scriptOrientacion.py "+ligOri+" "+ lig +" "+output)
		if opcion =="AD" and lig != "ligand."+extension:
			output=lig.split('/')[-1].split('.')[0]
			output=spath+output.split('.')[0]
			output=output[:-4]+"P.png"
			ligOri=lig[8:]
			ligOri=ligOri[:ligOri.find("_rec")]
			ligOri=pathOri+ligOri+"Lig.pdb"
			ligDock=spath+lig
			os.system("python "+rutaImagenesWEB+"scriptOrientacion.py "+ligOri+" "+ ligDock +" "+output)
