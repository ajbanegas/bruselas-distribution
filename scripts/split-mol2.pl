#!/usr/bin/perl
use strict;
use warnings;
use Cwd;

my $file="";
my @ligands=();
my $mol2="";
my $workdir=getcwd;
my $prefix="";
my $outdir=".";

for my $i (0..@ARGV-1) {
	if ($ARGV[$i] eq "-f") { ++$i; $file = $ARGV[$i]; }
	if ($ARGV[$i] eq "-p") { ++$i; $prefix = $ARGV[$i]; }
	if ($ARGV[$i] eq "-d") { ++$i; $outdir = $ARGV[$i]; }
}

open IN, "$file";
while (<IN>) {
	my $line = $_;
        chomp($line);
        if ($line =~ m/@<TRIPOS>MOLECULE/ && $mol2 eq "") {
        	$mol2.=$line."\n";
	} elsif ($line =~ m/@<TRIPOS>MOLECULE/ && $mol2 ne "" ) {
		my $ligand=pop(@ligands);
		my $end=substr $ligand, -2;
        	my $outfile.="./".$outdir."/".$ligand.".mol2";
                open OUT, ">$outfile";
                print OUT "$mol2";
                close OUT;
		$mol2=$line."\n";
        } else {
                if($line =~ m/$prefix/) {push(@ligands,$line);}
                $mol2.=$line."\n";
                chomp($line);
        }
}
my $ligand=pop(@ligands);
my $end=substr $ligand, -2;
my $outfile.="./".$outdir."/".$ligand.".mol2";
open OUT, ">$outfile";
print OUT "$mol2";
close OUT;

close IN;
exit;
