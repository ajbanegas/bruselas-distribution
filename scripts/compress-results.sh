#!/bin/bash

id=$1
path=$2 #../../imagenesWeb/${id}
dest=${path}/${id}

count() {
	echo $(find ${path} -name "$1" | wc -l)
}

copy() {
	name=$1
	pattern=$2

	n=$(count "${pattern}")
	if [[ "${n}" -gt 0 ]]; then
		echo -n "Copying ${name} files..."
		dir=${dest}/${name}
		rm -rf ${dir}
        	mkdir -p ${dir}
		cp ${path}/${pattern} ${dir}

		if [ -f ${path}/model.sdf ] && [ "${name}" == "Pharmer" ]; then
			cp ${path}/model.sdf ${dir}
		fi

		echo "[Done]"
	fi
}

# Crear la carpeta de destino
mkdir ${dest}

# Copiar la query
query=$(find ${path} -maxdepth 1 -name 'ligand.*' ! -name 'ligand.smi' ! -name 'ligand.png')
format=${query##*.}
cp ${path}/ligand.${format} ${dest}/query.${format}

# Verificar si hay compuestos y copiarlos
copy "LiSiCA" "VS-LI-*"
copy "WEGA" "VS-WG-*"
copy "Screen3D" "VS-S3-*"
copy "OptiPharm" "VS-OP-*"
copy "SHAFTS" "VS-SF-*"
copy "Pharmer" "VS-PH-*"

# Comprimir todo y eliminar carpeta temporal
cd ${path}
tar czf BRUSELAS-${id}.tar.gz ${id}
rm -rf ${id}
