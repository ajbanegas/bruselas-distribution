#!/usr/bin/python

import argparse, glob, os, pybel, sys


def encode_names(foo, ext):
	mols = []

	for mol in pybel.readfile(ext, foo):
		title = mol.OBMol.GetTitle().encode("hex")
		mol.OBMol.SetTitle(title)
		mols.append(mol.write(ext))

	with open(foo, "w") as f:
    		f.write("".join(mols))

def decode_names(foo, ext):
        mols = []

        for mol in pybel.readfile(ext, foo):
                title = mol.OBMol.GetTitle().decode("hex")
                mol.OBMol.SetTitle(title)
                mols.append(mol.write(ext))

        with open(foo, "w") as f:
                f.write("".join(mols))

# Main flow

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('--a', dest='action', required=True, help='Action to perform: encode | decode')
	parser.add_argument('--f', dest='foo', required=False, help='A file to process')
	parser.add_argument('--d', dest='dir', required=False, help='Process all the files in the directory matching the pattern "*aligned*"')
	args = parser.parse_args()
	
	if (args.action == 'encode'):
		filename = args.foo
		file_extension = os.path.splitext(filename)[1]
		file_extension = file_extension[1:]
		encode_names(filename, file_extension)
	elif (args.action == 'decode'):
		# list all the '*aligned*' files in the folder
		files = glob.glob(args.dir + '/*aligned*')
		for foo in files:
			filename = foo
			file_extension = os.path.splitext(filename)[1]
                	file_extension = file_extension[1:]
			decode_names(filename, file_extension)
	else:
		print('usage: renameMols.py [-h] --a ACTION [--f FOO] [--d DIR]')
		sys.exit(-1)
