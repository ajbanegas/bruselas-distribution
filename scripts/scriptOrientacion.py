import __main__
__main__.pymol_argv = [ 'pymol', '-qc'] # Quiet and no GUI

import sys, time, os
import pymol
pymol.finish_launching()

# DEPRECATED, NOT USED AND NOT DOCUMENTED

#_______________________________no se utiliza
pymol.cmd.set('ray_trace_frames', 1) 
def pnghack(filepath, width=612, height=384):
	pymol.cmd.viewport(width, height) 
	pymol.cmd.mpng(filepath)
	#pymol.cmd.mplay()  
#______________________________________________________________________

spath = os.path.abspath(sys.argv[1]) #ligando 1
lig1 = spath.split('/')[-1].split('.')[0] #nombre ligando 1
spath2 = os.path.abspath(sys.argv[2])	#ligando 2
lig2 = spath2.split('/')[-1].split('.')[0] #nombre ligando 2
outPut= sys.argv[3]
pymol.cmd.load( spath, lig1) #los cargo
pymol.cmd.load( spath2, lig2)
pymol.cmd.disable("all")	#deselecciono todo
pymol.cmd.enable(lig1,1)   #selecciono lig1 enable
pymol.cmd.enable(lig2,1)	
pymol.cmd.orient(lig1,animate=-1)
pymol.cmd.show("sticks"    ,"all")
pymol.cmd.set('stick_radius',0.08)
pymol.cmd.bg_color('white')
#pymol.cmd.space('cmyk')
#pymol.util.performance(0)

#pnghack(outPut+".png")

#save image
imageH = 612
imageW = 348
pymol.cmd.viewport(imageH,imageW)
pymol.cmd.ray()
pymol.cmd.png(outPut)
#pymol.cmd.png ("imagen.png")



#pymol.cmd.save("imagen.png")
# Get out!
pymol.cmd.quit()


