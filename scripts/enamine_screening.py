#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Author: Antonio Jesús Banegas-Luna
#   Email: ajbanegas@ucam.edu
#   Date:   01/03/2021
#   Description: Builds an on-the-fly library on the basis of screening Enamine datbase. It works on Python2
#   https://www.enaminestore.com/api
# ______________________________________________________________________________________________________________________
import argparse
import json
import os
import urllib, urllib2
from pybel import readfile, readstring

URL_API = 'https://www.enaminestore.com/api?code=search_{}_sim&sim={}&mode={}'
AUTH_HEADER_KEY = 'Authorization'
AUTH_HEADER_VALUE = 'login={}, pass={}'

""" Returns the extension of the given file """
def get_format(foo):
	filename, file_extension = os.path.splitext(foo)
	return file_extension[1:]

""" Run the search """
def screen(url):
    request = urllib2.Request(url)
    request.add_header(AUTH_HEADER_KEY, AUTH_HEADER_VALUE.format('biohpc2015@gmail.com', 'b10HPC3namin3'))
    response = urllib2.urlopen(request)
    data = response.read().decode('utf-8')
    return json.loads(data)

""" Convert a SMILES to 3D mol2 format """
def smi2mol2(id_, smiles_):
    smiles = '{} {}'.format(smiles_, id_)
    mol = readstring('smi', smiles)
    mol.make3D()
    mol2 = mol.write('mol2')
    return mol2

""" Write a hit into the output file """
def append_hits(id, smiles, file, gen3d):
    if gen3d:
        mol2 = smi2mol2(id, smiles)
        file.write('%s\n' % mol2)
    else:
        file.write('%s %s\n' % (smiles, id))

""" Perform VS on all then Enamine datasets """
def screening(input_file, smiles, output_file, threshold, max_size, gen3d):
    # if an input file is given, then take the first molecule in it and convert it to smiles
    if not input_file is None:
    	fmt = get_format(input_file)
    	mol = readfile(fmt, input_file).next()
    	smiles = mol.write('smi').split()[0]
    elif smiles is None: # if not, then a smiles code is required
    	raise Exception('Either a SMILES or a file is required.')

    #try:
    # call the API with the smiles (either from a file or from command line)
    datasets = ['SCR', 'BB', 'REAL']
    hits = []

    for dataset in datasets:
        print('Screening {} dataset'.format(dataset))

        try:
            smiles = urllib.quote(smiles)
            url = URL_API.format(smiles, str(threshold), dataset)
            result = screen(url)

            # loop through the results and join the molecules
            if result['result']['code'] != 0:
                print('Error while screening {} dataset: {}'.format(dataset, result['result']['message']))
                continue

            with open(output_file, 'a+') as f:
                print('Found {} hits'.format(len(result['data'])))

                for comp in result['data']:
                    if comp['Id'] in hits and len(hits) <= max_size:
                        continue
                    print('Escribiendo {}'.format(comp['Id']))
                    append_hits(comp['Id'], comp['smile'], f, gen3d)
                    hits.append(comp['Id'])
                    print('Encontrados: {}'.format(len(hits)))
    
        except Exception as e:
            continue

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-file', help='Input file in whatever format supported by Obabel', type=str)
    parser.add_argument('-s', '--smiles', help='SMILES representation of a molecule', type=str)
    parser.add_argument('-o', '--output-file', help='File where the hits will be written', type=str)
    parser.add_argument('-t', '--threshold', help='Minimun score required to consider a hit', type=float, default=0.0)
    parser.add_argument('-m', '--max_size', help='Maximum number of hits to include in the library', type=int, default=300)
    parser.add_argument('-g', '--gen3d', help='Whether the output must be converted to 3D mol2', action='store_true')

    args = parser.parse_args()
    screening(args.input_file, args.smiles, args.output_file, args.threshold, args.max_size, args.gen3d)
