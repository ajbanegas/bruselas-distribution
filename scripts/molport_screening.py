#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Author: Antonio Jesús Banegas-Luna
#   Email: ajbanegas@ucam.edu
#   Date:   01/03/2021
#   Description: Builds an on-the-fly library on the basis of screening MolPort database. It works on Python2
#   https://www.molport.com/shop/api
#   https://www.molport.com/shop/api-documentation-v-3-0
# ______________________________________________________________________________________________________________________
import argparse
import json
import os
import requests
from pybel import readfile, readstring

URL_API = 'https://api.molport.com/api/chemical-search/search'

""" Returns the extension of the given file """
def get_format(foo):
	filename, file_extension = os.path.splitext(foo)
	return file_extension[1:]

""" Run the search """
def screen(smiles, threshold, max_size):
    payload = { "User Name":"hperez@ucam.edu", "Authentication Code":"2apGpwNeoa9dWmy", "Structure":smiles, "Search Type":4, "Maximum Search Time":60000, "Maximum Result Count":max_size, "Chemical Similarity Index":threshold }
    r = requests.post(URL_API, json=payload)
    return r.json()

""" Convert a SMILES to 3D mol2 format """
def smi2mol2(id_, smiles_):
    try:
        smiles = '{} {}'.format(smiles_, id_)
        mol = readstring('smi', smiles)
        mol.make3D()
        mol2 = mol.write('mol2')
        return mol2
    except Exception as e:
        return None

""" Write a hit into the output file """
def append_hits(id, smiles, file, gen3d):
    if gen3d:
        mol2 = smi2mol2(id, smiles)
        if not mol2 is None:
             file.write('%s\n' % mol2)
    else:
        file.write('%s %s\n' % (smiles, id))

""" Perform VS on all then MolPort datasets """
def screening(input_file, smiles, output_file, threshold, max_size, gen3d):
    # if an input file is given, then take the first molecule in it and convert it to smiles
    if not input_file is None:
    	fmt = get_format(input_file)
    	mol = readfile(fmt, input_file).next()
    	smiles = mol.write('smi').split()[0]
    elif smiles is None: # if not, then a smiles code is required
    	raise Exception('Either a SMILES or a file is required.')

    # call the API with the smiles (either from a file or from command line)
    try:
        result = screen(smiles, float(threshold), int(max_size))

        # loop through the results and join the molecules
        if result['Result']['Status'] != 1:
            raise Exception('Error while screening dataset: {}'.format(result['Result']['Message']))            

        with open(output_file, 'a+') as f:
            print('Found {} hits'.format(len(result['Data']['Molecules'])))

            for comp in result['Data']['Molecules']:
                comp_smiles = comp['SMILES'] if 'Canonical SMILES' not in comp else comp['Canonical SMILES']
                append_hits(comp["MolPort Id"], comp_smiles, f, gen3d)
    
    except Exception as e:
        raise e

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-file', help='Input file in whatever format supported by Obabel', type=str)
    parser.add_argument('-s', '--smiles', help='SMILES representation of a molecule', type=str)
    parser.add_argument('-o', '--output-file', help='File where the hits will be written', type=str)
    parser.add_argument('-t', '--threshold', help='Minimun score required to consider a hit', type=float, default=0.0)
    parser.add_argument('-m', '--max_size', help='Maximum number of hits to include in the library', type=int, default=300)
    parser.add_argument('-g', '--gen3d', help='Whether the output must be converted to 3D mol2', action='store_true')

    args = parser.parse_args()
    screening(args.input_file, args.smiles, args.output_file, args.threshold, args.max_size, args.gen3d)

