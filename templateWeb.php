<?php

$title="Bruselas";					
$logos="UCAM.png:SENECA.png:NHLPC2.png:NILS.png:IOANNINON2.png";
//$path="http://bio-hpc.ucam.edu/".$title."/";
$path="http://localhost:82/".$title."/";
$nav="Home:Calculations:Results:Browse:Tutorial:FAQ:Funding:Contact:References";
// It can be home or another depending on the URL
if ($_SERVER['REQUEST_URI']=="/".$title."/" || $_SERVER['REQUEST_URI']=="/".$title."/index.php") {	
	$pagina="Home";						// name of the home page
	$pathTemplate="../template/";		// path to the template
	$pathWeb="web/";					// path to web content
	$pathRoot="";
} else {
	$pag=split('/',$_SERVER['REQUEST_URI']);	
	$pag=split('\.',$pag[4] );
	$pagina=$pag[0];						// page must be named as the php file
	$pathWeb="../";							// path of webs
	$pathRoot="../../";	
	$pathTemplate="../../../template/";		// template
}
include $pathTemplate.'index.php';
?>
