###																														###
###		Script que automatiza El proceso de filtrado para encontrar los mejores resultados en las pruebas realizadas	###	
###		de dcking																										###
###_____________________________________________________________________________________________________________________###
import os,sys,string,re
##clases propias###################################################################
from Get_histogram.get_Histogram import Histogram
from Get_histogram.saveResults import SaveResults
from Get_histogram.energyHistogram import EnergyHistogram
from Get_histogram.generateFilePymol import GenerateFilePymol
from Get_histogram.poseViewGrap import PoseviewGrap
from Get_histogram.distanceGraph import DistanceGraph
from Get_histogram.mutacionGraphs import MutacionGraphs
###################################################################################
import commands
import os,sys,string,re,getopt


#import matplotlib.path as path
import os.path
RESULTADOSFICHEROS =100 #numero de mejores resultados que se guardaran en ficheros
RESULDAOSMEJORSCORE=10
extension=".txt"
bestScore={}


###_______________________________________________________________________________________________________________________
###
###		Crea las rutas necesarias para guardar los resultados							 
###_______________________________________________________________________________________________________________________
def crearRutas():
	ruta=fileEntrada+fileEntrada;
	ruteBestScore=ruta+"bestScore/"
	if os.path.exists(ruta): 
		comando="rm -r "+ruta
		commands.getoutput(comando)
	comando="mkdir " +ruta 
	commands.getoutput(comando)
	if  os.path.exists(ruteBestScore): #esto deberia ser imposible
		comando="rm -r "+ruteBestScore
		commands.getoutput(comando)
	comando="mkdir " +ruteBestScore 
	commands.getoutput(comando)
	return ruta, ruteBestScore
	
###___________________________________________________________________________________________________________________
###
###						Main
###___________________________________________________________________________________________________________________
if len(sys.argv) == 1:
		print("\nUsage: %s -i input Docking -p proteinFile -l ligFile -c min Score -s poseview y -z clusterizado y" % sys.argv[0]+"\n")
		exit()
# Read command line args
myopts, args = getopt.getopt(sys.argv[1:],"i:p:l:c:s:z:t:d:")
fileEntrada='' 
fileProteina=''
fileLigando=''
poseview="y"
engCorte=0
intaracciones="y"
clusterizado="n"
distanceGraph="n"
modeDeug="100"
###############################
# o == option
# a == argument passed to the o
###############################
for o, a in myopts:
	if o == '-i':
		fileEntrada=a
	elif o == '-p':
		fileProteina=a
	elif o == '-c':
		engCorte=a
	elif o == '-l':
		fileLigando=a	
	elif o == '-s':
		poseview=a
	elif o == '-z':
		clusterizado=a
	elif o == '-t':
		intaracciones=a
	elif o == '-d':
		modeDeug=a
	else :
		print "pro"
		print("\nUsage: %s -i input Docking -p proteinFile -l ligFile -c min Score -s poseview y -z clusterizado y -t inteacciones y -d debug y" % sys.argv[0]+"\n")
		exit()



# 'ff.html'
comando="module load openbabel"
commands.getoutput(comando)	
comando=" module load imagemagick"
commands.getoutput(comando)
#print  fileEntrada
#fileEntrada=os.path.basename(fileEntrada)
#print  fileEntrada
#exit()
if fileEntrada.count('/') > 1: #esto es por si se lanza desde el cluseter que le quite la ruta absoluta 
	fileEntrada=fileEntrada[:fileEntrada.rindex("/")]	
	fileEntrada=fileEntrada[fileEntrada.rindex("/")+1:]+"/"	
ruta, ruteBestScore =crearRutas()

tmpCadena=fileEntrada[fileEntrada.index("-")+1:]  ##subcadena para obtener el programa
programa=tmpCadena[:tmpCadena.index("-")]		  ##subcadena para obtener el programa
opcion=fileEntrada[:fileEntrada.index("-")]		  ## variable que define el programa segun el nombre 
countSamples=0
fileSalida=ruteBestScore+ fileEntrada[:fileEntrada.rindex("/")]

#___________________________________________________________________________________________________________________
#						Se llama a la clase que genera el historgrama
#___________________________________________________________________________________________________________________
histograma=Histogram(fileEntrada,programa, opcion,RESULTADOSFICHEROS,fileProteina,fileLigando, fileSalida, engCorte,modeDeug)
histograma.getData()
histograma.generateHistogram()
bestScore=histograma.bestScore ##devueleve un diccionario con los mejores ligandos y sus energias
print "Histograma Creado"
#___________________________________________________________________________________________________________________
#	1					Se llama a la clase que genera el saveResults que almacena en carpetas los resutaldos
#___________________________________________________________________________________________________________________

saveResults=SaveResults(bestScore,RESULDAOSMEJORSCORE, fileProteina, programa,ruteBestScore,ruta, fileEntrada,opcion,fileLigando,clusterizado,modeDeug)
saveResults.saveFilesResutls()
print "Resultados Guardados"


#_____________________________________________________________________________________________________________
#	2					Genera los ficheros pml para puybol si es BD genera bolas tb
#_________________________________________________________________________________________________________________
generateFilePymol=GenerateFilePymol(fileProteina,saveResults.nombresLigandos,fileSalida,fileEntrada,saveResults.allfich,ruteBestScore,opcion,fileLigando,clusterizado,engCorte,programa,histograma.data,modeDeug)
generateFilePymol.crearBolasProt(saveResults.extProteina)
generateFilePymol.crearFicheroPmlPymol()
#print saveResults.nombresLigandos
saveResults.nombresLigandos=generateFilePymol.nombresLigandos ##si no se modifica es el mismo y si se modifica se guardan los nuevos datos
#print saveResults.nombresLigandos
print "Fichero pymol Creado"

#_____________________________________________________________________________________________________________
#	3					Si existe custerizado se buscan las distancias entre clusters
#_________________________________________________________________________________________________________________

if clusterizado=="y":
	distanceGraph=DistanceGraph(ruteBestScore,programa,saveResults.nombresLigandos,modeDeug)
	distanceGraph.generateDistGraph()
	print "Grafica de distancias generada"


#_____________________________________________________________________________________________________________
#	4					Genera los diagramas de poseView
#_________________________________________________________________________________________________________________
if (poseview=="y"):
	poseViewGrap=PoseviewGrap(fileProteina,ruteBestScore,saveResults.nombresLigandos,saveResults.extProteina,modeDeug)
	poseViewGrap.generatePoseView()
	print "Ficheros PostView generados"

#_____________________________________________________________________________________________________________
#	5					Clases que generarn las graficas de energias
#_________________________________________________________________________________________________________________
if intaracciones == "y":
	energyHistogram=EnergyHistogram(ruteBestScore,programa,modeDeug)
	energyHistogram.generateHistograms()
	energyHistogram.graficaEnergiaColectiva(saveResults.extProteina, saveResults.nombresLigandos) ##se le pasa la extesion tempora previ
	print "Graficas de energias generadas"

#___________________________________________________________________________________________
#						Solo para el caso de mutacion 
#_________________________________________________________________________________________
if opcion == "SD":
	mutacionGraphs=MutacionGraphs(ruteBestScore,saveResults.extProteina,fileLigando,modeDeug)
	mutacionGraphs.generateGraph()





tarGz=fileEntrada+fileEntrada[:fileEntrada.rindex("/")]+".tar.gz"
comando="tar czvf "+tarGz+" "+ruta
commands.getoutput(comando)	
comando="mv  "+tarGz+	" ."
commands.getoutput(comando)	


#	print "El resultado se encuentra en:"	
#		print tarGz	




	
	
	 



