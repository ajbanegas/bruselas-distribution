##################################################################
#
#	Pone las cargas de un ligando mol2 en otro pdbqt (Se hace por que los metodos como babel o prepare ligand modifican las cargas)
#	Antes de nada se debe transformar el ligando con babel
#	Ejecucinon python modCharges.py lig1 lig2
#	lig1= ligando con las cargas buenas 
#	Lig2= ligando que se modificaran las cargas
####################################################################
import sys, commands
if len(sys.argv) != 3:
		print("\nUsage:python modCharges ligando con cargas buenas ligando a modificar\n")
		exit()


lig1=sys.argv[1]
lig2=sys.argv[2]
contador=0
inFile=open(lig1,'r')

for i in inFile:
	i=i.rstrip("\n")
	if i.find("@<TRIPOS>BOND") !=-1: ## final de la conformacion
		contador+=1
	
	if contador==1:
		linea=i.split()
		atom=linea[1]		
		charge=round(float(linea[8]),3)
		charge="%.3f" % charge
		carga=str(charge)
		comando="cat "+lig2+ "  |grep -n \""+atom+" \"" 
		
		result=commands.getoutput(comando)	

		fila=result.split(':')
		comienzoFila=fila[1][:70]
		finFila=fila[1][76:]
		if carga[0] != "-":
			carga=" "+carga
		filaInsertar=comienzoFila + carga+ finFila
		#print filaInsertar
		comando="sed -i '"+fila[0]+"s/.*/"+filaInsertar+"/' "+lig2
		
		a=commands.getoutput(comando)	
		print a
		
	if i.find("@<TRIPOS>ATOM") != -1: ##principio de la conformacion
		contador+=1


