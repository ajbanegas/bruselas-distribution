#
#	Divide una proteina en sus diferentes modelos.
#	Se le pasa la proteina y lugegoi una carpeta que exsitsa para guardarlos modelos
	#python pymolSplitStates.py 3s5z.pdb /home/biohpc/3s5z
#
import __main__
__main__.pymol_argv = [ 'pymol', '-qc'] # Quiet and no GUI

import sys, time, os
import pymol
from pymol import stored

pymol.finish_launching()
##
# Read User Input
spath = os.path.basename(sys.argv[2])
dirName=os.path.dirname(sys.argv[2])
#sname = spath.split('/')[-1].split('.')[0]
proteina=sys.argv[1]
outPut=sys.argv[2]
pymol.cmd.load(proteina,"state1")
pymol.cmd.disable("all")
pymol.cmd.enable("state1")
prefix="grouped"
pymol.cmd.split_states("state1", prefix=prefix)
pymol.cmd.group("states",prefix+"*")
stored.models = set()
pymol.cmd.iterate("states", 'stored.models.add(model)')
#if oneFile:
#    pymol.cmd.save( g + ".pdb", g)
#else:
print dirName
conunt=0;
for x in stored.models:
  	pymol.cmd.save( dirName+"/"+str(conunt) +"-"+spath +".pdb", x)
  	conunt+=1

#pymol.cmd.extend("toGroup", toGroup)

#pymol.cmd.save("imagen.png")
# Get out!
pymol.cmd.quit()
