###
###		genera un mapa de calor con los ficheros generados con las mejores conformaciones de BDVs
###		python mapacaolor.py ficheros .txt
###		python mapacaolor.py fichsroesGenerados/*.txt
###


import matplotlib.pyplot as plt 
import numpy as np 
import sys 
import os
import numpy

proteinas=0
xLigand=[]

ficheros=[]
nameProt=[]
min=-9999
max=9999
for x in sys.argv:
	if x!= "mapaCalor.py":
		proteinas+=1
		nameProt.append(x[:x.rindex('.')])
		ficheros.append(x)
ligandos = sum(1 for line in open(sys.argv[1]))
#print proteinas
#print ligandos
datos = numpy.zeros(shape=(proteinas,ligandos))

###___________________________________________________________
###
###		Leer datos
###_______________________________________________________________-


contadorLig=0
for file in ficheros:
	infile = open(file, 'r')

	l=[]
	for line in infile:
		#print line
	    a=line.split()

	    a[4]=os.path.splitext(a[4])[0]
	    a[4]=a[4][a[4].rindex('/')+1:]
	    xLigand.append(a[4])
	    
	    if float(max) > float(a[0]):
		max = a[0]
	    if float(min) < float(a[0]):
			min=a[0]
	    
	    l.append(abs(float( a[0] ) ) ) 
	print  len (l)
	if len(l) != 0:
		datos[contadorLig]=l
		contadorLig+=1

	infile.close()



print "El min es: "+min
print "El max es: "+max








######################################################################
#title = ""
xlabel= "Ligands"
ylabel="Proteins"
#data =  np.random.rand(8,12)


fig, ax = plt.subplots( ) 

#ax = fig.add_subplot(111, projection="3d")
#plt.title(title)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
c = plt.pcolor(datos, edgecolors='k', cmap=plt.cm.rainbow)

def show_values(pc, fmt="%.2f", **kw):
    from itertools import izip
    pc.update_scalarmappable()
    ax = pc.get_axes()
    for p, color, value in izip(pc.get_paths(), pc.get_facecolors(), pc.get_array()):
        x, y = p.vertices[:-2, :].mean(0)

        if np.all(color[:3] > 0.5):
            color = (0.0, 0.0, 0.0)
        else:
        	color = (1.0, 1.0, 1.0)
        ax.text(x, y, fmt % value,  va="center",  fontsize=9, rotation="vertical" , color=color, **kw)
#show_values(c)
plt.colorbar(c)



ax.set_yticks(np.arange(datos.shape[0]) + 0.5, minor=False)
ax.set_xticks(np.arange(datos.shape[1]) + 0.5, minor=False)
ax.invert_yaxis()
ax.set_xticklabels(xLigand, minor=False) 
ax.set_yticklabels(nameProt, minor=False) 
ax.set_xlim(0, ligandos)
ax.set_ylim(0, proteinas)

###____________________________________________________________________________________________________________
###
###		Modifico el tamanio y la inclinacion del nombre de las labels x y los |- -|
###________________________________________________________________________________________________________________-
for tick in ax.xaxis.get_major_ticks():
	#tick.label.set_fontsize('x-small') 
	tick.label.set_fontsize(7) 
	tick.label.set_rotation('90')
	tick.tick2On = False
for t in ax.yaxis.get_major_ticks():
    #t.tick1On = False
    t.tick2On = False
###

#plt.show()
plt.savefig('mapaCalor.png',dpi=600)

################################################3





###__________________________________________________________________________________________________
###
###	Leyenda
###______________________________________________________________________________________________________
#plt.plot(1, 3, color="r",  linewidth=2.5, linestyle="-", label='Max '+max)
#plt.plot(1, 3, color="b",  linewidth=2.5, linestyle="-", label="Min "+min)
#plt.legend(loc='upper right', prop={'size':10})
#plt.savefig('mapaColor.png',dpi=600)
#plt.show()

#print "Proteinas son: " +str(proteinas)
#print datos
#datos = np.random.rand(ligandos,proteinas) 
#print type (datos)
#print datos
###______________________________________________________________
###
###		Generar Grafica
###____________________________________________________________-
#print "datos Shapwe 0: "+ str(datos.shape[0])
#print "datos Shapwe 1: "+ str(datos.shape[1])

#fig, ax = plt.subplots() 
#heatmap = ax.pcolor(datos, cmap=plt.cm.rainbow) ##Existen diferentes tipos de color "plt.cm.Blues"
#cbar = plt.colorbar(heatmap)
#ax.set_yticks(np.arange(datos.shape[0]) + 0.5, minor=False)
#ax.set_xticks(np.arange(datos.shape[1]) + 0.5, minor=False)
#ax.invert_yaxis()
#ax.set_xticklabels(xLigand, minor=False) 
#ax.set_yticklabels(nameProt, minor=False) 
#ax.set_xlim(0, ligandos)
#ax.set_ylim(0, proteinas)
