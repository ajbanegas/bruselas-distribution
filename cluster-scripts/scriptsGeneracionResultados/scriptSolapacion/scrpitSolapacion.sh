#!/bin/bash
while (( $# )) #recorro todos los parametros y los asigno
 do
    case $1 in
   		-l|-L ) ligando=$2;; #parametro -p proteina
		-f|-F ) directorio=$2;;
     esac
  shift
done
if [ -z $ligando ] || [ -z $directorio ]; then
	echo "./scriptSolapamiento -l ligando -f directorio"
	exit
fi
	
extension=".pdb"
ligando=${ligando%$extension}
./pdb2coords.sh $ligando
SasaLig=`./murcia  $ligando.xyzr |grep -i "MURCIA Sasa" |awk -F':' '{print $2}'` #se ejecurta murcia y se limpia el resultado para coger el valor
for dir in $directorio*$extension
do
	fichero=${dir%$extension}
	./pdb2coords.sh $fichero
	SasaFich=`./murcia  $fichero.xyzr |grep -i "MURCIA Sasa" |awk -F':' '{print $2}'`
	sumaSasas=$(echo $SasaFich + $SasaLig | bc -l) 
	cat $ligando.xyzr >>$fichero.xyzr
	SasaFinal=`./murcia  $fichero.xyzr |grep -i "MURCIA Sasa" |awk -F':' '{print $2}'`
	


	solapamiento=$(echo $sumaSasas - $SasaFinal | bc -l)
#	echo solapamiento $solapamiento
	#if [ $solapamiento < 1 ] ;then
#		
	#fi
	#cat $fichero.txt |awk '{print $1" "$2" "$3" "$4" "$5" "$6" %-7.5f"'$solapamiento'}'
	cat $fichero.txt |awk -vv="${solapamiento}" '{print $1" "$2" "$3" "$4" "$5" "$6" "v }' > $fichero.txt.new
	mv $fichero.txt.new $fichero.txt
	rm $fichero.xyzr
	 #echo "Ligando " $SasaLig
	#echo "Fich " $SasaFich
	#echo "Suma " $sumaSasas
	#echo "Final "$SasaFinal
	#echo "Acoplamiento "$solapamiento
done
