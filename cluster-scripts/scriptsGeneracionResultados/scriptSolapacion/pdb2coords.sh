#for i in `ls | grep ".pdb" | sed 's/.pdb//'  ` 
#do
		extension=".pdb"
		cat  $1$extension | sed '/ENDMDL/q' | egrep '^ATOM|^HETATM' | 
		awk '{
			if(substr($0, 17, 1)==" "||substr($0, 17, 1)=="A")
			{	
				printf "%s %s %s ", substr($0, 31, 8), substr($0, 39, 8) , substr($0, 47, 8)
				if(substr($0, 14, 1)=="C")printf " 1.8" 
				else if(substr($0, 14, 1)=="S")printf " 1.75" 
				else if(substr($0, 14, 1)=="N")printf " 1.6" 
				else if(substr($0, 14, 1)=="O")printf " 1.5" 
				else if(substr($0, 14, 1)=="H")printf " 1.2" 
				else printf " 3.14"
				printf "\n"	
			}		
		}'>$1".xyzr"
#done
#HETATM    1  C1 
