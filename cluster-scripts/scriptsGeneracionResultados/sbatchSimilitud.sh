#!/bin/sh
#SBATCH --output=salida/%j.out
#SBATCH --error=salida/%j.err
#SBATCH -J docking.sh
#SBATCH --time=10:30:00
dir="VS-WG-GAL-6_p0.50-59-sd-0-0-0/"
path=${PWD}"/" #directorio exclusiovo de malaga por que no coge = las rutas que islandia
module load python
python $path"scriptsGeneracionResultados/lstSimlilutd.py" $path $dir
	##### COLA SBATCH
	#####SBATCH -p <nombre de la cola>
	#####SBATCH -J <nombre del trabajo>
	##### Número de nodos
	#####SBATCH --nodes=2
	#### Número de tareas por CPU
	#####SBATCH --cpus-per-task=4
	####### Número de procesos totales
	#######SBATCH --ntasks=10
	###### Tiempo
	######SBATCH -t 30
	######export OMP_NUM_THREADS= X
	######srun -l programa_a_ejecutar
	#####SBATCH --cpus-per-task=5
	#####SBATCH --mail-type=END
	######SBATCH --mail-user=jorge.dlpg@gmail.com
