import matplotlib.patches as patches
import matplotlib.path as path
import os,sys,string,re
from collections import OrderedDict
import matplotlib
matplotlib.use("Agg")
from pylab import *
from os import listdir
import commands
import os,sys,string,re
import matplotlib.path as path
import os.path

ligA={}
ligB={}
ligC={}
ligD={}

def funcion4():
	contadorA=1;
	contadorB=1;
	contadorC=1;
	contadorD=1;
	print "posicionA\tligandoA\tenergiaA\tposicionB\tligandoB\tenergiaB\tposicionC\tligandoC\tenergiaC\tposicionD\tligandoD\tenergiaD"
	#Recorro cada posicion de ligA y comparco con LigB
	for kA, vA in ligA.items():
		kA=kA[kA.rindex("/")+1:kA.rindex(".")] ##quito la extension y apartir de lig
		for kB, vB in ligB.items():
			kB=kB[kB.rindex("/")+1:kB.rindex(".")] ##quito la extension y apartir de lig
			for kC, vC in ligC.items():
				kC=kC[kC.rindex("/")+1:kC.rindex(".")] ##quito la extension y apartir de lig
				for kD, vD in ligC.items():
					kD=kD[kD.rindex("/")+1:kD.rindex(".")] ##quito la extension y apartir de lig
					if kA == kB and kA == kD:	
						print str(contadorA) +"\t"+kA + "\t" +str(vA) +"\t" +str(contadorB) +"\t"+kB +"\t"+str(vB)\
						+"\t" +str(contadorC) +"\t"+kC +"\t"+str(vD)+"\t" +str(contadorD) +"\t"+kD +"\t"+str(vD)
					contadorD+=1
				contadorD=1
				contadorC+=1
			contadorD=1
			contadorC=1
			contadorB+=1
		contadorB=1 #contadores para saber la posicion que estan
		contadorD=1
		contadorC=1
		contadorA+=1

def funcion3():
	contadorA=1;
	contadorB=1;
	contadorC=1;
	print "posicionA\tligandoA\tenergiaA\tposicionB\tligandoB\tenergiaB\tposicionC\tligandoC\tenergiaC"
	#Recorro cada posicion de ligA y comparco con LigB
	for kA, vA in ligA.items():
		kA=kA[kA.rindex("/")+1:kA.rindex(".")] ##quito la extension y apartir de lig
		for kB, vB in ligB.items():
			kB=kB[kB.rindex("/")+1:kB.rindex(".")] ##quito la extension y apartir de lig
			for kC, vC in ligC.items():
				kC=kC[kC.rindex("/")+1:kC.rindex(".")] ##quito la extension y apartir de lig
				#print kA +"\t"+ kB +"\t" +kC
				if kA == kB and kA == kC:	
					print str(contadorA) +"\t"+kA + "\t" +str(vA) +"\t" +str(contadorB) +"\t"+kB +"\t"+str(vB)\
						+"\t" +str(contadorC) +"\t"+kC +"\t"+str(vC)
				contadorC+=1
			contadorC=1
			contadorB+=1

		contadorB=1 #contadores para saber la posicion que estan
		contadorC=1
		contadorA+=1

def funcion2():

	contadorA=1;
	contadorB=1;
	print "posicionA\tligandoA\tenergiaA\tposicionB\tligandoB\tenergiaB"
	#Recorro cada posicion de ligA y comparco con LigB
	for kA, vA in ligA.items():
		kA=kA[kA.rindex("/")+1:kA.rindex(".")] ##quito la extension y apartir de lig
		for kB, vB in ligB.items():
			kB=kB[kB.rindex("/")+1:kB.rindex(".")] ##quito la extension y apartir de lig
			if kA == kB:	
				print str(contadorA) +"   "+kA + "   " +str(vA) +"         " +str(contadorB) +"   "+kB +"   "+str(vB)
			contadorB+=1
		contadorB=1 #contadores para saber la posicion que estan
		contadorA+=1

################################################################
#	coge los datos de los ficheros y devuelve un hash con ellos
################################################################
def recogerDatos(directorio): 
	lig={}
	for cosa in listdir(directorio): #listo directorio
		if cosa.find(".txt") >= 0:
			f = open(directorio+cosa)
			datosDivididos=f.read().split(" ")
			if datosDivididos[0] != "":
				#print datosDivididos[4]
				lig[datosDivididos[4]] = float (datosDivididos[0]) 
			f.close()
	return lig


################################################################
#	Formatear Salida
################################################################
#def formatearSalida(col1,col2,col3,col4,col5,col6):
#	vectorCarctereMax=[3,15,8,3,15,8]
#	v=[col1,col2,col3,col4,col5,col6]
#cadena=v[0]
#print cadena	
##	for int 	
bestScore="mejorScore/"


if len(sys.argv) > 5:
	print "Indique el nombre de la carpeta con los .txt  el fichero de los ligandos max 4"
	exit();
fileEntradaA = sys.argv[1] #fichero donde se encuentrn los ficheros txt para graficrlos
fileEntradaB = sys.argv[2]

if len(sys.argv) == 4: #casos que pued3en estar o no
	fileEntradaC = sys.argv[3]
	ligC=recogerDatos(fileEntradaC)
	ligC.update(recogerDatos(fileEntradaC+bestScore))
	ligC= OrderedDict(sorted(ligC.items(), key=lambda x: x[1]))
if len(sys.argv) == 5: #caso que puede estar o no
	fileEntradaD= sys.argv[4]
	ligD=recogerDatos(fileEntradaD)
	ligD.update(recogerDatos(fileEntradaD+bestScore))
	ligD = OrderedDict(sorted(ligD.items(), key=lambda x: x[1]))

#cojo datos de entrada de los ficheros txt
ligA=recogerDatos(fileEntradaA)
ligA.update(recogerDatos(fileEntradaA+bestScore))
ligB=recogerDatos(fileEntradaB)
ligB.update(recogerDatos(fileEntradaB+bestScore))
##ordeno las hash
ligA = OrderedDict(sorted(ligA.items(), key=lambda x: x[1]))
ligB = OrderedDict(sorted(ligB.items(), key=lambda x: x[1]))

if len(sys.argv)==3:
	funcion2()
if len(sys.argv)==4:
	funcion3()
if len(sys.argv)==5:
	funcion4()