#!/usr/bin/env python
#
#	Se le pasa el nombre del fichero de energias y el gnera una grafica con el mismo nombre +E.png
#

import numpy as np
import sys
import matplotlib.pyplot as plt
import commands
extDock=".pdbqt"
#extEn=".en"
valores={}
####__________________________________________________________________________________________________________
####
####	Pesos por defecto de Vina
####__________________________________________________________________________________________________________
vGauss1=-0.035579
vGauss2=-0.005156
vRepulsion=0.84024500000000002
vHydrophobic=-0.035069000000000003
vHydrogen=-0.58743900000000004
vRot=0.058459999999999998
###############################################################################################################
####___________________________________________________________________________________________________________
####
####	Leer fichero energia AD y almaceno en variables
####___________________________________________________________________________________________________________
def LeerEnergia(fich):
	global valores
	comenzar=False
	infile=open(fich, 'r')
	token="Intermolecular contributions to the terms, before weighting:"
	i=0
	for line in infile:
		line=line[:-1]
		if comenzar==True:		
			p=line.split(':');
			valores[i]=float(p[1])
			i=i+1
		if line.find(token)!=-1: #cuando encuentra el tokken se comienzan a examinar las lineas
			comenzar=True
	infile.close()
 

####____________________________________________________________________________________________________________
####
####	Main
####________________________________________________________________________________________________________-
try:
	if len(sys.argv) != 2:
		print "indique el nombre del fichero para realizar la grafica"
		exit();
	fileIn=sys.argv[1]
	ext="."+fileIn[fileIn.rindex(".")+1:] ##extension
	fileIn=fileIn[:fileIn.rindex(".")]
	nomLig=fileIn[fileIn.rindex("/")+1:]




	print "Generando Grafica interacciones individual vina "+ fileIn

	comando="cat " +fileIn+ext+"|grep  'Affinity' |awk '{print $2}'"
	#print comando
	energia=float (commands.getoutput(comando))
	comando="cat "+fileIn+extDock+"|tail -1 | awk '{print $2}'"
	#print comando
	torsion=float(commands.getoutput(comando))
	#print comando
	#torsion=torsion+14
	LeerEnergia(fileIn+ext)
	g1=valores[0]*vGauss1
	g2=valores[1]*vGauss2
	r1=valores[2]*vRepulsion
	h1=valores[3]*vHydrophobic
	h2=valores[4]*vHydrogen

	ro=torsion*vRot

except:
	print "Error recogiendo datos scriptGeneracionGraficaVina"
"""
print nomLig
print "energia "+ str(energia )
print "g1= "+ str(valores[0]) + " * " + str(vGauss1) + "  = " +str(valores[0]*vGauss1)
print "g2= "+ str(valores[1]) + " * " + str(vGauss2) + "  = " +str(valores[1]*vGauss2)
print "r1= "+ str(valores[2]) + " * " + str(vRepulsion) + "  = "+ str(valores[2]*vRepulsion)
print "h1= "+ str(valores[3]) + " * " + str(vHydrophobic) + "  = "+ str(valores[3]*vHydrophobic)
print "h2= "+ str(valores[4]) + " * " + str(vHydrogen) + "  = " +str(valores[4]*vHydrogen)
print "ro= "+ str(torsion) + " * " + str(vRot) + "  = " +str(torsion*vRot)
print "Total = "+str(g1+g2+r1+h1+h2+ro)
print ""
"""


#print "h1"+ str(h1)
#print "h2 "+str(h2)
#suma=g1+g2+r1+h1+h2+ro
#print "gaus 1: "+str(g1)
#print "gaus 2: "+str(g2)
#print "Repulsion: "+str(r1)
#print "hydrophobic: "+ str(h1)
#print "hydrogen: "+str(h2)
#print "vRot: "+str(ro)
#print "suma: "+str(suma)


###_______________________________________________________________________________________________________
###
###		Grafica
###_______________________________________________________________________________________________________
try:
	N = 1
	ind = np.arange(N)  # the x locations for the groups
	width = 0.1       # the width of the bars

	fig = plt.figure()
	ax = fig.add_subplot(111)

	rects1 = ax.bar(ind, g1, width, color='b')
	rects2 = ax.bar(ind+width, g2, width, color='g')
	rects3 = ax.bar(ind+2*width, r1, width, color='r')
	rects4 = ax.bar(ind+3*width, h1, width, color='c')
	rects5 = ax.bar(ind+4*width, h2, width, color='m')
	rects6 = ax.bar(ind+5*width, ro, width, color='y')
	rects6 = ax.bar(ind+6*width, energia, width, color='k')


	# add some
	ax.set_ylabel('Contribution (Kcal/mol)')
	ax.set_title(nomLig+'\nEnergetic contributions to binding energy ')
	#ax.set_xticks(ind+width+3.5*width)
	ax.set_xticklabels("")
	#ax.set_xticklabels( (nomLigando, 'Lycopene', 'Chlorogenic acid', 'Naringenin') )
	ax.set_xlim(-0.80,1.5)
	#ax.set_ylim(-15,7)

	###_____________________________________________________________________________________________________________________
	###
	###		Leyenda
	###______________________________________________________________________________________________________________________-
	plt.plot(1, 3, color="b", linewidth=2.5, linestyle="-", label="Gauss1")
	plt.plot(1, 3, color="g",  linewidth=2.5, linestyle="-", label="Gauss2")
	plt.plot(1, 3, color="r",  linewidth=2.5, linestyle="-", label="Repulsion")
	plt.plot(1, 3, color="c",  linewidth=2.5, linestyle="-", label="Hydrophobic")
	plt.plot(1, 3, color="m",  linewidth=2.5, linestyle="-", label="Hydrogen")
	plt.plot(1, 3, color="y",  linewidth=2.5, linestyle="-", label="Rot")
	plt.plot(1, 3, color="k",  linewidth=2.5, linestyle="-", label="Affinity")
	plt.legend(loc='upper left', prop={'size':6})
	#############################################################

	plt.savefig(fileIn+'E.png',dpi=600)
#	plt.show()
except:
	print "ERROR generacion Grafica scriptGeneracionGraficaVina"

