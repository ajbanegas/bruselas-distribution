#!/usr/bin/env python
# a bar plot with errorbars
import numpy as np
import sys
import os
import matplotlib.pyplot as plt
import commands

def autolabel(rects,rmsd):
    # attach some text labels
    count=0;
    for rect in rects:
        #height = rect.get_height()
        ax.text((rect.get_x()+rect.get_width()/2), 1, round(rmsd[count],1), ha='center', va='bottom',size=9)
        count+=1

def autolabelCristal(rects,rmsd):
    # attach some text labels
    count=0;
    for rect in rects:
        #height = rect.get_height()
        ax.text((rect.get_x()+rect.get_width()/2), 1.4, round(rmsd[count],1), ha='center', va='bottom',size=9)
        count+=1


def getCoords ( i, ext):
		comando="python  scriptLanzador/standarFileCoords.py "+fileIn+i[:-4]+ext
		return commands.getoutput(comando)

def getCoordsCristal ( i,):
		comando="python  scriptLanzador/standarFileCoords.py "+i
		return commands.getoutput(comando)

def getRmsd(coordsNoMutate, coordsMutate):
	#print coordsNoMutate
	#print coordsMutate
	sumatorio=0
	contadorAtomo=0
	cMutateSplit=coordsMutate.split('\n')
	for cnmuta in coordsNoMutate.split('\n'):
		if not 	cnmuta.startswith('#'):
			cMutate=cMutateSplit[contadorAtomo].split(':')
			cNoMutate=cnmuta.split(':')

			x= (float(cMutate[0]) - float(cNoMutate[0]))**2
			y= (float(cMutate[1]) - float(cNoMutate[1]))**2
			z= (float(cMutate[2]) - float(cNoMutate[2]))**2
			sumatorio+= (x+y+z) 
			contadorAtomo+=1

	
	return math.sqrt(sumatorio/contadorAtomo)
	

		
####____________________________________________________________________________________________________________
####
####	Main
####________________________________________________________________________________________________________-
if len(sys.argv) != 4:
	print "Indique la ruta de los ficheros, la extension  y el fichero del ligando original"
	exit();
fileIn=sys.argv[1]
ext=sys.argv[2]
fileLigando=sys.argv[3]
scoreNoMutate=0 #score del docking antes de cualquier mutacion
residuos=[]
scores=[]
scoreNoMutado=[]	##primer docking sin sinnguna mutacion
coordsNoMutate=[]	#primer docking sin sinnguna mutacion
coordsMutate=[]		#coordenadas con mutacion
rmsd=[]				#rms vs primer docking
rmsdCristal=[]		#rmsd vs cristalografica
coordsCristal=[]	#coords critalografica

print "Generando Grafica de Mutacion "+ fileIn
###__________________________________________________________________________________________________
###
###	Leo todos los datos de entrada de la carpeta indicada
###___________________________________________________________________________________________________
#primero el 0 que es el docking no mutado
for i in os.listdir(fileIn):
	if i.startswith("0")  and i.find(".txt") != -1:
		comando="cat "+fileIn+i+ " |grep -v \# |awk '{print $1}'"
		scoreNoMutate=commands.getoutput(comando)
		coordsNoMutate=getCoords(i, ext  )  # se le pasa el numero del ligando (nExp) y devolvera un vector con coordenadas
		break
#luego el resto, excluyendo al res 0
#comando="cat "+fileLigando+ " |grep -v \# |awk '{print $1}'"
#print  comando

coordsCristal=getCoordsCristal(fileLigando ) ##coordenadas del cristal



count=0
for i in os.listdir(fileIn):
	if not i.startswith("0")  and i.find(".txt") != -1:
		comando="cat "+fileIn+i+ " |grep -v \# |awk '{print $1}'"
		scores.append(float(commands.getoutput(comando)))
		#coordsMutate=getCoords(i, ext  )  # se le pasa el numero del ligando (nExp) y devolvera un vector con coordenadas
		rmsd.append(getRmsd(getCoords(i, ext  ), coordsNoMutate))
		rmsdCristal.append(getRmsd(getCoords(i, ext  ), coordsCristal))
		i=i[i.index("-")+1:] ##no encuentor como se hace en python un idex de la posicion que quiero, lo hago de uno en uno
		i=i[i.index("-")+1:]
		i=i[i.index("-")+1:]
		i=i[:i.index("-")]
		i=i.split("_")[1]+"A"
		residuos.append(i)
		scoreNoMutado.append(float(scoreNoMutate))
		count+=1


#print scoreNoMutado
#print scores
#print residuos
#print rmsd
###_______________________________________________________________________________________________________
###
###		Grafica
###_______________________________________________________________________________________________________
#try:
N = count
ind = np.arange(N)  # the x locations for the groups
width = 0.1       # the width of the bars
fig = plt.figure()
ax = fig.add_subplot(111)
tamLineaBordeBar=[0.1]
rects1 = ax.bar(ind, scoreNoMutado, width, color='b',linewidth=tamLineaBordeBar )
rects2 = ax.bar(ind+width, scores, width, color='g',linewidth=tamLineaBordeBar )

autolabel(rects1,rmsd)		##add labels vs first docking
autolabelCristal(rects1,rmsdCristal)	##add labels vs Cristal


# add some
ax.set_ylabel('Contribution (Kcal/mol)')
#ax.set_title(nomLig+'\nEnergetic contributions to binding energy ')
#ax.set_xticks(ind+width+3.5*width)
ax.set_xticklabels("")
ax.set_xlim(-0.5,len(scores)-0.5)
plt.axhline(y=0, xmin=-0.5, xmax=float(len(scores))-0.5, linewidth=0.5, color = 'k')
ax.set_xticks(ind)
ax.set_xticklabels( residuos )
###____________________________________________________________________________________________________________
###
###		Modifico el tamanio y la inclinacion del nombre de las labels x
###________________________________________________________________________________________________________________-
for tick in ax.xaxis.get_major_ticks():
	tick.label.set_fontsize('x-small') 
	tick.label.set_rotation('35')

###_____________________________________________________________________________________________________________________
###
###		Leyenda
###______________________________________________________________________________________________________________________-
plt.plot(1, 3, color="b", linewidth=2.5, linestyle="-", label="Original")
plt.plot(1, 3, color="g",  linewidth=2.5, linestyle="-", label="Mutate")
plt.legend(loc='upper left', prop={'size':10})
#############################################################

plt.savefig(fileIn+'Mutate.png',dpi=600)
plt.show()
#except:
#	print "ERROR generacion Grafica scriptGeneracionGraficaVina"




#i=i[i.index("-")+1:] 
#i=i[i.index("-")+1:] 
#programa=i[:i.index("-")] 
#print programa
