###_____________________________________________________________________________________________________________-
###
###Genera una grafica desglosada por atmos para LF
###
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import sys
import os
import matplotlib.pyplot as plt
import commands

extEn=".log"



nomLig=""  ##se almacena el nombre del ligando limpio
atomo=[]
number=[]
charge=[]
eVdw=[]
eMetal=[]
eSol=[]
eHbons=[]
eElec=[]
eInternal=[]
eTotal=[]



####____________________________________________________________________________________________________________
####
####	Main
####________________________________________________________________________________________________________-
if len(sys.argv) != 2:
	print "Indique el nombre del fichero sin extension"
	exit();

fileIn=sys.argv[1]
nomLig=fileIn[fileIn.rindex("/")+1:]
#print fileIn
##busco el comiennzo de los atomos y quito la cabecera
comando="grep -n \"Name\" "+fileIn+extEn +"| awk -F: '{print $1}'"
numInicio=commands.getoutput(comando)
comando="sed '1,"+numInicio+"'d "+fileIn+extEn+ " > "+fileIn+extEn+extEn
commands.getoutput(comando)
##corto el final del ficheo
comando="grep -n \"Total\" "+fileIn+extEn+extEn+"| awk -F: '{print $1}'"
numFin=commands.getoutput(comando)
comando="sed '"+numFin+",$d' -i "+fileIn+extEn+extEn
commands.getoutput(comando)


j=0


infile = open(fileIn+extEn+extEn, 'r')
for line in infile:
	
	line=line[:-1]
	cadena=line.split()
	atomo.append(cadena[0])
	number.append(cadena[1])
	charge.append(float(cadena[2]))
	eVdw.append(float(cadena[4]))
	eMetal.append(float(cadena[5]))
	eSol.append(float(cadena[6]))
	eHbons.append(float(cadena[7]))
	eElec.append(float(cadena[8]))
	eInternal.append(float(cadena[9]))
	eTotal.append(float(cadena[10]))
	j=j+1
	"""
	print(atomo)
	print(number)
	print(charge)
	print(eVdw)
	print(eMetal)
	print(eSol)
	print(eHbons)
	print(eElec)
	print(eInternal)
	print(eTotal)
	"""
infile.close()



#print valores
#nomLig=fileIn[fileIn.rindex("/")+1:]

#comando="cat "+fileIn+extEn +" |tail -1 |awk -F, '{print $2\":\"$6\":\"$7\":\"$8\":\"$9\":\"$10\":\"$11\":\"$12\":\"$13\":\"$14}'"
#cadenaTmp=commands.getoutput(comando)
#cadena=cadenaTmp.split(":")

###_______________________________________________________________________________________________________
###
###		Grafica
###_______________________________________________________________________________________________________

N = j
ind = np.arange(N)  # the x locations for the groups
width = 0.1       # the width of the bars
#print ind
fig = plt.figure()
ax = fig.add_subplot(111)
#plt.axes((0.2,0.1,0.7,0.8))

#  HORIZONTAL
#plt.axhline(y=0, xmin=-1, xmax=j, linewidth=0.5, color = 'k')
##barras
#height=1

tamLineaBordeBar=[0.1]
rects1 = ax.bar(ind , eVdw, width, color='b',linewidth=tamLineaBordeBar )
rects2 = ax.bar(ind+width, eSol, width, color='g',linewidth=tamLineaBordeBar)
rects3 = ax.bar(ind+2*width, eHbons, width, color='r',linewidth=tamLineaBordeBar)
rects4 = ax.bar(ind+3*width, eElec, width, color='c',linewidth=tamLineaBordeBar)
rects5 = ax.bar(ind+4*width, eInternal, width, color='m',linewidth=tamLineaBordeBar)
rects6 = ax.bar(ind+5*width, eMetal, width, color='y',linewidth=tamLineaBordeBar)

rects7 = ax.bar(ind+6*width, charge,width, color='#bbefaa',linewidth=tamLineaBordeBar)
rects8 = ax.bar(ind+7*width, eTotal, width, color='k',linewidth=tamLineaBordeBar)
#eeefaa
#ax.hist(rects1, orientation='horizontal')
#ax.set_yticks(rects1, atomo)
# add some




#ax.set_ylabel('Contribution (Kcal/mol)')
#ax.set_title('Energetic contributions to binding energy ')

#ax.set_yticks(ind+width+3.5*width)
ax.set_xticks(ind)
ax.set_xticklabels( atomo )
#ax.set_xticklabels( ('HMGCoA', 'Lycopene', 'Chlorogenic acid', 'Naringenin') )
ax.set_xlim(-0.2,j)
ax.set_xticks(ind+width+3.5*width)

plt.gca().xaxis.grid(True,linewidth=0.3)

###____________________________________________________________________________________________________________
###
###		Modifico el tamanio y la inclinacion del nombre de las labels x
###________________________________________________________________________________________________________________-
for tick in ax.xaxis.get_major_ticks():
	tick.label.set_fontsize('x-small') 
	tick.label.set_rotation('35')
#####################################################################################################################
#ax.set_xlim(-1,j+0.5)
###_____________________________________________________________________________________________________________________
###
###		Leyenda
###______________________________________________________________________________________________________________________-
plt.plot(1, 1.5, color="blue",  linewidth=2.5, linestyle="-", label="E(VDW)")
plt.plot(1, 1.5, color="green",  linewidth=2.5, linestyle="-", label="E(sol)")
plt.plot(1, 1.5, color="r",  linewidth=2.5, linestyle="-", label="E(H-bonds)")
plt.plot(1, 1.5, color="cyan",  linewidth=2.5, linestyle="-", label="E(elect)")
plt.plot(1, 1.5, color="m",  linewidth=2.5, linestyle="-", label="E(internal)")
plt.plot(1, 1.5, color='#eeefaa',  linewidth=2.5, linestyle="-", label="E(metal)")
###no comun a energias totoal
plt.plot(1, 1.5, color="#bbefaa", linewidth=2.5, linestyle="-", label="Charge")
plt.plot(1, 1.5, color='k',  linewidth=2.5, linestyle="-", label="E(total)")
plt.legend(loc='upper left', prop={'size':6})
#############################################################


plt.savefig(fileIn+'EA.png',dpi=600)
#plt.show()

