#!/usr/bin/env python
# a bar plot with errorbars
import numpy as np
import sys
import matplotlib.pyplot as plt
import commands
from collections import OrderedDict

bestScore={}
##############################################################################
####___________________________________________________________________________________________________________
####
####	Leer fichero energia AD y almaceno en variables
####___________________________________________________________________________________________________________
def LeerEnergia(fich):
	global valores
	comenzar=False
	infile=open(fich, 'r')
	i=0
	for line in infile:
		line=line[:-1]
		p=line.split(':');
		bestScore[round(float (p[0]),2)] = float(p[1])
	infile.close()
 

####____________________________________________________________________________________________________________
####
####	Main
####________________________________________________________________________________________________________-
try:
	if len(sys.argv) != 2:
		print "Indique el nombre del fichero sin extension (incluyendo la ruta absoluta)"
		exit();
	fileIn=sys.argv[1]
	nomLig=fileIn[fileIn.rindex("/")+1:]
	print "Generando Grafica de Clusterizado "+ fileIn
	LeerEnergia(fileIn+".clt")

except:
	print "Error recogiendo datos scriptGeneracionGraficaVina"


###_______________________________________________________________________________________________________
###
###		Grafica
###_______________________________________________________________________________________________________

fig = plt.figure()
ax = fig.add_subplot(111)
bestScore = OrderedDict(sorted( bestScore.items(), key=lambda x: x[1], reverse=True ))
#plt.bar(range(len(bestScore)), bestScore.values(), align='center')
plt.bar(range(len(bestScore)), bestScore.values())
plt.xticks(range(len(bestScore)), bestScore.keys())
ax.set_xlabel('Binding energy (Kcal/Mol)')
ax.set_ylabel('Number of ocurrences')
plt.savefig(fileIn+"Clr.png") 
