#!/usr/bin/env python
# a bar plot with errorbars
import numpy as np
import sys
import os

import matplotlib.pyplot as plt
import commands
extDock=".pdbqt"
extEn=".en1"
valores = []


fileTmp={} ##se almacenan los fichreos temporales al hacer el dir
torsion={} ##se almacena la variable toorsion
nomLig={}  ##se almacena el nombre del ligando limpio
energia={}
zinc={}
g1=[]	   ##energias del docking
g2=[]
r1=[]
h1=[]
h2=[]
ro=[]
en=[]

####__________________________________________________________________________________________________________
####
####	Pesos por defecto de Vina
####__________________________________________________________________________________________________________
vGauss1=-0.035579
vGauss2=-0.005156
vRepulsion=0.84024500000000002
vHydrophobic=-0.035069000000000003
vHydrogen=-0.58743900000000004
vRot=0.058459999999999998
###############################################################################################################
####___________________________________________________________________________________________________________
####
####	Leer fichero energia AD y almaceno en variables
####___________________________________________________________________________________________________________
def LeerEnergia(fich,j):
		
		global valores
		comenzar=False
		infile=open(fich, 'r')
		token="Intermolecular contributions to the terms, before weighting:"

		valores.append([])
		for line in infile:
			line=line[:-1]
			if comenzar==True:		
				p=line.split(':');
				valores[j].append(float(p[1]))
			if line.find(token)!=-1: #cuando encuentra el tokken se comienzan a examinar las lineas
				comenzar=True
		infile.close()
	
 

####____________________________________________________________________________________________________________
####
####	Main
####________________________________________________________________________________________________________-

try :
	if len(sys.argv) != 2:
		print "Indique el nombre del directorio sin extension"
		exit();
	fileIn=sys.argv[1]
	j=0
	for fichero in os.listdir(fileIn):
		if fichero.find("en1")!=-1:
			fileTmp[j]=fichero[:fichero.find("en1")-1]  
			comando="cat " +fileIn+fileTmp[j]+extEn+"|grep  'Affinity' |awk '{print $2}'"
			
			energia[j]=float(commands.getoutput(comando))
			#print energia[j]
			comando="cat "+fileIn+fileTmp[j]+extDock+" |grep ZINC" 
			a=commands.getoutput(comando)
			zinc[j]=a[a.find("ZINC"):]
			comando="cat "+fileIn+fileTmp[j]+extDock+"|tail -1 | awk '{print $2}'"
			torsion[j]=float(commands.getoutput(comando))
			LeerEnergia(fileIn+fileTmp[j]+extEn,j)	
			j=j+1

	#print valores
	#nomLig=fileIn[fileIn.rindex("/")+1:]
	t=0
	#guardo los valores en vectores
	for i in range(j):
		g1.append(valores[i][0]*vGauss1)
		g2.append(valores[i][1]*vGauss2)
		r1.append(valores[i][2]*vRepulsion)
		h1.append(valores[i][3]*vHydrophobic)
		h2.append(valores[i][4]*vHydrogen)
		ro.append(torsion[i]*vRot)
		en.append(energia[i])

	#guardo el nombre del liagndo "Hay que limpiarlo puesto que sera una cadena tal que VS-LF-proteina-ligando-coordenadas"
	if fileTmp[i][:fileTmp[i].find("-")]=="VS": #si el fichero no contiene VS al princiio quire decir que es un bB (Chapu)
		opcion="VS"
	else:
		opcion="BD"
	for i in range(j):
		if opcion == "VS":
			nomLig[i]=fileTmp[i][fileTmp[i].find("-")+1:]
			nomLig[i]=nomLig[i][nomLig[i].find("-")+1:]
			nomLig[i]=nomLig[i][nomLig[i].find("-")+1:]
		#	nomLig[i]=nomLig[i][:nomLig[i].find("-")]  ##ojo aniadido ultimo momento
		if opcion== "BD":
			nomLig[i]=fileTmp[i][fileTmp[i].find("-")+1:]
			nomLig[i]=nomLig[i][nomLig[i].find("-")+1:]
			nomLig[i]=nomLig[i][nomLig[i].find("-")+1:]
			nomLig[i]=nomLig[i][nomLig[i].find("-")+1:]
		nomLig[i]=nomLig[i][:nomLig[i].find("-")]
		nomLig[i]=nomLig[i]+"\n"+zinc[i]


except:
	print "ERROR!  Con los datos de entrada "


###_______________________________________________________________________________________________________
###
###		Grafica
###_______________________________________________________________________________________________________
try: 
	N = j
	ind = np.arange(N)  # the x locations for the groups
	width = 0.1       # the width of the bars

	fig = plt.figure()
	ax = fig.add_subplot(111)

	rects1 = ax.bar(ind, g1, width, color='b')
	rects2 = ax.bar(ind+width, g2, width, color='g')
	rects3 = ax.bar(ind+2*width, r1, width, color='r')
	rects4 = ax.bar(ind+3*width, h1, width, color='c')
	rects5 = ax.bar(ind+4*width, h2, width, color='m')
	rects6 = ax.bar(ind+5*width, ro, width, color='y')
	rects7 = ax.bar(ind+6*width, en, width, color='k')

	# add some
	ax.set_ylabel('Contribution (Kcal/mol)')
	ax.set_title('Energetic contributions to binding energy ')
	ax.set_xticks(ind+width+3.5*width)
	ax.set_xticklabels( nomLig )
	###____________________________________________________________________________________________________________
	###
	###		Modifico el tamanio y la inclinacion del nombre de las labels x
	###________________________________________________________________________________________________________________-
	for tick in ax.xaxis.get_major_ticks():
		tick.label.set_fontsize('6.5') 
		tick.label.set_rotation('35')
	#####################################################################################################################
	ax.set_xlim(-1,j+0.5)

	###_____________________________________________________________________________________________________________________
	###
	###		Leyenda
	###______________________________________________________________________________________________________________________-
	plt.plot(1, 3, color="b", linewidth=2.5, linestyle="-", label="Gauss1")
	plt.plot(1, 3, color="g",  linewidth=2.5, linestyle="-", label="Gauss2")
	plt.plot(1, 3, color="r",  linewidth=2.5, linestyle="-", label="Repulsion")
	plt.plot(1, 3, color="c",  linewidth=2.5, linestyle="-", label="Hydrophobic")
	plt.plot(1, 3, color="m",  linewidth=2.5, linestyle="-", label="Hydrogen")
	plt.plot(1, 3, color="y",  linewidth=2.5, linestyle="-", label="Rot")
	plt.plot(1, 3, color="k",  linewidth=2.5, linestyle="-", label="Affinity")
	plt.legend(loc='upper left', prop={'size':6})
	#############################################################


	plt.savefig(fileIn+'graficaEnergias.png',dpi=600)
#	plt.show()
except:
	print "Error generando grafica 10 Vina"

