#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import sys
import os
import matplotlib.pyplot as plt
import commands
extEn=".csv"


nomLig=""  ##se almacena el nombre del ligando limpio


####____________________________________________________________________________________________________________
####
####	Main
####________________________________________________________________________________________________________-
if len(sys.argv) != 2:
	print "Indique el nombre del fichero sin extension"
	exit();
fileIn=sys.argv[1]
#print fileIn+extEn

nomLig=fileIn[fileIn.rindex("/")+1:]
comando="cat "+fileIn+extEn +" |tail -1 |awk -F, '{print $2\":\"$6\":\"$7\":\"$8\":\"$9\":\"$10\":\"$11\":\"$12\":\"$13\":\"$14}'"
#print (comando)
cadenaTmp=commands.getoutput(comando)
cadena=cadenaTmp.split(":")


#cadena[0]= predicted binding energy(black) 10
#cadena[1]= van der waals (blue) 1
#cadena[2]= E(metal) (eeefaa)7
#cadena[3]= E(solvation)(green) 2
#cadena[4]= E(hydrogen bonds) (red) 3 
#cadena[5]= E(electrostatic) (cyan) 4
#cadena[6]= E(internal)  (magenta) 5
#cadena[7]= rotable bonds(yellow) 6
#cadena[8]= E(dihedral) (#aaefff) 8 
#cadena[9]= E(penalty) (#bbefaa) 9


###_______________________________________________________________________________________________________
###
###		Grafica
###_______________________________________________________________________________________________________
j=1
N = j
ind = np.arange(N)  # the x locations for the groups
width = 0.075       # the width of the bars

fig = plt.figure()
ax = fig.add_subplot(111)

#  HORIZONTAL
#plt.axhline(y=0, xmin=a, xmax=-5, linewidth=1, color = 'k')
##barras
#similares a desglse atomo
rects1 = ax.bar(ind, float(cadena[1]), width, color='blue')
rects2 = ax.bar(ind+width, float(cadena[3]), width, color='green')
rects3 = ax.bar(ind+2*width, float(cadena[4]), width, color='r')
rects4 = ax.bar(ind+3*width, float(cadena[5]), width, color='cyan')
rects5 = ax.bar(ind+4*width, float(cadena[6]), width, color='magenta')
rects6 = ax.bar(ind+5*width, float(cadena[2]), width, color='#eeefaa')
#no comun a desglose atom
rects7 = ax.bar(ind+6*width, float(cadena[7]), width, color='y')
rects7 = ax.bar(ind+7*width, float(cadena[8]), width, color='#aaefff')
rects8 = ax.bar(ind+8*width, float(cadena[9]), width, color='#bbefaa')
rects9 = ax.bar(ind+9*width, float(cadena[0]), width, color='black')

# add some
ax.set_ylabel('Contribution (Kcal/mol)')
ax.set_title(nomLig+'\nEnergetic contributions to binding energy ')
ax.set_xticks(ind+width+3.5*width)
ax.set_xticklabels( nomLig )
###____________________________________________________________________________________________________________
###
###		Modifico el tamanio y la inclinacion del nombre de las labels x
###________________________________________________________________________________________________________________-
for tick in ax.xaxis.get_major_ticks():
	tick.label.set_fontsize('x-small') 
	tick.label.set_rotation('35')
#####################################################################################################################
ax.set_xlim(-1,j+0.5)
###_____________________________________________________________________________________________________________________
###
###		Leyenda
###______________________________________________________________________________________________________________________-
"""
cadena[0]= E(predicted binding)10
cadena[1]= van der waals (blue) 1
cadena[2]= E(metal) (eeefaa)7
cadena[3]= E(solvation)(green) 2
cadena[4]= E(hydrogen bonds) (m) 3 
cadena[5]= E(electrostatic) (cyan) 4
cadena[6]= E(internal)  (k) 5
cadena[7]= rotable bonds(yellow) 6

cadena[8]= E(dihedral) (#aaefff) 8 
cadena[9]= E(penalty) (#bbefaa) 9
"""

plt.plot(1, 4, color="blue", linewidth=2.5, linestyle="-", label="van der waals")
plt.plot(1, 4, color="green",  linewidth=2.5, linestyle="-", label="E(solvation)")
plt.plot(1, 4, color='red',  linewidth=2.5, linestyle="-", label="E(hydrogen bonds)")
plt.plot(1, 4, color="cyan",  linewidth=2.5, linestyle="-", label="E(electrostatic)")
plt.plot(1, 4, color="magenta",  linewidth=2.5, linestyle="-", label="E(internal)")
plt.plot(1, 4, color="#eeefaa",  linewidth=2.5, linestyle="-", label="E(metal)")

plt.plot(1, 4, color="yellow",  linewidth=2.5, linestyle="-", label="rotable bonds")
plt.plot(1, 4, color='#aaefff',  linewidth=2.5, linestyle="-", label="E(dihedral)")
plt.plot(1, 4, color='#bbefaa',  linewidth=2.5, linestyle="-", label="E(penalty)")
plt.plot(1, 4, color="black",  linewidth=2.5, linestyle="-", label="E(predicted binding)")
plt.legend(loc='upper left', prop={'size':6})
#############################################################


plt.savefig(fileIn+'E.png',dpi=600)
plt.show()
#		print cadena[1]
#		torsion[j]=float()
#		LeerEnergia(fileIn+fileTmp[j]+extEn,j)	
		

#cat fileIn+fileTmp[j]+extDock |tail -1 |awk -F, '{print $1":"$2":"$3 }'
#num= grep -n "Name" VS-LF-3s5z_enz-fda_lig_1-19-13-31.log
#sed '1,numd' VS-LF-3s5z_enz-fda_lig_1-19-13-31.log
#grep -n "Total" VS-LF-3s5z_enz-fda_lig_1-19-13-31.log
# sed '69,$d' VS-LF-3s5z_enz-fda_lig_1-19-13-31.log

#print valores
#nomLig=fileIn[fileIn.rindex("/")+1:]




