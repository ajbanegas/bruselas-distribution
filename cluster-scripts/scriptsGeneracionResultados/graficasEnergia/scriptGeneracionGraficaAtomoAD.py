###_____________________________________________________________________________________________________________-
###
###Genera una grafica desglosada por atmos para LF
###
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import sys
import os
import matplotlib.pyplot as plt
import commands

#extEn=".en"


nomLig=""  ##se almacena el nombre del ligando limpio
atomo=[]
number=[]
charge=[]
gauss1=[]
gauss2=[]
repulsion=[]
hydrophobic=[]
hydrogen=[]
eTotal=[]

weight_gauss1      = -0.035579;
weight_gauss2      = -0.005156;
weight_repulsion   =  0.840245;
weight_hydrophobic = -0.035069;
weight_hydrogen    = -0.587439;
weight_rot         =  0.05846;


####____________________________________________________________________________________________________________
####
####	Main
####________________________________________________________________________________________________________-
if len(sys.argv) != 2:
	print "Indique el nombre del fichero sin extension"
	exit();
fileIn=sys.argv[1]
outFile=fileIn[:fileIn.rindex(".")]

##busco el comiennzo de los atomos y quito la cabecera
#comando="grep -n \"Name\" "+fileIn+extEn +"| awk -F: '{print $1}'"
#numInicio=commands.getoutput(comando)
#comando="sed '1,"+numInicio+"'d "+fileIn+extEn+ " > "+fileIn+extEn+extEn
#commands.getoutput(comando)
##corto el final del ficheo
#comando="grep -n \"Total\" "+fileIn+extEn+extEn+"| awk -F: '{print $1}'"
#numFin=commands.getoutput(comando)
#comando="sed '"+numFin+",$d' -i "+fileIn+extEn+extEn
#commands.getoutput(comando)
###arrayCiclostrinas=["O1","O2","C5","C6","C7","C8","C9","C10","C11","C12","C13","C14","C15","C18","C19","C20","C21","C22","C23","C24","O3","O4","C16","C17"]
###arrayCiclostrinas=["C5","C6","C7","C8","C9","C10","C11","C12","C13","C16","C18","C19","C20","C21","C22","C23","C24","C17","O4","O3","C17","O2","O1","C14","C15"]
j=0
#print (fileIn)
contador=0;
infile = open(fileIn, 'r')
#print fileIn
for line in infile:
	line=line[:-1]
	if line.find ("_________")!=-1: #marca para comenzar a leer
		contador=contador+1 ;
	if contador == 1 and line.find ("_________")==-1:
		#print (line)
		cadena=line.split()
		#print cadena[6], cadena[7],cadena[8], cadena[9],cadena[10]
		if cadena[6] != "0" or cadena[7] !="0" or cadena[8] !="0" or cadena[9]!="0" or cadena[10]!="0": #filtrando atomos que no den iteracion
		#if cadena[1] in arrayCiclostrinas:
			atomo.append(cadena[1])
			number.append(cadena[0])
			#charge.append(float(cadena[5]))
			gauss1.append(float(cadena[6])*weight_gauss1)
			gauss2.append(float(cadena[7])*weight_gauss2)
			repulsion.append(float(cadena[8])*weight_repulsion)
			hydrophobic.append(float(cadena[9])*weight_hydrophobic)
			hydrogen.append(float(cadena[10])*weight_hydrogen)
			"""
			print(atomo[j])
			print(number[j])
			print(gauss1[j])
			print(gauss2[j])
			print(repulsion[j])
			print(hydrophobic[j])
			print(hydrogen[j])
			"""
			j=j+1	
	

infile.close()


#print valores
#nomLig=fileIn[fileIn.rindex("/")+1:]

#comando="cat "+fileIn+extEn +" |tail -1 |awk -F, '{print $2\":\"$6\":\"$7\":\"$8\":\"$9\":\"$10\":\"$11\":\"$12\":\"$13\":\"$14}'"
#cadenaTmp=commands.getoutput(comando)
#cadena=cadenaTmp.split(":")

###_______________________________________________________________________________________________________
###
###		Grafica
###_______________________________________________________________________________________________________

N = j
ind = np.arange(N)  # the x locations for the groups
width = 0.1       # the width of the bars
#print ind
fig = plt.figure()
ax = fig.add_subplot(111)
#plt.axes((0.2,0.1,0.7,0.8))

# linea HORIZONTAL en la coordenada  y 0
plt.axhline(y=0, xmin=-1, xmax=j, linewidth=0.2, color = 'k')
##barras
#height=1

tamLineaBordeBar=[0.1]
rects1 = ax.bar(ind , gauss1, width, color='b',linewidth=tamLineaBordeBar )
rects2 = ax.bar(ind+width, gauss2, width, color='g',linewidth=tamLineaBordeBar)
rects3 = ax.bar(ind+2*width, repulsion, width, color='#E92424',linewidth=tamLineaBordeBar)
rects4 = ax.bar(ind+3*width, hydrophobic, width, color='c',linewidth=tamLineaBordeBar)
rects5 = ax.bar(ind+4*width, hydrogen, width, color='m',linewidth=tamLineaBordeBar)





#ax.set_ylabel('Contribution (Kcal/mol)')
#ax.set_title('Energetic contributions to binding energy ')

#ax.set_yticks(ind+width+3.5*width)
ax.set_xticks(ind)
ax.set_xticklabels( atomo )
#ax.set_xticklabels( ('HMGCoA', 'Lycopene', 'Chlorogenic acid', 'Naringenin') )
ax.set_xlim(-0.2,j)
ax.set_ylim(-0.7,0.7) ##comentar
#ax.set_xticks(ind+width+3.5*width)
ax.set_xticks(ind+width+1.5*width)

plt.gca().xaxis.grid(True,linewidth=0.3)

###____________________________________________________________________________________________________________
###
###		Modifico el tamanio y la inclinacion del nombre de las labels x
###________________________________________________________________________________________________________________-
for tick in ax.xaxis.get_major_ticks():
	tick.label.set_fontsize(8) 
	tick.label.set_rotation('90')
#####################################################################################################################
#ax.set_xlim(-1,j+0.5)
###_____________________________________________________________________________________________________________________
###
###		Leyenda
###______________________________________________________________________________________________________________________-
plt.plot(1, 1.5, color="b", linewidth=2.5, linestyle="-", label="gauss1")
plt.plot(1, 1.5, color="g",  linewidth=2.5, linestyle="-", label="gauss2")
plt.plot(1, 1.5, color='#E92424',  linewidth=2.5, linestyle="-", label="repulsion")
plt.plot(1, 1.5, color="c",  linewidth=2.5, linestyle="-", label="hydrophobic")
plt.plot(1, 1.5, color="m",  linewidth=2.5, linestyle="-", label="hydrogen")
plt.legend(loc='upper left', prop={'size':6})
#############################################################
plt.savefig(outFile+'EA.png',dpi=600)
#plt.show()

