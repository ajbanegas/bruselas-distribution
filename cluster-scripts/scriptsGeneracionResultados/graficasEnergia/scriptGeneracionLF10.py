#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import sys
import os
import matplotlib.pyplot as plt
import commands
extEn=".csv"

fileTmp={} ##se almacenan los fichreos temporales al hacer el dir
nomLig={}  ##se almacena el nombre del ligando limpio
####_________________________________________________________
####
####	Energias LF
####________________________________________________________
dG=[]	   ##energias del docking
eLJ=[]
eMetal=[]
eSol=[]
eHbonds=[]
eElect=[]
eInternal=[]
eTors=[]
eDihidral=[]
ePenaltu=[]

####____________________________________________________________________________________________________________
####
####	Main
####________________________________________________________________________________________________________-
if len(sys.argv) != 2:
	print "Indique el nombre del fichero sin extension"
	exit();
fileIn=sys.argv[1]
j=0
#print fileIn
for fichero in os.listdir(fileIn):
	if fichero.find(extEn)!=0-1:
		fileTmp[j]=fichero[:fichero.find(extEn)]  
		comando="cat "+fileIn+fileTmp[j]+extEn +" |tail -1 |awk -F, '{print $2\":\"$6\":\"$7\":\"$8\":\"$9\":\"$10\":\"$11\":\"$12\":\"$13\":\"$14}'"
		#print comando
		cadenaTmp=commands.getoutput(comando)
		cadena=cadenaTmp.split(":")
		dG.append(float(cadena[0]))
		eLJ.append(float(cadena[1]))
		eMetal.append(float(cadena[2]))
		eSol.append(float(cadena[3]))
		eHbonds.append(float(cadena[4]))
		eElect.append(float(cadena[5]))
		eInternal.append(float(cadena[6]))
		eTors.append(float(cadena[7]))
		eDihidral.append(float(cadena[8]))
		ePenaltu.append(float(cadena[9]))
		j=j+1

#guardo el nombre del liagndo "Hay que limpiarlo puesto que sera una cadena tal que VS-LF-proteina-ligando-coordenadas"
opcion=os.path.basename(fileTmp[0])
if opcion[0].isdigit():
	opcion="BD"
else: 
	opcion="VS"
for i in range(j):
	if opcion == "VS":
		nomLig[i]=fileTmp[i][fileTmp[i].find("-")+1:]	
		nomLig[i]=nomLig[i][nomLig[i].find("-")+1:]
		nomLig[i]=nomLig[i][nomLig[i].find("-")+1:]
	if opcion== "BD":
		nomLig[i]=fileTmp[i][fileTmp[i].find("-")+1:]
	#	print nomLig[i]
		nomLig[i]=nomLig[i][nomLig[i].find("-")+1:]
	#	print nomLig[i]
		nomLig[i]=nomLig[i][nomLig[i].find("-")+1:]
	#	print nomLig[i]
		nomLig[i]=nomLig[i][nomLig[i].find("-")+1:]
	nomLig[i]=nomLig[i][:nomLig[i].find("-")]
	#print nomLig[i]


###_______________________________________________________________________________________________________
###
###		Grafica
###_______________________________________________________________________________________________________

N = j

ind = np.arange(N)  # the x locations for the groups
width = 0.075       # the width of the bars

fig = plt.figure()
ax = fig.add_subplot(111)

#  HORIZONTAL
#plt.axhline(y=0, xmin=a, xmax=-5, linewidth=1, color = 'k')
##barras
#similares a desglse atomo
rects1 = ax.bar(ind, eLJ, width, color='blue')
rects2 = ax.bar(ind+width, eSol, width, color='green')
rects3 = ax.bar(ind+2*width, eHbonds, width, color='r')
rects4 = ax.bar(ind+3*width, eElect, width, color='cyan')
rects5 = ax.bar(ind+4*width, eInternal, width, color='magenta')
rects6 = ax.bar(ind+5*width, eMetal, width, color='#eeefaa')
#no comun a desglose atom
rects7 = ax.bar(ind+6*width, eTors, width, color='y')
rects7 = ax.bar(ind+7*width, eDihidral, width, color='#aaefff')
rects8 = ax.bar(ind+8*width, ePenaltu, width, color='#bbefaa')
rects9 = ax.bar(ind+9*width, dG, width, color='black')


# add some
ax.set_ylabel('Contribution (Kcal/mol)')
ax.set_title('Energetic contributions to binding energy ')
ax.set_xticks(ind+width+3.5*width)
ax.set_xticklabels( nomLig )
###____________________________________________________________________________________________________________
###
###		Modifico el tamaño y la inclinacion del nombre de las labels x
###________________________________________________________________________________________________________________-
for tick in ax.xaxis.get_major_ticks():
	tick.label.set_fontsize('x-small') 
	tick.label.set_rotation('35')
#####################################################################################################################
ax.set_xlim(-1,j+0.5)
###_____________________________________________________________________________________________________________________
###
###		Leyenda
###______________________________________________________________________________________________________________________-
plt.plot(1, 4, color="blue", linewidth=2.5, linestyle="-", label="van der waals")
plt.plot(1, 4, color="green",  linewidth=2.5, linestyle="-", label="E(solvation)")
plt.plot(1, 4, color='red',  linewidth=2.5, linestyle="-", label="E(hydrogen bonds)")
plt.plot(1, 4, color="cyan",  linewidth=2.5, linestyle="-", label="E(electrostatic)")
plt.plot(1, 4, color="magenta",  linewidth=2.5, linestyle="-", label="E(internal)")
plt.plot(1, 4, color="#eeefaa",  linewidth=2.5, linestyle="-", label="E(metal)")
plt.plot(1, 4, color="yellow",  linewidth=2.5, linestyle="-", label="rotable bonds")
plt.plot(1, 4, color='#aaefff',  linewidth=2.5, linestyle="-", label="E(dihedral)")
plt.plot(1, 4, color='#bbefaa',  linewidth=2.5, linestyle="-", label="E(penalty)")
plt.plot(1, 4, color="black",  linewidth=2.5, linestyle="-", label="E(predicted binding)")
plt.legend(loc='upper left', prop={'size':6})
#############################################################
plt.savefig(fileIn+'graficaEnergias.png',dpi=600)
plt.show()


