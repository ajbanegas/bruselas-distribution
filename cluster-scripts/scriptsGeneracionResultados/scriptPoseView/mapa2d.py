import urllib
import pycurl
from io import BytesIO
import time
import commands
import StringIO
import os, sys
from cStringIO import StringIO
import urllib2
import commands

###________________________________________________________________________________________________________
###
###Urls utilizadas
###________________________________________________________________________________________________________
urlIndex='http://poseview.zbh.uni-hamburg.de/poseview/wizard/receptor.html?rec=file'
urlLigand='http://poseview.zbh.uni-hamburg.de/poseview/wizard/ligand.html'
urlDisplay='http://poseview.zbh.uni-hamburg.de/poseview/wizard/display.html'
urlImage='http://poseview.zbh.uni-hamburg.de'

bufferNull = BytesIO() ##buffer para descartar
buffer=BytesIO()
cokkie=""

#Class which holds a file reference and the read callback
class FileReader:
	def __init__(self, fp):
	 	self.fp = fp
 	def read_callback(self, size):
	 	return self.fp.read(size)
###__________________________________________________________-
###
###Subir el primer fichero del receptor
###___________________________________________________________--
def subirFichero(url, fichero):
	
	c = pycurl.Curl()
  	c.setopt(pycurl.COOKIEFILE, cokkie)
   	c.setopt(c.URL, url)
   	c.setopt(c.WRITEDATA, bufferNull)
   	c.setopt(c.HTTPPOST,[ ("authenticity_token", token),("receptor[data]", (c.FORM_FILE, fichero,c.FORM_CONTENTTYPE, "application/x-gzip"))])
   	#c.setopt(pycurl.VERBOSE, 1)
   	c.perform()
   	c.close()
	
###____________________________________________________________________________
###
### Subir el fichero del ligando
###___________________________________________________________________________-

def subirFichero2(url, fichero):
  
  c = pycurl.Curl()
  c.setopt(pycurl.COOKIEFILE, cokkie)
  c.setopt(c.URL, url)
  c.setopt(c.HTTPPOST,[ ("authenticity_token", token),("lig", "file"),("ligand[data]",(c.FORM_FILE, fichero, c.FORM_CONTENTTYPE, "application/x-gzip")) ])
  c.setopt(c.WRITEDATA, buffer)
  c.setopt(pycurl.FOLLOWLOCATION, 1) 
  #c.setopt(pycurl.VERBOSE, 1)
  c.perform()
  c.close()
  




#try:
if len(sys.argv) != 3:
	print 'Debe introducir proteina y ligando'
	exit()
fileReceptor=sys.argv[1] 
fileLigando=sys.argv[2] 
fileLigando=fileLigando[:fileLigando.rindex(".")] 

print "Generando mapa de interacciones "+fileLigando
cokkie=fileLigando+".ck"
comando="python "+os.getcwd()+"/scriptsGeneracionResultados/scriptPoseView/dividirMol.py "+fileLigando
commands.getoutput(comando)
fileLigand=fileLigando+"tmp.mol2"
	#print fileLigand 
###__________________________________________________________________________________________________________
###
###  Conecto con el servidor para recoger el token y la cookie
###__________________________________________________________________________________________________________
c = pycurl.Curl()
c.setopt(pycurl.COOKIEJAR,cokkie)	#recojo cookie
c.setopt(pycurl.URL, urlIndex)	
c.setopt(c.WRITEDATA, bufferNull) #leo datos
#c.setopt(pycurl.VERBOSE, 1) 
c.perform()					  #ejecutar	
#c.close()
body = bufferNull.getvalue() #convierto a string normal
#filtro el token
p= body.decode('iso-8859-1').find("authenticity_token")
p=(body.decode('iso-8859-1')[p:])
token=p[p.find("value")+7:p.find("div")-6]
c.close()

# subo fichero Proteina 
#filename='1DFC.pdb'
subirFichero(urlLigand,fileReceptor)
#subo fichero ligando
#filename='VS-LF-1DFC_rec-fda_lig_143.mol2'
subirFichero2(urlDisplay,fileLigand)
body = buffer.getvalue()
#print body.decode('iso-8859-1') 
link=body.decode('iso-8859-1').find("/poseview/generated/")
link=(body.decode('iso-8859-1')[link:])
link=link[:link.find("\"")]
#descargo y booro el fihcero temporal
#wget -O example.html http://www.electrictoolbox.com/wget-save-different-filename/

comando="wget -O "+fileLigando+".png "+urlImage+link
commands.getoutput(comando)	
#	comando="rm "+fileLigand
#	commands.getoutput(comando)	
#except:
#	print "ERROR!  Al generar el mapa de interacciones " #+fileReceptor +"  "+fileLigando
#else:
#	comando="rm "+fileLigando+".ck"
#	commands.getoutput(comando)
#	comando="rm "+fileLigand
#	commands.getoutput(comando)
#	comando ="rm "+fileLigando+".mol2"
#	commands.getoutput(comando)


        




###______________________________________________________________________________________________________________________
###
###mirar cabeceras
###_______________________________________________________________________________________________________________________
#print '-'*100
#print "URL ", c.getinfo(pycurl.EFFECTIVE_URL)
#print "HTTP-code:", c.getinfo(c.HTTP_CODE)
#print "Total-time:", c.getinfo(c.TOTAL_TIME)
#print "Download speed: %.2f bytes/second" % c.getinfo(c.SPEED_DOWNLOAD)
#print "Document size: %d bytes" % c.getinfo(c.SIZE_DOWNLOAD)
#print "Effective URL:", c.getinfo(c.EFFECTIVE_URL)
#print "Content-type:", c.getinfo(c.CONTENT_TYPE)
#print "Namelookup-time:", c.getinfo(c.NAMELOOKUP_TIME)
#print "Redirect-time:", c.getinfo(c.REDIRECT_TIME)
#print "Redirect-count:", c.getinfo(c.REDIRECT_COUNT)
#epoch = c.getinfo(c.INFO_FILETIME)
#print "Filetime: %d (%s)" % (epoch, time.ctime(epoch))
#print '-'*10
#c9s6jn+gjk3op8Jleo/XBhlrihHA2SuTeIdP0rG8EB0=
#9s6jn+gjk3op8Jleo/XBhlrihHA2SuTeIdP0rG8EB0=
#value = output.getvalue()
#if value.strip():
#    print value
#c.close()




