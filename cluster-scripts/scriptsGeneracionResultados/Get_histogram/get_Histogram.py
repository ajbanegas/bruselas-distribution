from os import listdir
from collections import OrderedDict
import matplotlib.patches as patches
import matplotlib.path as path
import matplotlib
matplotlib.use("Agg")
from  Get_histogram.debug import Debug
from  Get_histogram.debug import bcolors
from pylab import *
lvlDebug=10
class Histogram():
	cords=[]
	data=[]
	bestScore={}
	countSamples=0
	def __init__(self,fileEntrada, programa, opcion,maxResultados, fileProteina,fileLigando,fileSalida,engCorte,modeDebug):
		self.fileEntrada=fileEntrada
		self.programa=programa
		self.opcion=opcion
		self.maxResultados=maxResultados
		self.fileProteina=fileProteina
		self.fileLigando=fileLigando
		self.fileOutputHistogram=fileSalida
		self.engCorte=engCorte
		#self.modeDebug=modeDebug
		self.debug=Debug(modeDebug)

	###__________________________________________________________________________________________________________________________
	###
	###		Recoge los datos de los ficheros txt generados en las ejecuciones y los guarda en data y mejorEcore							 
	###____________________________________________________________________________________________________________________________

	def getData(self):
		for cosa in listdir(self.fileEntrada):
			if cosa.find(".txt") >= float(0):	
				f = open(self.fileEntrada+cosa)
				cords=f.read().split(" ")
				if cords[0] != "" and cords[0] != "nan" : ##nan por si ha fallado leadfinder 
					if self.programa == "WG" or self.programa == "LS":	
						if float(cords[0]) > self.engCorte : # 0 : filtro resultados solo desde -100 hasta 0			
							self.countSamples=self.countSamples+1						
							self.data.append(float (cords[0]))
							if self.opcion == "VS":
								self.debug.show(cords[0]+" , "+cords[4], bcolors.GREEN,lvlDebug)
								self.guardarMejoreScore(float (cords[0]), cords[4]) #
							else:
								self.debug.show(cords[0]+","+cords[4], bcolors.GREEN,lvlDebug)
								self.guardarMejoreScore(float (cords[0]), self.fileLigando)
					else :
						#print cords[0]

						if float(cords[0]) < float(self.engCorte) : # 0: filtro resultados solo desde  hasta 0			
		#					countSamples=countSamples+1					
							#data.append(float (cords[0]))
							if self.opcion == "VS":
								#print str(cords[0]) + "    "+ str(cords[4])
								if self.programa=="LF":
									if float(cords[0]) > -20:
										self.countSamples=self.countSamples+1					
										self.data.append(float (cords[0]))
										self.debug.show(cords[0]+","+cords[4], bcolors.GREEN,lvlDebug)
										self.guardarMejoreScore(float (cords[0]), cords[4]) #
										#print "coords 0:" +str(cords[0]) + "cords 4 "+  str(cords[4]) #
								else :
									self.countSamples=self.countSamples+1					
									self.data.append(float (cords[0]))
									self.debug.show(cords[0]+","+cords[4], bcolors.GREEN,lvlDebug)
									self.guardarMejoreScore(float (cords[0]), cords[4]) #
							else:
								self.countSamples=self.countSamples+1					
								self.data.append(float (cords[0]))
								self.debug.show(cords[0]+","+str(cords[5]+cords[4]), bcolors.GREEN,lvlDebug)
								self.guardarMejoreScore(float (cords[0]), cords[5]+cords[4]) #
				f.close()
	###__________________________________________________________________________________________________________________________
	###
	###	Almacena la mejor energia de las mejores pruebas el numero se indica en: RESULTADOSFICHEROS  para guardar los resultados
	### se podria mejorar el meotodo para hacerlo mas rapido							 
	###____________________________________________________________________________________________________________________________
	def guardarMejoreScore(self,energia, nombre): 
		#bestScore.update({nombre:energia}) #otra foram
		self.bestScore[nombre] = energia 
		if self.programa == "WG" or self.programa == "LS":
			self.bestScore = OrderedDict(sorted( self.bestScore.items(), key=lambda x: x[1], reverse=True ))
		else:
			self.bestScore = OrderedDict(sorted(self.bestScore.items(), key=lambda x: x[1]))
		count=0
		#bucle para eliminar el ultimo 
		for k, v in self.bestScore.items():
			if count >= self.maxResultados:
				self.bestScore.pop(k)
			count= count+1;		

	###__________________________________________________________________________________________________________________________
	###
	###		Genera una grafica en matplotlib con los datos leidos previamente				 
	###____________________________________________________________________________________________________________________________	
	
	def generateHistogram(self):
		fig = plt.figure()
		ax = fig.add_subplot(111)
		if  self.programa == "WG": #caso especial para cambiar los titulos
			ejeXLabel="Similarity"
			ax.set_xlim(-0.2, 1.2)
			visualizacionProteina="sticks"
		if  self.programa == "LS":
			ejeXLabel="Pharmacophore-Fit Score"
			visualizacionProteina="sticks"
		n, bins = np.histogram(self.data, 50) 
		###___________________________LEENDA CON LOS CSASOS
		#plt.legend(loc = 2) 
		#labelLeyenda="samples: "+str(self.countSamples)
		#plt.hist(self.data,  50,label=labelLeyenda)
		###___________________________________________
		plt.hist(self.data,  50)
		left = np.array(bins[:-1])
		right = np.array(bins[1:])
		bottom = np.zeros(len(left))
		top = bottom + n
		#ax.set_xlim(left[0]-0.5, right[-1]+0.5)
		ax.set_xlim(left[0]-0.5, right[-1]+0.5)
		ax.set_xlabel('Binding energy (Kcal/Mol)')
		ax.set_ylabel('Number of ocurrences')
		
		ax.plot([0,1,2],[10,10,100],marker='o',linestyle='-')
		ax.set_yscale('symlog') 
		p=self.fileProteina[:self.fileProteina.rindex(".")]
		p=p[p.rindex("/")+1:]

		if self.opcion == "VS" or self.opcion == "BDVS": #dependiendo de si es BS o BD se pone un titulo u otro
			l=self.fileLigando[:self.fileLigando.rindex("/")]
			l=l[l.rindex("/")+1:]
			self.debug.show("titulo "+l,bcolors.GREEN,10)
		else:
			l=self.fileLigando[:self.fileLigando.rindex(".")]
			l=l[l.rindex("/")+1:]
			self.debug.show("titulo "+l,bcolors.GREEN,10)

		titulo=" Docking results "+ self.opcion + " " +self.programa + " "+p+ " "+l
		title(titulo)
		plt.savefig(self.fileOutputHistogram+".png") 
		self.debug.show("Grafica salvada es: "+self.fileOutputHistogram+".png",bcolors.GREEN,10)

	
