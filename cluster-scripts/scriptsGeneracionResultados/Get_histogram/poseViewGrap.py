
import commands
import os
from  Get_histogram.debug import Debug
from  Get_histogram.debug import bcolors
lvlDebug=10					#lvl estandar para el debug
color=bcolors.GREEN 		#color standar para el debug
class PoseviewGrap():


	def __init__(self, fileProteina,ruteBestScore,nombresLigandos,extTemporal,modeDebug):
		self.fileProteina=fileProteina
		self.protOrig=""
		self.ruteBestScore=ruteBestScore
		self.nombresLigandos=nombresLigandos
		self.extTemporal=extTemporal
		self.urlPdbs="http://www.rcsb.org/pdb/files/"
		self.debug=Debug(modeDebug)

	def generatePoseView(self):
		self.descargarPdbProteina()
		self.poseView()
					

	###__________________________________________________________________________________________________
	###
	###	Descarga El fichero de la proteina para PoseView
	###__________________________________________________________________________________________________

	#descarga de proteina
	def descargarPdbProteina(self):
		#quito la extension a la proteina y la copio al fichero tmp de ligPluis
		prt=os.path.split(self.fileProteina)
		aux=prt[1];
		self.protOrig=aux[:aux.rindex(".")]
		self.protOrig=self.protOrig.split(".")[0]
		if os.path.exists("proteinas/"+self.protOrig+".pdb"): #si existe el pdb en ligandos
			comando="cp proteinas/"+self.protOrig+".pdb scriptsGeneracionResultados/scriptPoseView/tmp/"
		else:
			self.protOrig=self.protOrig[0:4]
			comando="wget "+self.urlPdbs+self.protOrig+".pdb -P scriptsGeneracionResultados/scriptPoseView/tmp/"
			commands.getoutput(comando)	
			self.debug.show(comando, color,lvlDebug)
			#print comando	
			#se mueve a la carpeta temporal del script ligplus
			#comando="mv "+self.protOrig+".pdb scriptsGeneracionResultados/scriptPoseView/tmp/"
		self.debug.show(comando, color,lvlDebug)
		commands.getoutput(comando)	
	###__________________________________________________________________________________________________________________
	###
	###		Genera El mapa de interaccion proteina ligando
	###____________________________________________________________________________________________________________________
	def poseView(self):
		for i in self.nombresLigandos:
			if i.find(self.extTemporal) >= 0:
				i=i[:i.rindex(self.extTemporal)] ##falrta quitar extension
				if self.extTemporal != ".mol2":
					comando="babel -h "+self.ruteBestScore+i+self.extTemporal +" "+self.ruteBestScore+i+".mol2"
					self.debug.show(comando, color,lvlDebug)
					commands.getoutput(comando)
				#comando="python scriptsGeneracionResultados/scriptPoseView/mapa2d.py scriptsGeneracionResultados/scriptPoseView/tmp/"+self.protOrig+".pdb  "+self.ruteBestScore+i+".mol2"
				comando="./scriptsGeneracionResultados/poseView/poseview -t \" \" " + \
				        "-c ./scriptsGeneracionResultados/poseView/settings.pxx " +	\
                        "-p scriptsGeneracionResultados/scriptPoseView/tmp/"+self.protOrig+".pdb " + \
                        "-l "+self.ruteBestScore+i+".mol2 -o " + self.ruteBestScore+i+ ".png"
				self.debug.show(comando, color,lvlDebug)
				print commands.getoutput(comando)	
				#sftp://malaga/mnt/home/users/ac_001_um/jorgedlpg/pruebasDoking/SENECA/docking/scriptsGeneracionResultados/poseView