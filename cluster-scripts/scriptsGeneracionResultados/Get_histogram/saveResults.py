
import commands
from  Get_histogram.debug import Debug
from  Get_histogram.debug import bcolors
lvlDebug=10
class SaveResults():
	nombresLigandos=[]
	allfich=[] #contiene todos los ficheros del directorio cuando se hace BD
	def __init__(self, bestScore, resultBestScore,fileProteina, programa,ruteBestScore, rute,fileEntrada, opcion, fileLigando,clusterizado,modeDebug):
		self.bestScore=bestScore 
		self.resultBestScore=resultBestScore
		self.fileProteina=fileProteina
		self.programa=programa
		self.ruteBestScore=ruteBestScore
		self.rute=rute
		self.fileEntrada=fileEntrada
		self.opcion=opcion
		self.fileLigando=fileLigando
		self.contador=0
		self.clusterizado=clusterizado
		self.debug=Debug(modeDebug)

	###______________________________________________________________________________________________________________________
	###
	###		guarda los 10 en la carpeta mejor Resutlado y los 90 siguientes en otra carpeta
	###_______________________________________________________________________________________________________________________
	def saveFilesResutls(self):
		self.findExt() ##encuentra la extension 
		for k, v in self.bestScore.items():
			if self.fileProteina[self.fileProteina.rindex("/")+1:]  != k[k.rindex("/")+1:]: #el caso wega q el ligando sea el mismo que el almacenado y casca
				if self.contador < self.resultBestScore and self.clusterizado=="n": 		#para guardar los x mejores en mejor Score
					rutaTmp=self.ruteBestScore	
				else:
					rutaTmp=self.rute	
				#######sabeidno el numero hago un ls al fichero con un grep para que me devuelva la ruta y el nomrbe original
				fich,fileOri=self.buscarLigando(k,self.fileEntrada,self.extProteina)
				
				######file ya contiene la ruta del ligando sin extension para copiar todo al directorio de resultaods *	
				if self.opcion == "SD": ####CHAPU ANIADIDA el 10-9-2015 para que valga el modo sd y copie las proteinas mod
					comando="cp "+self.fileEntrada + "0-* "+ rutaTmp	 ##se copia todo el rato HAY QUE CAMBIARLO
					self.debug.show(comando, bcolors.GREEN,lvlDebug)
					commands.getoutput(comando)	
					k=k[:k.index("-")] 									##quito la cadena de antes del numero	
					comando="cp "+self.fileEntrada + k + "-* "+ rutaTmp	
					self.debug.show(comando, bcolors.GREEN,lvlDebug)
					fileOri=self.fileLigando
					resultado = commands.getoutput(comando)	
				else:
					comando="cp "+self.fileEntrada + fich + "* "+ rutaTmp	##copio todos los ficheros con ese nombre y diferente extension 
					self.debug.show(comando, bcolors.GREEN,lvlDebug)
					commands.getoutput(comando)

				if self.clusterizado=="n": #chapu
				##si son de los mejores 10 lo agrego a una lista para luego generar el fichero pml
					if self.contador < self.resultBestScore:
						self.nombresLigandos.append(fich + self.extProteina)	
						if self.opcion=="VS":
							self.nombresLigandos.append(fileOri)	
				self.copyLigandOri(fileOri, rutaTmp)	
				self.contador +=1
		
			if self.opcion == "BD":	
				self.allfich.append(fich)
		comando="cp "+self.fileProteina+ " "+self.ruteBestScore
		commands.getoutput(comando)		
		self.debug.show(comando, bcolors.GREEN,lvlDebug)
		if self.programa=="WG":							
			comando="cat "+self.ruteBestScore+"*.sdf |grep -v target  |sort -r -t , -k +3> "+self.ruteBestScore+"ResultsAlignWithQuery.sdf"
			commands.getoutput(comando)			##esto no fundionra
			self.debug.show(comando, bcolors.GREEN,lvlDebug)
		if self.programa=="LS":							
			comando="cat "+self.ruteBestScore+"*.sd |grep -v target  |sort -r -t , -k +3> "+self.ruteBestScore+"ResultsAlignWithQuery.sd"
			commands.getoutput(comando)				
			self.debug.show(comando, bcolors.GREEN,lvlDebug)
			comando ="cat "+self.ruteBestScore+"*.sdf > 10mejores.sdf"##esto no funciona

	def copyLigandOri(self,fileOri, rutaTmp):
		#copiar ligando original en la carpeta mejorScore
		#en BD solo se copia una vez y VS todas
		if self.opcion == "BD" and self.contador==0:
			self.nombresLigandos.append(fileOri[fileOri.rindex("/")+1:])	
			comando="cp "+fileOri+" " + rutaTmp		#lo pego en la carpeta de buenos
			self.debug.show(comando, bcolors.GREEN,lvlDebug)
			commands.getoutput(comando)
		if self.opcion=="VS":
			comando="cp "+self.fileLigando+""+fileOri+" " + rutaTmp		#lo pego en la carpeta de buenos
			self.debug.show(comando, bcolors.GREEN,lvlDebug)
			commands.getoutput(comando)
	###________________________________________________________________________________________________________
	###
	###		Funcion el fichero del liggando
	###		FileOri es el ligando orgiginal introducido por el user ligandos/GLA.xxx
	###		resultaod es un fichero para copiar con el comodin *
	###________________________________________________________________________________________________________
	def buscarLigando(self,k,fileEntrada,extProteina):
	 	if self.opcion!="LS":
		 	if self.opcion == "VS" :
				k=k[:k.rindex(".")] 									##quito la extension al dato guardado como ligando
				k=k[k.rindex("/")+1:] 									##quito la cadena de antes del nombre		
				comando="ls "+ self.fileEntrada+ " |grep  \"\\\-" +str(k)+ "\-\" |grep -i .txt"		##busco solo el nombre del ligando sin extension siempre tiene que existir un .txt si la prueba sale bien	
				comando2="ls "+ self.fileLigando+ " |grep  "  +str(k)+"\\"+extProteina##busco en el directoro de entrada el lig_xxx."Original"		
				self.debug.show(comando2, bcolors.GREEN,lvlDebug)
				fileOri=commands.getoutput(comando2)
			if self.opcion == "BD" or self.opcion == "SD" :
				k=k[:k.index("-")] 									##quito la cadena de antes del numero		
				comando="ls "+ fileEntrada+ " |grep  ^"+str(k)+"- |head -1" 	##busco en el directoro de entrada el lig_xxx.	
				fileOri=self.fileLigando	
			if self.opcion =="BDVS" or self.opcion == "BDC":
				k=k[:k.index("-")] 
				fileOri=self.fileLigando
				comando="ls "+ self.fileEntrada+ " |grep  \"^" +str(k)+ "-\""
			self.debug.show(comando, bcolors.GREEN,lvlDebug)
			resultado = commands.getoutput(comando)	
			return resultado[:resultado.rindex(".")],fileOri
		else:
			return "",""
	
	##hay que modificar scriptAD para hacer que el docking salga sin -out.pdbqt solo .pdbqt
	def findExt(self):
		if self.programa == "LF":
			self.extProteina=".pdb"
		else:
			self.extProteina=self.fileProteina[self.fileProteina.rindex("."):]
		






		########################descargarPdbProteina()
		
		"""
		if self.programa == "AD":  #esto esta un poco chapu
			self.extTemporal=".pdbqt"
		if self.programa == "LF":
			self.extTemporal=".pdb"
		if self.programa == "FB":
			self.extTemporal=".mol2"
		if self.programa == "WG":
			self.extTemporal=".sd"
		if self.programa == "LS":
			self.extTemporal=".sdf"
		"""


