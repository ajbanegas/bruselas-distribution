#from os import listdir
import os 
import sys
import commands
import numpy as np 
import math
import matplotlib.pyplot as plt 
from  Get_histogram.debug import Debug
from  Get_histogram.debug import bcolors
lvlDebug=10					#lvl estandar para el debug
color=bcolors.GREEN 		#color standar para el debug
class DistanceGraph:
	def __init__(self, ruteBesScore, programa, nombresLigandos,modeDebug):
		self.ruteBestScore=ruteBesScore
		self.programa=programa
		self.nombresLigandos=nombresLigandos
		self.debug=Debug(modeDebug)
		
	def generateDistGraph(self):
		nomLig=[];  #ligando + 
		pointsLig=[]#x + y + z  
		contadorAtom=0;
		x=0;
		y=0;
		z=0;
		countLig=0;
		if len(self.nombresLigandos) > 1:
			for fich in self.nombresLigandos:	
				comando="python scriptLanzador/standarFileCoords.py "+self.ruteBestScore+fich +"> "+self.ruteBestScore+fich+".TMP"
				self.debug.show(comando, color,lvlDebug)
				commands.getoutput(comando)
				f = open(self.ruteBestScore+fich+".TMP")
				for line in f:
					line=line.rstrip('\n')
					if (line.find("##")==-1) :
						s=line.split(':')
						x+=float(s[0])
						y+=float(s[1])
						z+=float(s[2])
						contadorAtom+=1
				f.close()
				s=fich.split("-")
				##nomLig.append(s[0]+"-"+s[4]) ##luego descomentar
				nomLig.append("CL" +str(countLig+1))
				pointsLig.append([])
				pointsLig[countLig].append(x/contadorAtom)
				pointsLig[countLig].append(y/contadorAtom)
				pointsLig[countLig].append(z/contadorAtom)
				x=y=z=contadorAtom=0
				if countLig >= 10 : #apano solo para una prueba
					break

				countLig+=1

			
				os.remove(self.ruteBestScore+fich+".TMP")
				self.debug.show("save "+self.ruteBestScore+fich+".TMP", color,lvlDebug)
			
			#print s[0]+"-"+s[4], x/contadorAtom , y/contadorAtom , z/contadorAtom, contadorAtom
			datos = np.zeros(shape=(countLig,countLig))
			count=0;
			#for i in range(countLig-1, -1,-1):
			for i in range(0, countLig):
				for zi in range(0, countLig): 
				#for zi in range(countLig-1, count-1,-1): 
					x=(pointsLig[i][0]-pointsLig[zi][0])**2
					y=(pointsLig[i][1]-pointsLig[zi][1])**2
					z=(pointsLig[i][2]-pointsLig[zi][2])**2
					d=math.sqrt((x+y+z))
					datos[i][zi]=d
				count+=1

			mask =  np.tri(datos.shape[0], k=-1) ## mascara para ocultar los datos
			for i in range(0, countLig): ## doy la vuelta a la mask manualmente
				for z in range(0,countLig):
					if mask[i][z]==0:
						mask[i][z]=1
					else:
						mask[i][z]=0
			datos = np.ma.array(datos, mask=mask)
			######################################################################
			title = "Distances"
			xlabel= "Ligands"
			ylabel="Ligands"
			fig, ax = plt.subplots( ) 
			plt.title(title)
			plt.xlabel(xlabel)
			plt.ylabel(ylabel)

			c = plt.pcolor(datos, edgecolors='k', cmap=plt.cm.rainbow)

			def show_values(pc, fmt="%.2f", **kw):
		   		from itertools import izip
		   		pc.update_scalarmappable()
		   		ax = pc.get_axes()
		   		for p, color, value in izip(pc.get_paths(), pc.get_facecolors(), pc.get_array()):
					x, y = p.vertices[:-2, :].mean(0)
					if np.all(color[:3] > 0.5):
						color = (0.0, 0.0, 0.0)
					else:
						color = (1.0, 1.0, 1.0)
					ax.text(x, y, fmt % value,  va="center",  fontsize=5, rotation="vertical" , color=color, **kw)
			show_values(c)

			plt.colorbar(c)

			###____________________________________________-
			###centra los sticks
			###____________________________________________
			ax.set_yticks(np.arange(datos.shape[0]) + 0.5, minor=False)
			ax.set_xticks(np.arange(datos.shape[1]) + 0.5, minor=False)

			#ax.invert_yaxis()
			#ax.invert_xaxis()
			###_____________________________________________________________
			###Pone nombre a los stricks e invirte el eje y
			###_______________________________________________________
			ax.set_xticklabels(nomLig, minor=False)
			revertNomLig = list(nomLig)
			revertNomLig.reverse()
			ax.set_yticklabels(nomLig, minor=False) 
			ax.set_xlim(0, countLig)
			ax.set_ylim(0, countLig)

			###____________________________________________________________________________________________________________
			###
			###		Modifico el tamanio y la inclinacion del nombre de las labels x y los |- -|
			###________________________________________________________________________________________________________________-
			for tick in ax.xaxis.get_major_ticks():
				#tick.label.set_fontsize('x-small') 
				tick.label.set_fontsize(7) 

				tick.label.set_rotation('90')
				tick.tick2On = False
			for t in ax.yaxis.get_major_ticks():
			    #t.tick1On = False
			    t.label.set_fontsize(7) 
			    t.tick2On = False
			###

			plt.savefig(self.ruteBestScore+"graphDistances.png",dpi=300)
			self.debug.show("save "+self.ruteBestScore+"graphDistances.png" ,color,lvlDebug)

		
		

