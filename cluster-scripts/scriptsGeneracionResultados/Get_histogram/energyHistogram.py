from os import listdir
import commands
from  Get_histogram.debug import Debug
from  Get_histogram.debug import bcolors
lvlDebug=10					#lvl estandar para el debug
color=bcolors.GREEN 		#color standar para el debug

class EnergyHistogram():
	def __init__(self, ruteBesScore, programa,modeDebug):
		self.ruteBestScore=ruteBesScore
		self.programa=programa
		self.debug=Debug(modeDebug)

	def generateHistograms(self):
		for fich in listdir(self.ruteBestScore):
			
			if fich.find(".en1") >= 0:
				comando="python scriptsGeneracionResultados/graficasEnergia/scriptGeneracionGraficaVina.py "+self.ruteBestScore+fich
				self.debug.show(comando, color,lvlDebug)
				print commands.getoutput(comando)
				comando="python scriptsGeneracionResultados/graficasEnergia/scriptGeneracionGraficaAtomoAD.py "+self.ruteBestScore+fich
				self.debug.show(comando, color,lvlDebug)
				print commands.getoutput(comando)
			
			if fich.find(".csv") >= 0:
				fich=fich[:-4] 				#le quito la extension por las graficas estan pensadas asi
				comando="python scriptsGeneracionResultados/graficasEnergia/scriptGeneracionGraficaLF.py "+self.ruteBestScore+fich
				self.debug.show(comando, color,lvlDebug)
				commands.getoutput(comando)	
				comando="python scriptsGeneracionResultados/graficasEnergia/scriptGeneracionGraficaAtomoLf.py "+self.ruteBestScore+fich
				self.debug.show(comando, color,lvlDebug)
				commands.getoutput(comando)
	###_____________________________________________________________________________________________________________________
	###
	###		Hace las graficas de energia de los 10 mejores y junta las imagenes
	###_____________________________________________________________________________________________________________________
	def graficaEnergiaColectiva(self,extTemporal, nombresLigandos):
		if self.programa=="AD":
			#print "graficas energias colectiva"
			comando="python scriptsGeneracionResultados/graficasEnergia/scriptGeneracionVina10.py "+self.ruteBestScore
			print commands.getoutput(comando)	
		#	print extTemporal
			for i in nombresLigandos:
				#print "entro al for"
				if i.find("VS") !=-1 or i.find ("BD")!=-1: 	
					i=i[:i.rindex(extTemporal)] 

					salidaPack=self.ruteBestScore+i+"Pack.png"
					imagenA=self.ruteBestScore+i+"E.png"
					imagenC=self.ruteBestScore+i+"EA.png"
					imagenB=self.ruteBestScore+i+".png"					
	 				comando="convert "+imagenB +" -gravity South  -chop  0x40 "+imagenB 
	 				self.debug.show(comando, color,lvlDebug)
					commands.getoutput(comando)	
					comando="montage -mode concatenate -tile 2x2 -geometry 800x600 "+imagenA + " "+imagenB+ " "+imagenC+" "+salidaPack	
					self.debug.show(comando, color,lvlDebug)
					commands.getoutput(comando)		
		if self.programa=="LF":
			comando="python scriptsGeneracionResultados/graficasEnergia/scriptGeneracionLF10.py "+self.ruteBestScore
			self.debug.show(comando, color,lvlDebug)
			print commands.getoutput(comando)	
			for i in nombresLigandos:
				if i.find("VS") !=-1 or i.find ("BD")!=-1: 
					i=i[:i.rindex(extTemporal)] 
					salidaPack=self.ruteBestScore+i+"Pack.png"
					imagenA=self.ruteBestScore+i+"E.png"
					imagenB=self.ruteBestScore+i+".png"
					imagenC=self.ruteBestScore+i+"EA.png"
					comando="convert "+imagenB +" -gravity South  -chop  0x40 "+imagenB 
					self.debug.show(comando, color,lvlDebug)
					commands.getoutput(comando)	
					comando="montage -mode concatenate -tile 2x2 -geometry 800x600 "+imagenA + " "+imagenB+" "+imagenC+ " "+salidaPack	
					self.debug.show(comando, color,lvlDebug)
					commands.getoutput(comando)	

