import commands
import os, string

#matplotlib
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import numpy as np
from  Get_histogram.debug import Debug
from  Get_histogram.debug import bcolors
lvlDebug=10					#lvl estandar para el debug
color=bcolors.GREEN 		#color standar para el debug
class GenerateFilePymol():
	
	def __init__(self, fileProteina, nombresLigandos, fileSalida, fileEntrada, allFich,ruteBestScore,opcion, fileLigando, clusterizado, engCorte, programa,datos,modeDebug):
		self.fileProteina=fileProteina
		self.nombresLigandos=nombresLigandos
		self.fileSalida=fileSalida
		self.fileEntrada=fileEntrada
		self.allFich=allFich
		self.opcion=opcion
		self.ruteBestScore=ruteBestScore
		self.fileLigando=fileLigando
		self.visualizacionProteina="surface" #sirve para el modo wega ponerlo en sticks y en el resto en surface
		self.clusterizado=clusterizado
		self.engCorte=engCorte
		self.programa=programa
		self.datos=datos
		self.debug=Debug(modeDebug)

	###_____________________________________________________________________________________________________
	###
	###		Crea un fichero pml con los datos de la proteina los mejores ligandos para pymol
	###______________________________________________________________________________________________________
	def crearFicheroPmlPymol(self):
		
		prt=os.path.split(self.fileProteina)
		aux=prt[1];
		proteinasinExtension=aux[:aux.rindex(".")] 	#nombre de la proteina sin extension para cargar en pymol
		f=open(self.fileSalida+".pml","a")
		f.write ( "load "+prt[1]+" \n"		)
		f.write ( "cmd.hide(\"everything\",\"" + proteinasinExtension + "\")\n")
		f.write ( "cmd.show(\""+self.visualizacionProteina+ "\" ,\"" +proteinasinExtension+"\")\n")
		f.write ("cmd.color(3,\""+proteinasinExtension+"\")\n")
		#if self.clusterizado=="no":
		for i in self.nombresLigandos:
			f.write ("load "+ i +"\n")
			f.write ("cmd.show(\"sticks\" ,\"" + i +" \")\n")
		f.write ("save session.pse\n")
		f.close()
	def find(self,f, seq):
		for item in seq:
			if item ==  f: 
				return True

	###____________________________________________________________________
	###
	###		Si es BD se crea el fichero de bolas
	###______________________________________________________________________________________
	#####cat BD-AD-3ua0-curcumin/*.txt |grep -v "#" |sort -r -t',' -k 1,1g |awk -F"[, ]" '!a[$5]++'
	###find BDVS-AD-3DGA-ligThomas -name "*.txt" |xargs cat | grep -v "#" |sort -r -t',' -k 1,1g |awk -F"[, ]" '!a[$5]++'

	def crearBolasProt(self,extTemporal):
		
		if self.opcion == "BD" or self.opcion =="BDVS" or self.opcion=="BDC":
			comando="find "+self.fileEntrada+" -name \"*.txt\" |xargs -P 5 cat | grep -v \"#\" | awk \' $1<="+str(self.engCorte)+"\'  |sort -r -t',' -k 1,1g  |uniq >"+self.fileEntrada+"ficheroGenerarBola"
			self.debug.show(comando, color,lvlDebug)
			commands.getoutput(comando)

			
			###_________________________________________________________________________________________________________________
			###
			###Cuando es BDVS se genera un fichero con los top de los ligandos que sean en el caso de thomas 37
			###_________________________________________________________________________________________________________________
			if self.opcion=="BDVS":
				self.debug.show("Opcion BDVS", color,lvlDebug)
				#se genera el fichero con los 10 mejores ligandaos para esa protena 
				comando="find "+self.fileEntrada+" -name \"*.txt\" |xargs -P 5 cat | grep -v \"#\" | awk \' $1<="+str(self.engCorte)+"\'  |sort -r -t',' -k 1,1g  |awk -F\"[, ]\" '!a[$5]++' | sort -k 5 -V>"+self.ruteBestScore+"TopBest.txt"
				self.debug.show(comando, color,lvlDebug)
				commands.getoutput(comando)
			
			if self.clusterizado=="n":
				self.debug.show("Clusterizado No", color,lvlDebug)
				comando="python scriptsDocking/from_xyz_to_pymol.py "+ self.fileProteina+" "+ self.fileLigando+" "+self.allFich[0]+extTemporal +" "+self.fileEntrada+"ficheroGenerarBola "+ self.ruteBestScore +"bolas.pse "+ self.ruteBestScore +"bolas.png >> " + self.fileSalida+".pml"
				self.debug.show(comando, color,lvlDebug)
				commands.getoutput(comando)
				comando="rm "+self.fileEntrada+"ficheroGenerarBola" 
				self.debug.show(comando, color,lvlDebug)
				commands.getoutput(comando) 
			carpeta=""

##__________________________________________________________________________________________________________________________
##
##		Codigo para el clusterizado
##___________________________________________________________________________________________________________________________
			if self.clusterizado=="y":
				self.debug.show("Clusterizado Yes", color,lvlDebug)
				##______________________________________________________________________________________________________
				##
				##	Declaro las varibales a utilizar
				##________________________________________________________________________________________________________
				eng="0" 		#
				cont=0			
				listaLigCluster=[] 		##contiene una lista con los ligandos del cluster
				self.nombresLigandos=[]	##vacio nombreLigandos 
				countCluster=0			##numero de cluster que se hacen
				countLif=0				
				listaLigandos=[];		
				while True:
					firstLig=""			##el ligando princiapl del cluster
					db_file = open(self.fileEntrada+"ficheroGenerarBola","r")
					self.debug.show("Fichero: "+self.fileEntrada+"ficheroGenerarBola", color,lvlDebug)
					lineasBorrar=[];	##lista con las lineas a borrar del fichero
					contador=1;			##contador de lineas del fichero txt que se han de borrar (siempre empieza por el final a eliminar por que si no se descuadraria)
					lig=[]
					countLigMismoCluster=0;			#contador para saber los ligandos que caen el el cluster
					for line in db_file:			## se empieza a leer el fichero de ligandos generado previamente
						ll=string.split(line)		
						
						###____________________________________________________________________________________________________________
						###
						###		Se guarda el primer ligando (El del clusterizadp)
						###____________________________________________________________________________________________________________

						if firstLig=="":
							countLigMismoCluster+=1
							listaLigCluster=[]
							eng=ll[0]				#eng Ligand
							x=ll[1]					#xLigand
							y=ll[2]					#yligand
							z=ll[3]					#zLigand
							ejecucion=ll[5]			#num Ejecucion Ligand
							comando="ls "+self.fileEntrada+ejecucion+"* |grep  \\\\\""+ extTemporal+"\""	
							self.debug.show(comando, color,lvlDebug)			
							firstLig=commands.getoutput(comando)
							comando="scriptsGeneracionResultados/modMurcia/modMurcia "+ firstLig +"|grep -i volume |awk '{print $2 }' "
							#print comando
							self.debug.show(comando, color,lvlDebug)
							volumen=commands.getoutput(comando)
							#################################
							carpeta=firstLig+"-Dir"
							countCluster+=1
							if self.programa == "AD": 
								ficheros=firstLig[:firstLig.rindex(extTemporal)]
								self.debug.show("Prg BD-AD fichero: "+firstLig, color,lvlDebug)
								if self.programa == "BDVS":
									ligAdd=firstLig[:firstLig.rindex(".pdbqt")]
									ligAdd=ligAdd[:ligAdd.rindex(".pdbqt")]
									ligAdd=ligAdd[ligAdd.index("/"):]
									ligAdd=ligAdd[ligAdd.index("-"):]
									listaLigCluster.append(ligAdd)
									self.debug.show("Prg BDVS-AD fichero: "+ligAdd, color,lvlDebug)	
							elif self.programa=="LF":
								ficheros=firstLig[:firstLig.rindex(".pdb")]
								self.debug.show("Prg BD-LF fichero: "+firstLig, color,lvlDebug)
							elif  self.programa=="FB":
								ficheros=firstLig[:firstLig.rindex(".mol2")]
								self.debug.show("Prg BD-FB fichero: "+firstLig, color,lvlDebug)
							#print "cp "+ficheros +"* "+self.ruteBestScore
							comando="cp "+ficheros +"* "+self.ruteBestScore #+"/"+str(unichr(countLetra))+str(countLif)
							self.debug.show(comando, color,lvlDebug)
							
							commands.getoutput(comando)
							###################################################
							lineasBorrar.append(1)
							lig.append(ejecucion)
							
						else:
							engAux=ll[0]
							xAux=ll[1]
							yAux=ll[2]
							zAux=ll[3]
							ejecucionAux=ll[5]
						
							comandoAux="ls "+self.fileEntrada+ejecucionAux+"* |grep   \\\\\""+ extTemporal+"\""
							self.debug.show(comando, color,lvlDebug)
							firstLigAux=commands.getoutput(comandoAux)

							if self.programa == "AD": 
								ficheros=firstLigAux[:firstLigAux.rindex(extTemporal)]
								self.debug.show("Prog BD-AD: "+ficheros, color,lvlDebug)
								if self.programa == "BDVS":  
									ligAdd=firstLigAux[:firstLigAux.rindex(".pdbqt")]
									ligAdd=ligAdd[:ligAdd.rindex(".pdbqt")]
									ligAdd=ligAdd[ligAdd.index("/"):]
									ligAdd=ligAdd[ligAdd.index("-"):]
									self.debug.show("Prog BDVS-AD: "+ligAdd, color,lvlDebug)
									if  not self.find(ligAdd,listaLigCluster):
										listaLigCluster.append(ligAdd)
										comando ="cp "+ficheros +"* "+self.ruteBestScore #+"/"+str(unichr(countLetra))+str(countLif)
										commands.getoutput(comando)
										self.debug.show(comando, color,lvlDebug)
							elif self.programa=="LF":
								ficheros=firstLig[:firstLig.rindex(".pdb")]
								self.debug.show("Prog BDVS-LF: "+firstLig, color,lvlDebug)

							
							comandoAux="scriptsGeneracionResultados/modMurcia/modMurcia "+ firstLigAux +"|grep -i volume |awk '{print $2 }' "
							self.debug.show(comandoAux, color,lvlDebug)
							volumenAux=commands.getoutput(comandoAux)
							self.debug.show("firstLig: "+ firstLig +" secLig "+firstLigAux, color,lvlDebug)
							firstLig=firstLig.replace("\n", '')
							firstLigAux=firstLigAux.replace("\n", '')
							comandoAuxMerged="scriptsGeneracionResultados/modMurcia/modMurcia "+ firstLigAux +" "+ firstLig+"|grep -i volume |awk '{print $2 }' "
							self.debug.show(comandoAuxMerged, color,lvlDebug)
							volumenAuxMerged=commands.getoutput(comandoAuxMerged)	
							self.debug.show("Volumen: "+volumen +"\t VolumenAux "+volumenAux, color,lvlDebug)
							suma=float(volumen)+float(volumenAux)
							self.debug.show("Sum Volumen: "+str(suma) , color,lvlDebug)
							if (float(volumenAuxMerged)) < (float(suma)-0.1):
								###########
								###countLif+=1
								###comando ="cp "+firstLigAux +" "+carpeta+"/"+str(unichr(countLetra))+str(countLif)
								#print comando
								###commands.getoutput(comando)
								###############
								lineasBorrar.append(contador)
								lig.append(ejecucionAux)
								countLigMismoCluster+=1
								#lineasBK.append(str(contador) + " A "+volumen +"  B "+volumenAux +" Juntos "+volumenAuxMerged +"  Suma A+B "+str(suma ) + x+" "+y+" "+z +" xAux "+xAux+" "+yAux+" "+zAux)
							#	print str(contador) + " A "+volumen +"  B "+volumenAux +" Juntos "+volumenAuxMerged +"  Suma A+B "+str(suma )
						contador+=1				
					db_file.close()
				
					##Se empiezan a borrar lineas desde el final del fichero para no descuadralo
					for i in reversed(lineasBorrar):
						#print "borro linea "+str(i)
						comando=("sed "+"\'"+str(i)+"d\' "+self.fileEntrada+"ficheroGenerarBola -i")
						self.debug.show(comando , color,lvlDebug)
						commands.getoutput(comando)
					#if(len(eng)==0): 
					#	comando="echo pseudoatom Cluster"+str(countCluster)+"en, pos=["+x+", "+y+", "+z+"], b="+eng +">> " + self.fileSalida+".pml"
					#else:
						#si es BDVS se indican los ligandos diferentes que han caido en el pockect (lig1, lig2, lig9...)
						if len(listaLigCluster) > 0:
							comando="echo pseudoatom C"+str(countCluster)+"E"+eng+"N-"+str(len(listaLigCluster))+", pos=["+x+", "+y+", "+z+"], b="+eng +">> " + self.fileSalida+".pml"
						else:
							comando="echo pseudoatom C"+str(countCluster)+"E"+eng+", pos=["+x+", "+y+", "+z+"], b="+eng +">> " + self.fileSalida+".pml"
					self.debug.show(comando , color,lvlDebug)
					commands.getoutput(comando)

					###guarda ligandos encontrados en el cluster con el cluster de la energia 
					comando= "echo "+str(eng)+":"+str(countLigMismoCluster)+">>"+ self.fileSalida+".clt"
					self.debug.show(comando , color,lvlDebug)
					commands.getoutput(comando)
					###_________________________________________________________________________

					if len(listaLigCluster) > 0:
						comando="echo cmd.show\(\"\\\"spheres\"\\\",\"\\\"C"+str(countCluster)+"E"+eng+"N"+str(len(listaLigCluster))+"\"\\\"\)" +">> " + self.fileSalida+".pml"
					else:
						comando="echo cmd.show\(\"\\\"spheres\"\\\",\"\\\"C"+str(countCluster)+"E"+eng+"\"\\\"\)" +">> " + self.fileSalida+".pml"
					self.debug.show(comando , color,lvlDebug)
					commands.getoutput(comando)
					 
					##esta linea la comento para BD no se si sera un fallo
					self.debug.show("ADD in nombresLigandos "+firstLig[firstLig.rindex("/")+1:] , color,lvlDebug)
					self.nombresLigandos.append(firstLig[firstLig.rindex("/")+1:])
										
					cadena=""	
					for i in lig:
						cadena+=i+" "
					cont+=1;
					self.debug.show("Ligadndos en el cluster: "+str(countLigMismoCluster) , color,5)
					self.debug.show("contador ligandos cluster: "+str(cont) , color,5)
					comando="echo "+cadena+ ">> " + self.fileSalida+".pml"
					self.debug.show(comando , color,lvlDebug)
					commands.getoutput(comando)
					comando="cat "+self.fileEntrada+"ficheroGenerarBola |wc -l"
					self.debug.show(comando , color,lvlDebug)
					fin=commands.getoutput(comando)
					self.debug.show("Lineas que faltan para terminar Clusterizado "+ fin,bcolors.FAIL,5)

					if int(fin)==0:
						break	
				comando="echo pseudoatom Cluster"+str(countCluster)+", pos=[999, 999, 999], b=0>> " + self.fileSalida+".pml"
				self.debug.show(comando , color,lvlDebug)
				commands.getoutput(comando)	
				comando="echo spectrum b"+ ">> " + self.fileSalida+".pml"
				self.debug.show(comando , color,lvlDebug)
				commands.getoutput(comando)
				f=open(self.fileSalida+".pml","a")
				for i in listaLigandos:
					f.write ("load "+ i +"\n")
					f.write ("cmd.show(\"sticks\" ,\"" + i +" \")\n")
				f.close()
				##grafica de clusterizado
			
				self.fusionGraficasEnCl()

	

 	def fusionGraficasEnCl(self):		
	
		plt.clf()   # Clear figure #expendiente X no se por que tengo que limpiar la figura...
		clusters=[] ##leer
		fig = plt.figure()
		ax = fig.add_subplot(111)
		
		infile=open(self.fileSalida+".clt", 'r')
		for line in infile:
			line=line[:-1]
			p=line.split(':');
			
			for i in range(int(p[1])):
				clusters.append(float (p[0]))
			#clusters[round(float (p[0]),2)] = float(p[1])
		infile.close()
		ax.set_xlim(min(self.datos)-0.5, max(self.datos)+0.5)
		plt.hist(clusters,bins=50,alpha=1,label='Group 1', facecolor='blue')
		plt.hist(self.datos,bins=50,alpha=0.5, label='Group 2',facecolor='red')
		ax.plot([0,1,2],[10,10,100],marker='o',linestyle='-')
		ax.set_yscale('symlog') 
		ax.set_xlabel('Scoring Function')
		ax.set_ylabel('Number of occurrences')
		##eliminar ticks
		plt.tick_params(
	    	axis='x',         	# changes apply to the x-axis
    		bottom='on',      	# ticks along the bottom edge are off
	    	top='off')        	# ticks along the top edge are off
		plt.tick_params(
    		axis='y',          	# changes apply to the x-axis
	    	which='both',      	# both major and minor ticks are affected
	    	left='on',      	# ticks along the bottom edge are off
	    	right='off')        # ticks along the top edge are off
    		plt.savefig(self.fileSalida+"MergedEC.png") 
	    	self.debug.show("save GraphMerded:" +self.fileSalida+"MergedEC.png", color,lvlDebug)
    	
		#plt.show()



