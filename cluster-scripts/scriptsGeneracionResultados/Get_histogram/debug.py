class bcolors:
    HEADER = '\033[95m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
class Debug():

	def __init__(self, modeDebug):
		self.modeDebug=modeDebug
	
	def show(self, texto, color, num):
		if int(self.modeDebug)>int(num):
			t=texto.split("\n")
			for i in t:
				print bcolors.WARNING + "DEBUG: "+color+i  + bcolors.ENDC
			


