#!/bin/sh
#PBS -A is2014-NILSBIO
#PBS -N lstSimilitud.sh
#PBS -e salida/$PBS_JOBID.err 
#PBS -o salida/$PBS_JOBID.out
#PBS -l walltime=10:30:00
dir="VS-WG-4nxs_2OZ-6_p0.0-9-sd-0-0-0/"
#dir="VS-WG-GAL-6_p0.0-9-0-0-0/"
#dir=$1
path=${PWD}"/pruebasDocking/docking/"
module load python
python $path"scriptsGeneracionResultados/lstSimlilutd.py" $path $dir




	######COLA QSUB
	#### declare a name for this job to be sample_jobqsu
	####PBS -N my_serial_job  # request the queue (enter the possible names, if omitted, serial is the default)
	####PBS -q serial     # request 1 node
	####PBS -l nodes=1 # request 4 hours and 30 minutes of cpu time
	####PBS -l cput=04:30:00     # request 4 hours and 30 minutes of cpu 
   # mail is sent to you when the job starts and when it terminates or aborts
	####PBS -m bea # specify your email address
	#####PBS -M John.Smith@dartmouth.edu # By default, PBS scripts execute in your home directory, not the # directory from which they were submitted. The following line # places you in the directory from which the job was submitted.  
		####PBS -o salida/$JOB_ID.out
		#####PBS -e salida/$JOB_ID.err
	#######PBS -A is2014-NILSBIO
	###PBS -e salida/my.err 
	###PBS -o salida/my.out
