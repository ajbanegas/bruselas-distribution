###																														###
###					Genera graficas de LF para y Poseview de los resultados que se encuentren en una carpeta			###	
###																														###
###_____________________________________________________________________________________________________________________###

import matplotlib.patches as patches
import matplotlib.path as path
import os,sys,string,re
from collections import OrderedDict
import matplotlib
matplotlib.use("Agg")
from pylab import *
from os import listdir
import commands
import os,sys,string,re
import matplotlib.path as path
import os.path



###__________________________________________________________________________________________________
###
###	Descarga El fichero de la proteina para PoseView
###__________________________________________________________________________________________________

urlPdbs="http://www.rcsb.org/pdb/files/"
#descarga de proteina
def descargarPdbProteina():
	global protOrig
	#quito la extension a la proteina y la copio al fichero tmp de ligPluis
	#prt=os.path.split(fileProteina)
	#aux=prt[1];
	#protOrig=aux[:aux.rindex(".")] 	
	#protOrig=protOrig[0:4]
	comando="wget "+urlPdbs+protOrig+".pdb"
	commands.getoutput(comando)		
	#se mueve a la carpeta temporal del script ligplus
	comando="mv "+protOrig+".pdb scriptsGeneracionResultados/scriptPoseView/tmp/"
	commands.getoutput(comando)	
###__________________________________________________________________________________________________________________
###
###		Genera El mapa de interaccion proteina ligando OK
###____________________________________________________________________________________________________________________
def poseView(fichLig, extTemporal):
	
	#if extTemporal != ".mol2":
	#	comando="babel "+rutaMejorScore+fichLig+extTemporal +" "+rutaMejorScore+fichLig+".mol2" 
	#	commands.getoutput(comando)	
	comando="python scriptsGeneracionResultados/scriptPoseView/mapa2d.py scriptsGeneracionResultados/scriptPoseView/tmp/"+protOrig+".pdb  "+fichLig+".mol2"
	#print comando
	print commands.getoutput(comando)	
###__________________________________________________________________________________________________________________
###
###		Genera Graficas de Energia Individuales
###____________________________________________________________________________________________________________________
def graficaEnergiaIndividual(fichlig):
	if programa == "AD":
		comando="python scriptsGeneracionResultados/graficasEnergia/scriptGeneracionGraficaVina.py "+fichlig
		#print comando
		print commands.getoutput(comando)
	if programa== "LF":
		#print fileEntrada
		comando="python scriptsGeneracionResultados/graficasEnergia/scriptGeneracionGraficaLF.py "+fichlig
		#print comando
		print (commands.getoutput(comando)	)
		comando="python scriptsGeneracionResultados/graficasEnergia/scriptGeneracionGraficaAtomoLf.py "+fichlig
		#print comando
		print commands.getoutput(comando)	
	
###___________________________________________________________________________________________________________________
###
###						Main
###___________________________________________________________________________________________________________________
if len(sys.argv) != 3:
	print "Indique el nombre de la carpeta con los ficheros originales y proteina(.pdb)"
	exit();
fileEntrada = sys.argv[1] #fichero donde se encuentrn los ficheros txt para graficrlos
proteina= sys.argv[2]


comando="module load openbabel"
commands.getoutput(comando)	
comando=" module load imagemagick"
commands.getoutput(comando)	

###_________________________________________________________________________________________________________________
###
###		Recoger datos de entrada, son ficheros txt con un formato determinado
###__________________________________________________________________________________________________________________
extension=proteina[proteina.rindex("."):]							##quito la extension	(fichero del ligando sin extension)
proteina=proteina[proteina.rindex("/")+1:]
proteina=proteina[:proteina.rindex(".")]							##quito la extension	(fichero del ligando sin extension)
#protOrig=proteina[:4]
protOrig=proteina

CWD= os.getcwd()
#descargarPdbProteina()
for cosa in listdir(fileEntrada):
	#print cosa
	if extension in cosa:
		extLig=cosa[cosa.rindex("."):]	
		ligando=cosa[:cosa.rindex(".")]
		if ligando.find("BD")!=-1 or ligando.find("VS")!=-1:
			if extLig!= ".mol2" :
				comando="babel "+CWD+"/"+fileEntrada+ligando+extLig +" "+CWD+"/"+fileEntrada+ligando+".mol2"
				commands.getoutput(comando)	
			#
			#	Si los docking no cumplen la generalizacion de nombre esto fallara
			#
			tmpCadena=ligando[ligando.index("-")+1:]  #subcadena para obtener el programa	
			programa=tmpCadena[:tmpCadena.index("-")]		  #obtiene el rpograma (se repite siempre :S
			poseView(CWD+"/"+fileEntrada+ligando,"null")
			graficaEnergiaIndividual(CWD+"/"+fileEntrada+ligando)
			#exit()
	
	












	
	




	
	
	 



