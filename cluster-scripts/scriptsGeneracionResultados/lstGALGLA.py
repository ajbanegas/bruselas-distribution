import matplotlib.patches as patches
import matplotlib.path as path
import os,sys,string,re
from collections import OrderedDict
import matplotlib
matplotlib.use("Agg")
from pylab import *
from os import listdir
import commands
import os,sys,string,re
import matplotlib.path as path
import os.path
mejorScore={}  #se guardan los mejor score del directorio fileA
peorScore={}
RESULTADOSFICHEROS =100 #numero de mejores resultados que se guardaran en ficheros




def guardarMejoreScore(energia, nombre): 

	global mejorScore
	nombre=nombre[:nombre.rindex(".")] 	 
	#mejorScore.update({nombre:energia}) #otra foram
	mejorScore[nombre] = energia 
	mejorScore = OrderedDict(sorted(mejorScore.items(), key=lambda x: x[1]))
	count=0
	for k, v in mejorScore.items():
		if count >= RESULTADOSFICHEROS:
			mejorScore.pop(k)
		count= count+1;
	

def guardarPeorScore(energia, nombre): 

	global peorScore
	#mejorScore.update({nombre:energia}) #otra foram
	nombre=nombre[:nombre.rindex(".")] 
	peorScore[nombre] = energia 
#	peorScore = OrderedDict(sorted(mejorScore.items(), key=lambda x: x[1]))
#	count=0
#	for k, v in mejorScore.items():
#		if count >= RESULTADOSFICHEROS:
#			mejorScore.pop(k)
#		count= count+1;


if len(sys.argv) != 4:
	print "Indique el nombre de la carpeta con los .txt fichero de los ligandos originales y el fichero de proteina y la energia de diferencia"
	exit();
fileA = sys.argv[1] #fichero donde se encuentrn los ficheros txt para graficrlos
fileB = sys.argv[2] #el ligando
rangoEnergia=float(sys.argv[3]) #el ligando

##si no existe la ruta se crea
tmpCadena=fileA[fileA.index("-")+1:]  #subcadena para obtener el programa
programa=tmpCadena[:tmpCadena.index("-")]		 #subcadena para obtener el programa
opcion=fileA[:fileA.index("-")]## variable que define el programa segun el nombre 
for cosa in listdir(fileA):
	if cosa.find(".txt") >= 0:	
		f = open(fileA+cosa)
		cords=f.read().split(" ")
		if cords[0] != "" and cords[0] != "nan" : ##nan por si ha fallado leadfinder 	
			#if float(cords[0]) > -1000 and float(cords[0]) < 0 : #filtro resultados solo desde -100 hasta 0				
#				data .append(float (cords[0]))
				if opcion == "VS":
					guardarMejoreScore(float (cords[0]), cords[4]) #
				else:
					guardarMejoreScore(float (cords[0]), cords[5]+cords[4]) #
					
		f.close()
for cosa in listdir(fileB):
	if cosa.find(".txt") >= 0:	
		f = open(fileB+cosa)
		cords=f.read().split(" ")
		if cords[0] != "" and cords[0] != "nan" : ##nan por si ha fallado leadfinder 	
			#if float(cords[0]) > -1000 and float(cords[0]) < 0 : #filtro resultados solo desde -100 hasta 0		
#				data .append(float (cords[0]))
				if opcion == "VS":
					guardarPeorScore(float (cords[0]), cords[4]) #
				else:
					guardarPeorScore(float (cords[0]), cords[5]+cords[4]) #
					
		f.close()

#a1=nombre ligando sin extension
#a2=energia del ligando
#b1=nombre ligando sin extension
#b2=energia del ligando
for a1,a2 in mejorScore.items():    
	if opcion == "VS":
		auxA1=a1[a1.rindex("_")+1:] 	# me queod solo con el numero del ligando puesto que son ligandos diferentes
	for b1,b2 in peorScore.items():
		auxB1=b1[b1.rindex("_")+1:] 	# me queod solo con el numero del ligando puesto que son ligandos diferentes
		if  auxA1 == auxB1 :
			resultado=(abs(float(a2))-abs(float(b2))) 
			if resultado >= rangoEnergia:
				print str(a1)  +"    "+ str(a2) + "   "+ str(b1)  +"    "+ str(b2)
#


