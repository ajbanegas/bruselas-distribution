#!/bin/bash

cadena=A
model=1
contLinLig=0;
numberLigand=999;

echo "$# parameters";
if [ $# != 2 ]; then
	echo "Debe introducir el fichero pdb y el fichero de ligando generado por docking"
	exit
fi

#Lectura ligando 
linea=$fila
echo $2
while read lin;
do
#	echo $lin
	if [ "${lin:0:3}" == "END" ];then 
		model=`expr $model + 1` #termina el modelo
		break
	fi
	#leo cada fila del ligando y la formateo para pegarla al final del pdb
	if [ $model -eq 1 ];then
		lin=${lin/${lin:17:3}/PUP} #remplazo en la poscion 17+3 s
		linAux=${lin:0:21}$cadena" " #añado la cadena
		lin=$linAux$numberLigand${lin:26}	
		lin="${lin/ATOM  /HETATM}" #sustituyo los atmos por	
		linea=$linea$lin"\n"
		echo $linea
		contLinLig=`expr $contLinLig + 1`
	fi
	if [ "${lin:0:5}" == "MODEL" ];then 
		model=`expr $model + 1` #empieza un modelo
	fi
done < $2

#linea final del pdb -1
numLig=`cat $1 |wc -l`
numLig=`expr $numLig - 1`
#inserto el ligandoa
echo $numLig 
echo $numLinea
#sed $numLig"a $linea" $1 > $1.TMP

###ligplot
./hbadd $1.TMP components.cif -wkdir tmp/
./hbplus -L -f tmp/hbplus.rc -h 2.90 -d 3.60 -N $1.TMP -wkdir tmp/
./hbplus -L -f tmp/hbplus.rc -h 2.70 -d 3.35 $1.TMP -wkdir tmp/
#999 es un numero que le he indicado en la lectrura del ligando
./ligplot $1.TMP  $numberLigand  $numberLigand  $cadena -wkdir tmp/ -prm ligplot.prm -ctype 1 
#rm $1.TMP





#countN=1
#countH=1
#countO=1
#countS=1
#countC=1
# la cadena exsitente por PUP
		#if [ "${lin:13:1}" == "N " ];then
		#	lin=${lin/${lin:13:1}/N$countN}
		#	countN=`expr $countN + 1`
		#elif [ "${lin:13:1}" == "H " ];then		
		#	lin=${lin/${lin:13:1}/H$countH}
		#	countH=`expr $countH + 1`
		#elif [ "${lin:13:1}" == "O " ];then		
		#	lin=${lin/${lin:13:1}/O$countO}
		#	countO=`expr $countO + 1`
		#elif [ "${lin:13:1}" == "S " ];then		
		#	lin=${lin/${lin:13:1}/S$countS}
		#	countS=`expr $countS + 1`
		#elif [ "${lin:13:1}" == "C " ];then		
		#	lin=${lin/${lin:13:1}/$countC}
		#	countC=`expr $countC + 1`
		#fis
		#echo ${lin:13:2}
	 
		#lin="${lin/ 1    /$cadena $numberLigand}"
		#lin="${lin/999/999    }"
		#echo ${lin:5:50}
		#echo $lin

#echo $numUltHet
############################################

###añado la fila en RET para un nuevo ligando
#numUltHet=`cat 3s5z.pdb |grep -nw ^HET |tail -1|awk  -F ':' '{print $1}'`
#fila="HET    PUP  "$cadena" 999       "$contLinLig
#sed $numUltHet"a $fila" $1 > $1.Tmp

#######
#numero de fila del ultimo conect
#numLig=0
#numLig=`cat $1.Tmp |grep -n CONECT |head -1 |awk  -F ':' '{print $1}'`

########Leo fichero del ligando
#if [ -z $numLig ];then
#	numLig=`cat $1.Tmp |wc -l`
#fi
