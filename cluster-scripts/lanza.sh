#!/bin/sh
#SBATCH --output=/home/hperez/antonio/dragon.out
#SBATCH --error=/home/hperez/antonio/dragon.err
#SBATCH -J DRAGONKEGG
#SBATCH --time=30:30:00
#SBATCH --cpus=1
#SBATCH --nice=10000

#/home/hperez/antonio/mydragon.sh $1
#/home/hperez/antonio/scriptsWeb/711-lisica-alignment.py /home/hperez/antonio/VS-LI-711-lig_mol2-0-0-0//711.mol2 /home/hperez/antonio/VS-LI-711-lig_mol2-0-0-0//VS-LI-CHEMBL100164_8.mol2 /home/hperez/antonio/VS-LI-711-lig_mol2-0-0-0//VS-LI-CHEMBL100164_8-aligned.mol2
cd /home/hperez/antonio/scriptsDocking/externalSw/dragon6
./dragon6shell -ig drg_license.txt -s adeno_1382203096.drs
