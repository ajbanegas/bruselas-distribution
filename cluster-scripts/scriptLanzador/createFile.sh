################################################################################################
#	Obtien la ruta donde se encuentra y crea un directorio (si no existiera)
#	con la nomenclatura [VS|BD][LF|AD|FB][nombreProteina][ligando]
#  Si se le indica con -d a lanzador el directorio, este no se creara
###########################################################################################################
if [ -z $directorio ];then 														##si no se ha introducido directorio
	if [ $opcion == "VS" ] || [ $opcion == "SM" ];then	
		directorio=${PWD}/$opcion"-"$programa"-"$nomProteina-$nomLigando-$x-$y-$z/ 	##ruta para el directorio
		#localitation="VirtualScreening/"$nomProteina"/"$opcion"-"$programa"-"$nomProteina-$tmp-$x-$y-$z/  #se usaba para subir al excell
	fi
	if [ $opcion == "BD" ] || [ $opcion == "BDVS" ]  || [ $opcion == "BDC" ] || [ $opcion == "SD" ] || [ $opcion == "QT" ];then	
		directorio=${PWD}/$opcion"-"$programa"-"$nomProteina-$nomLigando/ 		##ruta directorio
		#localitation="BlindingDocking/"$nomProteina"/"$opcion"-"$programa"-"$nomProteina-$nomLigando #se usaba para subir al excell
	fi
else
	directorio=${PWD}/$directorio
fi
#directorio donde se guardan los resultados
if [ ! -d $directorio ];then
	mkdir $directorio
fi


























#tmp=$nomLigando
		#lastChar=$(expr substr $nomLigando $(expr length $nomLigando) 1)
		##si el usuario introduce la ruta sin la /
		#if [ $lastChar == "/" ];then
		#	tmp=`echo $nomLigando | sed 's/.$//g'` 
		#fi
#if [ $opcion == "BDVS" ];then	
	#	dir=${PWD}/$opcion"-"$programa"-"$nomLigando ##ruta directorio
	#	localitation="BlindingDocking/"$nomProteina"/"$opcion"-"$programa"-"$nomProteina-$nomLigando ##ruta directorio
	#fi
	#if [ $opcion == "SM" ];then	
		#tmp=$nomLigando
		#lastChar=$(expr substr $nomLigando $(expr length $nomLigando) 1)
		##si el usuario introduce la ruta sin la /
		#if [ $lastChar == "/" ];then
		#	tmp=`echo $nomLigando | sed 's/.$//g'` 
		#fi
		#if [ $programa == "LS" ];then
		#	nomProteina=${nomProteina%".pmz"}
		#	nomProteina=${nomProteina%".pml"}
		#fi
	#	directorio=${PWD}/$opcion"-"$programa"-"$nomProteina-$tmp-$x-$y-$z/ ##ruta para el directorio
	#fi