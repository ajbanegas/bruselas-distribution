###___________________________________________________________________________________________________
###
###	Funciones para lanzar jobs, se usan en BD o VS o cualquier opcion, normalmente duncionEnviarJobb para un solo job como BD
###_____________________________________________________________________________________________________
jobsIDs="" #variable para guardar los idJobs
comandDependenci=""
#____________________________________________________________________________________________________________________________________
#
#	Bucle que envia los jobs por paquetes
#_________________________________________________________________________________________________________________________________
funcionEnviarJobs()
{
	###bucle para enviar jobs
	SALIR=0
	final=""
	contIni=0
	resto=`expr $numFicheros \% $numPorJobs`		 #resto por si sobra alguno se puede lanzar un job mas
	if [ $numFicheros -lt $numPorJobs ];then
		SALIR=1;
		resto=`expr $numFicheros \% $numPorJobs`		 #resto por si sobra alguno se puede lanzar un job mas
	else
		nRuns=`expr $numFicheros \/ $numPorJobs` 		#nRuns son los numeros de ejecuciones que tendra un job
		contFin=`expr $contIni \+ $nRuns`
		numPorJobs=$nRuns
	fi 
	while [  $SALIR == 0 ]
	do	
		funcionJob
		contIni=$contFin
		contFin=`expr $contFin + $numPorJobs`
		if [ `expr $contFin + $resto`  -gt `expr $numFicheros` ]; then #+1 Ojo
	  		 SALIR=1
		fi	
	done
	if [ $resto -ne 0 ];then 
		
		contFin=`expr $contIni + $resto`
		funcionJob
	fi
	if [ "$histograms" != "N/A" ]; then
		funcionGetHistogram
		${comandDependenci}${jobsIDs}  $directorio/templateGetHystogram.sh
	fi

	# se ejecutan las tareas finales cuando todos los jobs han terminado
	sbatch --time=1440 --output=/dev/null --error=/dev/null --depend=afterok${jobsIDs} ${CWD}scriptsWeb/BRU_notify.sh -d ${directorio} -np ${nomProteina} -s ${programa} -cwd ${CWD}
}

#
#	Crea una plantilla que tendra dependencias de los jobs enviados
#
funcionGetHistogram()
{
	echo "#!/bin/sh" >	$directorio/templateGetHystogram.sh #esto hayq uqe hacerlo dinamico (no tengo tiempo)
	if [ $comando == squeue ];then #dependiendo del cluster

		echo "#SBATCH --output=$directorio/out/a.out">>$directorio/templateGetHystogram.sh
		echo "#SBATCH --error=$directorio/out/a.err	">>$directorio/templateGetHystogram.sh
		echo "#SBATCH -J $job-g_h" >>$directorio/templateGetHystogram.sh
		echo "#SBATCH --time=5:00:00" >>$directorio/templateGetHystogram.sh
		echo "#SBATCH --cpus=1" >>$directorio/templateGetHystogram.sh
		echo "#SBATCH --nice=10000" >>$directorio/templateGetHystogram.sh
		comandDependenci="sbatch --depend=afterok" # se repites siempre pero es por no hacer otro if
	else

		echo "#PBS -o /home/users/macromol/fgb/horacio/helena/docking/BD-AD-ABCG2_homology_qingcheng-ko143//out/0-45.out ">>$directorio/templateGetHystogram.sh
		echo "#PBS -e /home/users/macromol/fgb/horacio/helena/docking/BD-AD-ABCG2_homology_qingcheng-ko143//out/0-45.err ">>$directorio/templateGetHystogram.sh
		echo "#PBS -N AD-BD-3" >>$directorio/templateGetHystogram.sh
		echo "#PBS -lwalltime=10:30:00">>$directorio/templateGetHystogram.sh
		echo "#PBS -l nodes=1:ppn=1">>$directorio/templateGetHystogram.sh
		comandDependenci="qsub -W depend=afterok"
	fi

	if [ "$opcion" == "VS" ];then
		echo "python ${CWD}scriptsGeneracionResultados/get_histogram_picture.py -i $directorio -p $proteina -l $ligando -c 0 -s y -z n">>$directorio/templateGetHystogram.sh
	elif [ "$opcion" == "BD" ];then
		echo "python ${CWD}scriptsGeneracionResultados/get_histogram_picture.py -i $directorio -p $proteina -l $ligando -c 0 -s y -z y">>$directorio/templateGetHystogram.sh
	fi
}


#
#	Envia un jobs al cluster por paquetitos 
#
funcionJob()
{
	#echo "${CWD}scriptsDocking/lanzadorJobsCola.sh -c $CWD \
	#-o $opcion 	-d $directorio -s $programa \
	#-l $ligando -i $contIni -f $contFin \
	#-p $proteina -x $x -y $y -z $z \
	#-np $nomProteina  -nj $job -g $gestorColas \
	#-n $numPoses -cm $comando -se $secuencial \
	#-fl $flexFile -fx $flex  -project ${project} -sa $contIni-$contFin -na $numAminoacdo -nl ${nomLigando}"
	jobID=""

	if [ "$secuencial" == "N/A" ];then  
	jobID=`${CWD}scriptsDocking/lanzadorJobsCola.sh -c $CWD \
	-o $opcion 	-d $directorio -s $programa \
	-l $ligando -i $contIni -f $contFin \
	-p $proteina -x $x -y $y -z $z \
	-np $nomProteina  -nj $job -g $gestorColas \
	-n $numPoses -cm $comando -se $secuencial \
	-fl $flexFile -fx $flex  -project ${project} -sa $contIni-$contFin -na $numAminoacdo -nl ${nomLigando} \
	-th $umbral -em $destinatario -jd "$jobDescription"`
	else

		${CWD}scriptsDocking/lanzadorJobsCola.sh -c $CWD \
		-o $opcion 	-d $directorio -s $programa \
		-l $ligando -i $contIni -f $contFin \
		-p $proteina -x $x -y $y -z $z \
		-np $nomProteina  -nj $job -g $gestorColas \
		-n $numPoses -cm $comando -se $secuencial \
		-fl $flexFile -fx $flex  -project ${project} -sa $contIni-$contFin -na $numAminoacdo -nl ${nomLigando} \
		-th $umbral -em $destinatario -jd "$jobDescription"
	fi

	if [ $comando == squeue ];then #dependiendo del cluster
		jobID=`echo ${jobID}|awk '{print $4}'`
	else
		jobID=`echo ${jobID} | cut -d "." -f 1` 	
	fi
	echo "JOB: "${jobID}
	jobsIDs=${jobsIDs}:${jobID} 
}


	#echo "${CWD}scriptsDocking/lanzadorJobsCola.sh -c $CWD 
	#-o $opcion 	-d $directorio -s $programa \
	#-l $ligando -i $contIni -f $contFin \
	#-p $proteina -x $x -y $y -z $z \
	#-np $nomProteina  -nj $job -g $gestorColas \
	#-n $numPoses -cm $comando  $final $flex "
#funcionEnviarJobBD()
# {

	#${CWD}scriptsDocking/lanzadorJobsCola.sh -c $CWD \
	#-o $opcion 	-d $directorio -s $programa \
	#-l $ligando -i $i 	-f """"$nomLigando \"""""
	#-p $proteina -x $x -y $y -z $z \
	#-np $nomProteina  -nj $job -g $gestorColas -cm $comando  -n $numPoses \
	#-fl $flexFile -se $secuencial $final -fx $flex \
	#-na $numAminoacdo -ch $chain -project ${project}  -sa $i
	

	#echo "${CWD}scriptsDocking/lanzadorJobsCola.sh -c $CWD \
	#-o $opcion 	-d $directorio -s $programa \
	#-l $ligando -i $i 	-f $nomLigando \
	#-p $proteina -x $x -y $y -z $z \
	#-np $nomProteina  -nj $job -g $gestorColas -cm $comando  -n $numPoses \
	#-fl $flexFile -se $secuencial $final -fx $flex \
	#-na $numAminoacdo -ch $chain "
# }
