########################################################
#	genera un docking mutando los residuos de la proteina pro alanina
#	se le indica la cercania mediante la opcion -j	
#
########################################################

##esto debe desaparecer y juntarse con el codigo de lanzaJob
#________________________________________________________________________________________________________________________________________
#funcionEnviarJobBD()
# {
#				${CWD}scriptsDocking/lanzadorJobsCola.sh -c $CWD \
#				-o $opcion	-d $directorio -s $programa -l $ligando \
#				-i $i -f $nomLigando -p ${finalProt}.pdbqt -x $x -y $y -z $z \
#				 -np ${nomProteina}_${amin}  -nj $job -g $gestorColas -cm $comando \
#				 -n $numPoses -fl $flexFile -se $secuencial $final-fx $flex \
#				 -na $numAminoacdo -ch $chain
#}

##
##_____________________________________________________________________________________________________________________________________-
##

if [ -f proteinas/${nomProteina}.pdb ];then 
	source ${CWD}/scriptLanzador/lanzaJob.sh
	scp proteinas/${nomProteina}.pdb scriptsLanzador:
	chain="A"
	
	###______________________________________________________________________________________________________-
	###
	###	se genera el primer docking con la proteina sin modificar
	###______________________________________________________________________________________________________
	i=0
	amin=0
	a=`basename ${directorio%.*}`/ ##nombre del directorio solo

	finalProt=${a}${i}-${nomProteina}_${amin}
	cp ${CWD}${proteina} ${CWD}${finalProt}.pdb
	${CWD}scriptsDocking/externalSw/mgltools_x86_64Linux2_latest/bin/pythonsh ${CWD}scriptsDocking/externalSw/mgltools_x86_64Linux2_latest/MGLToolsPckgs/AutoDockTools/Utilities24/prepare_receptor4.py -r ${CWD}${finalProt}.pdb -o ${CWD}${finalProt}.pdbqt -A checkhydrogens 
	#parseo variables para enviarlo a lanzaJob
	p=proteina
	np=${nomProteina}
	proteina=${finalProt}.pdbqt
	nomProteina=${nomProteina}_${amin}
	funcionEnviarJobBD
	proteina=$p
	nomProteina=$np
	#scriptsDocking/externalSw/mgltools_x86_64Linux2_latest/MGLToolsPckgs/AutoDockTools/Utilities24/prepare_receptor4.py

	
	##se copia la proteina a scriptsLanzador para poder usar pymol
	#echo "ssh scriptsLanzador python scriptDockingSimilitud.py ${nomProteina}.pdb $x $y $z $chain |sort -n |grep ATOM |uniq -f 1 |awk ' {print $2$1}'" #|sed 's/\" "/\\n/g'` #|tr '\n' ',' ` #|sed 's/.$//g'
	
	cadena=`ssh scriptsLanzador python scriptDockingSimilitud.py ${nomProteina}.pdb $x $y $z $chain $numPorJobs |sort -n |grep ATOM |uniq -f 1 |awk ' {print $2"-"$1}'` #|sed 's/\" "/\\n/g'` #|tr '\n' ',' ` #|sed 's/.$//g'` 
	#echo "ssh scriptsLanzador python scriptDockingSimilitud.py ${nomProteina}.pdb $x $y $z $chain $numPorJobs |sort -n |grep ATOM |uniq -f 1 |awk ' {print $2"-"$1}'"
	#echo $cadena
	###______________________________________________________________________________________________________
	###
	###	comienza a modificar la proteina y hacer sdockings
	###_________________________________________________________________________________________________
	i=1

	for amin in $cadena; do
	
		numAmin=`echo $amin | cut -d \- -f 2`
		amin=`echo $amin | cut -d \- -f 1`$numAmin
		#echo $numAmin
		#echo $amin
		#echo "ssh scriptsLanzador pymol -qc changeAtomProt.py $nomProteina A/${i}/ ALA $extension"
		ssh scriptsLanzador pymol -qc changeAtomProt.py $nomProteina A/${numAmin}/ ALA $extension > /dev/null ##modificacion de la porteina en remoto
		finalProt=${a}${i}-${nomProteina}_${amin} ##se guarda la proteina en la carpeta de SD-AD_...
		scp scriptsLanzador:/home/lanzador/${nomProteina}_R.pdb ${finalProt}.pdb						## se trae la proteina mdificada
		finalProt=${a}${i}-${nomProteina}_${amin} ##se guarda la proteina en la carpeta de SD-AD_...
		#aux=$finalProt #variable auxsiliar para saber donde se encuentra la proteina pdb
		#finalProt=${a}${i}-${nomProteina}_${amin} ##se guarda la proteina en la carpeta de SD-AD_...
		#${CWD}scriptsDocking/MGtoolsBasic/ejecutables/pythonsh $CWD""scriptsDocking/MGtoolsBasic/ejecutables/prepare_receptor4.py -r $CWD""${aux}.pdb -o $CWD${finalProt}.pdbqt -A checkhydrogens ##se convierte a pdbqt	
		${CWD}scriptsDocking/externalSw/mgltools_x86_64Linux2_latest/bin/pythonsh ${CWD}scriptsDocking/externalSw/mgltools_x86_64Linux2_latest/MGLToolsPckgs/AutoDockTools/Utilities24/prepare_receptor4.py  -r $CWD""${finalProt}.pdb -o $CWD${finalProt}.pdbqt -A checkhydrogens
		

		p=proteina
		np=${nomProteina}
		proteina=${finalProt}.pdbqt
		nomProteina=${nomProteina}_${amin}
		funcionEnviarJobBD
		proteina=$p
		nomProteina=$np
		


		i=`expr $i + 1` #num de exp
		#rm ${aux}.pdb
	done
	##al finalizar se elimina las proteinas de scriptLanzador para no ocupar espaciop
	ssh scriptsLanzador rm ${nomProteina}_R.pdb
	ssh scriptsLanzador rm ${nomProteina}.pdb

	#scp scriptsLanzador:/home/lanzador/${nomProteina}${extension}
else
	echo "No existe el fichero de la proteina"
fi




#configuracion automatica para enviar las ejecuciones dentro de jobs
#numFicheros=`ls -d $ligando*$extension |wc -l`	######OJO AÑADIDO -d por LS
#div=`expr $numFicheros \/ $numPorJobs`
#resto=`expr $numFicheros \% $numPorJobs`
#contIni=1
#contFin=`expr $contIni \+ $numPorJobs` 
#salida=$directorio$opcion"-"$programa"-"$nomProteina-$tmp-$x-$y-$z
#$CWD""scriptsDocking/scriptGenerarGrid.sh -s $programa -x $x -y $y -z $z -d $salida -p $proteina -c $CWD -o $opcion
#funcionEnviarJobs
