##_______________________________________________________________________________________
##
##		Normaliza los pdbs, pdbqt y mol2 a formato X:Y:Z:type:nAtom:Chain
##		ejemplo python standarFileCoords.py 3s5z.pdbqt
##________________________________________________________________________________________-
####Solo para un modelo
#!/usr/bin/env python
# -*- coding: utf-8 -*-
from os import listdir
import os,sys,string,re
if len(sys.argv) != 2:
	print "Indique el nombre del fichero de la proteina ()"
	exit();
fileEntrada=sys.argv[1]
#path=os.path.dirname(os.path.abspath(sys.argv[1]))+"/"
filename, file_extension = os.path.splitext(sys.argv[1])
delimiter=":"
"""
      1  O          1.6010   -0.2400    1.1620 O.2     1  LIG1       -0.2830
"""
def fMol2(f):
	count=0;
	for line in f:
		line=line.rstrip()
		if (line.find("@<TRIPOS>BOND")!=-1): ##final coordenadas primera conformacion
			count=count+1
		if count == 1:
			#print line
			s=line.split()
			#print line
			countSplit=0
			for i in s[7]: ##separo el tipo de aminoacido de su numero
				countSplit=countSplit+1
				if i.isdigit():
					break
			cadena=\
			str(round(float(s[2]),3))+delimiter+\
			str(round(float(s[3]),3))+delimiter+\
			str(round(float(s[4]),3))+delimiter+\
			s[1]+delimiter+\
			s[0]+delimiter+\
			s[7][:countSplit-1]+delimiter+\
			s[7][countSplit-1:]
			print cadena
			#cat $proteina |grep -i CA |awk '{print $3":"$4":"$5 }' >$proteina".Tmp"
		if (line.startswith("@<TRIPOS>ATOM")): ##cominezo de las coordenadas primera conformacion
			count=count+1
	print '##_____________________________________________'
	print '##'
	print '## X : Y : Z : type : nAtom : typeRes : NRes : Chain'	
	print '##_____________________________________________'
###_____________________________________________________________________________________
###
###		Conversion pdbqt
###______________________________________________________________________________________
"""
1 - 6 	Record name 	"ATOM "
7 - 11 	Integer 	serial 	Atom serial number.
13 - 16 	Atom 	name 	Atom name.
17 	Character 	altLoc 	Alternate location indicator. IGNORED
18 - 21 	Residue name 	resName 	Residue name.
22 	Character 	chainID 	Chain identifier.
23 - 26 	Integer 	resSeq 	Residue sequence number.
27 	AChar 	iCode 	Code for insertion of residues. IGNORED
31 - 38 	Real(8.3) 	x 	Orthogonal coordinates for X in Angstroms.
39 - 46 	Real(8.3) 	y 	Orthogonal coordinates for Y in Angstroms.
47 - 54 	Real(8.3) 	z 	Orthogonal coordinates for Z in Angstroms.
55 - 60 	Real(6.2) 	occupancy 	Occupancy.
61 - 66 	Real(6.2) 	tempFactor 	Temperature factor.
67 - 76 	Real(10.4) 	partialChrg 	Gasteiger PEOE partial charge q.
79 - 80 	LString(2) 	atomType 	AutoDOCK atom type t. ##ESTE ESTA MAL es desde la 78
ATOM     30  O   LIG    1       -5.496   1.759   0.000  0.00  0.00    -0.225 OA
"""
#cad=[0,5,6,10,12,15,16,16,17,120]
def fPdbqt(f):
	for line in f:
		line=line.rstrip()
		if (line.startswith("ATOM") or line.startswith("HETATM")): #OJO los HETATM
			"""
			cadena=line[0:6].strip()+delimiter+\
			line[6:11].strip ()+delimiter+\
			line[12:16].strip ()+delimiter+\
			line[16:17].strip ()+delimiter+\
			line[17:21].strip()+delimiter+\
			line[21:22].strip()+delimiter+\
			line[22:26].strip()+delimiter+\
			line[26:27].strip()+delimiter+\
			line[30:38].strip ()+delimiter+\
			line[38:46].strip()+delimiter+\
			line[46:54].strip()+delimiter+\
			line[54:60].strip()+delimiter+\
			line[60:66].strip()+delimiter+\
			line[66:76].strip()+delimiter+\
			line[76:77].strip()+delimiter+\
			line[77:80].strip()
			"""
			cadena=	\
			str(round(float(line[30:38].strip()),3))+delimiter+\
			str(round(float(line[38:46].strip()),3))+delimiter+\
			str(round(float(line[46:54].strip()),3))+delimiter+\
			line[12:16].strip ()+delimiter+\
			line[6:11].strip()+delimiter+\
			line[17:21].strip()+delimiter+\
			line[22:26].strip()+delimiter+\
			line[21:22].strip()
			
			print cadena
	print '##_____________________________________________'
	print '##'
	print '## X : Y : Z : type : nAtom : typeRes : NRes : Chain'	
	print '##_____________________________________________'	

###
###
"""
Label ATOM
COLUMNS        DATA TYPE       CONTENTS                            
--------------------------------------------------------------------------------
 1 -  6        Record name     "ATOM  "                                            
 7 - 11        Integer         Atom serial number.                   
13 - 16        Atom            Atom name.                            
17             Character       Alternate location indicator.         
18 - 20        Residue name    Residue name.                         
22             Character       Chain identifier.                     
23 - 26        Integer         Residue sequence number.              
27             AChar           Code for insertion of residues.       
31 - 38        Real(8.3)       Orthogonal coordinates for X in Angstroms.                       
39 - 46        Real(8.3)       Orthogonal coordinates for Y in Angstroms.                            
47 - 54        Real(8.3)       Orthogonal coordinates for Z in Angstroms.                            
55 - 60        Real(6.2)       Occupancy.                            
61 - 66        Real(6.2)       Temperature factor (Default = 0.0).                   
73 - 76        LString(4)      Segment identifier, left-justified.   
77 - 78        LString(2)      Element symbol, right-justified.      
79 - 80        LString(2)      Charge on the atom.       
Example: 
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890

ATOM    145  N   VAL A  25      32.433  16.336  57.540  1.00 11.92      A1   N
"""
def fPdb(f): 
	for line in f:
		line=line.rstrip()
		if (line.startswith("ATOM") or line.startswith("HETATM")): #OJO los HETATM 
			"""
			cadena=line[0:6].strip()+delimiter+\
			line[6:11].strip ()+delimiter+\
			line[12:16].strip ()+delimiter+\
			line[16:17].strip ()+delimiter+\
			line[17:20].strip()+delimiter+\
			line[21:22].strip()+delimiter+\
			line[22:26].strip()+delimiter+\
			line[26:27].strip()+delimiter+\
			line[30:38].strip ()+delimiter+\
			line[38:46].strip()+delimiter+\
			line[46:54].strip()+delimiter+\
			line[54:60].strip()+delimiter+\
			line[60:66].strip()+delimiter+\
			line[72:76].strip()+delimiter+\
			line[76:78].strip()+delimiter+\
			line[78:80].strip()
			"""
			cadena=\
			str(round(float(line[30:38].strip()),3))+delimiter+\
			str(round(float(line[38:46].strip()),3))+delimiter+\
			str(round(float(line[46:54].strip()),3))+delimiter+\
			line[12:16].strip ()+delimiter+\
			line[6:12].strip()+delimiter+\
			line[17:20].strip()+delimiter+\
			line[22:26].strip()+delimiter+\
			line[21:22].strip()
			print cadena
	print '##_____________________________________________'
	print '##'
	print '## X : Y : Z : type : nAtom : typeRes : NRes : Chain'	
	print '##_____________________________________________'
	

f = open(fileEntrada)
if (file_extension==".pdb"):
	fPdb(f)
elif(file_extension==".mol2"):
	fMol2(f)
elif(file_extension==".pdbqt"):
	fPdbqt(f)
else:
	print "Error con el fichero de la proteina"
f.close()
exit()

