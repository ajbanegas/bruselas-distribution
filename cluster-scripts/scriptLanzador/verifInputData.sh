###########################################################
#	comprueba que la opcion y el program estes en sus rangos
#
#############################################################

auxCmd=""
paramCmd=""
extensiones=""

funcionTest()
{
	errorOld=$error;
	while read linea
	do
		a=`echo $linea | cut -d \: -f 1`

		#tengo que filtrar "#""
		if [ $1 == $a ];then

			extensiones=`echo $linea | cut -d : -f 2`
			coordenadas=`echo $linea | cut -d : -f 3` #se usa para los programas VS que no usan coordenadas, wega y demas
			#echo la x es: $extensiones
			if [ "$opcion" == "VS" ];then
				if [[  -n "$coordenadas" ]] && [ $error -eq 7 ];then
					x=0;y=0;z=0
				elif [ -z "$x" ] || [ -z "$y" ] || [ -z "$z" ] || [ -z "$numPorJobs" ]  ;then
					error=7
					break
				fi
			fi
			error=$errorOld
			break;
		else
			error=$3
		fi
	done < $2
}

comprobarExtension()
{
	funcionTest $programa ${CWD}scriptLanzador/SW.txt 7
	if [ -z "$numPorJobs" ] || [ -z "$x"  ] || [ -z "$y" ] || [ -z "$z" ] && [ opcion == "VS" ];then
			error=7
	fi

	funcionTest $opcion ${CWD}scriptLanzador/OP.txt 2
	funcionTest $programa ${CWD}scriptLanzador/SW.txt 1
	extension="."${proteina##*.}

	if [ $error -eq 0 ];then
		#if	echo "$proteina" | grep -q "\-" ;then
		#	 error=9
		#elif echo "$ligando" | grep -q "\-" ;then
		#	 error=9

		if [ $extension == ".ser" ];then
		    sed -i -r "s|Java Path=.+|Java Path=$CWD""scriptsDocking/externalSw/jre/|" ./scriptsDocking/externalSw/ChemAxon/JChem/bin/java.ini
		elif [ -z "$extension" ];then #no se que hacia
			echo "" > /dev/null ##chapu
		elif [ "$extensiones" != "$extension" ];then
			##________________________________________________________________________________________
			##
			##		Caso especial con AD y LS
			##______________________________________________________________________________________

			if [ $opcion == "SD" ];then
				extension=".pdb"
			elif [ $programa == "AD" ] && ([ $extension == ".pdb" ] || [ $extension == ".mol2" ]);then
				fileTmp=`expr ${proteina%$extension}`

				echo "${CWD}scriptsDocking/externalSw/mgltools_x86_64Linux2_latest/bin/pythonsh ${CWD}scriptsDocking/externalSw/mgltools_x86_64Linux2_latest/MGLToolsPckgs/AutoDockTools/Utilities24/prepare_receptor4.py -r ${CWD}${fileTmp}${extension} -o ${CWD}${fileTmp}".pdbqt" -A checkhydrogens "

				extension=".pdbqt"
				proteina=$fileTmp".pdbqt"
			elif [ $extension == ".pmz" ] || [ $extension == ".pml" ]  ;then
				extension=".ldb"
            elif [ $programa == "GMA" ] && [ $extension == ".mol" ]; then
                extension=".sdf"
			else
				#echo entro a error
				error=3
			fi
		fi
	fi
}

#comprobacion datos entrada
gridY=25 #modificar  #Hacer esto dinamico!!!!
gridZ=25 #modifica
gridX=25 #modifica


comprobarExtension

###_____________________________________________________________________________________________--
###
###	Si es Vs tiene que existeir  el fichd de proteina y el directorio
###______________________________________________________________________________________________
if [ "$opcion" == "VS" ];then
	if [ ! -s "$CWD$proteina" ] || [ ! -d "$CWD$ligando" ];then
		error=8
	fi
elif [ "$opcion" != "QT" ];then
	if [ ! -s "$CWD$proteina" ] || [ ! -s "$CWD$ligando" ];then
		error=8
	fi
else
	if [ ! -d "$CWD${auxDir}" ] ;then
		error=8
	fi
fi

if [ $error == 0 ];then
	if [ $opcion == "VS" ];then
		lastChar=$(expr substr $ligando $(expr length $ligando) 1) ##si el usuario introduce la ruta sin la /
		if [ $lastChar != "/" ];then
			ligando=$ligando"/"
		fi
		###### modificado 11-11-2014 añadido -d OJO numFicheros=`ls $CWD$ligando*$extension |wc -l` #por si el directorio de ligando esta mal
		numFicheros=`ls -d $CWD$ligando*$extension |wc -l` #por si el directorio de ligando esta mal
		#echo "entro aqui la extension es: "$extension
	fi
	if [ $opcion == "BD" ];then
		if [ -d $ligando ];then
				error=4
		fi 
	fi
	#por si no se encuentran el fichero de la proteina o el directorio de ligandos esta vacios
	#echo "ls $proteina >/dev/null"
	##ls $proteina >/dev/null #Ni idea para que es
	if [ "$programa" == "LS" ]  ;then
		extension=".ldb"
		ls $ligando >/dev/null
		numFicheros=$?
    elif [ "$programa" == "GMA" ]  ;then
        extension=".sdf"
        ls $ligando >/dev/null
        numFicheros=$?
	fi
fi


