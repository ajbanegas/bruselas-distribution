#____________________________________________________________________________________________________________________________________________________________#
#
#				Funcion para subir a excell las pruebas (No se utiliza demomento)
#____________________________________________________________________________________________________________________________________________________________#


if [ "$disease" !=  "N/A" ];then
	if [ $opcion == "BD" ];then	
		#DISEASE	PROTEIN	PDB	FILENAME	LIGAND	SW	CLUSTER	CALCULATION	LOCATION
		java -jar ${CWD}scriptsGeneracionResultados/updateExcel.jar $opcion $fckUser $disease $proteinName $nomProteina $nomLigando $programa $gestorColas RUNNING $localitation
	else
		#DISEASE	PROTEIN	PDB	FILENAME	LIGAND	SUBSET	SW	BINDING	CLUSTER	CALCULATION LOCATION
		java -jar ${CWD}scriptsGeneracionResultados/updateExcel.jar $opcion $fckUser $disease $proteinName $nomProteina $nomLigando $subset $programa $x,$y,$z $gestorColas RUNNING $localitation
	fi
fi
