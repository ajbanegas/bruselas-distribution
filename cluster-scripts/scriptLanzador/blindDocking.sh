#___________________________________________________________________________________________________________________________________________________#
#																																					#
#											Funciones BD																							#
#___________________________________________________________________________________________________________________________________________________#

#######################################################################
#	Convierte el ligando en caso de que fuera diferente a pdbqt, solo pdb o mol2 , no funciona ahoa
#######################################################################
funcionConvertLignad()
{
	if [ $extensionLigando == ".mol2" ] || [ $extensionLigando == ".pdb" ] ;then
		fileTmp=`expr ${ligando%$extensionLigando}`
#scriptsDocking/externalSw/mgltools_x86_64Linux2_latest/bin/pythonsh scriptsDocking/externalSw/mgltools_x86_64Linux2_latest/MGLToolsPckgs/AutoDockTools/Utilities24/prepare_ligand4.py 
		$CWD""scriptsDocking/MGtoolsBasic/ejecutables/pythonsh $CWD""scriptsDocking/MGtoolsBasic/ejecutables/prepare_ligand4.py -l $CWD""$fileTmp$extensionLigando -o $CWD""$fileTmp".pdbqt" -A 'hydrogens' -U \'\' 
		ligando=$fileTmp".pdbqt"
	fi
}
###___________________________________________________________________________________________________
###
###		Genera un fichero nombreProteina.tmp con todos los CAS de la misma
###___________________________________________________________________________________________________
funcionFindCoordsCas()
{
	coordsAux=`python ${CWD}scriptLanzador/standarFileCoords.py $proteina |grep -v "##" |grep CA `
	
	
}
###_______________________________________________________________________________________________________
###
###		Lee un el fichero proteina.tmp y lanza 1 job con esas coordenadas por cada fila  
###________________________________________________________________________________________
funcionBlindDocking()
{

	#funcionFindCoordsCas
	#coords=($coordsAux) # divido en frilas
	#echo ${#coords[*]}
	#for i in "${coords[@]}"; do
    #	echo 1 $i
	#done
	#echo "$coordsAux"
	#echo
	#echo "${coordsAux[@]}" 
	#IFS=' ' read -ra NAMES <<< "$coordsAux" 
	#echo ${NAMES[2]}
	#IFS=\n
	#ary=($str)
	#for key in "${!coordenadas[@]}"; do echo "1  ${coordenadas[$key]}"; done
	#coords=$(echo $coordsAux | tr "\n" "\n")
	
	#echo ${coords[0]}
	#echo $coordenadas
	#exit
		## if [  "$flex" != "N/A" ];then #se crea el fichero de flexibilidad conextandose con el servidor de 1and1 que tiene pymol	
		## 	funcionBDFlex
		## fi		
		## i=0
		## if [ "$dynamic" != "N/A" ];then ##flexibilidad vina
		## 	ficheroFlex=${proteina%$extension}F.txt
		## 	ultimaLinea=`cat $ficheroFlex |wc -l` 
		## 	ultimaLinea=`expr $ultimaLinea - 1`    
		## 	while read line
		## 		do
		## 		z=`echo "${line##*' '}"` 
		## 		line=`echo "${line%' '*}"` 
		## 		y=`echo "${line##*' '}"` 
		## 		line=`echo "${line%' '*}"` 
		## 		x=`echo "${line##*' '}"` 
		## 		line=`echo "${line%' '*}"` 
		## 		numAminoacdo=`echo $line | cut -d ' ' -f1`
		## 		chain =`echo $line | cut -d ' ' -f2`
		## 		if [ $ultimaLinea -eq $i ];then
		## 			final="-ul"
		## 		fi
		## 		funcionEnviarJobBD
		## 	 	i=`expr $i + 1`
		## 	done < $ficheroFlex
		## 	#rm $ficheroFlex"F.txt"
		##else #bd normal
		numFicheros=`python ${CWD}scriptLanzador/standarFileCoords.py ${CWD}${proteina} |grep -v "##" |grep CA |wc -l`
		x=0 			#es necesario por una modificacion y poderlo hacer compatible con VS
		y=0 			#es necesario por una modificacion
		z=0 			#es necesario por una modificacion
		numAminoacdo=0 	#es necesario por una modificacion
		funcionEnviarJobs
		#numFicheros=${#coords[*]}		######OJO AÑADIDO -d por LS
		
		#echo resto: $resto
		#echo contIni: $contIni
		#echo contFin: $contFin
		#echo div: $div
		#echo numFicheros: $numFicheros
		
}

###_____________________________________________________________________________________________________________________
###
###			Genera la flexibilidad y la guarda en un fichero proteinaF.txt para que luego hacer docking con vina
###______________________________________________________________________________________________________________________
funcionBDFlex()
{
	
	ficheroFlex=${proteina%$extension}F.txt
	if [ -f $ficheroFlex ];then 
		echo "existe el fichero de flexibilidad, Si desa generar flexibilidad nueva borrelo"
		#rm $ficheroFlex
	elif [ -n "$dinamyc" ];then
		if [ -d $ficheroFlex ];then
				cat  $dirDinamyc*.txt |grep -v "#" |sort -k6 -n >$proteina.aux
				while read linea
				do
					chain=`echo $linea | awk '{print $9}'` 
					numAmino=`echo $linea | awk '{print $8}'` 
					x=`echo $linea | awk '{print $2}'` 
					y=`echo $linea | awk '{print $3}'` 
					z=`echo $linea | awk '{print $4}'` 
				  	cadenaFlex=`ssh scriptsLanzador python scriptPymolFlexCoords.py proteinas/$nomProteina.pdbqt $x $y $z $chain |sort -n |grep ATOM |uniq -f 1 |awk ' {print "'$nomProteina':" $3":" $2$1}'|tr '\n' ',' |sed 's/.$//g'` 
   					echo $numAmino $chain $a $x $y $z >> $ficheroFlex
			done <  $proteina.aux
		else
				echo "Lanzador: No existe el directorio con los txt del BD anterior"
				ayuda
		fi
	else	
		scp $proteina scriptsLanzador:proteinas/
		for line in $(cat $proteina".Tmp"  ); # | head -1 quitar el head-3
		do
			x=`echo $line | cut -d\: -f1`
			y=`echo $line | cut -d\: -f2`
			z=`echo $line | cut -d\: -f3`
			numAminoacdo=`echo $line | cut -d\: -f4`
			chain=`echo $line | cut -d\: -f5`
			cadenaFlex=`ssh scriptsLanzador python scriptPymolFlex.py proteinas/$nomProteina.pdbqt $numAminoacdo $chain|sort -n |grep ATOM |uniq -f 1 |awk ' {print "'$nomProteina':" $3":" $2$1}'|tr '\n' ',' |sed 's/.$//g'`
			echo "$numAminoacdo $chain $cadenaFlex  $x $y $z >> $ficheroFlex"
			echo "$numAminoacdo $chain $cadenaFlex $x $y $z" >> $ficheroFlex
		done
		ssh scriptsLanzador rm proteinas/$nomProteina.pdbqt
	fi
}



funcionunBlindDockkingCube()
{
	echo $minX
	if [ $extension == ".pdbqt" ];then
		maxX=`cat $proteina |grep ATOM |awk 'BEGIN{max=-99999999}{if(int(substr($0,30,8))>max) max=int(substr($0,30,8))}END {print max}'` 
		minX=`cat $proteina |grep ATOM |awk 'BEGIN{min=99999999}{if(int(substr($0,30,8))<min) min=int(substr($0,30,8))}END {print min}'`
		maxY=`cat $proteina |grep ATOM |awk 'BEGIN{max=-99999999}{if(int(substr($0,39,8))>max) max=int(substr($0,39,8))}END {print max}'` 
		minY=`cat $proteina |grep ATOM |awk 'BEGIN{min=99999999}{if(int(substr($0,39,8))<min) min=int(substr($0,39,8))}END {print min}'`
		maxZ=`cat $proteina |grep ATOM |awk 'BEGIN{max=-99999999}{if(int(substr($0,47,8))>max) max=int(substr($0,47,8))}END {print max}'` 
		minZ=`cat $proteina |grep ATOM |awk 'BEGIN{min=99999999}{if(int(substr($0,47,8))<min) min=int(substr($0,47,8))}END {print min}'`
		echo $minX $maxX 
		echo $minY $maxY
		echo $minZ $maxZ
		


		#'BEGIN{max=-99999}{if((substr($0,47,8))>max) print (substr($0,47,8))'} # maxZ=(substr($0,47,8))}END {print $maxZ}'
		# awk '{for(x=1;x<=NF;x++)
		#								a[++y]=$x}
		#							END{c=asort(a)
		#							;print "min:",a[1];print "max:",a[c]}'
		#awk '{print substr($0,30,8) }'

	elif [ $extension == ".mol2" ];then
		cat $proteina |grep -i CA |awk '{print $3":"$4":"$5 }' 
	else
		ayuda
	fi
	if [ -z $x ] || [ -z $y ] || [ -z $z ] || [ -z $t ];then
		echo "debe introducir la x, y , z del centro del cubo y el tamaño del lado"
		ayuda
	fi 
	exit
	mitad=`expr $t / 2`
	min=`expr $x - $mitad` 
	max=`expr $x + $mitad` 
	puntosGrid=15
	contador=0
	for ((  zz = $min ;  $zz < $max;  zz=zz+$puntosGrid  )) do
		z=$zz
		for ((  yy = $min ;  $yy < $max;  yy=yy+$puntosGrid  )) do
			y=$yy
			for ((  xx = $min ;  $xx < $max;  xx=xx+$puntosGrid  )) do
				x=$xx
				funcionEnviarJobBD
				contador=`expr $contador + 1`
				i=$contador
			done
			echo ""
		done
		echo ""
	done


}




###_______________________________________________________________________________________________----
###	Funciones similaridad
###__________________________________________________________________________________________
funcionSimilaridad()
{

	datosVS
	if [ "$programa" == "LS" ];then
	numFicheros=`$CWD""scriptsDocking/ligandscout3/idbinfo -d $ligando |grep Database |awk '{print $3}'`	######OJO AÑADIDO -d por LS
	div=`expr $numFicheros \/ $numPorJobs`
	resto=`expr $numFicheros \% $numPorJobs`
	contIni=1
	contFin=`expr $contIni \+ $numPorJobs` 
	extension="ldb"
	SALIR=0
	final=""
	while [  $SALIR == 0 ]
	do	
	
		funcionJob
		contIni=$contFin
		contFin=`expr $contFin + $numPorJobs`
		if [ $contFin -gt `expr $numFicheros + 1` ]; then #+1 Ojo
	  		 SALIR=1
		fi	
	done
	if [ $resto -ne 0 ];then 
		if [ $contFin -gt $numFicheros ];then
			final="-ul"
		fi
	
		contFin=`expr $contIni + $resto`
		funcionJob
	fi
	fi
}

####______________________________________________________________________________________________________________________________
####
####
####_____________________________________________________________________________________________________________________________
funcionBDVS()
{

	funcionFindCoordsCas
	extensionLig=".pdbqt" ##OJO DEPENDE DEL PROGRAMA
	extensionLig="*.mol2"
	###numLigandos=`ls $ligando*$extension |wc -l`			######OJO AÑADIDO -d por LS
	numLigandos=`ls ${ligando}*${extensionLig} |wc -l`		######OJO AÑADIDO -d por LS
	atomsProtCA=`cat $proteina |grep CA |wc -l`
	numFicheros=`expr $numLigandos \* $atomsProtCA` 
	div=`expr $numFicheros \/ $numPorJobs`
		resto=`expr $numFicheros \% $numPorJobs`
	contIni=0
	contAomCA=0;
	contFin=`expr $contIni \+ $numPorJobs` 
	SALIR=0
	if [ $numPorJobs -ge $atomsProtCA ];then
		echo "Script Lanzador -j no puede ser mayor o igual que el numero de carbono alfas de la proteina"
		ayuda
	fi
	while [ $SALIR == 0 ]
	do	
		funcionJob
		contIni=$contFin
		contFin=`expr $contFin + $numPorJobs`
		if [ $contFin -gt `expr $numFicheros + 1` ]; then #+1 Ojo
	  		 SALIR=1
		fi	

	done
	if [ $resto -ne 0 ];then 
		if [ $contFin -gt $numFicheros ];then
			final="-ul"
		fi
		#echo  coun ini $contIni
		#echo count fin $contFin
		contFin=`expr $contIni + $resto`
		funcionJob
	fi
}
#funcionunDocking()
# {
#echo "Funcion"
#echo " $CWD""scriptsDocking/scriptGenerarGrid.sh -s $programa -x $x -y $y -z $z -d $dir -p $proteina -c $CWD -o $opcion"
#			$CWD""scriptsDocking/scriptDockingComando.sh -o $opcion -d $dir -s $programa \
#					-l $ligando -e 0 	-nl $nomLigando\
#					-p $proteina -x $x -y $y -z $z \
#					-np $nomProteina -c $CWD
#}

###_________________________________________________________________________________________________________
###
###	Dependiendo de la opcion se llama a una funcion
###________________________________________________________________________________________________________
source ${CWD}/scriptLanzador/lanzaJob.sh
case $opcion in
	BD )   funcionBlindDocking;;
	BDVS ) funcionBDVS;;
	BDC	 ) funcionunBlindDockkingCube;;
esac










# funcionBDFlex()
# {
	
# 	ficheroFlex=${proteina%$extension}F.txt
# 	if [ -f $ficheroFlex ];then 
# 		echo "existe el fichero de flexibilidad, Si desa generar flexibilidad nueva borrelo"
# 		#rm $ficheroFlex
# 	elif [ -n "$dinamyc" ];then
# 		if [ -d $ficheroFlex ];then
# 				cat  $dirDinamyc*.txt |grep -v "#" |sort -k6 -n >$proteina.aux
# 				while read linea
# 				do
# 					chain=`echo $linea | awk '{print $9}'` 
# 					numAmino=`echo $linea | awk '{print $8}'` 
# 					x=`echo $linea | awk '{print $2}'` 
# 					y=`echo $linea | awk '{print $3}'` 
# 					z=`echo $linea | awk '{print $4}'` 
# 				  	cadenaFlex=`ssh scriptsLanzador python scriptPymolFlexCoords.py proteinas/$nomProteina.pdbqt $x $y $z $chain |sort -n |grep ATOM |uniq -f 1 |awk ' {print "'$nomProteina':" $3":" $2$1}'|tr '\n' ',' |sed 's/.$//g'` 
#    					echo $numAmino $chain $a $x $y $z >> $ficheroFlex
# 			done <  $proteina.aux
# 		else
# 				echo "Lanzador: No existe el directorio con los txt del BD anterior"
# 				ayuda
# 		fi
# 	else	
# 		scp $proteina scriptsLanzador:proteinas/
		

# 		for line in $(ls $CWD""$ficheroBD*.en);do
# 			line=${line%".en"}
# 			numAminoacido=`cat $line".txt" |grep -v "#"|awk '{print $8}'` 
# 			z=`cat $line".txt" |grep -v "#"|awk '{print $4}'` 
# 			y=`cat $line".txt" |grep -v "#"|awk '{print $3}'` 
# 			x=`cat $line".txt" |grep -v "#"|awk '{print $4}'` 
# 			chain=`cat $line".txt" |grep -v "#"|awk '{print $9}'` 
# 			#scp $line".en" scriptsLanzador:energias/
# 			echo ${line%.*}
# 			#cadenaFlex=`ssh scriptsLanzador python scriptPymolFlex.py proteinas/$nomProteina.pdbqt $numAminoacdo $chain|sort -n |grep ATOM |uniq -f 1 |awk ' {print "'$nomProteina':" $3":" $2$1}'|tr '\n' ',' |sed 
# 		done


# 		#for line in $(cat $proteina".Tmp"  ); # | head -1 quitar el head-3
# 		#do
# 		#	x=`echo $line | cut -d\: -f1`
# 		#	y=`echo $line | cut -d\: -f2`
# 		#	z=`echo $line | cut -d\: -f3`
# 		#	numAminoacdo=`echo $line | cut -d\: -f4`
# 		#	chain=`echo $line | cut -d\: -f5`
# 		#	cadenaFlex=`ssh scriptsLanzador python scriptPymolFlex.py proteinas/$nomProteina.pdbqt $numAminoacdo $chain|sort -n |grep ATOM |uniq -f 1 |awk ' {print "'$nomProteina':" $3":" $2$1}'|tr '\n' ',' |sed 's/.$//g'`
# 		#	echo "$numAminoacdo $chain $cadenaFlex  $x $y $z >> $ficheroFlex"
# 		#	echo "$numAminoacdo $chain $cadenaFlex $x $y $z" >> $ficheroFlex
# 		#done
# 		#ssh scriptsLanzador rm proteinas/$nomProteina.pdbqt
# 	fi
# }
#if [ $extension == ".pdbqt" ];then #dependiendo del programa el fichero de proteina pueded ser .mol2 o pdqt
	#	module load python
	#	python $CWD""scriptsDocking/buscaCoordenadas.py $proteina
	#	#extension del ligando para 
	#	extensionLigando="."${ligando##*.} #se la extendsion del ligando # si es diferente a pdbqt se transforma
	#	funcionConvertLignad
	#	#	cat $proteina |grep -i CA |awk '{if ($5 ~ /^[A-Z][0-9]/) print $6":"$7":"$8; else print $7":" $8":"$9}' > $proteina".Tmp"
	#elif [ $extension == ".mol2" ];then
	#	cat $proteina |grep -i CA |awk '{print $3":"$4":"$5 }' >$proteina".Tmp"
	#else
	#	ayuda
	#fi