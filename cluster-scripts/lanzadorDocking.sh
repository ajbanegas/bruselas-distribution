#!/bin/bash
##Variable para saber el error
error=0
#___________________________________________________________________________
#error=0 --> OK
#error=1 -->Error SW
#error=2 --> Error opcion
#error=3 -->extension no valida para el prgo
#error=4 --> fichero de proteina, ligando vacio o ligando invalido
#error=5 --> hostname del cluster no encontrado
#error=6 --> no encontrado opcion de docking
#error=7 --> numJobs, x, y, o z estan vacios
#error=8 -->  Fichero o directorio o ligandos no existen
#error=9 --> No se pueden usar guiones medios (-) en los ficheros de la proteina o ligandos
#error=10 --> Gestor de colas erroneo, revise el script scriptLanzador/queueManager.sh 




#_________________________________________________________________________
###_______________________________________________________________________
###
###		Ayuda
###_____________________________________________________________________
ayuda(){

	${rutaScriptsLanzador}help.sh $1
	exit
}

###______________________________________________________________________
###
###	Funcion para indicar si existe alg´un error de los datos de entrada
###____________________________________________________________________
funcionError()
{
	
	laError="ERROR: LanzadorDocking.sh,"
	case $error in
		1) echo -e  "${RED} $laError Programa de docking incorrecto ${NONE}"   ; ayuda;;
		2) echo -e  "${RED} $laError Opcion de docking incorrecto ${NONE}"; ayuda;;
		3) echo -e  "${RED} $laError Extension no valida para el programa de docking ${NONE}"; ayuda;;
		4) echo -e  "${RED} $laError fichero ligando o de proteina vacio ${NONE}"; ayuda;;
		5) echo -e  "${RED} $laError Hostanme del cluster no encontrado ${NONE}"; ayuda;;
		6) echo -e  "${RED} $laError No encontrada opciond de docking ${NONE}"; ayuda;;
		7) echo -e  "${RED} $laError numJobs, x, y, o z estan vacios ${NONE}"; ayuda;;
		8) echo -e  "${RED} $laError Fichero de la proteina, carpeta de ligandos o fichero de ligando NO EXIST ${NONE}";ayuda;;
		9) echo -e  "${RED} $laError No se pueden usar guiones medios (-) en los nombres de ficheros de proteinas o ligandos ${NONE}";ayuda;;
		10) echo -e "${RED} $laError Gestor de colas erroneo, revise el script scriptLanzador/queueManager.sh ${NONE}";ayuda;;

	esac
	
}

#_______________________________________________________________________________________________________________#
#														#
#						      MAIN							#
#_______________________________________________________________________________________________________________#
ulimit -s unlimited		#util para leadfinder y FB
source scriptLanzador/colors.sh			  ##colores del script
rutaScriptsLanzador=${CWD}scriptLanzador/ ##rutas de los scripts de lanzador
source scriptsDocking/parameters.sh 	  ##parametros 
if [ -n "$proteina" ] && [ "$opcion" == "QT" ];then
	auxDir=$proteina
	p=`basename $proteina`
	proteina=`echo $p | cut -d - -f 1`
	ligando=`echo $p | cut -d - -f 2`
	
fi

if [ -z "$opcion" ] || [ -z "$programa"  ] || [ -z "$proteina" ] || [ -z "$ligando" ] || [ -z $numPorJobs ];then 
		echo -e "${RED}Error proteina, ligando, opcion, programa vacios o numero por jobs vacio ${NONE}"
		ayuda
fi

#echo $error
source ${rutaScriptsLanzador}verifInputData.sh 	#comprueba los datos de entrada con los ficheros SW.txt y OP.txt
#echo $error
source ${rutaScriptsLanzador}queueManager.sh 		#busca el cluster donde se encuentra (Se podria mejorar)
#echo $error
source ${rutaScriptsLanzador}nomJob.sh 			#busca un nombre para el job
#echo $error
funcionError 								#se comprueba a ver si ha dado algun error
#echo $error
source ${rutaScriptsLanzador}clearNames.sh 		#busca el nombre del ligando y proteina sin extensiones ni paths
#echo $error
source ${rutaScriptsLanzador}createFile.sh 		#crea un directorio
#echo $error
funcionError 								#se comprueba a ver si ha dado algun error

case $opcion in
	VS )  source ${rutaScriptsLanzador}virtualScreening.sh 	#funcionConfiguracionAutomaticaLanzarJobs
			source ${rutaScriptsLanzador}subirExcell.sh 		#sube al excel
			;;	
	BD|\
	BDVS|\
	BDC	) 	source scriptLanzador/blindDocking.sh #funcionBlindDocking
			source scriptLanzador/subirExcell.sh 		#sube al excel
			;;	
	SM )  	funcionSimilaridad
			funcionSubirExcell 	#sube al escel
			;;
	SD ) 	source ${rutaScriptsLanzador}dockingSustitucion.sh
			;;
	QT ) source ${rutaScriptsLanzador}scriptQuantum.sh
			;;
	* ) echo "error Con la opcion"
		error=6;;
esac
	
