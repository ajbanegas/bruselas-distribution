ejecutar()
{
	case $opcion in
        VS)
		swDir=${CWD}scriptsDocking/externalSw/pharmer
		id=$(cat /dev/urandom | tr -dc '0-9' | fold -w 256 | head -n 1 | head --bytes 6)
		libDir=library${id}
		model=model${id}
		script=genqueries${id}
		split=split${id}

		cp ${CWD}scriptsWeb/genqueries.py ${directorio}${script}.py
		cp ${CWD}scriptsWeb/splitPH.py ${directorio}${split}.py

		${swDir}/pharmer.static -dbdir ${directorio}${libDir} -in ${CWD}${ligando} --cmd dbcreate
		${swDir}/pharmer.static -in ${CWD}${proteina} -out ${directorio}${model}.json -cmd pharma
		${swDir}/pharmer.static -in ${CWD}${proteina} -out ${directorio}${model}.sdf -cmd pharma
		python ${directorio}${script}.py ${directorio}${model}.json ${directorio}${model}-adapted.json
		${swDir}/pharmer.static -dbdir ${directorio}${libDir} -in ${directorio}${model}-adapted.json -print -cmd dbsearch -out ${salida}.sdf --extra-info -reduceconfs=1 > ${salida}.out

		sed -i '1d' ${salida}.out
		sed -i "$(($(wc -l < ${salida}.out) - 1)),\$d" ${salida}.out
		awk -F ',' '{print $2"####"$5}' ${salida}.out > ${salida}.txt

		if [ -f ${salida}.sdf ]; then
			cd ${directorio}
			python ${directorio}${split}.py ${salida}.sdf
			cd ${CWD}
		fi
                ;;
                # energia la x la y la z siempre seran 0 el ligando y el numero de linea del resultado
        *)
        	echo "El programa Pharmer solo admite opcion -o VS"
                ayuda
		;;
        esac
}
