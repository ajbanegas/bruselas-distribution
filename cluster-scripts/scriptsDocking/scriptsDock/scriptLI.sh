ejecutar()
{
	${CWD}scriptsDocking/externalSw/lisica/lisica -R $proteina -T $ligando -d 3 -w 500 -n 1 -m 2.5 -f $salida > $salida".out"
	awk '{printf "%s####",$1;for(i=2;i<=NF;i++){ printf "%s ",$i};print ""}' ${salida}.out > ${salida}.txt

	(IFS='
'
	for line in $(head -n-2 ${salida}.txt)
	do
		line=$(echo ${line} | sed 's/ $//g')
		ligand=$(echo ${line} | awk -F '####' '{for(i=2;i<=NF;i++){printf "%s ",$i}}' | sed 's/ $//g' | sed 's/[\x5D.,(){}\x27\x5B ]/\\&/g')
		ligand2=$(echo ${line} | awk -F '####' '{for(i=2;i<=NF;i++){printf "%s ",$i}}' | sed 's/ $//g')
                dir=$(dirname -- ${salida})/
                query=${dir}$(basename ${proteina})
		ligando=$(find ${dir} -name "*${ligand}*.mol2")
                alineado=${dir}${opcion}-${programa}-${ligand2}-aligned.mol2
                script=${dir}${ligand}.py

		cp ${CWD}scriptsWeb/lisica-alignment.py ${script}
		python "${script}" "${query}" "${ligando}" "${alineado}"
               	if [ "$?" != "0" ]; then
                	echo "Error while aligning ${ligand} (script ${script})"
                fi
		rm ${script}
	done)
}

#./lisica[.exe] -R <path to reference molecule> -T <path to target molecules> [parameters];
#-n <number of CPU threads> Default value: the default is to try to detect the number of CPUs or, failing that, use 1
#-d <product graph dimension> Possible input: 2 or 3; Default value: 2
#-m <maximum allowed atom spatial distance for the 3D product graph measured in angstroms> Default value: 1.0
#-s <maximum allowed shortest path size for the 2D product graph measured in the number of covalent bonds> Default value: 1
