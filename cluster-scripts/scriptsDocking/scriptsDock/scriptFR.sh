ejecutar()
{
    case $opcion in
        VS)
            export OE_LICENSE=$CWD""scriptsDocking/externalSw/oedocking/bin/oe_license.txt
	        ${CWD}scriptsDocking/externalSw/oedocking/bin/fred \
                -receptor $CWD$proteina \
                -dbase $CWD$ligando \
                -prefix ${salida}_
            rm -f ${salida}_{settings.param,status.txt,report.txt}
            ;;

            *)
            echo "El programa 'fred' solo admite opcion -o VS"
            ayuda;;
    esac

}
