ejecutar()
{
	case $opcion in
        VS)
		${CWD}scriptsDocking/externalSw/wega/wega2016 -outsd ${salida}.sd -qsd ${CWD}${proteina} -sd ${CWD}${ligando} -M 0 -ST 0 -CC true -SO2 true &> /dev/null
		cd ${directorio}

		mv ${salida}.txt ${salida}.out
		awk -F ',' '{for(i=1;i<=(NF-2);i++){ printf "%s,",$i}; print "####"$(NF-1)"####"$NF}' ${salida}.out | sed 's/,####/####/g' > ${salida}.txt

		python ${CWD}scriptsWeb/splitWG.py ${salida}_Aligned.sd
		cd ${CWD}
                ;;
                # energia la x la y la z siempre seran 0 el ligando y el numero de linea del resultado
        *)
        	echo "El programa wega solo admite opcion -o VS"
                ayuda
		;;
        esac
}
