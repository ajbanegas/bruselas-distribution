ejecutar()
{
    case $opcion in
        VS)
            export OE_LICENSE=$CWD""scriptsDocking/externalSw/posit/bin/oe_license.txt
	        ${CWD}scriptsDocking/externalSw/posit/bin/posit \
                -receptor $CWD$proteina \
                -in $CWD$ligando \
                -prefix ${salida}_
            rm -f ${salida}_{settings.param,status.txt,report.txt}
            ;;

            *)
            echo "El programa 'posix' solo admite opcion -o VS"
            ayuda;;
    esac
}
