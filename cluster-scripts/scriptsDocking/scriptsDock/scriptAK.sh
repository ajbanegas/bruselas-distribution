##___________________________________________________________________________________
##
##	Hace docking con Autodock4
##	1º Genera la grid tanto para VS como para BD por que depende del ligando
##	2º hace docking
##___________________________________________________________________________________
ejecutar()
{
	${CWD}scriptsDocking/grids/generateGridAK.sh $CWD $proteina $ligando $salida $x $y $z  #generamos fichero grid y fichero de docking
	${CWD}scriptsDocking/externalSw/autodock4/autogrid4 -p ${salida}Grid.gpf -l ${salida}Grid.gpf.out 2> ${salida}Grid.gpf.out.err  #se genera la grid
	${CWD}scriptsDocking/externalSw/autodock4/autodock4 -p ${salida}Dock.dpf -l ${salida}.dock
	
	rm ${salida}*.map
	rm ${salida}Grid.gpf*
	rm ${salida}.maps.xyz
	rm ${salida}.maps.fld
	
	a=`cat ${salida}.dock |grep  "Estimated Free Energy of Binding" |grep -v DOCKED: |head -1 |awk '{print $8}'` # se coge el primer resutlado proi que los ordena
	echo $a $x $y $z $ligando $numAminoacido $chain >$salida.txt 
	cat ${salida}.dock  |grep '^DOCKED'    | cut -c9- > ${salida}.pdbqt
	

	#echo "cat ${salida}.dock  |grep '^DOCKED'    | cut -c9- > ${salida}.pdbqt"
	#enrgia corX cordY cordZ ligando nºEjecucion nºresultadoVINALF nº aminoacdio(Solo vina) chain
	#-5.4 20.1775 63.1003333333 11.285 ligandos/GLA.pdbqt 1- 1 33 A


}