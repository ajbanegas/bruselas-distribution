ejecutar()
{
	export NCIPLOT_HOME=${CWD}scriptsDocking/externalSw/nciplot
	sed "2 i ${salida}$P" ${proteina}".xyz" > ${salida}P.xyz	#se crea el primer fichero a raiz de la plantilla
	cat ${numAminoacido}${nomLigando}".com" |grep "(Fragment=2)" | sed  "s/(Fragment=2)//g"  > $salida.xyz		##se crea planmtilla para fich2
	sed -i "1 i `cat ${salida}.xyz |wc -l`"  	${salida}".xyz" ##se le indica el numero de atomos linea 2 (primero se cuentan)
	sed -i "2 i ${salida}"  ${salida}".xyz"
	sed -i 's/\\\t/  /g' ${salida}.xyz
	sed -i s/\\\t/"  "/g ${salida}.xyz
	echo "2" 							> ${salida}.nci
	echo ${salida}P.xyz					>> ${salida}.nci
	echo ${salida}.xyz					>> ${salida}.nci
	echo "INTERMOLECULAR"  				>> ${salida}.nci
	echo "CUTOFFS 0.1 1.0" 				>> ${salida}.nci
	echo "CUTPLOT 0.07 0.4"				>> ${salida}.nci
	cd $directorio
	./nciplot ${salida}".nci"
}

	##num Aminoacido es el directorio auxiliar donde se entucntran los .com y la plantilla
	#echo salida: $salida
	#echo proteina: $proteina
	#echo nomLigando: $nomLigando
	#echo Ligando: $ligando
	#echo onumAmino : $numAminoacido //nombre del fichero original
	#echo 
