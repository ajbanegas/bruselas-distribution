ejecutar()
{
	TAG=`echo $(basename $BASH_SOURCE)` #En todos los comandos debe existir esta etiqueta
	case $opcion in
    	VS)
		${CWD}scriptsDocking/externalSw/shafts/cynthia -q ${CWD}${proteina} -t ${CWD}${ligando} -o ${salida} -postOpt -normalizeType tanimoto $optAux &>${salida}.ucm
		if [ $? == 0 ];then
			sed -i '1d' ${salida}Result.list
		        cd ${directorio}
                	python ${CWD}scriptsWeb/splitSF.py ${salida}Hits.mol2
			rm ${salida}.ucm
		fi
		;;
	*)
		echo "Shafts requiere opcion VS";;
	esac
}
