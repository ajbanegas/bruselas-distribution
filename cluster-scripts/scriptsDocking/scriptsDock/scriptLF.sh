###funcion para hacer docking con leadFinder
ejecutar()
{
	#en caso de que no existea la grid se crea Esto sobretodo es por BD
	case $opcion in
		VS) #virtualscreening

			ligVS=`echo $ligVS | sed 's/.$//g'` 
			ligVS=${ligVS##*/}
			salida2=$directorio$numeroEjecucion$opcion"-"$programa"-"$nomProteina-$ligVS-$x-$y-$z
			${CWD}scriptsDocking/scriptGenerarGrid.sh -s $programa -x $x -y $y -z $z -d $salida2 -p ${proteina} -c $CWD
			${CWD}scriptsDocking/externalSw/leadFinder/leadfinder -q --load-grid=${salida2}grid.bin \
			--protein=${CWD}${proteina} --ligand=$CWD$ligando --output-tabular=${salida}.csv \
			--output-poses=${salida}.pdb --text-report=${salida}.log --max-poses=${numPoses} 
			#-xp #--poolsize=16 --popsize=4 #> $salida.ucam 2>> $salida.ucam
			salida2=$directorio$numeroEjecucion$opcion"-"$programa"-"$nomProteina-$ligVS-$x-$y-$z
			cat $salida".csv" |grep '^\0,'|head -1| awk -F"," '{print $2" '$x' '$y' '$z' '$ligando' "$1}' > $salida".txt";;
		BD ) #blindDocking
			${CWD}scriptsDocking/scriptGenerarGrid.sh -s $programa -x $x -y $y -z $z -d $salida -p $proteina -c $CWD
			${CWD}scriptsDocking/externalSw/leadFinder/leadfinder -q --load-grid=$salida"grid.bin" \
			--protein=$CWD$proteina --ligand=$CWD$ligando --output-tabular=$salida".csv" \
			--output-poses=$salida".pdb" --text-report=$salida".log" --max-poses=${numPoses}   >  $salida.ucam 2>> $salida.ucam
			
			a=`cat $salida".csv" |grep '^\0,'|head -1| awk -F"," '{print "'$salida' " $2" '$ligando' '$numeroEjecucion' " $1}'`	
			
			module load python
			python ${CWD}scriptsDocking/getCOM.py $a 0 0
			rm $salida"grid.bin" ;;  ###OJO DESCOMENTA para no llenar el hd de grids
		BDVS )	
			
			grid=${directorio}${x}-${y}-${z}
			${CWD}scriptsDocking/scriptGenerarGrid.sh -s $programa -x $x -y $y -z $z -d $grid -p $proteina -c $CWD
			${CWD}scriptsDocking/externalSw/leadFinder/leadfinder -q --load-grid="${grid}"grid.bin" \
			--protein=$CWD$proteina --ligand=$CWD$ligando --output-tabular=$salida".csv" \
			--output-poses=$salida".pdb" --text-report=$salida".log" --max-poses=${numPoses}   >  $salida.ucam 2>> $salida.ucam"
			a=`cat $salida".csv" |grep '^\0,'|head -1| awk -F"," '{print "'$salida' " $2" '$ligando' '$numeroEjecucion' " $1}'`	
			module load python
			python $CWD""scriptsDocking/getCOM.py $a 0 0
			#rm $salida"grid.bin"  ###OJO DESCOMENTA para no llenar el hd de grids
			 ;; 


		*)
			echo "No eligió ninguna opción valida -o vs o -o bd"
			ayuda;;
	esac

}
