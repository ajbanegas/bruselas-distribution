ejecutar()
{
	ulimit -s unlimited ##util para leadfinder
	case $opcion in
		VS) #virtualscreening
			ligVS=`echo $ligVS | sed 's/.$//g'` 
			ligVS=${ligVS##*/}
			salida2=$directorio$numeroEjecucion$opcion"-"$programa"-"$nomProteina-$ligVS-$x-$y-$z
			$CWD""scriptsDocking/externalSw/FlexScreen_Biotin/scriptGenerarDock.sh  -p $proteina -d $salida -d2 $salida2 -l $ligando -x $x -y $y -z $z -c $CWD  > $salida.ucam 2>> $salida.ucam
			#echo "$CWD""scriptsDocking/externalSw/FlexScreen_Biotin/scriptGenerarDock.sh  -p $proteina -d $salida -d2 $salida2 -l $ligando -x $x -y $y -z $z -c $CWD" # > $salida.ucam 2>> $salida.ucam
			#echo "cat $salida".mol"|tr -d "\r" > $salida.mol2" #no se para que era esto
			#echo "rm $salida".mol""				#no se para que era esto
			$CWD"scriptsDocking/externalSw/FlexScreen_Biotin/dock" $salida"dock.inp"
			rm $salida"dock.inp"
			#echo "$CWD"scriptsDocking/externalSw/FlexScreen_Biotin/dock" $salida"dock.inp""
			cat $salida".dat" |head -1 |awk '{print $7" '$x' '$y' '$z' '$ligando'"}' > $salida.txt


			;;
			
		BD | BDC ) #blindDocking 
			#echo $salida
			${CWD}scriptsDocking/externalSw/scriptGenerarGrid.sh -p $proteina -d $salida -c $CWD -s $programa -x $x -y $y -z $z  
			${CWD}scriptsDocking/externalSw/FlexScreen_Biotin/scriptGenerarDock.sh  -p $proteina -d $salida -l $ligando -x $x -y $y -z $z -c $CWD
			${CWD}"scriptsDocking/externalSw/FlexScreen_Biotin/dock" $salida"dock.inp" #> $salida.ucam 2>> $salida.ucam	
			a=`cat $salida".dat" |head -1 |awk '{print "'$salida' " $7" '$ligando' '$numeroEjecucion' " 0}'`
			cat $salida".dat" |head -1 |awk '{print "'$salida' " $7" '$ligando' '$numeroEjecucion' " 0}'
			#echo "$a"
			cat $salida".mol"|tr -d "\r" > $salida.mol2
			rm $salida".mol"

			module load python
			#echo "cat $salida".dat" |head -1 |awk '{print "'$salida' " $7" '$ligando' '$numeroEjecucion' " 0}"
			#echo $a
			python $CWD""scriptsDocking/getCOM.py $a 0 0
			rm $salida"dock.inp" #se borran los ficheros generados durante la ejecucion que no se van a volver a utilizar
			rm $salida"g.mol2"
			rm $salida"grid.inp"
			rm $salida"l.es"
			rm $salida"r.mol2"	
			rm $salida"s.vdw"  

			;;
		*)
			echo "No eligió ninguna opción valida -o vs o -o bd"
			ayuda;;
	esac
}

