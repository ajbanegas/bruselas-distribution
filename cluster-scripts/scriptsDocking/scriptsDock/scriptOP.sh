ejecutar()
{
	case $opcion in
        VS)	
		cd ${CWD}scriptsDocking/externalSw/OptiPharm
		./OPShapeSimilarity -c FMOL -q ${CWD}/${proteina} -d ${CWD}${ligando} -smol 1 -S -o ${directorio}/ | awk '{printf "%s####",$1; for(i=2;i<=(NF-15);i++){ printf "%s ",$i}; print "####"$NF}'  > ${salida}.txt
		sed -i 's/ ####/####/g' ${salida}.txt

		cd ${CWD}
		while read line
		do
			query=$(echo ${line} | awk -F '####' '{ print $1 }' | sed 's/[\x5D.,(){}\x27\x5B ]/\\&/g')
			molecule=$(echo ${line} | awk -F '####' '{ print $2 }' | sed 's/[\x5D.,(){}\x27\x5B ]/\\&/g')
			echo "mv -v ${directorio}MolR-${query}-${molecule}.mol2 ${directorio}${opcion}-${programa}-${molecule}-aligned.mol2" | sh
		done < ${salida}.txt
                ;;
        *)
        	echo "El programa optipharm solo admite opcion -o VS"
                ayuda
		;;
        esac
}
