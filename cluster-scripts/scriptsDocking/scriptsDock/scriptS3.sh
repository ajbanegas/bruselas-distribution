ejecutar()
{
	case ${opcion} in
		VS)
			cd ${directorio}

			# Preparación del query, si no lo tenemos ya: incluimos flexibilidad y generación de ambos métodos
			cp ${CWD}${ligando} . 			# Se copia el ligando a la carpeta de la prueba
			# Screening
			${CWD}scriptsDocking/externalSw/ChemAxon/JChem/bin/screen3d s -q ${nomProteina}.ser -t ${nomLigando}.ser -oformat mol2 -writeQuery

			traducir	# Obtener el nombre de cada ligando asociado a su puntuacion
			dividir		# Dividir la salida en ficheros individuales
			alinear		# Alinear cada query con su ligando
			;;		
		*)
			echo "El programa screen3d solo admite opcion -o VS"
			ayuda;;
	esac
}

traducir() {
	# contar las lineas ya existentes
	n_lineas=$(wc -l ${directorio}*${nomLigando}*_screenOut.txt | cut -d ' ' -f 1)

	# generar lineas: (score, nombre-ligando)
	(IFS='
'
	for line in $(tail -n +2 ${directorio}*${nomLigando}*_screenOut.txt)
	do
		index=$(echo ${line} | awk '{print $1}')
		score=$(echo ${line} | awk '{print $3}')
		linea=$(expr $index + 1)
		ligando=$(awk "NR==${linea}" ${directorio}lig_ser/${nomLigando}.mol2.txt)

		echo "${ligando}####${score}" >> *${nomLigando}*_screenOut.txt
	done)

	# eliminar lineas iniciales
	sed -i "1,${n_lineas}d" ${directorio}*${nomLigando}*_screenOut.txt
}

dividir() {
        input=$(find ${directorio} -maxdepth 1 -type f -name "*${nomLigando}*_aligned.mol2")
        map=$(find ${directorio}/lig_ser -type f -maxdepth 1 -name "${nomLigando}.mol2.txt")
	python ${CWD}scriptsWeb/splitS3.py ${input} ${map} ${programa}
}

alinear() {
	(IFS='
'
	for file in ${directorio}${opcion}-${programa}-*_aligned.mol2
	do
		foo=$(echo "${file}" | sed 's/[\x5D.,(){}\x27\x5B ]/\\&/g')
		ligand=$(echo "basename ${foo} _aligned.mol2 | sed 's/${opcion}-${programa}-//g'" | sh)
		escaped=$(echo "${ligand}" | sed 's/[\x5D.,(){}\x27\x5B ]/\\&/g')
		script=${directorio}${escaped}.py

		# Hacer copia de la plantilla para evitar errores de concurrencia
		cp ${CWD}scriptsWeb/s3-alignment.py ${script}

		# Generar los alineamientos
		out=${opcion}-${programa}-${escaped}-aligned.mol2
		fragment1=${directorio}ligand_${out}
                fragment2=${directorio}${out}

                echo "python \"${script}\" ${foo} ${fragment1} fragment1" | sh
                echo "python \"${script}\" ${foo} ${fragment2} fragment2" | sh
	done)
}
