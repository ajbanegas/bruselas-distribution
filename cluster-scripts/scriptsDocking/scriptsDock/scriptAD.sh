###___________________________________________________________________________________________________________________________
###
###		funcion para hacer docking con autodock vina
###_____________________________________________________________________________________________________________________________
ejecutar()  #hace Docking en la posicion indicada con el ligando indicado
{
	if [ -z "$numAminoacido" ];then #si no existe ekl nnumero de aminoacdio por que es docking normal (No flexible) se pone a 0		#COMPROBAR SI ES NECESARIO
          numAminoacido=0
    fi
    if [ -z "$chain" ];then #si no existe el nnumero de aminoacdio por que es docking normal (No flexible) se pone a 0		#COMPROBAR SI ES NECESARIO
          chain=A
    fi
	b=""
	fileFlexi=""
	if [ "$flexFile" != "N/A" ];then
		fileFlexi="--flex "${CWD}$flexFile
	fi

	funcionFlexibilidad
	case $opcion in
	VS)
		${CWD}scriptsDocking/externalSw/autodock/vina --out $salida.pdbqt --receptor $CWD$proteina  --ligand $CWD$ligando $b $fileFlexi \
	    --center_x $x --center_y $y --center_z $z --size_x 30 --size_y 30 --size_z 30 --energy_range 4 --num_modes $numPoses --cpu 6 --seed 2015|sed -n 29p |awk '{print $2" '$x' '$y' '$z' '$ligando' "$1}'  > $salida.txt
	    funcionAdScore;;
	BD |BDC | SD)
		a=`${CWD}scriptsDocking/externalSw/autodock/vina --out ${salida}.pdbqt --receptor $CWD$proteina  --ligand ${CWD}${ligando} $b $fileFlexi \
           --center_x $x --center_y $y --center_z $z --size_x 30 --size_y 30 --size_z 30 --energy_range 4 --num_modes $numPoses   --cpu 6 --seed 2015 |grep '\-----+------------+----------+----------' -A 1 | tail -1 |awk '{print "'$salida' " $2" '$ligando' '$numeroEjecucion' " $1}'`
	
		funcionAdScore
		python ${CWD}scriptsDocking/getCOM.py $a $numAminoacido $chain
		;;
	BDVS)

		a=` $CWD""scriptsDocking/externalSw/autodock/vina --out $salida.pdbqt --receptor $CWD$proteina  --ligand $CWD$ligando $b $fileFlexi\
            --center_x $x --center_y $y --center_z $z --size_x 70 --size_y 70 --size_z 70 --energy_range 4 --num_modes $numPoses   --cpu 8 --seed 2015 |grep '\-----+------------+----------+----------' -A 1 | tail -1 |awk '{print "'$salida' " $2" '$ligando' '$numeroEjecucion' " $1}'` 
	
		funcionAdScore
		python ${CWD}scriptsDocking/getCOM.py $a $numAminoacido $chain
		;;
	*)	
		echo "No eligió ninguna opción valida -o vs o -o bd"
		ayuda;;
	esac
	#if [ -d  "$dirFL" ] ;then #en caso de que se haga flexibilidad se borra la carpeta para no ocupar tamaño
	#	rm -r $dirFL;
	#fi
}

funcionFlexibilidad()
{	
	if [  "$flex" != "N/A" ];then ##si no es null
		ligando=${ligando}GLA.pdbqt
		
		#scriptsDocking/externalSw/mgltools_x86_64Linux2_latest/bin/pythonsh ${CWD}scriptsDocking/externalSw/mgltools_x86_64Linux2_latest/MGLToolsPckgs/AutoDockTools/Utilities24/prepare_receptor4.py
		pythonsh=${CWD}scriptsDocking/externalSw/mgltools_x86_64Linux2_latest/bin/pythonsh
		prepRec=${CWD}scriptsDocking/externalSw/mgltools_x86_64Linux2_latest/MGLToolsPckgs/AutoDockTools/Utilities24/prepare_flexreceptor4.py
		if [ "$opcion" == "BD" ];then
			fl=`cat $CWD"proteinas/"$nomProteina"F.txt" |grep -w  $chain |grep -w ^$numAminoacido |head -1|awk '{print \$2}'`  #Obtengo los  residuos que deben ser flexibles en la proteina#head -1 no se si se usa 
			
			#quitar brazos en ciertos aminoacdios
			#fl=`echo $fl | sed 's/3s5zNA:A:[A-Z][A-Z][A-Z]168,//g'`
			#fl=`echo $fl | sed 's/3s5zNA:A:[A-Z][A-Z][A-Z]47,//g'`
			#dirFL="scriptsDocking/externalSw/MGtoolsBasic/flex/$chain$nomProteina$numAminoacido" #directorio donde se guardan rigido flexible
			aux=${chain}${nomProteina}${numAminoacido}
			dirFL=${directorio}${aux}
		else
			fl=`cat ${CWD}proteinas/${nomProteina}F.txt  |head -1|awk '{print \$2}'`  #head -1 no se si se usa
			aux=${chain}${nomProteina}-${nomLigando}-${x}-${y}-${z}-${numAminoacido}
			dirFL=${directorio}${aux}
		fi
		if [ -d  "$dirFL" ] ;then 	
			rm -r $dirFL 
		fi
		
		
		
		mkdir $dirFL #si exite el fichero lo elimno y creo
		cp ${CWD}${proteina} ${dirFL}/${nomProteina}.pdbqt
		echo "$pythonsh  ${prepRec} -r $dirFL"/"$nomProteina".pdbqt" -s $fl -g $dirFL"/"$nomProteina"_rec.pdbqt -x "$dirFL"/"$nomProteina"_flex.pdbqt"  > /dev/null"
		$pythonsh  ${prepRec} -r $dirFL"/"$nomProteina".pdbqt" -s $fl -g $dirFL"/"$nomProteina"_rec.pdbqt -x "$dirFL"/"$nomProteina"_flex.pdbqt"  > /dev/null
		proteina=`basename $directorio`/${aux}/$nomProteina"_rec.pdbqt"
		#echo $proteina
		#proteina=$dirFL"/"$nomProteina"_rec.pdbqt"
		b="--flex "$dirFL"/"$nomProteina"_flex.pdbqt" 
		

	fi
}
funcionAdScore()
{

	####NOTa creo que estos dos lineas no funcionan con flexibilidad solo seria para rigido IMPORTANTE

	if [ "${flex}" == "N/A" ] && [ "${flexFile}" == "N/A" ];then #si no hay flexibilidad 

		
		if [ "$numPoses" -eq "1" ];then  		## si tiene solo tiene una pose
			#echo "ENTRO A NUMPOSES 1"
			sed -i '1d' $salida.pdbqt 		#elimino la primera linea del fichero "Model"
			sed -i  '$d' $salida.pdbqt 		#elimino la ultima linea del fichero "End model"
			${CWD}scriptsDocking/externalSw/autodock/vina --score_only --ligand $salida.pdbqt --receptor  $CWD$proteina > $salida.en1
		else ##si tiene varias poses
			echo "ENTRO A NUM POSES VARIASASSS"
			${CWD}/scriptsDocking/externalSw/autodock/vina_split --input ${salida}.pdbqt > /dev/null ##dicido el ligando
			for i in `seq 1 $numPoses` ;do #para cada pose paso vina --score only
				${CWD}scriptsDocking/externalSw/autodock/vina --score_only --ligand ${salida}_ligand_${i}.pdbqt --receptor  $CWD$proteina > $salida${i}.en 
			done
			mv ${salida}.pdbqt ${salida}-ALL.pdbqt
			mv ${salida}_ligand_1.pdbqt ${salida}.pdbqt
			mv ${salida}1.en ${salida}.en1

		fi
		
	else	
		echo "ENRO A FLEXIBLE"
		sed -i '1d' $salida.pdbqt 		#elimino la primera linea del fichero "Model"
		sed -i  '$d' $salida.pdbqt 		#elimino la ultima linea del fichero "End model"
		numLinBorrar=`cat $salida.pdbqt |grep -n "BEGIN_RES" | head -1 |awk  -F: ' {print $1}'`	
		n=`expr $numLinBorrar - 1`
		sed "1,$n"d $salida.pdbqt >  $salida-fl.pdbqt
		numFinLinea=`wc -l $salida.pdbqt  |awk '{print $1}'`
		sed -i  "$numLinBorrar,$numFinLinea"d $salida.pdbqt		
		#echo "sed -i  "$numLinBorrar,$numFinLinea"d $salida.pdbqt		"
		#echo "sed -i  "$numLinBorrar,$numFinLinea"d $salida.pdbqt	"

		${CWD}scriptsDocking/externalSw/autodock/vina --score_only  --flex $salida-fl.pdbqt --ligand $salida.pdbqt --receptor  $CWD$proteina > $salida.en1
		#################################################################################################################
	fi
}

	#	echo "sed -i   $numLinBorrar,$numFinLinea d $salida-out.pdbqt"
		#echo "cp $CWD$proteina $salida.$nomProteina".pdbqt

