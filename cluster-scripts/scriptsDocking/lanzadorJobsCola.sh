#!/bin/bash
ayuda(){
	${CWD}scriptsDocking/ayuda.sh $1
	exit
}
nombrePlantilla()
{
	
       nomJob="job-"${nomProteina}-${nomLigando}                #busco un nombre para el fichero de plantilla  
      ## nomJob=`echo $nomJob | sed 's/.$//g'` ;                 #borra el ultimo caracter
       nomJob=$nomJob"-"$confAdicionalI"-"$confAdicionalF".sh" #le añado las pos x y z y cont ini cont fin
       if [ ! -d ${directorio}/jobs/ ];then
       		mkdir ${directorio}/jobs/  #carpeta para guardar los nombres del job
       fi
       if [ ! -d ${directorio}/out/ ];then 
     	    mkdir ${directorio}/out/		#carpeta para guardar las salidas del job
       fi
       nomJob=${directorio}/jobs/$nomJob
       echo $nomJob
}

source ${2}/scriptsDocking/parameters.sh
#solo vina soporta varias CPUS, se optimiza para el cluster (Se deja en 1 por problemas con alg´un cluster)
#if [ "$programa" == "AD" ] || [ "$programa" == "LS" ];then
# cpus=1
#fi
cpus=1
nomJob=$(nombrePlantilla)	#nombre de la plantilla que se lanzara
source ${CWD}/scriptsDocking/cabecerasCola/clusters/gest_${gestorColas}.sh  #Se crea y se lanza la plantilla del job
if [ "$secuencial" == "N/A" ];then
  $execute $nomJob
else
  sh $nomJob #esjecutar lanzador en secuencial
fi


#rm $nomJob
#rm $nomJob
#if [ -z "$directorio" ] || [ -z "$proteina" ] || [ -z "$ligando" ] || [ -z "$x" ] || [ -z "$y" ] || [ -z "$z" ] || [ -z "$programa" ] || [ -z "$nomProteina" ] || [ -z "$opcion" ] || [ -z "$confAdicionalI" ]   || [ -z "$confAdicionalF" ] || [ -z "$CWD" ];then
#   echo "lazadorJobsCola: Error lanzando Job"
#   ayuda
#fi
