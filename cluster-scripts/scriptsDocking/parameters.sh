###______________________________________________________________________________________________________________________-
###
###			Parametros posibles
###_________________________________________________________________________________________________________________________

#
#	Parametros por defecto
#
##TIMEDOCKING=15			#tiempo en minutos de un docking (Esto es malo para programas de similaridad por que se reservara mucho mas tiempo en el cluster)
disease="N/A"			#enfermedad
localitation="N/A"		#localizacion del cluster
proteinName="N/A"		#nombre de la proteina
subset="N/A"			#datos de excel
flex="N/A"				#flexibilidad
flexFile="N/A"			#si con vina se le introduce un fichero de flexibilidad generado previamente, defecto no
numPoses=1				#numero de conformaciones que se genraran
CWD=${PWD}/ 			#la ruta del script para trabajar con rutas completas en los jobs y asi evitar posibles errores
job=""					#nombre del job paara indicarselo al gestor de colas
gestorJob=""			#gestor de colas utilizado
comando=""				#comando para ver los jobs (squeue)
nomProteina=""			#nombre de la proteina sin extension ni ruta
nomLigando=""  			#nombre ligando sin extension ni patch
directorio=""			#directorio de la prueba
auxDir=""				#especial para qt
secuencial="N/A"		#lanzador sin enviar jobs al cluster
dynamic="N/A"			#flexibilidad dinamica necesita conexsion con el servidor 1and1
outJob="N/A"			
ultimo="N/A"			#no se usa
histograms="N/A"
TIEMPOMAXRUN=900 ##900 segundos = 15 minutos ## tiempo maximo que se hara dde una ejecucion de docking en el job (Por si se queda pillado que no fallen las demas ejecuciones)
#
#	parametros del usuario e internos
#
while (( $# )) #recorro todos los parametros y los asigno
 do
   case $1 in
   	#entrada de datos desde lanzador 
	-x|-X )  x=$2;;				#posicion centro x
	-y|-Y )  y=$2;;				#posicion centro y
	-z|-Z )  z=$2;;				#posicion centro z
	-d|-D )  directorio=$2;;	#directorio donde se guardaran los datos (no necesario)
	-p|-P )  proteina=$2;;		#fichero proteina o query 
	-l|-L )  ligando=$2;;		#directorio o fichero ligando 
	-se|-SE) secuencial=$2;;	#para ejecutar lanzador en secuencial
	-fx|-FX) flex=$2;;			#para indicarle que se queire flexibilidad (Vina)
	-fl|-FL) flexFile=$2;; 		#Indicarle el fichero de flexibilidad previamente generado(prepare_flexreceptor4.py) cuando se divide en 2 -p 3s5z_rig -fl 3s5z_flex
	-dy|DY)  dinamyc="-dy";;	#para indicarle flexibilidad dinamica, tiene que existir un BD previamente (Vina)
	-dd|DD)  dirDinamyc=$2;;	#para indicarle el directorio de la flex (Se usa con dy)
	-fi|-FI) ficheroBD=$2 ;;    #fichero de flexibilidad
	-n |-N )  numPoses=$2 ;;	#numero de poses para la prueba defecto (1)	
	-a| -a)  t=$2;;  			##tamaño para el lado cuando se ahce BDC
	-hi|-HI) histograms=$2;; 		##generar histograma despues de lanzador
	-o|-O )  opcion=`echo $2 | awk '{print toupper($0)}'`;;			#bd | BDVS | BDC |o vs
	-s|-S )  programa=`echo $2 | awk '{print toupper($0)}'`;;		 #programa a utilizar
	-pr|-PR) disease=$2;;		#enfermedad para subir al excell, si esta vacio no se sube
	-pn|-PN) proteinName=$2;;	#nombre de la proteina para subir al excell
	-j|-J )  numPorJobs=$2;;	#numero de job o en docking mutacion numero de distancia
##	-ti|-TI) timpoJob=$2;;		~futuro calcular el tiempo del job dependiendo del programa
	##uso interno
	-np|NP)  nomProteina=$2;;	 #nombre de la proteina a secas
	-g|-G )  gestorColas=$2;;	 #gestor de colas
	-o|-O )  opcion=$2;;		 #BD o VS
	-i|-I )  confAdicionalI=$2;; ##campos adicionales para VS o BD
	-f|-F )	 confAdicionalF=$2;; ##campos adicionales para VS o BD
	-c|-C )  CWD=$2;;			 ##ruta donde se ejcuta
	-nj|-NJ) NombreJob=$2;;		 #nombre del job identificativo
	-cm|-CM) comando=$2;;		 #comando que se usa en el cluster
	-ul|-UL) ultimo=$2;;		 #si se le pasa el -ul quiere decir que es el ultimo job
	-na|-NA) numAminoacido=$2;;  #num de aminoacido de la prot
	-ch|-CH) chain=$2;;			 #cadena de la proteina
	
	#scriptDocking
	-e|-e   ) numeroEjecucion=$2;;  #numero de la ejecucion, util para BD (el numeor de atomo no puede ser poe que algun pdb esta mal y pueden tener 2 carbonos alfas con el mismo num atomo)
	-nl|-NL ) nomLigando=$2;;		#nombre del liagndo
	-t|-T   ) ligVS=$2;;			#vs nombre del ligando
	-in|-IN ) ini=$2;;			    #iniicio para VS de los ficheros
	-project) project=$2;;  		#nombre del proyecto para el gesto de colas
	-fn|-FN ) fin=$2;;      		#final para VS de los ficheros
	-sa|SA  ) outJob=$2;;   		#nombre con el que se guardaran las salidas del job


	-ex|-EX ) nExp=$2;;
	-em|-EM ) destinatario=$2;;
	
	-th|-TH ) umbral=$2;;			# valor minimo de similaridad a partir del cual se filtra
	-jd|-JD ) jobDescription=$2;;		# texto descriptivo del job para incluir en el correo

	-h|-H ) if [ -n $2 ];then
				ayuda y
			else
			 	ayuda 
			fi
			;;
    esac
  shift #Modificamos $2 a $1, $3 a $2 ....
done 

 # scriptDocking Comando
# while (( $# )) #recorro todos los parametros y los asigno
# do
#   case $1 in	
 #  	-p|-P ) proteina=$2;; #parametro -p proteina
 #  	-l|-L ) ligando=$2;;  #parametro -l ligando
	# -x|-X ) x=$2;;
	# -y|-Y ) y=$2;;
	# -z|-Z ) z=$2;;
	# -d|-D ) directorio=$2;;
	# -o|-O ) opcion=`echo $2 |tr [:lower:] [:upper:]`;;			#bd o vs
	# -s|-S ) programa=`echo $2 |tr [:lower:] [:upper:]`;;  		#programa a utilizar
	# -e|-e ) numeroEjecucion=$2;;
	# -nl|-NL) nomLigando=$2;;
	# -np|-NP)nomProteina=$2;;
	# -c|-C) CWD=$2;;
	# -t|-T ) ligVS=$2;;
	# -cm|-CM) comando=$2;;
	# -ul|-UL) ultimo="-ul";;
	# -nj|-NJ) nombreJob=$2;; #nombre expecifico del job
	# -na|-NA)numAminoacido=$2;;
	# -fx|-FX)flex="-fx";;
	# -ch|-CH)chain=$2;;
	
	# -in|-IN)ini=$2;;
	# -fi|-FI)fin=$2;;

 #     esac
 #  shift
 #done
