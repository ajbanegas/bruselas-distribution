#!/bin/bash
ayuda(){
	echo "ScriptVirtualScreeningSM:"
	$CWD""scriptsDocking/ayuda.sh
	exit
}

######################################################
#	dependiendo del tipo de cluster "las colas implementads" usa unos parametros u otros
######################################################
while (( $# )) #recorro todos los parametros y los asigno
 do
   case $1 in
   	-p|-P ) proteina=$2;; #parametro -p proteina
	-l|-L ) ligando=$2;;  #parametro -l ligando
	-x|-X ) x=$2;;
	-y|-Y ) y=$2;;
	-z|-Z ) z=$2;;
	-d|-D ) directorio=$2;;
	-s|-S ) programa=`echo $2 |tr [:lower:] [:upper:]`;;
	-np|NP) nomProteina=$2;;
	-i|-I ) ini=$2;;
	-f|-F ) fin=$2;;
	-c|-C ) CWD=$2;;
	-o|-O ) opcion=$2;;
 	-cm|-CM) comando=$2;;  #coamdno del cluseter
	-nj|-NJ) nombreJob=$2;; #nombre expecifico del job
	-ul|-UL) ultimo="-ul";;
	-fx|-FX) flex="-fx";;
	-h|-H ) ayuda ;;
     esac
  shift #Modificamos $2 a $1, $3 a $2 ....
done

if  [ -z "$proteina"  ] || [ -z "$ligando" ] || [ -z $x ] || [ -z $y ] || [ -z $z ] || [ -z $programa ] || [ -z $directorio ];then
	ayuda
fi
nomLigando=""
extension="."${proteina##*.} #se coje la extension segun la proteina de entrada
 $CWD""scriptsDocking/scriptDockingComando.sh  -d $directorio -s $programa -o $opcion -l $ligando -p $proteina -x $x -y $y -z $z -np $nomProteina -nl $nomLigandoSalida -c $CWD -t $ligando -cm $comando -nj $nombreJob	-n ${numPoses} -fl ${flexFile} $ult $flex -in $ini -fi $fin