#!/usr/bin/env python
# -*- coding: utf-8 -*- 

########
#
#
#####
from os import listdir
import os,sys,string,re

ll=() #vector con las coordenadas x,y,z
valoresCoordenadas=[0,0,0]
nAtom=0;
extension=".pdbqt"

###############################################################
# Dependiendo si es fichero pdb/pdbqt o mol las columanas son diferentes
################################################################
def fucniongetCom():
	global nAtom;
	if extension == ".mol2":  #los ficheros mol 2 tienen  las cordenadas en 2 3 4
		if contadorDeModelo== 1 and linea.find("@<TRIPOS>ATOM")==-1:
			nAtom+=1;
			ll=linea.split( );
		#	print str(ll[2])+"   "+str(ll[3])+"    "+str(ll[4])
			valoresCoordenadas[0]+=float (ll[2]);
			valoresCoordenadas[1]+=float(ll[3]);
			valoresCoordenadas[2]+=float(ll[4]);
	else:
		if contadorDeModelo==1 and ('ATOM ' in linea or 'HETATM ' in linea ) : ##5, 6, 7 son la poscion de xyz del atomo	
			nAtom+=1;
			x=linea[30:38]
			y=linea[38:46]
			z=linea[46:54]
			#sprint x +" " +y +" "+z

			#print y
			#print z
			valoresCoordenadas[0]+=float(x);
			valoresCoordenadas[1]+=float(y);
			valoresCoordenadas[2]+=float(z);
			#print  str(valoresCoordenadas[0]) + " "+str(valoresCoordenadas[1]) +" "+str(valoresCoordenadas[2] )


###############################################################
# almacena los valores leidos del fichero x y z "Los va sumando"
################################################################
def getCOM(): ##
	global nAtom;
	nAtom+=1;
	valoresCoordenadas[0]+=float (ll[5]);
	valoresCoordenadas[1]+=float(ll[6]);
	valoresCoordenadas[2]+=float(ll[7]);


##############################################################
#	recoger parametros
##############################################################
narg=len(sys.argv)
if narg != 8:
	print "escript getCOM.py"
	print "Se especificar los datos de entrada correctamante directorio energia ligando numEjecucion numVina(0) numAminoacidos chain";
#	print "directorio "+sys.argv[1]; 
#	print "energia "+sys.argv[2];
#	print "ligando "+sys.argv[3];
#	print "numEjecucion "+sys.argv[4];
	exit();

#if not os.path.exists(sys.argv[1]): 
#	print "Se debe especificar un directorio donde se encuentren los ligandos";
#	exit();
#1 (fichero del ligando docking)
#2 energia
#3 ligando original
#4 num Ejecucion
#5 numVina (0)
#6 numAminoacidos
# chain
directorio=sys.argv[1];
datos=[ sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5],sys.argv[6],sys.argv[7] ];

if not os.path.exists(directorio+extension): ## si no existe el pdbqt quiere decir que usamos pqt
	extension=".pdb"
if not os.path.exists(directorio+extension): ## si no existe el pdbqt quiere decir que usamos mol
	extension=".mol2"
directorio=directorio+extension



f=open(directorio);
contadorDeModelo=0;
for linea in f:
	#cadena exacta len(re.findall('\\bROOT\\b', linea))>0
	if  "ENDMDL" in linea or "MODEL" in linea or len(re.findall('\\bROOT\\b', linea))>0 or "TORSDOF" in linea or "@<TRIPOS>ATOM" in linea or "@<TRIPOS>BOND" in linea:
		contadorDeModelo+=1;
	linea=linea[:-1]
	fucniongetCom()
f.close()


valoresCoordenadas[0] /= nAtom;
valoresCoordenadas[1] /= nAtom;
valoresCoordenadas[2] /= nAtom;

#valoresCoordenadas[0]=round(valoresCoordenadas[0],3);
#valoresCoordenadas[1]=round(valoresCoordenadas[1],3);
#valoresCoordenadas[2]=round(valoresCoordenadas[2],3);
directorio=directorio.replace(extension, ".txt");
#print datos[5]
fila=datos[0]+" "+str(valoresCoordenadas[0])+" "+str(valoresCoordenadas[1])+" "+str(valoresCoordenadas[2])+" "+ datos[1]+ " "+datos[2]+" "+datos[3]+" "+datos[4]+" "+datos[5];
if os.path.exists(directorio):
	os.remove(directorio)
f = open(directorio, 'w');
f.write(fila+"\n");
f.write("#enrgia corX cordY cordZ ligando nºEjecucion nºresultadoVINALF nº aminoacdio(Solo vina) chain\n");
f.close();


