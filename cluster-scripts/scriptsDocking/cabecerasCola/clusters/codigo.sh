echo "echo \"start job\" 1>&2" 		>>$nomJob
echo "date +\"%s   %c\" 1>&2"		>>$nomJob
echo "opcion=${opcion}" 		>>$nomJob
echo "if [ $opcion == \"VS\" ];then" 	>>$nomJob      #VS usa scriptVirutalScreening que necesita el numero de ligando de inicio y de final los ligandos deben estar numerodaso lig_1.mol2 lig_2.mol2
echo "      ${CWD}scriptsDocking/scriptVS.sh -c ${CWD} -d ${directorio} -s ${programa} -p ${proteina} -l ${ligando} -x ${x} -y ${y} -z ${z}  -np ${nomProteina} -o ${opcion} -in ${confAdicionalI} -fn ${confAdicionalF} -cm ${comando} -n ${numPoses} -fl ${flexFile} -nj ${NombreJob} ${ultimo} -fx ${flex} -th ${umbral}">>$nomJob
echo "elif [ $opcion == \"BD\" ] || [ $opcion == \"BDC\" ] || [ $opcion == \"SD\" ] ;then">>$nomJob     ##BD utiliza el nombre del ligando y el numero de ejecucion
echo "      ${CWD}scriptsDocking/scriptBD.sh -c ${CWD} -d ${directorio} -s ${programa} -p ${proteina} -l ${ligando} -x ${x} -y ${y} -z ${z}  -np ${nomProteina}  -o ${opcion} -in ${confAdicionalI} -fn ${confAdicionalF}  -cm ${comando}  -nj ${NombreJob} -n ${numPoses} -fl ${flexFile} ${ultimo} -fx ${flex} -na ${numAminoacido} -ch ${chain} -nl ${nomLigando}">>$nomJob
echo "elif [ $opcion == \"BDVS\" ];then"	>>$nomJob 
echo "      ${CWD}scriptsDocking/scriptVirutalScreeningBDVS.sh -c ${CWD} -d ${directorio} -s ${programa} -p ${proteina} -l ${ligando} -x ${x} -y ${y} -z ${z}  -np ${nomProteina}  -o ${opcion} -in ${confAdicionalI} -fn ${confAdicionalF} -cm ${comando} -nj ${NombreJob} -n ${numPoses} -fl ${flexFile} ${ultimo} -fx ${flex}">>$nomJob
echo "elif [ $opcion == \"QT\" ];then"		>>$nomJob 
echo "      ${CWD}scriptsDocking/scriptDockingComando.sh -c ${CWD} -d ${directorio} -s ${programa} -p ${proteina} -l ${ligando} -x ${x} -y ${y} -z ${z}  -np ${nomProteina}  -o ${opcion} -e ${confAdicionalI} -nl ${confAdicionalF}  -cm ${comando}  -nj ${NombreJob} -n ${numPoses} -fl ${flexFile} ${ultimo} -fx ${flex} -na ${numAminoacido} -ch ${chain}">>$nomJob
echo "else"			>>$nomJob
echo "      ${CWD}scriptsDocking/scriptVirutalScreeningBDVS.sh -c ${CWD} -d ${directorio} -s ${programa} -p ${proteina} -l ${ligando} -x ${x} -y ${y} -z ${z}  -np ${nomProteina}   -o ${opcion} -in ${confAdicionalI} -fn ${confAdicionalF} -cm ${comando} -nj ${NombreJob} -n ${numPoses} -fl  ${flexFile} ${ultimo} -fx ${flex}">>$nomJob
echo "fi"			>>$nomJob
echo "echo \"end job\" 1>&2" 	>>$nomJob
echo "date +\"%s   %c\" 1>&2"	>>$nomJob
echo "rm $nomJob" 		>>$nomJob
