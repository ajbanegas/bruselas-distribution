echo "#!/bin/bash" 						> $nomJob
echo "#$ -cwd"							>>$nomJob
echo "#$ -o ${directorio}/out/"			>>$nomJob
echo "#$ -e ${directorio}/out/"			>>$nomJob
echo "#$ -N $NombreJob"					>>$nomJob
echo "#$ -l h_rt=20:30:00"				>>$nomJob
echo "#$ -S /bin/bash"					>>$nomJob
echo "#$ -pe orte $cpus"                >>$nomJob
echo "#$ -r y"                          >>$nomJob
source ${CWD}/scriptsDocking/cabecerasCola/clusters/codigo.sh
execute="qsub" #coamndo de ejecucion
