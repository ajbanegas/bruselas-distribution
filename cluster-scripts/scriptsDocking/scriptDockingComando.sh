#!/bin/bash

module load libglu/9.0.0

ayuda() { 
	echo "scriptGenerarGrid.sh"
	$CWD""scriptsDocking/ayuda.sh
	exit
}
#
#	Main
#
source ${2}/scriptsDocking/parameters.sh
re='^[0-9]' ##todos los scores comienzan por un numero si esto no es cierto no se generaran los txt (no es el mejor criterio)
salida=$directorio$numeroEjecucion$opcion"-"$programa"-"$nomProteina-$nomLigando-$x-$y-$z
extension="."${proteina##*.} #se coje la extension segun la proteina de entrada ESTA REPETIDO EN EL CODIGO
source ${CWD}scriptsDocking/scriptsDock/script${programa}.sh
ejecutar



#if [ ! -z $numeroEjecucion ];then #si numero de ejecucion esta vacio le añadimos un guion para los nombres
#	numeroEjecucion=$numeroEjecucion"-"
#fi
#lastChar=$(expr substr $directorio $(expr length $directorio) 1)
##si el usuario introduce la ruta sin la /
#if [ $lastChar != "/" ];then
#	directorio=$directorio"/"
#fi
#echo $flex
#funcionGetResults
#echo el programa es: $programa 
#exit
