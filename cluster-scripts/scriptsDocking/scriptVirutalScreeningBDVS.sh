#!/bin/bash
ayuda(){
	echo "ScriptVirtualScreeningSM:"
	$CWD""scriptsDocking/ayuda.sh
	exit
}
lig=""
leerDatosLig()
{
	nLin=` expr $resto`
	
	linea=`sed -n ${nLin}p ${proteina}.Tmp` 
	x=`echo $linea | cut -d\: -f1`
	y=`echo $linea | cut -d\: -f2`
	z=`echo $linea | cut -d\: -f3`
	numAminoacdo=`echo $linea | cut -d\: -f4`
	chain=`echo $linea | cut -d\: -f5`
	nomLigando=${lig##$CWD}		##nombre del ligando sin la ruta del directorio de ejecución
	nomLigandoSalida=$ligando	#el ligando salida es solo el nombre de ligando sin extension y sin directorio
	d=`dirname $nomLigando`/	#quitamos directorio
	nomLigando=${nomLigando#$d}
	#nomLigando=${nomLigando}
}
functionBucle()
{
	SALIR=0
	lig=$2

	final=$3
	if [ $1 -eq 0 ];then
		aux=1	
	else
		aux=`expr $1 + 1`
	fi
	while [  $SALIR == 0 ]	
	do

		if [ $aux == $final ];then #el final al ser un mod da 0
			resto=$final
			resto=`expr $resto % $atomsProtCA` 
			if [ $resto -eq 0 ] ;then
				resto=$atomsProtCA
			fi
			
		else
			resto=$aux
			resto=`expr $resto % $atomsProtCA` 
		fi
		if [ "$resto" == "0" ] || [ $aux  -gt $final ];then #cada vez que salgo me he pasado un ligando o el numero de Vs maximo 
			SALIR=1
			break;
		fi
		leerDatosLig
		#if [ "$lig" == "ligandos/10-lig-drunagbak/fda_lig_3.pdbqt" ];then
		${CWD}scriptsDocking/scriptDockingComando.sh -c ${CWD} -o $opcion -d $directorio -s $programa -l ${lig} -e  ${aux}	-nl $nomLigando	-p $proteina -x $x -y $y -z $z 	-fx {flex} -np $nomProteina -n ${numPoses} -fl ${flexFile}
		#fi
		aux=`expr $aux + 1` 


		
	done
}


source ${2}/scriptsDocking/parameters.sh

extension="."${proteina##*.} #se coje la extension segun la proteina de entrada
atomsProtCA=`cat $proteina |grep CA |wc -l` #contar atomos CAs

encontrarLig()
{
	contador=1;
	for lig in $(ls ${ligando}*${extension})
	do
		if [ $1 -eq $contador ];then
			break
		fi
		contador=`expr $contador + 1`
	done
}
ligandoIni=`expr $ini / $atomsProtCA`
ligandoIni=`expr $ligandoIni + 1` 
if [ `expr $fin % $atomsProtCA` == 0 ];then 
	ligandoFin=$ligandoIni
else
	ligandoFin=`expr $fin / $atomsProtCA`
	ligandoFin=`expr $ligandoFin + 1` 
fi 
if [ $ligandoIni -eq $ligandoFin ];then
		encontrarLig "$ligandoIni"
		functionBucle $ini $lig $fin
else
	encontrarLig "$ligandoIni"
	finAux=`expr $ligandoIni \* $atomsProtCA`
	functionBucle $ini $lig $finAux
	encontrarLig "$ligandoFin"
	functionBucle $finAux $lig $fin
fi
