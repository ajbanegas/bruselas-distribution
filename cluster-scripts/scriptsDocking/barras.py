#!/usr/bin/env python
# a bar plot with errorbars
import numpy as np
import matplotlib.pyplot as plt

N = 4
LJ=(-6.22962, -5.59167 ,-3.16263, -1.70055)
Solvation=(-9.23839, -10.36018 ,-6.53890, -6.20007)
Hbonds=(-0.018566, 1.45645, -0.49803, 0.48422)
Elect=(-0.738165, 0.00825, -0.82025, -0.37952)
Internal=(5.73411, 0.22742, 0.65770, 0.03119)
Rots=(2.8224, 0.82320, 0.58800, 0.11760)
dG =(-7.66, -13.42, -9.76, -8.09)
# menStd =   (2, 3, 4, 1, 2)

ind = np.arange(N)  # the x locations for the groups
width = 0.1       # the width of the bars

fig = plt.figure()
ax = fig.add_subplot(111)

rects1 = ax.bar(ind, LJ, width, color='b')
rects2 = ax.bar(ind+width, Solvation, width, color='g')
rects3 = ax.bar(ind+2*width, Hbonds, width, color='r')
rects4 = ax.bar(ind+3*width, Elect, width, color='c')
rects5 = ax.bar(ind+4*width, Internal, width, color='m')
rects6 = ax.bar(ind+5*width, Rots, width, color='y')
rects7 = ax.bar(ind+6*width, dG, width, color='k')

# add some
ax.set_ylabel('Contribution (Kcal/mol)')
ax.set_title('Energetic contributions to binding energy')
ax.set_xticks(ind+width+3.5*width)
ax.set_xticklabels( ('HMGCoA', 'Lycopene', 'Chlorogenic acid', 'Naringenin') )
ax.set_xlim(-0.2,4)
ax.set_ylim(-15,7)

# ax.legend( (rects1[0], rects2[0], rects3[0], rects4[0], rects5[0], rects6[0], rects7[0]), ('LJ', 'Solvation', 'Hbonds','Elect','Internal','Rots','dG'))
# 

plt.savefig('Fig_histogram.png',dpi=600)
plt.show()

