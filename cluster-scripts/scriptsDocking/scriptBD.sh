#!/bin/bash
ayuda(){
	echo "scriptBD:"
	${CWD}scriptsDocking/ayuda.sh
	exit
}
source ${2}/scriptsDocking/parameters.sh
coordsAux=`python ${CWD}scriptLanzador/standarFileCoords.py $proteina |grep -v "##" |grep CA`
coords=($coordsAux)
final=`expr $fin - 1`
for i in `seq $ini $final`;
do
	line=${coords[i]}
    x=`echo $line | cut -d\: -f1`
	y=`echo $line | cut -d\: -f2`
	z=`echo $line | cut -d\: -f3`
	numAminoacdo=`echo $line | cut -d\: -f7`
	chain=`echo $line | cut -d\: -f8`
	timeout $TIEMPOMAXRUN ${CWD}scriptsDocking/scriptDockingComando.sh  -c $CWD -d $directorio -s $programa -o $opcion -l $ligando -p $proteina -x $x -y $y -z $z -np $nomProteina -nl $nomLigando  -t $ligando -cm $comando -nj $nombreJob -fx $flex -in $ini -fi $fin -n ${numPoses} -fl ${flexFile} -na ${numAminoacdo} -ch ${chain} -e ${i}-
done  


