#!/usr/bin/env python
import os, sys, re, string, array, linecache, math

# input parameters
# -receptor name (must be in the same directory)
# -first ligand (crystal ligand)
# -second ligand (specified pose)
# -file with surface screening results

# TODO
# -print on screen Emin, Emax
# -color bar

narg=len(sys.argv)
#print narg
#print "@length arg= ", narg
if narg != 7:
	print "@Usage: receptor_file crystal_lig_file pose_lig_file surface_screening_results_file pymol_output_session output_picture" 
	print "it uses internally only negative affinity values; positive ones are discarded"
	sys.exit()

rec=sys.argv[1]
crys_lig=sys.argv[2]
pose_lig=sys.argv[3]
db = sys.argv[4]
pymol_session=sys.argv[5]
output_picture=sys.argv[6] 

db_file = open(db,"r")
cont=0

for line in db_file:
		ll=string.split(line)
		#print ll[0]
		if float(ll[0]) <= -0:
			#print ll
			print "pseudoatom BEAD"+str(cont)+str(ll[5])+", pos=["+ll[1]+", "+ll[2]+", "+ll[3]+"], b="+ll[0]
			print 'cmd.show("spheres","BEAD'+str(cont)+'")'
			cont=cont+1
		
	
		
print "zoom *,10"
print "spectrum b"

######### RECEPTOR 
#load input receptor
print'load '+str(rec)
rec_label= rec.replace(".mol2", "")
print 'cmd.hide("everything","'+str(rec_label)+'")'
#print 'cmd.show("surface"   ,"2BYR_rec")'
#print 'cmd.show("surface"   ,"1AZX_1_jun09")'
#print "cmd.set('transparency',0.20);"
#print "cmd.disable('1AZX_1_jun09')"
#print "cmd.enable('1AZX_1_jun09',1)"
#cmd.disable('1AZX_1_jun09')

######### CRYSTAL LIG 
print "load "+str(crys_lig)
crys_label= crys_lig.replace(".mol2", "")
print 'cmd.show( "sticks","'+str(crys_label)+'"  )  '

######### POSE LIG 
print "load "+str(pose_lig)
pose_label= pose_lig.replace(".mol2_poses", "")
print 'cmd.show( "sticks","'+str(pose_label)+'"  )  '
print "select allbeads,BEAD*"
print "alter allbeads,vdw=1.2"
print "rebuild"
print "cmd.disable('allbeads')"


######### POSE LIG 

######### SET VIEW

print 'set_view (\
     0.325062990,   -0.904721379,    0.275364190,\
     0.353790134,    0.386372656,    0.851799786,\
    -0.877024949,   -0.179465488,    0.445675373,\
    -0.000044644,    0.000088021, -244.072509766,\
    61.547016144,  104.428787231,  116.484710693,\
    50.418960571,  437.784667969,    0.000000000 ) '

######### SAVE PIC 
#print 'ray 1024,768'
#print 'png '+str(output_picture)+'.png'


######### SAVE PYMOL SESSION

#print 'save '+str(pymol_session)+'.pse'


