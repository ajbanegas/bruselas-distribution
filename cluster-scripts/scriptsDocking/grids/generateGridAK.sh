#_____________________________________________________________________________________________________________
#
#	Genera fichero de grid, la grid y el fichero de docking para autodock4
#__________________________________________________________________________________________________________
CWD=$1
proteina=$2
ligando=$3
salida=$4
x=$5
y=$6
z=$7

##___________________________________________________________________________________________
##
##		Fichero Grid
##_____________________________________________________________________________________________
typeAtomsProt=`cat ${CWD}${proteina} |awk '{print  substr($0,78)}' |grep -v Type |grep -v ^_ |sort -V -u |sed '/^$/d' |tr '\n' ' '`  ##busca los typos de atomos de la prot
#cat ${CWD}${proteina} |awk '{print  substr($0,78)}' |grep -v Type |grep -v ^_ |sort -V -u |sed '/^$/d' |tr '\n' ' '
typeAtomsLig=`cat ${CWD}${ligando}  |awk '{print  substr($0,78)}' |grep -v Type |grep -v ^_ |sort -V -u |sed '/^$/d' |tr '\n' ' '` 	##busca tipos de atomos del lig


#echo "typeLongs" $typeAtomsLig
echo "npts 150 150 150                        # num.grid points in xyz"   					> ${salida}Grid.gpf
echo "gridfld ${salida}.maps.fld           # grid_data_file" 							>>	${salida}Grid.gpf
echo "spacing 0.375                        # spacing(A)	"								>>	${salida}Grid.gpf
echo "receptor_types ${typeAtomsProt}       # receptor atom types"						>>	${salida}Grid.gpf
echo "ligand_types ${typeAtomsLig}         # ligand atom types"							>>	${salida}Grid.gpf
echo "receptor ${CWD}${proteina}                 # macromolecule"								>>	${salida}Grid.gpf
echo "gridcenter ${x} ${y} ${z}            # xyz-coordinates or auto "					>>	${salida}Grid.gpf
echo "smooth 0.5                           # store minimum energy w/in rad(A) #"		>>	${salida}Grid.gpf

arr=$(echo $typeAtomsLig | tr " " "\n")													#divido los atomosLigs
for sal in $arr																			
do
	echo "map $salida.${sal}.map                    # atom-specific affinity map	" 	>>	${salida}Grid.gpf
done
echo "elecmap ${salida}.e.map              # electrostatic potential map "				>>	${salida}Grid.gpf
echo "dsolvmap ${salida}.d.map             # desolvation potential map "				>>	${salida}Grid.gpf
echo "dielectric -0.1465                   # <0, AD4 distance-dep.diel;>0, constant " 	>>	${salida}Grid.gpf


#_________________________________________________________________________________________________________________
#
#	Fichero de docking
#___________________________________________________________________________________________________________________
echo "autodock_parameter_version 4.2       # used by autodock to validate parameter set #	"				>  ${salida}Dock.dpf
echo "outlev 1                             # diagnostic output level						"				>> ${salida}Dock.dpf
echo "intelec                              # calculate internal electrostatics				"				>> ${salida}Dock.dpf
#echo "seed pid time                        # seeds for random generator						"				>> ${salida}Dock.dpf
echo "ligand_types ${typeAtomsLig}         # atoms types in ligand 							"				>> ${salida}Dock.dpf
echo "fld ${salida}.maps.fld           	   # grid_data_file #								"				>> ${salida}Dock.dpf
arr=$(echo $typeAtomsLig | tr " " "\n")													#divido los atomosLigs
for sal in $arr																			
do
#	echo "eno"
	echo "map $salida.${sal}.map                    # atom-specific affinity map			" 				>>	${salida}Dock.dpf
done

echo "elecmap ${salida}.e.map              # electrostatics map#							"				>> ${salida}Dock.dpf
echo "desolvmap ${salida}.d.map            # desolvation map#								"				>> ${salida}Dock.dpf
echo "move ${CWD}$ligando                  # small molecule#								"				>> ${salida}Dock.dpf
#echo "about $x $y $z 				       # small molecule centeri #igualk centro grid 	"				>> ${salida}Dock.dpf
#echo "tran0 random                         # initial coordinates/A or random				"				>> ${salida}Dock.dpf
#echo "quaternion0 random                   # initial orientation							"				>> ${salida}Dock.dpf
#echo "dihe0 random                         # initial dihedrals (relative) or random			"				>> ${salida}Dock.dpf
#echo "torsdof ${torsiones}                 # torsional degrees of freedom #torsionLigand	"				>> ${salida}Dock.dpf
echo "seed 2015 2015					   #semilla											"				>> ${salida}Dock.dpf
#echo "rmstol 2.0                           # cluster_tolerance/A 							"				>> ${salida}Dock.dpf
#echo "extnrg 1000.0                        # external grid energy							"				>> ${salida}Dock.dpf
#echo "e0max 0.0 10000                      # max initial energy; max number of retries		"				>> ${salida}Dock.dpf
echo "ga_pop_size 150                      # number of individuals in population			"				>> ${salida}Dock.dpf
echo "ga_num_evals 2500000                 # maximum number of energy evaluations			"				>> ${salida}Dock.dpf
echo "ga_num_generations 27000             # maximum number of generations 					"				>> ${salida}Dock.dpf
#echo "ga_elitism 1                         # number of top individuals to survive to next generation 	"	>> ${salida}Dock.dpf
#echo "ga_mutation_rate 0.02                # rate of gene mutation 							"				>> ${salida}Dock.dpf
#echo "ga_crossover_rate 0.8                # rate of crossover 								"				>> ${salida}Dock.dpf
#echo "ga_window_size 10                    # 												"				>> ${salida}Dock.dpf
#echo "ga_cauchy_alpha 0.0                  # Alpha parameter of Cauchy distribution			"				>> ${salida}Dock.dpf
#echo "ga_cauchy_beta 1.0                   # Beta parameter Cauchy distribution 			"				>> ${salida}Dock.dpf
echo "set_ga                               # set the above parameters for GA or LGA 		"				>> ${salida}Dock.dpf
#echo "sw_max_its 300                       # iterations of Solis & Wets local search 		"				>> ${salida}Dock.dpf
#echo "sw_max_succ 4                        # consecutive successes before changing rho 		"				>> ${salida}Dock.dpf
#echo "sw_max_fail 4                        # consecutive failures before changing rho 		"				>> ${salida}Dock.dpf
#echo "sw_rho 1.0                           # size of local search space to sample 			"				>> ${salida}Dock.dpf
#echo "sw_lb_rho 0.01                       # lower bound on rho 							"				>> ${salida}Dock.dpf
#echo "ls_search_freq 0.06                  # probability of performing local search on individual 		"	>> ${salida}Dock.dpf
echo "set_psw1                             # set the above pseudo-Solis & Wets parameters 	"				>> ${salida}Dock.dpf
#echo "unbound_model bound                  # state of unbound ligand 						"				>> ${salida}Dock.dpf
echo "ga_run 10                            # do this many hybrid GA-LS runs					"				>> ${salida}Dock.dpf
echo "rmstol 0.5                           # cluster_tolerance/A 							"				>> ${salida}Dock.dpf
echo "analysis                             # perform a ranked cluster analysis				"				>> ${salida}Dock.dpf





