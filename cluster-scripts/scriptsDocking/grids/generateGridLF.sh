
########################################################
#	Genera el grid para LF
########################################################
#generarGridLF() #genera la grid para leadFinder
# { 
	gridX=40
	gridY=40
	gridZ=40
	if [ ! -f $salida"grid.bin" ];then
		#datos que necesita el fichero.Par 	#grid-center=33,13,-6 	#grid-size=15,15,15
		echo "grid-center="$x","$y","$z >$salida".par"
		echo "grid-size="$gridX","$gridY","$gridZ >>$salida".par"
		$CWD""scriptsDocking/externalSw/leadFinder/leadfinder\
		--grid-only \
		--protein=$CWD$proteina \
		--save-grid=$salida"grid.bin" \
		--parameters=$salida".par"
		
		#rm $salida".par"
		
	fi
#}
