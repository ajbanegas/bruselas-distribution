#!/bin/bash
#SBATCH -J DDDDD-dockingWG.sh
#SBATCH --output=salida/DDDDD.out
#SBATCH --error=salida/DDDDD.err
#SBATCH --time=5:30:00
#SBATCH --nice=10000

###____________________________________________________________________________________________________________
###
###	Ejecuta el lanzador las veces que haga falta
###_____________________________________________________________________________________________________________
function ejecutarDocking()
{
	# se queda solo con el nombre del ligando
	for linea in $ligandos*$extension;do
		contadorLigandos=`expr $contadorLigandos + 1`
		nomLigando=$(limNomLigand $linea)
		${CWD}scriptsDocking/scriptDockingComando.sh -c $CWD -d $uid -s $programa -o $opcion -p ligandos/lig_sd/${nomLigando}.sd -l $ligando -x 0 -y 0 -z 0 -np 'pro' -nl $nomLigando -t $linea -em $destinatario -th $umbral -jd \"$jobDescription\"
		rutaEnergias=${ruta}${uidBBDD}/${expOut}.png
	done
}

###____________________________________________________________________________________________________________________
###
###	Limpiar el nombre del ligando 
###__________________________________________________________________________________________________________________
function limNomLigand()
{
	t=$1
	d=`dirname $t`/
	n=""
	if [ $d != "./" ];then
		n=${t#$d}
		n=${n%$extension}
	else
		n=${t%$extension}
	fi
	echo "$n"
}

###_____________________________________________________________________________________________________________________
###
###	Envio de correo y formatea en html 
###_____________________________________________________________________________________________________________________
function enviarCorreo()	# TODO actualizarlo
{
	limNomLigand 
	direccionWeb=$rutaResults"?idExp=$uidBBDD&email=$destinatario"
	echo "<h3>Similarity results for ligand with smiles code: $smile</h3>"> $uid"/correo.tmp"
	echo "<p>You have received this email because you submitted a job to the DIA-DB server.</p>" >> $uid"/correo.tmp"
	echo "<p>Please click the link below to access a summary of the results obtained at DIA-DB.</p>">> $uid"/correo.tmp"
	echo "<p> <a href=$direccionWeb>Similarity results $uidBBDD</a></a></p>">> $uid"/correo.tmp"
	echo "<p>Results will expire in two weeks.</p>">> $uid"/correo.tmp"
	echo "<p>In case of questions please reply directly to this email.</p>">> $uid"/correo.tmp"
	echo "<br/>">> $uid"/correo.tmp"
	echo "<br/>">> $uid"/correo.tmp"
	echo "<p>Copyright&#169; 2015 - Bioinformatics and High Performance Computing Research Group, Universidad Cat&oacute;lica San Antonio de Murcia (UCAM). All Rights Reserved
			</p>	">> $uid"/correo.tmp"
	body=`cat $uid"/correo.tmp"`
(
echo "To: $destinatario"
echo "From:Horacio Pérez-Sánchez <$remitente>"
echo "Subject: $asunto"
echo "Content-Type: text/html"
echo
echo "$body"
echo

) | /usr/sbin/sendmail -t

}

###_______________________________________________________________________________________________________________________________
###
###	Envio a la BBDD
###______________________________________________________________________________________________________________________________
function enviarBBDD()	# TODO hacerlo variable
{
#	truncate -s0 $uid"/tabla.t" #fichero vacio
	echo "Use Antonio;" >$uid"/"$uidBBDD".sql"
	for file in $uid/*".txt";do
		pruebas=`cat $file`
		energia=`echo $pruebas | cut -d' ' -f1`	
		l=$(limNomLigand $file)
		#img1=${l::-4}
		img1=$ruta$uidBBDD"/"$img1"P.png"
      		l=${l%'lig-0-0-0.txt'}
		l=${l#'VS-WG-pro-'}
		echo "INSERT into similarity values ( $uidBBDD,'$smile', '$l','$energia','$img1','','');" >>$uid"/"$uidBBDD".sql"
	done	
	echo "UPDATE experimental SET status='executed' where id_exp=$uidBBDD " >>$uid"/"$uidBBDD".sql" #final para poner el job en terminado
	$CWD""scriptCrearCarpeta.sh $uidBBDD " WG" 
}

###_______________________________________________________________________________________________________________________________-
###
###			Variables de la plantilla
###_______________________________________________________________________________________________________________________________
CWD=PPPPP
destinatario=EEEEE
smile=SSSSS
uidBBDD="DDDDD"
uid=$CWD"DDDDD"
ligando=DDDDD/ligand.sd
contadorLigandos=0
opcion="VS"
programa="WG"
ruta="../../imagenesWeb/"
rutaResults="http://bio-hpc.ucam.edu/Bruselas/web/results/public.php"
remitente="hperez@ucam.edu"
asunto="Ligand similarity results from DIA-DB"
ligandos=$CWD"ligandos/lig_sd/"
extension=".sd"
umbral=0.3		# TODO parametro
jobDescription=""	# TODO parametro
ejecutarDocking
#enviarBBDD
#enviarCorreo

#rm -r $uid                          # esto descomentar al final
