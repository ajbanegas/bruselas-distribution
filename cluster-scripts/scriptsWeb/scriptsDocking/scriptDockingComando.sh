#!/bin/bash
ayuda() { 
	echo "scriptGenerarGrid.sh"
	$CWD""scriptsDocking/ayuda.sh
	exit
}

###___________________________________________________________________________________________________________________________
###
###		funcion para hacer docking con wega
###_____________________________________________________________________________________________________________________________
funcionWG() {
	case $opcion in 
		VS)
            # se crea un fichero temporal puesto que wega no deja renombrar la segunda salida "ResultsAlignWithQuery.sd"
	        if [ ! -d $salida/ ];then
		      mkdir $salida/
	        fi
            cd $salida/
            $CWD""scriptsDocking/wega/wega -outsd $salida.sd -qsd $CWD$proteina  -sd $CWD$ligando -M 0 -TN 5 -CC true -SO2 true  > $salida".ucam" 2>> $salida".ucam" 

            mv $salida".txt" $salida".sdf"
            cat $salida".sdf" |tail -1 | awk -F"," '{print $3" '$x' '$y' '$z' '$ligando' "1}' >> $salida".txt"
            cd $directorio 
            mv $salida/ResultsAlignWithQuery.sd $salida.sd
            rm -r $salida/
        ;;			
		# energia la x la y la z siempre seran 0 el ligando y el numero de linea del resultado
		*)
			echo "El programa wega solo admite opcion -o VS"
			ayuda;;
	esac
}
###_____________________________________________________________________________________________-
###
###	El ultimo job genera los resultados
###_____________________________________________________________________________________________
funcionGetResults()
{
	if [ "$ultimo" == "-ul" ];then
		timeFin=`date +"%H"` #tiempo de inciio del bucle
		timeFin=`expr $timeFin + 5`
		timeFin=`expr $timeFin % 24`  #si se pasa de las 24 horas se hace mod
		if [ -n $comando ] && [ -n $nombreJob ];then #si la cadena contiene
			fckUser=$USER 
			#echo  "$comando -u $fckUser |grep -w $nombreJob |wc -l" 
			a=`$comando -u $fckUser |grep -w $nombreJob |wc -l`
			#echo "[ $a != 1 && `date +"%H"` != $timeFin ]"
			while [ $a != 1 ]  && [ `date +"%H"` != $timeFin ];do ##este bucle puiede tener mucho peligro el mismo
				sleep 60
				a=`$comando -u $fckUser |grep -w $nombreJob |wc -l`
			done
			python $CWD"scriptsGeneracionResultados/get_histogram_picture.py" $directorio $CWD$proteina $CWD$ligando
			
		fi
	fi
}

###MAIN
while (( $# )) #recorro todos los parametros y los asigno
 do
   case $1 in
		
  	-p|-P ) proteina=$2;; # parametro -p proteina
  	-l|-L ) ligando=$2;;  # parametro -l ligando
	-x|-X ) x=$2;;
	-y|-Y ) y=$2;;
	-z|-Z ) z=$2;;
	-d|-D ) directorio=$2;;
	-o|-O ) opcion=`echo $2 |tr [:lower:] [:upper:]`;;			# bd o vs
	-s|-S ) programa=`echo $2 |tr [:lower:] [:upper:]`;;  		# programa a utilizar
	-e|-e ) numeroEjecucion=$2;;
	-nl|-NL) nomLigando=$2;;
	-np|-NP)nomProteina=$2;;
	-c|-C) CWD=$2;;
	-t|-T ) ligVS=$2;;
	-cm|-CM) comando=$2;;
	-ul|-UL) ultimo="-ul";;
	-nj|-NJ) nombreJob=$2;;                                     # nombre expecifico del job
	-h|-H ) ayuda ;;
     esac
  shift
done

if [ ! -z $numeroEjecucion ];then
	numeroEjecucion=$numeroEjecucion"-"
fi
lastChar=$(expr substr $directorio $(expr length $directorio) 1)
##si el usuario introduce la ruta sin la /
if [ $lastChar != "/" ];then
	directorio=$directorio"/"
fi
salida=$directorio$numeroEjecucion$opcion"-"$programa"-"$nomProteina-$nomLigando-$x-$y-$z
extension="."${proteina##*.}        # se coje la extension segun la proteina de entrada

case $programa in
	WG) funcionWG;;
	*)
		echo "No eligió ningun programa valido -s ad o -s lf"
		ayuda ;;
esac