#!/usr/bin/python

import sys, pybel

def split_file(foo):
	for sd in pybel.readfile("sdf", foo):
		filename = sd.OBMol.GetTitle()
		filename = "VS-PH-" + filename + "-aligned.sdf"
		sd.write("sdf", filename, True)	# overwrite file


if (len(sys.argv) == 1):
	sys.exit(0)

files = sys.argv[1:]

for infile in files:
	split_file(infile)
