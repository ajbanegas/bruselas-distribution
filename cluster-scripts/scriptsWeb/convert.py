import os, pybel, sys

atoms = {}

def help():
	print("Usage: convert <input file> <output format> <output file>")

def get_index(symbol):
	global atoms
	if not atoms.has_key(symbol):
		atoms[symbol] = 0
	atoms[symbol] = atoms[symbol] + 1
	return atoms[symbol]

def main():
	# se validan los parametros de entrada
	if len(sys.argv) != 4:
		help()
		exit(1)

	# recuperamos los parametros
	inputfile = sys.argv[1]
	outputformat = sys.argv[2]
	outputfile = sys.argv[3]
	filename, inputformat = os.path.splitext(inputfile)
	inputformat = inputformat[1:]

	# modificamos el nombre de los atomos, si es necesario
	ob = pybel.ob
	mol = pybel.readfile(inputformat, inputfile).next()
	res = ob.OBResidue()
	for atom in ob.OBMolAtomIter(mol.OBMol):
		elem = ob.OBElementTable().GetSymbol(atom.GetAtomicNum())
		res.AddAtom(atom)
		res.SetAtomID(atom, '%s%i' % (ob.OBElementTable().GetSymbol(atom.GetAtomicNum()), get_index(elem)))

	# generamos el fichero de salida
	mol.write(outputformat, outputfile, True)

if __name__ == '__main__':
	main()
