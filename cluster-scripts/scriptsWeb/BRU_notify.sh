#!/bin/bash

################# TAREAS REALIZADAS AL TERMINAR EL ULTIMO JOB ################

module load libGLU/9.0.0

path=/srv/www/Bruselas/bruselas/
root=/home/hperez/antonio

####_______________________________________________________________________________________________
####
####    Recoger parametros de entrada y comprobar
####_______________________________________________________________________________________________
while (( $# ))                  # recorro todos los parametros y los asigno
 do
    case $1 in
	-d|-D) dir=$2;;		# carpeta de salida
        -np|-NP) id=$2;;      	# expediente
	-s|-S) programa=$2;;	# programa
     esac
  shift
done

if [ "${programa}" == "PH" ]; then
	python ${root}/scriptsWeb/normalizePH.py ${dir}*.txt
fi

if [ ! -z "${dir}" ] || [ ! -z "${id}" ]; then
	sleep 10
	# avisar al servidor
	ssh diabetes php ${path}src/Modules/Core/MessageInterceptor.php ${id} ${dir} ${programa}
fi
