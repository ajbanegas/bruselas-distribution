#!/usr/bin/python

import __main__

import sys

molecule_header = '@<TRIPOS>MOLECULE'
atoms_header = '@<TRIPOS>ATOM'
bonds_header = '@<TRIPOS>BOND'
substructures_header = '@<TRIPOS>SUBSTRUCTURE'

# carga el fichero en una lista
def load_file(foo):
	f = open(foo, 'r')
	return f.readlines()

# elimina los retornos de carro para comparar cadenas sin problemas
def remove_carriages(content):
	data = []
	for l in content:
		data.append(l.replace('\n',''))
	return data

# extrae los atomos del fragmento 2
def extract_atoms(atoms, pattern):
	atm = atoms[1:len(atoms)]
	data = [atoms[0]]
	data += [k for k in atm if pattern in k]
	return data

# asocia los identificadores de atomo del fragmento nuevos y originales
def map_atoms(atoms):
	amap = {}
	i = 1
	for a in atoms:
		atom = a.split()
		amap[atom[0]] = str(i)
		i += 1

	return amap

# crea un mapa con los antiguos y nuevos indices de cada atomo
def translate_atoms(atoms, amap):
	data = []
	for a in atoms:
		atom = a.split()
		atom[0] = amap[atom[0]]
		data.append('\t'.join(atom))

	return data

# extrae los enlaces del fragmento 2
def extract_bonds(bonds, atoms_map):
	data = [ bonds[0] ]
	i = 1
	for b in bonds[1:len(bonds)]:
		cols = b.split()
		if cols[1] in atoms_map or cols[2] in atoms_map:
			cols[0] = str(i)
			cols[1] = atoms_map[cols[1]]
			cols[2] = atoms_map[cols[2]]
			i += 1
			data.append('\t'.join(cols))

	return data

# extraer el fragmento 2 del fichero
def parse(content, pattern):
	out_data = []
	atoms = []
	bonds = []
	atoms_map = []
	# buscar cabeceras
	mol_index = content.index(molecule_header)
	atoms_index = content.index(atoms_header)
	bonds_index = content.index(bonds_header)
	try:
		subs_index = content.index(substructures_header)
	except:
		subs_index = len(content)
	# clonar cabecera
	out_data += content[mol_index:atoms_index]

	# clonar atomos y eliminar los que no tengan fragment2
	atoms = extract_atoms(content[atoms_index:bonds_index], pattern)
	atoms_map = map_atoms(atoms[1:len(atoms)])
	out_data.append(atoms[0])
	out_data += translate_atoms(atoms[1:len(atoms)], atoms_map)

	# clonar los enlaces de los atomos seleccionados
	bonds = extract_bonds(content[bonds_index:subs_index], atoms_map)
	out_data += bonds

	# modificar los contadores
	counters = content[2].split()
	counters[0] = str(len(atoms)-1)
	counters[1] = str(len(bonds)-1)
	counters[2] = '1'
	out_data[2] = '\t'.join(counters)

	return out_data

# escribe el fichero de salida
def save_output(filename, content):
	f = open(filename, 'w')
	f.write('\n'.join(content))
	f.close()

# ##### Main #####
if len(sys.argv) < 4:
        print("Usage: s3-alignment.py <ligand.mol2> <output.mol2> <pattern>")
        exit()

try:
        # procesar parametros de entrada
        ligand = sys.argv[1]
        output = sys.argv[2]
	pattern = sys.argv[3]

	fragments = load_file(ligand)
	fragments = remove_carriages(fragments)
	data = parse(fragments, pattern)

	save_output(output, data)

except Exception as e:
        print("ERROR: (file):" + sys.argv[1])
        print(e)

