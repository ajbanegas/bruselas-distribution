#!/usr/bin/python

import pybel, sys

def split_file(foo):
	for sd in pybel.readfile("sd", foo):
		filename = sd.OBMol.GetTitle()
		filename = "VS-WG-" + filename + "-aligned.sd"
		sd.write("sd", filename, True)	# overwrite file


if (len(sys.argv) == 1):
	sys.exit(0)

files = sys.argv[1:]

for infile in files:
	split_file(infile)
