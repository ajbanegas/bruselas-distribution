#!/bin/bash

declare -A programas
programas["WG"]="sd"
programas["LI"]="mol2"
programas["S3"]="ser"
programas["SF"]="mol2"
programas["OP"]="mol2"
programas["PH"]="sdf"

getExtension() {
	arg=$1
	echo ${programas[$arg]}
}

getExp() {
	ligando=$1
	exp=${ligando##*/}
	exp=`echo "${exp%.*}"`
	echo $exp
}
