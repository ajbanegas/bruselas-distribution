#!/bin/bash

ligando=""
database=""
numPorJob=5
programa=""
output=""

formatear() {
        input=$1
        format=$2
        out=$3

        if [ -z ${input} ] || [ -z ${format} ] || [ -z ${out} ]; then
                return
        fi

        if [ "${format}" == "ser" ]; then
                dir=`dirname ${input}`
                foo=`basename ${input}`
                foo=${foo%.*}
                ${CWD}scriptsDocking/externalSw/ChemAxon/JChem/bin/screen3d g ${input} -F -match 1> /dev/null
                mv ${dir}/${foo}F.ser ${out}
        else
		python ${CWD}scriptsWeb/convert.py ${input} ${format} ${out}
        fi
}

crearDB() {
	total=$1
	step=$2
	ext=${expected_format}

	# dividir la libreria en ficheros
	init=1
	db=0
	while [ ${init} -lt ${total} ]
	do
		end=$(expr $init + $step)
		obabel ${database} -f ${init} -l ${end} -o ${ext} -O ${CWD}${output}/${lig_src}/database${db}.${ext}
		#python ${CWD}scriptsWeb/renameMols.py ${CWD}${output}/${lig_src}/database${db}.${ext}	# TO BE TESTED
		init=$(expr $end + 1)
		db=$(expr $db + 1)
	done

	# crear un indice con los nombres de los ligandos porque se pierden al generar el .ser
        if [ "${programa}" == "S3" ]; then
		for file in ${CWD}${output}/${lig_src}/database*.mol2; do
			sed -n '/^@<TRIPOS>MOLECULE$/{n;p;}' $file >> ${file}.txt
		done
        fi
}

convertirLigando() {
	formatear ${ligando} ${expected_format} ${CWD}${output}/${exp}.${expected_format}
	# conversion expecial para screen3d
	if [ "${programa}" == "S3" ]; then
		formatear ${CWD}${output}/${exp}.${expected_format} ser ${CWD}${output}/${exp}.ser
		expected_format="ser"
	fi

	ligando=${output}/${exp}"."${expected_format}
}

convertirLibreriaS3() {
	for file in ${CWD}${output}/lig_ser/*.mol2
	do
		foo=`basename $file`
		foo=${foo%.*}
		formatear $file ser ${CWD}${output}/lig_ser/${foo}.ser
	done
}

####_______________________________________________________________________________________________
####
####	Recoger parametros de entrada y comprobar
####_______________________________________________________________________________________________
while (( $# ))                      # recorro todos los parametros y los asigno
 do
    case $1 in
   	-q|-Q ) ligando=$2;;            # ligando de referencia
    	-d|-D ) database=$2;;           # carpeta de ligandos a comparar (ELIMINAR ??)
	-s|-s ) programa=$2;;           # programa
    	-n|-N ) numPorJobs=$2;;         # numero de jobs a dividir la tarea
	-w|-W ) output=$2;;		# carpeta de trabajo actual
     esac
  shift
done

if  [ -z "$ligando" ] || [ -z "$database" ] || [ -z "$programa" ] || [ -z "$output" ]; then
	echo "Error al particionar los datos"
	echo "Debe introducir"
	echo "-q ligando de referencia"
        echo "-d carpeta donde se almacena los ligandos a comparar"
	echo "-s programa"
	echo "-w carpeta de trabajo"
	exit
fi

CWD=`pwd`/
source ${CWD}scriptsWeb/extensiones.sh
expected_format=`getExtension ${programa}`
lig_src="lig_${expected_format}"
exp=`getExp $ligando`
query_format="${ligando##*.}"
lib_format="${database##*.}"

total=$(obabel ${database} -onul -v 'FFFFF' 2>&1 | cut -d" " -f1 2> /dev/null | tail -1)
step=$((total / numPorJobs))

# parche para screen3D
if [ "${programa}" == "S3" ]; then
	expected_format="mol2"
fi

# particionamos la base de datos
crearDB ${total} ${step}
# generamos el ligando en el formato requerido por el programa
convertirLigando
# si usamos S3 hay que convertir tambien la libreria
if [ "${programa}" == "S3" ]; then
	convertirLibreriaS3
fi
