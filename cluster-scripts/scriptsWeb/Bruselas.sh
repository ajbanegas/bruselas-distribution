#!/bin/bash

declare -A programas
programas["WG"]="sd"
programas["LI"]="mol2"
programas["S3"]="ser"
programas["SF"]="mol2"
programas["OP"]="mol2"
programas["PH"]="sdf"

ligando=""
database=""
opcion=""
numPorJob=5
output=""
destinatario=""
programa=""
jobDescription=""


valoresDefecto() 
{
	_opc=$1
	_nJobs=$2

	if [ -z $_opc ]; then
		opcion="VS"
	fi

	if [ -z $_nJobs ]; then
		numPorJobs=5
	fi
}

crearDirectorio() 
{
	lig_src="lig_"${programas["${programa}"]}
	output=${opcion}"-"${programa}"-"${exp}"-"${lig_src}"-0-0-0"

	if [ -d ${CWD}${output} ]; then
        	rm -rf ${CWD}${output}
	fi

	mkdir ${CWD}${output}
	mkdir ${CWD}${output}/${lig_src} # base de datos
}

encodeQueryName() {
	query=$1
	obabel ${query} --title $(obprop ${query} 2> /dev/null | grep "name" | awk '{print $2}' | md5sum | awk '{print $1}') -O ${query}
}

####_______________________________________________________________________________________________
####
####	Recoger parametros de entrada y comprobar
####_______________________________________________________________________________________________
while (( $# ))                      # recorro todos los parametros y los asigno
 do
    case $1 in
   	-l|-L ) ligando=$2;;            # ligando de referencia
    	-d|-D ) database=$2;;           # carpeta de ligandos a comparar (ELIMINAR ??)
	-r|-R ) output=$2;;             # carpeta donde se guardan los resultados
	-s|-s ) programa=$2;;           # programa
    	-o|-O ) opcion=$2;;             # opcion
    	-j|-J ) numPorJobs=$2;;         # numero de jobs a dividir la tarea
     esac
  shift
done

if  [ -z "$ligando" ] || [ -z "$database" ] || [ -z "$programa" ] ;then
	echo "Syntax error. Following arguments are required:"
	echo "-l Query ligand"
        echo "-d Ligand or protein database"
	echo "-s Comparison algorithm (WEGA, LiSiCA, Screen3D...)"
	exit
fi

cd /home/hperez/antonio		# para trabajar en mi carpeta local

exp=${ligando##*/}
exp=`echo "${exp%.*}"`
extension="${ligando##*.}"
CWD=${PWD}/			# la ruta del script para trabajar con rutas completas en los jobs y asi evitar posibles errores

# codificar el nombre de la query para evitar errores posteriores
encodeQueryName "${ligando}"

# valores por defecto
valoresDefecto "${opcion}" "${numPorJobs}"

# creamos el directorio de trabajo para la prueba
crearDirectorio

# dividir los datos en tareas y convertirlos al formato adecuado
${CWD}scriptsWeb/dataPartitioner.sh -q ${ligando} -d ${database} -n ${numPorJobs} -s ${programa} -w ${output}

# ejecutar lanzador
sw_ext=${programas["${programa}"]}
./lanzadorDocking.sh -p ${output}/${exp}.${sw_ext} -l ${output}/${lig_src} -s ${programa} -o ${opcion} -j ${numPorJobs} 1> ${output}/${exp}.out 2> ${output}/${exp}.err
