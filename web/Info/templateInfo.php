<style>
table, th, td, tr {
    border: 1px solid black;
    border-collapse: collapse;
    /*width:80%;*/
}
th, td {
    padding-left: 5px;
    padding-right: 5px;
    text-align: left;
}
th, caption {
    font-weight: bold;
}
</style>

<?php
require_once '../Services/impl/InfoServiceImpl.php';
$pathTemplate="../../../template/";

function printHistory($service) {
	$data = $service->history();

	echo "<table style='font-size:14px;'>";
	echo "	<caption>Last Experiments</caption>";
	echo "	<tr>";
	echo "		<th>Start Date</th>";
	echo "		<th>End Date</th>";
        echo "          <th>Similarity</th>";
        echo "          <th>Consensus</th>";
        echo "          <th>Cutoff</th>";
	echo "		<th>Requested Results</th>";
	echo "	</tr>";
	foreach($data as $record) {
		echo "<tr>";
        	echo "  <td>".$record["start_date"]."</td>";
        	echo "  <td>".$record["end_date"]."</td>";
        	echo "  <td>".$record["similarity"]."</td>";
        	echo "  <td>".$record["consensus"]."</td>";
        	echo "	<td>".$record["cutoff"]."</td>";
		echo "	<td style='text-align:right'>".$record["max_results"]."</td>";
		echo "</tr>";
	}
	echo "</table>";
}

function printWeeklyReport($service) {
        $data = $service->experimentsPerWeek();

        echo "<table style='font-size:14px;'>";
        echo "  <caption>Experiments per Week</caption>";
        echo "  <tr>";
        echo "          <th>Week</th>";
        echo "          <th>From</th>";
        echo "          <th>To</th>";
        echo "          <th>No.Experiments</th>";
        echo "  </tr>";
        foreach($data as $record) {
                echo "<tr>";
                echo "  <td style='text-align:center;'>".$record["week"]."</td>";
                echo "  <td>".$record["start_date"]."</td>";
                echo "  <td>".$record["end_date"]."</td>";
                echo "  <td style='text-align:right;'>".$record["counter"]."</td>";
                echo "</tr>";
        }
        echo "</table>";
}

$srv = new InfoServiceImpl();

echo "<div style='width:100%; display:float;'>";
echo "	<div style='width:49%;float:left;'>";
printHistory($srv);
echo "	</div>";
echo "	<div style='width:49%;float:left;'>";
printWeeklyReport($srv);
echo "	</div>";
echo "</div>";
?>
