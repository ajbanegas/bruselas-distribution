<?php
interface DrugService {
	
	// Find the compound with the given id.
	public function findDrugById($id);
	
	// Search for molecules whose names match the pattern.
	public function findDrugByName($table, $column, $pattern, $limit = 0) ;

	// Return the information of a compound regarding a given rule.
	public function findAlert($id, $code);

}
?>