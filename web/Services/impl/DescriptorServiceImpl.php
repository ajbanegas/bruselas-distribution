<?php
require_once __DIR__.'/../../Database/conexion.php';
require_once __DIR__.'/../DescriptorService.php';
require_once __DIR__.'/../DescriptorReturn.php';


class DescriptorServiceImpl implements DescriptorService {

	// Return the value of descriptor from a conformer.
	public function findByDrugAndCode($id_drug, $id_conf, $descriptor) {
		$res = query("select `$descriptor` from v_descriptors_dragon v join drug_conformation dc using(drug_conformation_id) where dc.drug_id=$id_drug and dc.seq_no=$id_conf");
		$row = fetch_array($res);
		return $row[$descriptor];
	}
	
	// Search for the real code of a descriptor based on its alias.
	public function findCodeByKeyCode($key_code) {
		$res = query("select code from descriptor where binary key_code='$key_code'");
		while ($fila = fetch_array($res)) {
			return $fila["code"];
		}
		return null;
	}

	// Search for the alias of a decriptor, based on its real name.
	public function findKeyCodeByCode($code) {
		$res = query("select key_code from descriptor where binary code='$code'");
		while ($fila = fetch_array($res)) {
			return $fila["key_code"];
		}
		return null;
	}
	
	// List all available descriptors linked to a given software.
	public function findBySoftware($software_id) {
		$res = query("select * from descriptor where software_id=$software_id order by description");
		
		$mapper = array();
		while ($fila = fetch_array($res)) {
			$obj = new DescriptorReturn();
			$obj->id = $fila["descriptor_id"];
			$obj->software = $fila["software_id"];
			$obj->code = $fila["code"];
			$obj->description = $fila["description"];
			$obj->proposed = $fila["proposed"];
			$obj->keyCode = $fila["key_code"];

			array_push($mapper, $obj);
		}
		return $mapper;
	}	
	
	// List all default descriptors
	public function findCodeDefault() { 
		$res = query("select code from descriptor where proposed=1");
		while ($fila = fetch_array($res)) {
			return $fila["code"];
		}
		return null;
	}

}
?>