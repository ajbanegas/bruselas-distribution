<?php
require_once __DIR__.'/../MailService.php';
require_once __DIR__.'/../../../bruselas/autoload.php';

use UCAM\BioHpc\Bruselas as Bruselas;

class MailServiceImpl implements MailService {

	public function sendMail($to, $subject, $body) {
		$message = new Bruselas\Message("", $to, $subject, $body);

		$sender = new Bruselas\SMTPNotifier();
		$sender->notifyUser($message);
	}

}
?>
