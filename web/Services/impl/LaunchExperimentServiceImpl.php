<?php
require_once __DIR__.'/../../../bruselas/autoload.php';
require_once __DIR__.'/../../Database/conexion.php';
require_once __DIR__.'/ExperimentServiceImpl.php';
require_once __DIR__.'/MailServiceImpl.php';
require_once __DIR__.'/MailBuilder.php';
require_once __DIR__.'/../ExperimentBuilder.php';
require_once __DIR__.'/../LaunchExperimentService.php';
require_once __DIR__.'/Exec.php';
require_once __DIR__.'/../PendingExperiment.php';


use UCAM\BioHpc\Bruselas as Bruselas;

/**
 * Search for pending experiments and launch them.
 */
class LaunchExperimentServiceImpl implements LaunchExperimentService {
	// services
	private $__expSrv;
	private $__mailSrv;

	function __construct() {
		$this->__expSrv = new ExperimentServiceImpl();
		$this->__mailSrv = new MailServiceImpl();
	}

	// Init the screening process.
	public function start() {
		// search for pending experiments
		$pending = $this->__listPendingExperiments();
		if ( count($pending) == 0) {
			return;
		}

		// firstly, make a dictionary of ($user => $list_of_experiments)
		// the dictionary is alphabetically sorted by key (user)
		$todo = $this->__buildMap( $pending );
		ksort($todo);

		// secondly, take the following user after the previous one (if any)
		// then take the first task of the user
		$lastUser = Bruselas\SystemConfig::get( "last_user" );
		$taskList = $this->__getTaskList( $todo, $lastUser );
		$task = $taskList[0];

		// mark the task as 'in_progress' to avoid double processing
		$this->__markAsProcessing( $task->experimentId );

		// convert it to a Bruselas\Experiment instance
		$params = json_decode( $task->value );
		$exp = $this->__createExperiment( $task->experimentId, $params );

		// save the user to take another one next time (if necessary)
		Bruselas\SystemConfig::set( "last_user", $params->email );

		// launch experiment on the cluster
		$this->__setStartDate( $exp );
		$this->__launchExperiment( $exp );
		sleep( 5 );

		exit(0);
	}

	// List all pending experiments
	private function __listPendingExperiments() {
		$list = array();
		$res = query( "select ep.value, e.experiment_id from experiment e join experiment_param ep using(experiment_id) where e.state='queued' and e.enabled=1 order by experiment_id" );
		if ( $res === FALSE ) {
			throw new Bruselas\UnknownError( "Error while searching for pending experiments" );
		}
		while ( $row = fetch_array( $res ) ) {
			$obj = new PendingExperiment();
			$obj->value = $row["value"];
			$obj->experimentId = $row["experiment_id"];
			array_push( $list, $obj );
		}
		return $list;
	}

	// Build a map of ($user => array($experiments))
	private function __buildMap($list) {
		$todo = array();
		array_walk($list, function($exp) use(&$todo) {
			$params = json_decode($exp->value);
			$user = $params->email;

			if (!array_key_exists($user, $todo)) {
				$todo[$user] = array($exp);
			} else {
				array_push($todo[$user], $exp);
			}
		});
		return $todo;		
	}

	// Return the list of tasks pending for the user stored
	// in the next entry following the given one
	private function __getTaskList( $todo, $lastUser ) {
		$dic = $todo;

		// if user not found, include it to find the following and sort again
		if ( ! array_key_exists( $lastUser, $dic ) ) {
			$dic[ $lastUser ] = "";
			ksort( $dic );
		}
		
		// if not, then find it and return the following
		$users = array_keys( $dic );
		$index = (array_search( $lastUser, $users ) + 1) % count( $users );
		return $dic[ $users[ $index ] ];
	}

	// Build an instance of Experiment from the input parameters
	private function __createExperiment( $id, $params ) {
		try {
        	return $this->__toExperiment($params, $id);
        } catch (Exception $e) {
            Bruselas\Log::error("Error creating experiment $id: ".$e->getMessage());

        	$subject = MailBuilder::getTitle();
        	$body = MailBuilder::getExceptionMsg($id, $e->getMessage());

            $this->__markAsError($id);         
            $this->__mailSrv->sendMail($params->email, $subject, $body);
        }
	}

	// Mark an experiment as 'in_progress'
	private function __markAsProcessing( $idExp ) {
		update("UPDATE experiment SET state='in_progress' WHERE experiment_id=$idExp");
	}

    // Mark an experiment as erroneus
    private function __markAsError( $idExp ) {
        update("UPDATE experiment SET state='error',end_date=now() WHERE experiment_id=$idExp");
    }

    // Set the real start date
    private function __setStartDate( $experiment ) {
    	update("UPDATE experiment SET start_date=now() WHERE experiment_id=".$experiment->getParams()->getId());
    }

	// Run the experiment
	private function __launchExperiment( $experiment ) {
		try {
			$runner = new Bruselas\Core();
			$runner->runExperiment($experiment);
		} catch (Exception $e) {
			Bruselas\Log::error("Error running experiment ".$experiment->getParams()->getId().": ".$e->getMessage());

			$this->__markAsError($experiment->getParams()->getId());
			$this->__notifyError($experiment, $e->getMessage());
			$this->__cleanError($experiment);
		}
	}

	// Notify by mail of a failure
	private function __notifyError($experiment, $message) {
		$idExp = $experiment->getParams()->getId();
		$user = $experiment->getParams()->getUser();
		$query = $experiment->getParams()->getQuery();
		$description = $experiment->getParams()->getDescription();

		$smiles = Exec::cmd("obabel ".$query." -osmi 2> /dev/null | tr '\t' ' ' | cut -d' ' -f1");

		$subject = MailBuilder::getTitle();
		$body = MailBuilder::getErrorMsg($idExp, $smiles, $description);
		$this->__mailSrv->sendMail($user, $subject, $body);
	}

	// Convert a SimilarityParams instance to Bruselas\Experiment
	private function __toExperiment($params, $id) {
		$builder = new ExperimentBuilder();
		return $builder->toExperiment($params, $id);
	}

	// Remove temporary files and folders in case of failure
	private function __cleanError($experiment) {
		$ssh = new Bruselas\SSH();
		$ssh->connect();
		$home_dir = Bruselas\SystemConfig::get("home_dir");
		$exp_id = $experiment->getParams()->getId();
		
		// query and library from web server
		if (strlen($experiment->getParams()->getQuery()) > 0) {
			@unlink($experiment->getParams()->getQuery());
		}
		if (strlen($experiment->getParams()->getLibrary()) > 0) {
			@unlink($experiment->getParams()->getLibrary());	
		}
		
		// target folder from the cluster
		$ssh->exec("rm -rf $home_dir/proteinas/$exp_id");
		
		// execution folders from the cluster
    	$manager = new Bruselas\QueueManager ();
    	$messages = array_filter($manager->listMessages(), function($elem) use($exp_id) {
    		return intval($elem->getExperiment()) === intval($exp_id);
    	});
    	foreach($messages as $msg) {
    		$ssh->exec("rm -rf ".$msg->getFolder());
    		$manager->removeMessage($msg);
    	}
	}

}

function errorHandlerFunction($errno, $errstr, $errfile, $errline) {
	Bruselas\Log::error("manejando error: $errstr");

 	if (!(error_reporting() & $errno)) {
        	// This error code isn't included in error_reporting
        	return;
    	}
	switch ($errno) {
    	case E_USER_ERROR || E_USER_NOTICE:
        	throw new Bruselas\UnknownError("Fatal error [$errno] \"$errstr\" in file $errfile:$errline");
        	break;
    	default:
    		throw new Bruselas\UnknownError("Unknown error [$errno] \"$errstr\" in file $errfile:$errline");
        	break;
    }

    /* Don't execute the PHP inner error manager */
    return true;
}

// catch any error
$error_handler = set_error_handler("errorHandlerFunction");
// launch the process
$service = new LaunchExperimentServiceImpl();
$service->start();
?>
