<?php
require_once __DIR__.'/../../Database/conexion.php';
require_once __DIR__.'/../DrugService.php';
require_once __DIR__.'/../DrugReturn.php';
require_once __DIR__.'/../DrugRuleReturn.php';

	
class DrugServiceImpl implements DrugService {
	
	// Find the compound with the given id.
	public function findDrugById($id) {
		$sql = "select d.drug_id, d.chemical_name, d.iupac_name, d.inchi_key, d.formula, d.canonical_smiles, d.atc_code, d.compound_type, df.drug_family_id, df.name as family_name, d.image_link, d.filename, ds.name as source_name, dhd.link, d.chembl_id, d.kegg_id, d.dia_id, d.drugbank_id from drug d left join drug_family df using(drug_family_id) left join drug_conformation dc using(drug_id) left join dataset_has_drug dhd using(drug_id) left join dataset ds using(dataset_id) where d.drug_id=$id";
		$res = query($sql);
		if (isEmpty($res)) {
			return null;
		}

		while ($row = fetch_array($res)) {
			$obj = new DrugReturn();
			$obj->id = $row["drug_id"];
			$obj->name = $row["chemical_name"];
			$obj->chemicalName = $row["iupac_name"];
			$obj->inchiKey = $row["inchi_key"];
			$obj->formula = $row["formula"];
			$obj->smiles = $row["canonical_smiles"];
			$obj->atcCode = $row["atc_code"];
			$obj->type = $row["compound_type"];
			$obj->familyId = $row["drug_family_id"];
			$obj->familyName = $row["family_name"];
			$obj->imageLink = $row["image_link"];
			$obj->filename = $row["filename"];
			$obj->srcName = $row["source_name"];
			$obj->srcLink = $row["link"];
			$obj->chemblId = $row["chembl_id"];
			$obj->drugbankId = $row["drugbank_id"];
			$obj->keggId = $row["kegg_id"];
			$obj->diadbId = $row["dia_id"];

			return $obj;
		}
		return null;
	}
	
	// Search for molecules whose names match the pattern.
	public function findDrugByName($table, $column, $pattern, $limit = 0) {
		$mapper = array();

		$terms = explode(" ", $pattern);
		$where = array_map(function($elem) {
			return "(lower(chemical_name) LIKE lower('%".$elem."%') COLLATE UTF8_GENERAL_CI OR lower(iupac_name) LIKE lower('%".$elem."%') COLLATE UTF8_GENERAL_CI OR lower(inchi_key) LIKE lower('%".$elem."%') COLLATE UTF8_GENERAL_CI OR lower(canonical_smiles) LIKE lower('%".$elem."%') COLLATE UTF8_GENERAL_CI OR lower(chembl_id) LIKE lower('%".$elem."%') COLLATE UTF8_GENERAL_CI OR lower(drugbank_id) LIKE lower('%".$elem."%') COLLATE UTF8_GENERAL_CI OR lower(kegg_id) LIKE lower('%".$elem."%') COLLATE UTF8_GENERAL_CI OR lower(dia_id) LIKE lower('%".$elem."%') COLLATE UTF8_GENERAL_CI)";
		}, $terms);

		$sql = "SELECT * FROM drug WHERE 1=1 AND (".implode(" AND ", $where).") limit $limit";

		$res = query($sql);
		if (isEmpty($res)) {
			return $mapper;
		}		
		while ($row = fetch_array($res)) {
			$obj = new DrugReturn();
			$obj->id = $row["drug_id"];
			$obj->name = $row["chemical_name"];
			$obj->formula = $row["formula"];
			$obj->smiles = $row["canonical_smiles"];

			array_push($mapper, $obj);
		}
		return $mapper;	
	}

	public function findAlert($id, $code) {
		$sql = "select * from drug_druglikeness where drug_id = $id";
		$res = query($sql);
		if (isEmpty($res)) {
			return null;
		}

		while ($row = fetch_array($res)) {
			$obj = new DrugRuleReturn();
			$obj->drugId = $row["drug_id"];
			if ($code === "ro5") {
				$obj->name = "Lipinski's rule of five";
				$obj->violations = $row["ro5_violations"];
				$obj->compliant = $row["ro5_compliant"];
			} else if ($code === "ghose") {
				$obj->name = "Ghose's rule";
				$obj->violations = $row["ghose_violations"];
				$obj->compliant = $row["ghose_compliant"];
			} else if ($code === "veber") {
				$obj->name = "Veber's rule";
				$obj->violations = $row["veber_violations"];
				$obj->compliant = $row["veber_compliant"];
			} else if ($code === "375") {
				$obj->name = "3/75";
				$obj->violations = $row["375_violations"];
				$obj->compliant = $row["375_compliant"];
			}
			return $obj;
		}
		return null;		
	}
	
}
?>
