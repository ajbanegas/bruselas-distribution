<?php
require_once __DIR__.'/../../Database/conexion.php';
require_once __DIR__.'/../SoftwareService.php';
require_once __DIR__.'/../SoftwareReturn.php';


class SoftwareServiceImpl implements SoftwareService {

	// Return the information concerning the shape similarity screening software.
	public function listSimilarity($active = TRUE) {
		$mapper = array();
		
		$sql = "select * from software where type_software_id=".SW_SIMILARITY.($active === TRUE ? " and active=1" : "")." order by name";
		$res = query($sql);
		if (isEmpty($res)) {
			return $mapper;
		}
		while ($fila = fetch_array($res)) {
			$obj = new SoftwareReturn();
			$obj->idSoftware = $fila["software_id"];
			$obj->code = $fila["code"];
			$obj->name = $fila["name"];
			array_push($mapper, $obj);
		}		
		return $mapper;
	}

	// Return the information concerning the pharmacophore screening software.
	public function listPharmacophore($active = TRUE) {
		$sql = "select * from software where type_software_id=".SW_PHARMACOPHORE.($active === TRUE ? " and active=1" : "")." order by name";
		$res = query($sql);
		if (isEmpty($res)) {
			return array();
		}
		$mapper = array();
		while ($fila = fetch_array($res)) {
			$obj = new SoftwareReturn();
			$obj->idSoftware = $fila["software_id"];
			$obj->code = $fila["code"];
			$obj->name = $fila["name"];
			array_push($mapper, $obj);
		}
		return $mapper;
	}
	
	// Return the information concerning the descriptor-wise software.
	public function listDescriptors($active = TRUE) {
		$mapper = array();
		
		$sql = "select * from software where type_software_id=".SW_DESCRIPTOR.($active === TRUE ? " and active=1" : "")." order by name";
		$res = query($sql);
		if (isEmpty($res)) {
			return $mapper;
		}
		while ($fila = fetch_array($res)) {
			$obj = new SoftwareReturn();
			$obj->idSoftware = $fila["software_id"];
			$obj->code = $fila["code"];
			$obj->name = $fila["name"];
			array_push($mapper, $obj);
		}
		return $mapper;
	}
	
	// Return the information concerning the software for pre-filtering.
	public function listPrefilterFunctions() {
		$res = query("select * from software where type_software_id=".SW_PREFILTER." and active=1 order by software_id");
        if (isEmpty($res)) return "";

        $arr = array();
        while ($row = fetch_array($res)) {
			$obj = new SoftwareReturn();
			$obj->idSoftware = $row["software_id"];
			$obj->code = $row["code"];
			$obj->name = $row["name"];
			array_push($arr, $obj);
        }
        return $arr;
	}

}
?>