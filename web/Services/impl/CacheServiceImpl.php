<?php
require_once __DIR__.'/../../../bruselas/autoload.php';
require_once __DIR__.'/../../Database/conexion.php';
require_once __DIR__.'/../CacheService.php';

class CacheServiceImpl implements CacheService {

	// Check if a set of descriptors has already been processed and its results are still available.
	// Returns the list of conformers to use if any. Otherwise, NULL is returned.
	public function getCached($input) {
		$str_in = $this->__encrypt($input);

		$sql = "SELECT output_library FROM experiment_cache WHERE input_params = '$str_in'";
		$res = query($sql);
		while ($row = fetch_array($res)) {
			return $this->__decrypt($row["output_library"]);
		}
		return NULL;
	}

	// Store a new entry in cache.
	public function cache($input, $output) {
		$str_in = $this->__encrypt($input);
		$str_out = $this->__encrypt($output);

		$sql = "INSERT INTO experiment_cache(input_params,output_library) VALUES('$str_in','$str_out');";
		update($sql);
	}

	private function __encrypt($obj) {
	    $encrypted = json_encode($obj);
	    $encrypted = base64_encode($encrypted);
	    return $encrypted;
	}

	private function __decrypt($string) {
	    $decrypted = base64_decode($string);
	    $decrypted = json_decode($decrypted);
	    return $decrypted;
	}

}
?>
