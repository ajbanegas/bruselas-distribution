<?php
require_once __DIR__.'/../../Database/conexion.php';
require_once __DIR__.'/MailServiceImpl.php';
require_once __DIR__.'/MailBuilder.php';
require_once __DIR__.'/../SurveyService.php';
require_once __DIR__.'/../UserReturn.php';

/**
 * Envía un correo de notificación de los experimentos lanzados el día anterior.
 */
class SurveyServiceImpl implements SurveyService {
	// servicios
	private $__mailSrv;

	function __construct() {
		$this->__mailSrv = new MailServiceImpl();
	}

	public function start() {
		$list = $this->__listUsers();
		array_walk($list, function($user) {
			$this->__mailSrv->sendMail($user->email, MailBuilder::getSurveyTitle(), MailBuilder::getSurveyMsg());
			$this->__markAsSent($user->email);
		});
	}

	public function save($responses) {
		$submission_id = update("INSERT INTO survey_submission(user_id) SELECT user_id FROM users WHERE email='".$responses['1']['1']."'");
		$question_id = 1;
		foreach ($responses as $r) {
			foreach($r as $opt => $val) {
				update("INSERT INTO survey_response(survey_submission_id,question_id,option_id,response) VALUES($submission_id,$question_id,$opt,'$val')");
			}
			$question_id++;
		}
	}

	public function unlockTasksByUser($email) {
		update("UPDATE users SET filled_survey=1 WHERE email='$email'");
		update("UPDATE experiment SET enabled=1 WHERE experiment_id IN (SELECT experiment_id FROM experiment_param WHERE value LIKE '%\"email\":\"$email\"%') AND enabled=0");
	}

	public function lockTasksByUser($email) {
		update("UPDATE experiment SET enabled=0 WHERE experiment_id IN (SELECT experiment_id FROM experiment_param WHERE value LIKE '%\"email\":\"$email\"%') AND state='queued'");
	}

	public function isUserUnlocked($email) {
		$res = query("SELECT num_jobs, filled_survey FROM users WHERE email='$email'");
		while ($row = fetch_array($res)) {
			if ($row["num_jobs"] > 5 && $row["filled_survey"] == 0)	{
				return FALSE;
			}
		}
		return TRUE;
	}

	private function __listUsers() {
		$list = array();
		$res = query("SELECT * FROM users WHERE sent_survey=0");
		while ($row = fetch_array($res)) {
			$obj = new UserReturn();
			$obj->email = $row["email"];
			array_push($list, $obj);
		}
		return $list;		
	}

	private function __markAsSent($email) {
		update("UPDATE users SET sent_survey=1 WHERE email='$email'");
	}
}

$service = new SurveyServiceImpl();
$service->start();
?>
