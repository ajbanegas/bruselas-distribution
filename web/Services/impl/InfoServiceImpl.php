<?php
require_once __DIR__.'/../../Database/conexion.php';
require_once __DIR__.'/../ExperimentEmailReturn.php';

class InfoServiceImpl {

	// Return the information concerning the experiments finished the previous day.
	public function experimentsPerCurrentDay() {
		$sql = "select * from experiment e join experiment_param p on e.experiment_id = p.experiment_id where date(e.start_date)=adddate(curdate(), -1)";
        $rs = query($sql);

        $result = array();
        while ($row = fetch_array($rs)) {
        	$obj = new ExperimentEmailReturn();
        	$obj->experimentId = $row["experiment_id"];
        	$obj->state = $row["state"];
        	$obj->startDate = date_create($row["start_date"]);
        	$obj->endDate = (strlen($row["end_date"]) > 0) ? date_create($row["end_date"]) : null;
        	
        	$params = json_decode($row["value"]);
        	$obj->email = $params->email;

            array_push($result, $obj);
        }
        return $result;
	}

}
?>
