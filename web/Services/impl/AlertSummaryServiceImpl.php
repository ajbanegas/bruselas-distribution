<?php
require_once __DIR__.'/InfoServiceImpl.php';
require_once __DIR__.'/MailServiceImpl.php';
require_once __DIR__.'/MailBuilder.php';
require_once __DIR__.'/../AlertSummaryService.php';
require_once __DIR__.'/../../../bruselas/autoload.php';

use UCAM\BioHpc\Bruselas as Bruselas;

/**
 * Send an email summarizing which experiments were started the previous day.
 */
class AlertSummaryServiceImpl implements AlertSummaryService {
	// services
	private $__infoSrv;
	private $__mailSrv;
	// variables
	private $__nExperiments;
	private $__nSuccess;
	private $__nFailures;
	private $__nIncompletes;
	private $__nUnclosed;
	private $__users;

	function __construct() {
		$this->__infoSrv = new InfoServiceImpl();
		$this->__mailSrv = new MailServiceImpl();
	}

	public function start() {
		$list = $this->__infoSrv->experimentsPerCurrentDay();

		if (count($list) > 0) {
			$this->__nExperiments = count($list);
			$this->__countStatus($list);
			$this->__listUsers($list);
			$message = $this->__buildMail();
		} else {
			$this->__users = array();
			$date = date('d/m/Y', mktime(0, 0, 0, date('m'),date('d')-1,date('Y')));
			$message = "No activity detected in BRUSELAS Server on <b>$date</b>";
		}

		$this->__sendMail($message);
	}

	private function __countStatus($list) {
		$this->__nSuccess = 0;
		$this->__nFailures = 0;
		$this->__nIncompletes = 0;
		$this->__nUnclosed = 0;

		array_walk($list, function($exp) {
			if ($exp->state === "executed" && !is_null($exp->endDate)) {
				$this->__nSuccess++;
			} else if ($exp->state === "error") {
				$this->__nFailures++;
			} else if ($exp->state === "in_progress" && is_null($exp->endDate)) {
				$this->__nIncompletes++;
			} else if ($exp->state === "executed" && is_null($exp->endDate)) {
				$this->__nUnclosed++;
			}
		});
	}

	private function __listUsers($list) {
		$this->__users = array_unique(array_map(function($exp) {
					return $exp->email;
				}, $list)
		);
	}

	private function __buildMail() {;
		$date = date('d/m/Y', mktime(0, 0, 0, date('m'),date('d')-1,date('Y')));
		$message = "Summary of experiments registered in BRUSELAS Server on: <b>$date</b><br><br>";
		$message .= "<table border='1'>";
		$message .= "	<tr><th>Event</th><th>Counter</th></tr>";
		$message .= "	<tr><td>Experiments registered</td><td>".$this->__nExperiments."</td></tr>";
		$message .= "	<tr><td>Successfully finished</td><td>".$this->__nSuccess."</td></tr>";
		$message .= "	<tr><td>Failures</td><td>".$this->__nFailures."</td></tr>";
		$message .= "	<tr><td>Still in progress</td><td>".$this->__nIncompletes."</td></tr>";
		$message .= "	<tr><td>Finished without end date</td><td>".$this->__nUnclosed."</td></tr>";
		$message .= "</table><br><br>";
		$message .= "Users:<br>".implode("<br>", $this->__users);
		return $message;
	}

	private function __sendMail($message) {
		$to = Bruselas\SystemConfig::get( "email_bcc" );
		$this->__mailSrv->sendMail($to, MailBuilder::getSummaryTitle(), $message);
	}
}

$service = new AlertSummaryServiceImpl();
$service->start();
?>
