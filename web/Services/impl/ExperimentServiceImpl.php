<?php
require_once __DIR__.'/../../Database/conexion.php';
require_once __DIR__.'/../Constants.php';
require_once __DIR__.'/../ExperimentService.php';
require_once __DIR__.'/../DatasetReturn.php';
require_once __DIR__.'/../ExperimentConfig.php';
require_once __DIR__.'/../ResultReturn.php';
require_once __DIR__.'/../ResultJsonReturn.php';


class ExperimentServiceImpl implements ExperimentService {

	// Check if an experiment exists.
	public function isExperiment($idExp) {
		$res = query("select * from experiment where experiment_id=".$idExp);
		return !isEmpty($res);
	}
	
	// Return the experiments's info.
	public function findConfig($idExp, $email) {
		$res = query("select ep.value, e.query_smiles, e.state, e.start_date, e.end_date from experiment e join experiment_param ep using(experiment_id) where ep.experiment_id=$idExp");
		while($row = fetch_array($res)) {
			$params = json_decode($row["value"]);

			if ($params->email !== $email) {
				return null;
			}

			$obj = new ExperimentConfig();
			$obj->idExp = $idExp;
			$obj->comments = $params->description;
			$obj->threshold = $params->cutoff;
			$obj->maxResults = $params->maxResults;
			$obj->consensus = $params->consensusFn;
			$obj->user = $params->email;
			$obj->smiles = $row["query_smiles"];
			$obj->status = $row["state"];
			$obj->start = date_format(date_create($row["start_date"]),"d/m/Y H:i:s");
			$obj->end = date_format(date_create($row["end_date"]),"d/m/Y H:i:s");
			$obj->prefilterFunc = $this->__getSoftware($params->distance)["name"];
			$obj->extractor = $this->__getSoftware($params->descriptorExtractor)["name"];
			$obj->similarity = $this->__getSoftwareFromArray($params->similarity);
			$obj->calculationType = $params->calculationType === "PH" ? "Pharmacophoric searching" : "Similarity searching";
			$obj->datasets = $this->__getDataset($params->datasets);
			$obj->keywords = $params->keywords;
			$obj->filters = $this->__getFilters($params->filters);

			return $obj;
		}
		return null;
	}
	
	// List the software's codes used in an experiment.
	public function findExperimentSoftware($idExp) {
		$res = query("select value from experiment_param where experiment_id=$idExp");
		while($row = fetch_array($res)) {
			$params = json_decode($row["value"]);
			return array_map(function($elem) {
					return $elem["code"];
				}, 
				array_map(function($elem) { 
					return $this->__getSoftware($elem); 
				}, $params->similarity));
		}
		return array();
	}
	
	// Load the list of results of an experiment.
	public function loadResults($idExp, $max = 100) {
		$sql = "select @rownum:=@rownum+1 as rank, er.experiment_result_id, coalesce(dc.drug_id, -1) drug_id, coalesce(d.chemical_name, er.ligand_name) chemical_name, coalesce(d.chembl_id, d.drugbank_id, d.dia_id, d.kegg_id) code, er.score, (select rd1.score from experiment_result er1 join experiment_result_detail rd1 using(experiment_result_id) join software s1 using(software_id) where s1.code = 'LI' and er1.experiment_id = $idExp and rd1.experiment_result_id = er.experiment_result_id) lisica, (select rd1.score from experiment_result er1 join experiment_result_detail rd1 using(experiment_result_id) join software s1 using(software_id) where s1.code = 'OP' and er1.experiment_id = $idExp and rd1.experiment_result_id = er.experiment_result_id) optipharm,(select rd1.score from experiment_result er1 join experiment_result_detail rd1 using(experiment_result_id) join software s1 using(software_id) where s1.code = 'S3' and er1.experiment_id = $idExp and rd1.experiment_result_id = er.experiment_result_id) screen3D,(select rd1.score from experiment_result er1 join experiment_result_detail rd1 using(experiment_result_id) join software s1 using(software_id) where s1.code = 'SF' and er1.experiment_id = $idExp and rd1.experiment_result_id = er.experiment_result_id) shafts,(select rd1.score from experiment_result er1 join experiment_result_detail rd1 using(experiment_result_id) join software s1 using(software_id) where s1.code = 'WG' and er1.experiment_id = $idExp and rd1.experiment_result_id = er.experiment_result_id) wega,(select rd1.score from experiment_result er1 join experiment_result_detail rd1 using(experiment_result_id) join software s1 using(software_id) where s1.code = 'PH' and er1.experiment_id = $idExp and rd1.experiment_result_id = er.experiment_result_id) pharmer from experiment_result er left join drug_conformation dc using(drug_conformation_id) left join drug d using(drug_id), (select @rownum:=0) r where er.experiment_id = $idExp order by er.score desc limit $max";
		$res = query($sql);
		$arr = array();
		while ($row = fetch_array($res)) {
			$record = [
				$row["rank"], 
				$row["experiment_result_id"], 
				$row["drug_id"],
				$row["chemical_name"], 
				$row["code"],
				$row["score"], 
				$row["lisica"],
				$row["optipharm"],
				$row["screen3D"],
				$row["shafts"],
				$row["wega"],
				$row["pharmer"],
				""
			];
			array_push($arr, $record);
		}
		return $arr;
	}

	// Return the results of an experiment.
	// The second paramenter indicates how many records will be returned. By default 100.
	// If 0 is given as the second paramenter, all the results are returned.
	public function findResults($idExp, $max = 100) {
		$sql = "select er.experiment_result_id, er.score, d.drug_id, coalesce(d.chemical_name, er.ligand_name) as chemical_name, d.iupac_name, @rownum:=@rownum+1 as rank, d.filename from experiment_result er left join drug_conformation dc using(drug_conformation_id) left join drug d using(drug_id), (select @rownum:=0) r where er.experiment_id=$idExp order by er.score desc limit $max";

		$res = query($sql);
		$result = array();
		while ($row = fetch_array($res)) {
			$obj = new ResultReturn();
			$obj->index = $row["rank"];
			$obj->score = number_format($row["score"], 4, ",", ".");
			$obj->id = $row["drug_id"];
			$obj->idSim = $row["experiment_result_id"];
			$obj->drugName = $row["chemical_name"];
			$obj->chemicalName = $row["iupac_name"];
			$obj->confFilename = $row["filename"];

			array_push($result, $obj);
		}

		return $result;
	}

	// Return the partial scores of a given result.
	public function findResultDetails($idResult) {
		$map = array();
		$res = query("select erd.score, erd.ligand_path, s.name, coalesce(d.chemical_name, er.ligand_name) chemical_name, er.experiment_id, erd.query_path, erd.experiment_result_detail_id, er.score final_score from experiment_result_detail erd join software s using(software_id) join experiment_result er using(experiment_result_id) left join drug_conformation dc using(drug_conformation_id) left join drug d using(drug_id) where experiment_result_id=$idResult order by name");
		while ($row = fetch_array($res)) {
			array_push($map, (object)$row);
		}
		return $map;
	}

	// Return the information needed to generate the JSON file.
	public function findResultsJson($idExp) {
		$sql = "select d.ligand_path,d.query_path,coalesce(dr.filename,d.ligand_path) filename,s.code,@rownum:=@rownum+1 as rank,r.score,c.seq_no from experiment_result r join experiment_result_detail d using(experiment_result_id) join software s using(software_id) left join drug_conformation c using(drug_conformation_id) left join drug dr using(drug_id),(select @rownum:=0) p where r.experiment_id=$idExp order by r.score desc";

		$res = query($sql);
		$result = array();
		while ($row = fetch_array($res)) {
			$obj = new ResultJsonReturn();
			$obj->ligand = $row["ligand_path"];
			$obj->query = $row["query_path"];
			$obj->code = pathinfo($row["filename"], PATHINFO_FILENAME);
			$obj->program = $row["code"];
			$obj->visible = $row["rank"];	// TODO
			$obj->score = number_format($row["score"], 4, ",", ".");
                        $obj->seq_no = $row["seq_no"];

			array_push($result, $obj);
		}

		return $result;
	}

	// List the available datasets.
	public function listDatasets() {
		$res = query("select * from dataset order by dataset_id");
		
		$mapper = array();
		while ($fila = fetch_array($res)) {
			$obj = new DatasetReturn();
			$obj->id = $fila["dataset_id"];
			$obj->name = $fila["name"];

			array_push($mapper, $obj);
		}
		return $mapper;
	}

	// Return the common query of an experiment.
	public function findQuery($id) {
		$res = query("select distinct query_path from experiment_result_detail join software using(software_id) where experiment_result_id in (select experiment_result_id from experiment_result where experiment_id=$id) and code <> 'S3'");
		while ($row = fetch_array($res)) {
			return $row["query_path"];
		}
		return null;
	}

	private function __getSoftware($id) {
		$res = query("select * from software where software_id=$id");
		while ($row = fetch_array($res)) {
			return $row;
		}
		return null;
	}

	private function __getSoftwareFromArray($list_id) {
		$res = query("select group_concat(name separator ', ') as name from software where software_id in (".implode(",",$list_id).")");
		while ($row = fetch_array($res)) {
			return $row["name"];
		}
		return null;
	}

	private function __getDataset($list_id) {
		$res = query("select group_concat(name separator ', ') as name from dataset where dataset_id in (".implode(",",$list_id).")");
		while ($row = fetch_array($res)) {
			return $row["name"];
		}
		return null;
	}

	private function __getFilters($list_id) {
		$descriptions = array_map(function($item) { 
			if ($item === "ro5_compliant") {
				return "Lipinski's rule of five";
			}
		}, $list_id);

		return implode(",", $descriptions);
	}

}
?>
