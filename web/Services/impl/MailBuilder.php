<?php
require_once __DIR__.'/../../../bruselas/autoload.php';

use UCAM\BioHpc\Bruselas as Bruselas;

class MailBuilder {

	public static function getTitle() {
		return "Ligand similarity results from BRUSELAS Server";
	}

	public static function getSurveyTitle() {
		return "BRUSELAS Server Survey";
	}

	public static function getSummaryTitle() {
		return "Summary of activity in BRUSELAS 2 Server";
	}

	public static function getErrorMsg($idExp, $smiles, $description) {
		$text = (strlen($description) > 0) ? $description : "";

		return "Similarity results for ligand with smiles code: $smiles<br><br>".
			"<b>ID: $idExp</b><br>".
			"<b>Job: $text</b><br><br>An unexpected error happened while running the experiment you submitted.<br>".
			"Please, try again or try another molecule.<br>Sorry for the inconvenience<br><br><br>".
			"<a href='http://bio-hpc.ucam.edu/Bruselas2'>http://bio-hpc.ucam.edu/Bruselas2</a>";
	}

	public static function getExceptionMsg($idExp, $msg) {
		return "Similarity results for BRUSELAS experiment<br><br>".
			"<b>ID: $idExp</b><br>".
			"An unexpected error happened while running the experiment you submitted.<br>".
			"Below you can find further information about the concrete error.<br><br>".
			"Please, try again or contact the administrator to investigate the issue.<br>".
			"Sorry for the inconvenience<br><br>Error description:<br>".
			"<i>$msg</i><br><br><br>".
			"<a href='http://bio-hpc.ucam.edu/Bruselas2'>http://bio-hpc.ucam.edu/Bruselas2</a>";
	}

	public static function getSurveyMsg() {
		$senderEmail = Bruselas\SystemConfig::get( "email_sender" );
		$senderName = Bruselas\SystemConfig::get( "email_sender_name" );

		return "Dear BRUSELAS VS Server user,<br><br>
			this is an automatically generated email sent to all users that have tested our BRUSELAS Server tool.<br><br>
			We are trying to improve the tool and direct feedback from users is greatly appreciated, therefore we would be really grateful if your could just take some minutes and fulfill this short survey:<br><br>
			<a href='http://bio-hpc.ucam.edu/Bruselas2/web/Survey/Survey.php'>http://bio-hpc.ucam.edu/Bruselas2/web/Survey/Survey.php</a><br><br>
			or email directly to:<br><br>".
			htmlentities($senderName).", PhD<br>
			Structural Bioinformatics and High Performance Computing Research Group (BIO-HPC)<br>
			Universidad Cat&oacute;lica San Antonio de Murcia (UCAM), Spain<br>
			Website: <a href='http://bio-hpc.eu'>http://bio-hpc.eu</a><br>
			Email: <a href='mailto:$senderEmail'>$senderEmail</a> <br>
			CV: <a href='https://goo.gl/K2liCb'>https://goo.gl/K2liCb</a><br>
			Researchgate: <a href='https://goo.gl/FA4RXy'>https://goo.gl/FA4RXy</a><br>
			LinkedIn: <a href='https://goo.gl/b4gIjs'>https://goo.gl/b4gIjs</a><br>
			<br>
			Thanks in advance";
	}

}
?>
