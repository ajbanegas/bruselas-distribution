<?php
class Exec {

	public static function cmd($command) {
		exec($command, $out, $retvar);
		return implode(PHP_EOL, $out);
	}

	public static function babel($input, $output, $args) {
		if (is_null($output) || strlen($output) === 0) {
			return exec("obabel $input $args");
		}
		return exec("obabel $input -O $output $args");
	}
	
}
?>