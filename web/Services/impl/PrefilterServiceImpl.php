<?php
require_once __DIR__.'/../../../bruselas/autoload.php';
require_once __DIR__.'/../../Database/conexion.php';
require_once __DIR__.'/../PrefilterService.php';

use UCAM\BioHpc\Bruselas as Bruselas;

/**
 * Call the Prefilter component from the architecture to get the full list of ligands selected for screening.
 */
class PrefilterServiceImpl implements PrefilterService {

	public function filter($prefilter, $descriptors, $N, $datasets, $keywords = null, $filters = null) {
		Bruselas\Log::debug( "Getting ligands..." );
		return $prefilter->getLigandFiles($descriptors, $datasets, $keywords, $filters);
	}

}
?>
