<?php
require_once __DIR__.'/../../../bruselas/autoload.php';
require_once __DIR__.'/../../Database/conexion.php';
require_once __DIR__.'/../Constants.php';
require_once __DIR__.'/ExperimentServiceImpl.php';
require_once __DIR__.'/../SimilarityParams.php';
require_once __DIR__.'/../InputValidator.php';
require_once __DIR__.'/Exec.php';
require_once __DIR__.'/../SimilarityService.php';

use UCAM\BioHpc\Bruselas as Bruselas;

class SimilarityServiceImpl implements SimilarityService {

	private $__validator;
	private $__params;

	function __construct() {
		$this->__validator = new InputValidator();
	}

	// Transform the content of a file to SMILES.
	private function __getSmiles($file) {
    	return Exec::babel("obabel $file -osmi 2> /dev/null | tr '\t' ' ' | cut -d' '  -f1");
	}

	// Search for the current user. If already exists in the system, his/her id is returned back.
	// Otherwise, the user is registered and the newly assigned id is returned instead.
	private function __identifyUser($email) {
		$res = query("SELECT user_id FROM users WHERE email='$email'");
		if(isEmpty($res)) {
			// if email doesn't exist, then create a new one
			$id_user = update("INSERT INTO users(email) VALUES('$email')");
		} else {
			// if does, then retrieve user's id
			$row = fetch_array($res);
			$id_user = $row["user_id"];
		}
		return $id_user;
	}

	// Check and format the scoring cutoff.
	private function __formatCutoff($value) {
		if (strlen($value) === 0) {
			return 0.0;
		}
		return str_replace(",", ".", $value);	// formato: 0.00000
	}
		
	// Calculate the weight assigned to each algorithm.
	private function __setAvgWeights($methods) {
		$pct = 1.0/count($methods);
		return array_fill_keys(array_keys($methods), $pct);
	}

	// Save the experiment into the database.
	private function __updateDB($idUser) {
		$idExp = update("INSERT INTO experiment(creation_date, start_date, end_date, query_smiles, description) VALUES(now(), NULL, NULL, '', '".$this->__params->description."')");

		#echo "INSERT INTO experiment(creation_date, start_date, end_date, query_smiles, description) VALUES(now(), NULL, NULL, '', '".$this->__params->description."')";
		#print_r($idExp);
		#die();

		$smiles = $this->__params->smiles;
		if ( $this->__params->isQueryFile ) {
			$smiles = $this->__getSmiles( $this->__params->queryPath );
		}
		if ( $this->__params->isQuerySmiles ) {
			$this->__params->smiles = str_replace("\\", "\\\\", $this->__params->smiles);
		}

		update("INSERT INTO experiment_param(experiment_id, value) VALUES($idExp,'".mysql_real_escape_string(json_encode($this->__params))."')");
		update("UPDATE experiment SET query_smiles='".$smiles."', state='queued' WHERE experiment_id=$idExp");
		update("UPDATE users SET num_jobs=num_jobs+1 WHERE user_id=$idUser");
		return $idExp;
	}

	// Launch the experiment on the cluster.
	// If the launc is successful, the id of the experiment is returned.
	// Otherwise, an exception is thrown.
	public function executeLBVS($args) {
		try {
			$this->__params = $args;
			$expSrv = new ExperimentServiceImpl();
			$this->__validator->validateParams($this->__params);
			
			// if a cutoff isn't set, then initialize it to 0
			$this->__params->cutoff = $this->__formatCutoff($this->__params->cutoff);
			
			// consensus
			if (strlen($this->__params->consensusFn) === 0) {
				$this->__params->consensusFn = CONS_AVG;	// peso 1/n
				$this->__params->consensusWeights = $this->__setAvgWeights($this->__params->similarity);
			}
			
			// return the user's id. if not exists, then record it
			$idUser = $this->__identifyUser($this->__params->email);
			
			// create the experiment
			$idExp = $this->__updateDB($idUser);

		} catch (Exception $e) {
			throw new Bruselas\VirtualScreeningException($e->getMessage());
		}

		// return the newly assigned identifier
		return $idExp;
	}

}
?>
