<?php
require_once __DIR__.'/../../../bruselas/vendor/XLSXWriter/XLSXWriter.php';

class Excel {

	static function build($idExp, $cols, $data) {
		$name = "BRUSELAS-" . $idExp;
		$path = sys_get_temp_dir() . "/$name.xlsx";

    	// verify which columns are present
		$wLisica = Excel::__isPresent($data, 4) ? 10 : 0;
		$wWega =  Excel::__isPresent($data, 5) ? 10 : 0;
		$wOptiPharm =  Excel::__isPresent($data, 6) ? 13 : 0;
		$wScreen3D =  Excel::__isPresent($data, 7) ? 13 : 0;
		$wShafts =  Excel::__isPresent($data, 8) ? 12 : 0;
		$wPharmer =  Excel::__isPresent($data, 9) ? 12 : 0;

		// configuration
		$headerTypes = array(
			'Rank' => 'integer',
			'Name' => 'string',
			'Code' => 'string',
			'Score' => 'score',
			'LiSiCA' => 'score',
			'WEGA' => 'score',
			'OptiPharm' => 'score',
			'Screen3D' => 'score',
			'SHAFTS' => 'score',
			'Pharmer' => 'score'
		);

		$widths = array(9, 85, 15, 10, $wLisica, $wWega, $wOptiPharm, $wScreen3D, $wShafts, $wPharmer);

		$headerOptions = Excel::__getHeaderOptions($widths);
		$dataStyles = Excel::__getDataStyle();

		// create the sheet
		$writer = new XLSXWriter();
		$writer->writeSheetHeader($name, $headerTypes, $headerOptions);
		
		for ($i = 0; $i < count($data); $i++) {
        	$writer->writeSheetRow($name, $rowdata = $data[$i], $dataStyles);
    	}

    	// write to temporary file
		$writer->writeToFile($path);
		return $path;
	}

	static function __getHeaderOptions($widths) {
		return array(
			'font' => 'Arial', 'font-size' => 10, 'font-style' => 'bold',
			'freeze_rows' => 1,
			'auto_filter' => 1,
			'widths' => $widths
		);
	}

	static function __getDataStyle() {
		return array([],[],[],['font-style'=>'bold','color'=>'#f00'],[],[],[],[],[],[]);
	}

	static function __isPresent($arr, $index) {
		$values = array_column($arr, $index);
		return !empty(array_filter($values, function ($a) { return $a !== null;}));
	}

}
?>
