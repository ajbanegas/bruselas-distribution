<?php
interface SoftwareService {
	
	// Return the information concerning the shape similarity screening software.
	public function listSimilarity($active = TRUE);

	// Return the information concerning the pharmacophore screening software.
	public function listPharmacophore($active = TRUE);
	
	// Return the information concerning the descriptor-wise software.
	public function listDescriptors($active = TRUE);

	// Return the information concerning the software for pre-filtering.
	public function listPrefilterFunctions();

}
?>