<?php
interface DescriptorService {
	
	// Return a descriptor's value.
	public function findByDrugAndCode($id_drug, $id_conf, $descriptor);

	// Search for the real code of a descriptor, based on its alias.
	public function findCodeByKeyCode($key_code);

	// Search for the alias of a decriptor, based on its real name.
	public function findKeyCodeByCode($code);
	
	// List all available descriptors linked to a given software.
	public function findBySoftware($software_id);

	// List all default descriptors
	public function findCodeDefault();

}
?>