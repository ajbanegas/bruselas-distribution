<?php
require_once __DIR__.'/../../bruselas/autoload.php';
require_once __DIR__.'/../Database/conexion.php';
require_once __DIR__.'/impl/Exec.php';
require_once __DIR__.'/impl/ExperimentServiceImpl.php';

use UCAM\BioHpc\Bruselas as Bruselas;

class BruselasWebTransformer implements Bruselas\ITransformer {

	private $__software;
	private $__expDir;
	private $__isUserLibrary;

	/**
	 * @param $params	ExperimentParam
	 * @param $results 	Result[]
	 * @param $objects 	ExperimentObjects
	 */
	public function saveResults($params, $results, $objects) {
		$idExp = $params->getId();
		$query = $params->getQuery();
		$query_format = pathinfo($query, PATHINFO_EXTENSION);
		if ($query_format == "smi") {
			$query_format = "mol2";
		}

		$this->__expDir = $this->__createFolder($idExp);
		$this->__createQuery($query, $this->__expDir, $idExp);

		// store results into the database
		$this->__software = $this->__loadSoftwareByCode();
		$output = array();
		$this->__isUserLibrary = FALSE;

		foreach ($results as $result) {
			$ligand = $result->getLigand();
			$score = $result->getScore();
			$partials = $result->getPartials();

			// path to ligand picture and final score
			$id_sim = $this->__insertResult($idExp, $score, $ligand);
			$files = $this->__insertDetails($partials, $this->__expDir, $id_sim, $query, $idExp, $query_format);
			$output = array_merge($output, $files);
		}

		$this->__copyFiles($this->__expDir, $objects->getComparators(), $output);

		// if there's a user library then decode ligand names
		if ($this->__isUserLibrary === TRUE) {
			$this->__decodeLigands($this->__expDir);
		}

		$this->__closeExperiment($idExp);
		$this->__createPyMOL($idExp, $this->__expDir);
		$this->__createTAR($idExp, $this->__expDir);
	}

	/**
	 * @param $params	ExperimentParam
	 */
	public function clean($params) {
    		@unlink($params->getQuery());
		@unlink($params->getLibrary());
		Exec::cmd("chmod 777 " . $this->__expDir . " -R");	// allow cron to remove it
	}

	// it creates a folder for the experiment
	private function __createFolder($idExp) {
       		$exp_dir = __DIR__."/../../imagenesWeb/$idExp";
    		Exec::cmd("mkdir $exp_dir");
		return $exp_dir;
	}

	// it generates the required files for the query
	private function __createQuery($query, $exp_dir, $idExp) {
		$query_format = pathinfo($query, PATHINFO_EXTENSION);
        	Exec::cmd("cp $query $exp_dir/ligand.$query_format");
        	$rs = query("SELECT query_smiles FROM experiment WHERE experiment_id=$idExp");
        	while ($row = fetch_array($rs)) {
        		Exec::babel("-:\"".$row["query_smiles"]."\"","$exp_dir/ligand.smi","-osmi");
        		Exec::babel("$exp_dir/ligand.smi","$exp_dir/ligand.png","-xp 500");
                        if ($query_format == "smi") {
                        	Exec::babel("$exp_dir/ligand.smi","$exp_dir/ligand.mol2","-omol2 --gen3d");
                        }
        	}
	}

	// save a single result
	private function __insertResult($idExp, $score, $ligand) {
		$name_parts = explode('_', $ligand);
		$drug_name = $name_parts[0];
		$seq_no = $name_parts[1];

		// check if any valid conformation already exists
		$idConf = -1;
		$rs = query("SELECT drug_conformation_id FROM drug_conformation dc JOIN drug d ON dc.drug_id = d.drug_id WHERE filename LIKE '%/".$drug_name.".tar.gz' AND dc.seq_no = ".$seq_no." LIMIT 1");
		while ($row = fetch_array($rs)) {
			$idConf = $row["drug_conformation_id"];
		}

		// if so, then insert it
		if (!is_null($idConf) && $idConf > 0) {
			update("INSERT INTO experiment_result(experiment_id,drug_conformation_id,score) VALUES($idExp,$idConf,$score)");
		} else {
			#Bruselas\Log::debug( "INSERT INTO experiment_result(experiment_id,ligand_name,score) VALUES($idExp,'".mysql_real_escape_string(hex2bin($ligand))."',$score)" );
			$this->__isUserLibrary = TRUE;
			// otherwise, insert the ligand manually
			update("INSERT INTO experiment_result(experiment_id,ligand_name,score) VALUES($idExp,'".mysql_real_escape_string(hex2bin($ligand))."',$score)");
		}

	 	// go on when anything has been already inserted
    		$rs = query("SELECT last_insert_id() AS id");
    		while ($row = fetch_array($rs)) {
        		return $row["id"];
    		}
		return 0;
	}

	// store partial scores of each algorithm
	private function __insertDetails($partials, $exp_dir, $id_sim, $query, $idExp, $query_format) {
		$sql = "INSERT INTO experiment_result_detail(experiment_result_id,software_id,score,query_path,ligand_path) VALUES";

		$files = array();
		foreach ($partials as $partial) {
			$method = $partial->getMethod();
			$score = $partial->getScore();
			$file = $partial->getFile();

			$ligand_query = new Bruselas\File( "ligand.".$query_format, null );
			$ligand_file = $exp_dir . "/" . basename($file->getName());

			if ($method === "S3" || $method === "OP") {
				// copy the query for the curren ligand
				$s3_query = new Bruselas\File( "ligand_".basename($ligand_file), null, $file->getPath() );
				$s3_query = $exp_dir . "/ligand_" . basename($ligand_file);
				$ligand_query = new Bruselas\File( basename($s3_query), null );
			}

			$software_id = $this->__software[$method];
			$ligand = "../../imagenesWeb/$idExp/".$ligand_query->getName();
			$query = "../../imagenesWeb/$idExp/".$file->getName();

			$sql .= "($id_sim,$software_id,$score,'".$ligand."','".mysql_real_escape_string($query)."'),";

			array_push($files, __DIR__."/".$ligand, __DIR__."/".$query);
		}

		$sql = rtrim($sql, ",");
		update($sql);
		return $files;
	}

	// create the file holding the alignment
	private function __createFile($exp_dir, $file) {
		$target = "$exp_dir/".$file->getName();
		file_put_contents($target, $file->getContent());
		return $target;
	}

	// flag experiment as finished
	private function __closeExperiment($idExp) {
    		update("UPDATE experiment SET state='executed',end_date=now() WHERE experiment_id=$idExp");
	}

	// copy all files generated in the cluster
	private function __copyFiles($dir, $comparators, $output) {
		array_walk($comparators, function($cmp) use($dir) {
			$cmp->copyFiles($dir);
		});

		// remove temporary files
		$str = Exec::cmd("find $dir -type f -name '*aligned*'");
		$files = explode(PHP_EOL, $str);
		$remove = array_diff($files, $output);

		array_walk($remove, function($file) {
			Exec::cmd("rm -f $file");
		});
	}

	// load screening software
	private function __loadSoftwareByCode() {
		$mapper = array();
		$res = query("select software_id, code from software");
		while ($row = fetch_array($res)) {
			$mapper[$row["code"]] = $row["software_id"];
		}
		return $mapper;
	}

	// generate PyMOL session in a single file inside the work folder
	private function __createPyMOL($idExp, $dir) {
		$srv = new ExperimentServiceImpl();

		$json_file = "$dir/$idExp.json";
		$sessionFile = "$dir/$idExp.pse";
		$results = $srv->findResultsJson($idExp);

		// create temporary JSON file
		$store = array();

		$json["id"] = $idExp;
		$json["data"] = array_map(function($item) use(&$store) 
		{
			$N = 5;

			$item->ligand = __DIR__."/".str_replace("\\", "/", $item->ligand);
			$item->query = __DIR__."/".str_replace("\\", "/", $item->query);
			$item->code = pathinfo($item->code, PATHINFO_FILENAME)."_".$item->seq_no;
			$item->score = str_replace(",", ".", $item->score);
			
			if (count($store) < $N || array_key_exists($item->code, $store)) {
				$item->visible = 1;
				$store[$item->code] = 1;
			} else {
				$item->visible = 0;
			}
			return $item;
		}, $results);

		file_put_contents($json_file, json_encode($json, JSON_PRETTY_PRINT));
		
		// generate PyMol session
		$out = Exec::cmd("python ".__DIR__."/../../scripts/pymol-bru.py $json_file");

		Bruselas\Log::debug( "out(pse): $out" );

		chmod("$sessionFile", 0777);
	}

	// create .tar.gz file containing the results
	private function __createTAR($idExp, $dir) {
		$out = Exec::cmd(__DIR__."/../../scripts/compress-results.sh $idExp $dir");
		Bruselas\Log::debug( "out(tar): $out" );
	}

	// decode the ligand names from a uer's library
	private function __decodeLigands($dir) {
		$out = Exec::cmd("python ".__DIR__."/../../scripts/renameMols.py --a decode --d $dir");
		Bruselas\Log::debug( "decode ligands: $out" );
	}

}
?>
