<?php
interface ExperimentService {

	// Check if an experiment exists.
	public function isExperiment($idExp);
	
	// Return the experiments's info.
	public function findConfig($idExp, $email);
	
	// List the software's codes used in an experiment.
	public function findExperimentSoftware($idExp);
	
	// Load the list of results of an experiment.
	public function loadResults($idExp, $max = 100);

	// Return the results of an experiment.
	// The second paramenter indicates how many records will be returned. By default 100.
	// If 0 is given as the second paramenter, all the results are returned.
	public function findResults($idExp, $max = 100);

	// Return the partial scores of a given result.
	public function findResultDetails($idResult);

	// Return the information needed to generate the JSON file.
	public function findResultsJson($idExp);

	// List the available datasets.
	public function listDatasets();

	// Return the common query of an experiment.
	public function findQuery($id);
	
}
?>