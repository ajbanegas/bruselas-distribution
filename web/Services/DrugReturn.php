<?php
class DrugReturn {
	public $id;
	public $name;
	public $chemicalName;
	public $iupacName;
	public $inchiKey;
	public $formula;
	public $smiles;
	public $atcCode;
	public $type;
	public $familyId;
	public $familyName;
	public $imageLink;
	public $filename;
	public $srcName;
	public $srcLink;
	public $chemblId;
	public $drugbankId;
	public $keggId;
	public $diadbId;
}
?>