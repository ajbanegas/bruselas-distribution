<?php
interface PrefilterService {
	
	// Filters to apply to get the compounds in the library
	public function filter($prefilter, $descriptors, $N, $datasets, $keywords = null, $filters = null);

}
?>