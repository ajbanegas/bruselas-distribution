<?php
interface InfoService {
	
	// Return the information concerning the experiments registered the previous day.
	public function experimentsPerCurrentDay();

}
?>