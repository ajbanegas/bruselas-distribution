<?php
class ExperimentConfig {
	public $idExp;
	public $comments;
	public $threshold;
	public $maxResults;
	public $consensus;
	public $user;
	public $smiles;
	public $status;
	public $start;
	public $end;
	public $prefilterFunc;
	public $extractor;
	public $similarity;
	public $calculationType;
	public $datasets;
	public $keywords;
	public $filters;
}
?>