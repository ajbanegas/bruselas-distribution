<?php
require_once __DIR__.'/../../bruselas/autoload.php';
require_once __DIR__.'/../Database/conexion.php';

use UCAM\BioHpc\Bruselas as Bruselas;

class BruselasManhattanPrefilter implements Bruselas\IPrefilter {
	private $__idExtractor;
	private $__size = 0;
	private $__maxConformers = 0;

	function __construct() {
		$this->__size = Bruselas\SystemConfig::get( "library_size" );
		$this->__maxConformers = Bruselas\SystemConfig::get( "max_conformers" );
	}

	public function setExtractor($extractor) {
		$this->__idExtractor = $extractor;
	}

	public function getLigandFiles($descriptors, $datasets, $keywords = null, $filters = null) {
		$this->__validate();

		$result = array();
		$drugs = array();
		$diversity = TRUE;

		$sql = $this->__buildQuery($descriptors, $datasets, $keywords, $filters);
		Bruselas\Log::debug( $sql );

		$res = query($sql);

		while (($row = fetch_array($res)) && count($result) < $this->__size) {
			if ($diversity === FALSE || ($diversity === TRUE && !in_array($row["drug_id"], $drugs))) {
				array_push($result, array($row["filename"], $row["seq_no"]));
			}
			if ($diversity === TRUE) {
				array_push($drugs, $row["drug_id"]);
			}
		}

        	return $result;
	}

	private function __validate() {
		if ( is_null($this->__idExtractor) || !is_int($this->__idExtractor) ) {
			throw new Bruselas\InvalidArgumentException("Extractor param must a be an integer");
		}
	}

	private function __buildQuery($descriptors, $datasets, $keywords, $filters) {
        	$fields = array_map(function($desc) {
                	return "(abs(ifnull(`".$desc->getName()."`,0)-".$desc->getValue().")/(select sigma from variances where desc_name=\"".$desc->getName()."\"))"; },
               		$descriptors
        	);

        	$from = array("drug d","drug_conformation dc ON d.drug_id = dc.drug_id","v_descriptors_dragon v ON v.drug_conformation_id = dc.drug_conformation_id");
        	$where = array("v.software_id = " . $this->__idExtractor);
        	if (!is_null($filters) && count($filters) > 0) {
        			$whereFilters = implode(" AND ", array_map(function($f) { return "$f = 1"; }, $filters));
        			array_push($where, "dc.drug_id IN (SELECT drug_id FROM drug_druglikeness WHERE $whereFilters)");
        	}
        	if (!is_null($keywords) && strlen($keywords) > 0) {
        			$delimiter = "#@$!%-";

        			// 1. look for quoted text and join it with '#@$!%-'
        			$keywords = preg_replace('~(?:\G(?!\A)|")[^"\s]*\K(?:\s|"(*SKIP)(*F))~', $delimiter, $keywords);

        			// 2. removed double quotes from text
        			$keywords = str_replace('"', '', $keywords);

        			// 3. split the keyterms by blank space (those terms that were quoted will remain as a single term)
        			$keys = explode(" ", $keywords);

        			// 4. process each term separately:
        			$match = array();
        			$notmatch = array();
        			$exclude = FALSE;

        			foreach ($keys as $key => $value) {
	        			// 4.1. if the current term contains '#@$!%-', then restore the former blank space
	        			$value = str_replace($delimiter, " ", $value);

	        			// 4.2. identify if the NOT operator is present
	        			if (strtoupper($value) === "NOT") {
							$exclude = TRUE;
							continue;				
	        			}

	        			// 4.3. if the previous term was NOT, then build a proper query (NOT MATCH)
	        			if ($exclude === TRUE) {
	        				array_push($notmatch, "LOCATE('".strtolower($value)."', text) <= 0");
	        			} else {
	        				// 4.4. if the previous term wasn't NOT, then build a proper query (MATCH)	
	        				array_push($match, "LOCATE('".strtolower($value)."', text) > 0");
	        			}

	        			$exclude = FALSE;
        			}

        			// 5. join all the OR clauses within one single external parenthesis
        			if (count($match) > 0) {
            			$whereKeywords = "(".implode(" OR ", $match).")";
            		}
            		if (count($notmatch) > 0) {
            			if (count($match) > 0) {
            				$whereKeywords .= " AND ";
            			}
            			$whereKeywords .= "(".implode(" AND ", $notmatch).")";
            		}
            		array_push($where, "dc.drug_id IN (SELECT drug_id FROM drug_keywords WHERE $whereKeywords)");
        	}
        	if (count($datasets) > 0) {
        			$whereDatasets = "dataset_id IN (" . implode(",", $datasets) . ")";
        			array_push($where, "dc.drug_id IN (SELECT drug_id FROM dataset_has_drug WHERE $whereDatasets)");
        	}

        	return "SELECT d.drug_id, d.filename, dc.seq_no, ".implode("+", $fields)." distance FROM ".implode(" INNER JOIN ",$from)." WHERE ".implode(" AND ", $where)." ORDER BY 4 LIMIT ".($this->__size*$this->__maxConformers);
	}

}
?>
