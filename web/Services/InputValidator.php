<?php
require_once __DIR__.'/Constants.php';

use UCAM\BioHpc\Bruselas as Bruselas;

class InputValidator {

	// Verify if the input parameters are correct.
	public function validateParams($params) {		
		$this->__validateUser($params->email);
		$this->__validateThreshold($params->calculationType, $params->cutoff);
		$this->__validateQuery($params->smiles, $params->queryPath);
		$this->__validateSimilarity($params->similarity);
		$this->__validateConsensus($params->consensusFn, $params->consensusWeights);
		if ((is_null($params->libraryPath) || $params->libraryPath === "") && count($params->datasets) === 0 && $params->isServerLibrary) {
			$this->__validatePrefilter($params->distance, $params->libraryPath);
		}
		$this->__validateLibrary($params->datasets, $params->libraryPath, $params->isServerLibrary);
	}
	
	// Validate the input email.
	private function __validateUser($email) {
		if (!isset($email) || strlen($email) === 0) {
			throw new Exception("Error while authentifying user: email is empty or wrong.");
		}
	}
	
	// Verify that the similarity cutoff is between 0 and 1.
	private function __validateThreshold($type, $threshold) {
		if ($type === "SI") {
			if ($threshold < 0 || $threshold > 1) {
				throw new Exception("The similarity cutoff must be a value between 0 and 1 (Tanimoto coefficient).");
			}
		} else if ($type === "PH") {
			if ($threshold < 0 || $threshold > 1) {
				throw new Exception("The similarity cutoff must be a value between 0 and 2 (Tanimoto coefficient)");
			}
		}
	}
	
	// Validate that a file or SMILES has been provided.
	private function __validateQuery($smiles, $filename) {
		if (strlen($smiles) === 0 && strlen($filename) === 0) {
			$error = "You must supply a query ligand. You can do it by:<br><br>";
			$error .= "A) Uploading a file in one of the accepted formats (mol2, sdf, pdb...).<br>";
			$error .= "B) Typing the Smiles chain of the query ligand.<br>";
			$error .= "C) Drawing the query ligand with help of the wizard.<br>";
			throw new Exception($error);
		}
	}
	
	// Validate that total weights assigned are in range [0, 1].
	private function __validateConsensus($consensus, $weights) {
		if ($consensus === CONS_WAVG) {
			$sum = 0.0;
			foreach ($weights as $k => $v) {
				$sum += $v;
			}
			if ($sum < 0 || $sum > 1) {
				throw new Exception("Consensus weights must not be neither lower than 0 nor bigger than 1.");
			}
		}
	}
	
	// Verify that at least one similarity algorithm was selected.
	private function __validateSimilarity($algorithms) {
		if (is_null($algorithms) || count($algorithms) === 0) {
			throw new Exception("At least one similarity method is required.");
		}
	}

	// Verify that at least one pre-filtering function was selected.
	private function __validatePrefilter($prefilterFunc, $libraryPath) {
		if (is_null($prefilterFunc) && is_null($libraryPath)) {
			throw new Exception("A function to carry out prefiltering is required.");
		}
	}
	
	// Verify that a library was provided or selected.
	private function __validateLibrary($serverDatasets, $library, $isServerLibrary) {
		if (is_null($library) && count($serverDatasets) === 0 && $isServerLibrary) { 
			throw new Exception("Either a custom library or one(s) provided by the server is required."); 
		}
	}
	
}
?>
