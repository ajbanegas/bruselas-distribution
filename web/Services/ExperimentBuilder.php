<?php
require_once __DIR__.'/../../bruselas/autoload.php';
require_once __DIR__.'/../Database/conexion.php';
require_once __DIR__.'/Constants.php';
require_once __DIR__.'/impl/PrefilterServiceImpl.php';
require_once __DIR__.'/impl/ExperimentServiceImpl.php';
require_once __DIR__.'/impl/DescriptorServiceImpl.php';
require_once __DIR__.'/CacheInput.php';
require_once __DIR__.'/CacheOutput.php';
require_once __DIR__.'/impl/CacheServiceImpl.php';
require_once __DIR__.'/BruselasManhattanPrefilter.php';
require_once __DIR__.'/BruselasEuclideanPrefilter.php';
require_once __DIR__.'/BruselasRangePrefilter.php';
require_once __DIR__.'/BruselasCanberraPrefilter.php';
require_once __DIR__.'/BruselasWebTransformer.php';
require_once __DIR__.'/impl/Exec.php';

use UCAM\BioHpc\Bruselas as Bruselas;


/**
 * Transform a SimilarityParams instance into a Bruselas\Experiment one.
 */
class ExperimentBuilder {
	private $__prefilterSrv;
	private $__expSrv;
	private $__descSrv;
	private $__cacheSrv;

	function __construct() {
		$this->__prefilterSrv = new PrefilterServiceImpl();
		$this->__expSrv = new ExperimentServiceImpl();
		$this->__descSrv = new DescriptorServiceImpl();
		$this->__cacheSrv = new CacheServiceImpl();
	}	

	// Convert a SimilarityParams instance into a Bruselas\Experiment one.
	public function toExperiment($params, $id) {
		if ( !$this->__validateInput($params) ) {
			return;
		}
		$cloned = clone $params;

		$cloned->similarity = $this->__parseSimilarity($params);
		$cloned->distance = $this->__parseDistance($params);
		$cloned->consensusFn = $this->__parseConsensus($params);
		$cloned->descriptorExtractor = $this->__parseDescriptorExtractor($params);

		// prepare query if needed
		if ($params->isQuerySmiles) {
			$cloned->queryPath = $this->__convertFromSmiles($params->smiles, "mol2");
			$cloned->smiles = null;
		}

		// build a library if need
		$queryProps = array();
		if ($params->isServerLibrary) {
			$codes = $this->__parseDescriptorsCode($params);
			$queryProps = $this->__calculateQueryDescriptors($cloned->queryPath, $cloned->descriptorExtractor, $codes);

			// search in cache first
			$inputCache = new CacheInput();
			$inputCache->descriptors = $queryProps;
			$inputCache->distanceFunction = $params->distance;
			$inputCache->librarySize = 200;
			$inputCache->keyterms = $params->keywords;
			$inputCache->descriptorExtractor = $params->descriptorExtractor;
			$inputCache->datasets = $params->datasets;
			$inputCache->filters = $params->filters;

			$cached = $this->__cacheSrv->getCached($inputCache);
			if (!is_null($cached)) {
				if ( count($cached->conformers) == 0 ) {
					throw new Bruselas\EmptyLibraryException("No compounds were found matching the criteria. Please, indicate a different set of parameters and try again.");
				}
				// get library
				$cloned->libraryPath = $this->__buildLibrary($cached->conformers);
			} else {
				$ligands = $this->__prefilterSrv->filter($cloned->distance, $queryProps, $cloned->maxResults, $cloned->datasets, $cloned->keywords, $cloned->filters);
				if ( count($ligands) == 0 ) {
					throw new Bruselas\EmptyLibraryException("No compounds were found matching the criteria. Please, indicate a different set of parameters and try again.");
				}
				$cloned->libraryPath = $this->__buildLibrary($ligands);

				// set in cache
				$outputCache = new CacheOutput();
				$outputCache->conformers = $ligands;
				$this->__cacheSrv->cache($inputCache, $outputCache);
			}
		} else if ($params->isEnamineLibrary || $params->isMolportLibrary) {
			$script = $params->isEnamineLibrary ? "enamine" : "molport";

			Bruselas\Log::debug("Creating library from $script");

			$tmp = tempnam(sys_get_temp_dir(), 'bru-lib');
			$cloned->libraryPath = $tmp.".mol2";

   			rename($tmp, $cloned->libraryPath);
			Exec::cmd("python ".__DIR__."/../../scripts/".$script."_screening.py -i ".$cloned->queryPath." -o ".$cloned->libraryPath." -t ".$cloned->cutoff." -g");
			if ( file_exists($cloned->libraryPath) === FALSE || filesize($cloned->libraryPath) == 0 ) {
				throw new Bruselas\EmptyLibraryException("No compounds were found matching the criteria. Please, indicate a different set of parameters and try again.");
			}

			$this->__removeDuplicates($cloned->libraryPath);
			$this->__encodeLigands($cloned->libraryPath);
		} else {
			$queryProps = $this->__parseDescriptors($params);
			$this->__removeDuplicates($params->libraryPath);
			$this->__encodeLigands($params->libraryPath);
		}

		$cloned->descriptorList = $queryProps;

		// assign weights for consensus if needed
		if (!is_null($cloned->consensusFn) && 
			($cloned->consensusFn instanceof Bruselas\ConsensusWavg ||
			$cloned->consensusFn instanceof Bruselas\ConsensusAvg)) 
		{
			// expected structure: array("LI"=>0.1, "WG"=>0.2, "S3"=>0.3)
			$weights = array();
			foreach ($cloned->consensusWeights as $k => $v) {
				$code = $this->__getSimilarityCode($k);
				$weights[$code] = $v;
			}
			$cloned->consensusFn->setParams($weights);
		}
		return $this->__buildExperiment($cloned, $id);
	}

	// Validate input data
	private function __validateInput($obj) {
		//2.1. If obj.library is not null but the file doesn't exist: Error: missing library.
		if ($obj->isUserLibrary && !file_exists($obj->libraryPath)) {
			throw new Bruselas\FileNotFoundException("Library file is not found.");
		}
		//2.2. If obj.queryPath is null and obj.smiles too: Error: missing query.
		if (!$obj->isQueryFile && strlen($obj->smiles) === 0) {
			throw new Bruselas\RequiredPropertyException("Query ligand is required.");	
		}
		//2.3. If obj.queryPath is not null but the file doesn't exist: Error: missing query.
		if ($obj->isQueryFile && !file_exists($obj->queryPath)) {
			throw new Bruselas\FileNotFoundException("Query file is not found.");
		}
		//2.4. If obj.email is null: Error: missing email.
		if (strlen($obj->email) === 0) {
			throw new Bruselas\RequiredPropertyException("An email address is required.");
		}
		return true;
	}

	// Transform similarity codes into instances.
	private function __parseSimilarity($obj) {
		return array_map(function($elem) {
			if (intval($elem) === SIM_WEGA) {
				return new Bruselas\ComparatorWega();
			} else if (intval($elem) === SIM_LISICA) {
				return new Bruselas\ComparatorLisica();
			} else if (intval($elem) === SIM_SCREEN3D) {
				return new Bruselas\ComparatorScreen3D();
			} else if (intval($elem) === SIM_SHAFTS) {
				return new Bruselas\ComparatorShafts();
			} else if (intval($elem) === SIM_OPTIPHARM) {
				return new Bruselas\ComparatorOptiPharm();
			} else if (intval($elem) === SIM_PHARMER) {
				return new Bruselas\ComparatorPharmer();
			}
			return null;
		}, $obj->similarity);
	}

	// Convert a SMILES chain to the expected format.
	// The name of the file containing the output is returned.
	private function __convertFromSmiles($smiles, $out_format) {
    		$tmp = tempnam(sys_get_temp_dir(),'bru');
    		$out_file = "$tmp.$out_format";
    		rename($tmp, $out_file);
			Exec::cmd("/home/antonio/jchem-15.6.15/bin/molconvert $out_format --smiles \"$smiles\" -o $out_file -3:S{fine}");
    		Exec::babel($out_file, $out_file, "-o $out_format");
    		chmod("$out_file", 0644);
    		return $out_file;
	}

	// Transform an id to text.
	private function __getSimilarityCode($id) {
		if (intval($id) === SIM_WEGA) {
			return "WG";
		} else if (intval($id) === SIM_LISICA) {
			return "LI";
		} else if (intval($id) === SIM_SCREEN3D) {
			return "S3";
		} else if (intval($id) === SIM_SHAFTS) {
			return "SF";
		} else if (intval($id) === SIM_OPTIPHARM) {
			return "OP";
		} else if (intval($id) === SIM_PHARMER) {
			return "PH";
		}
		return null;
	}

	// Transform descriptor codes to instances.
	private function __parseDescriptors($obj) {
		return array_map(function($elem) {
			$code = $this->__descSrv->findCodeByKeyCode($elem);
			return new Bruselas\Descriptor($code, 0.0);
		}, $obj->descriptorList);
	}

	// Replace a descriptor alias to real code.
	private function __parseDescriptorsCode($obj) {
		if (is_null($obj->descriptorList) || count($obj->descriptorList) < 1 || count($obj->descriptorList) > 100) {
			return $this->__descSrv->findCodeDefault();
		}
		return array_map(function($elem) {
			return $this->__descSrv->findCodeByKeyCode($elem);
		}, $obj->descriptorList);
	}

	// Transform a distance function's code to instance.
	private function __parseDistance($obj) {
		$fn = null;
		if (intval($obj->distance) === FN_MANHATTAN) {
			$fn = new BruselasManhattanPrefilter();
			$fn->setExtractor(intval($obj->descriptorExtractor));
		} else if (intval($obj->distance) === FN_EUCLIDEAN) {
			$fn = new BruselasEuclideanPrefilter();
			$fn->setExtractor(intval($obj->descriptorExtractor));
		} else if (intval($obj->distance) === FN_RANGE) {
			$fn = new BruselasRangePrefilter();
			$fn->setExtractor(intval($obj->descriptorExtractor));
		} else if (intval($obj->distance) === FN_CANBERRA) {
			$fn = new BruselasCanberraPrefilter();
			$fn->setExtractor(intval($obj->descriptorExtractor));
		}
		return $fn;
	}

	// Transform a consensus function's code to an instance.
	private function __parseConsensus($obj) {
		if ($obj->consensusFn === CONS_AVG) {
			return new Bruselas\ConsensusAvg();
		} else if ($obj->consensusFn === CONS_WAVG) {
			return new Bruselas\ConsensusWavg();
		} else if ($obj->consensusFn === CONS_MAX) {
			return new Bruselas\ConsensusMaximum();
		}
		return null;
	}

	// Transform a descriptor extractors's code to an instance.
	private function __parseDescriptorExtractor($obj) {
		if (intval($obj->descriptorExtractor) === intval(DESC_DRAGON)) {
			return new Bruselas\ExtractorDragon();
		}
		return null;
	}

	// Calculate the values of descriptors from the query.
	private function __calculateQueryDescriptors($queryPath, $extractor, $filter) {
		$runner = new Bruselas\Core();
		$descriptors = $runner->extractDescriptors($queryPath, $extractor, $filter);

		return array_map(function($elem) {
			$name = $this->__descSrv->findKeyCodeByCode($elem->getName());
			return new Bruselas\Descriptor($name, $elem->getValue());
		}, $descriptors);
	}

	// Create a file containing the library.
	private function __buildLibrary($ligands) {
		$tmp = tempnam(sys_get_temp_dir(), 'bru-lib');
		$foo = "$tmp.mol2";
		rename($tmp, $foo);

		foreach($ligands as $ligand) {
			$drug_file = $ligand[0];
			$drug_name = pathinfo(pathinfo($ligand[0])['filename'])['filename'];
			$seq_no = $ligand[1];
			exec("tar -axf $drug_file $drug_name"."_$seq_no.mol2 -O", $output);
			$content = implode(PHP_EOL, $output).PHP_EOL;
			file_put_contents($foo, $content, FILE_APPEND);
			$output = NULL;	# Need to clean the variable to avoid appending new content every time
		}

		chmod("$foo", 0644);
		return $foo;
	}

	// Build a Experiment instance from the input data.
	private function __buildExperiment($obj, $id) {
		$exp = new Bruselas\Experiment();
		$exp->getParams()->setId($id);
		$exp->getParams()->setQuery($obj->queryPath);
		$exp->getParams()->setLibrary($obj->libraryPath);
		$exp->getParams()->setUser($obj->email);
		$exp->getParams()->setCutoff($obj->cutoff);
		$exp->getParams()->setDescription($obj->description);
		$exp->getParams()->setResultSize($obj->maxResults);
		$exp->getParams()->setDescriptors($obj->descriptorList);
		$exp->getObjects()->setPrefilter($obj->distance);
		$exp->getObjects()->setConverter(new Bruselas\BabelConverter());
		$exp->getObjects()->setExtractor($obj->descriptorExtractor);
		$exp->getObjects()->setComparators($obj->similarity);
		$exp->getObjects()->setConsensuator($obj->consensusFn);
		$exp->getObjects()->setTransformer(new BruselasWebTransformer());
		$exp->getObjects()->setNotifier(new Bruselas\SMTPNotifier());
		return $exp;
	}

	// Remove the duplicate ligands based on the title
	private function __removeDuplicates($database) {
		$out = Exec::cmd( "obabel $database -O $database --unique title" );
		Bruselas\Log::debug( "remove duplicates: $out" );
	}

	// Encode the ligand names from a uer's library
	private function __encodeLigands($database) {
		$out = Exec::cmd( "python ".__DIR__."/../../scripts/renameMols.py --a encode --f $database" );
		Bruselas\Log::debug( "encode ligands: $out" );
	}
	
}
?>
