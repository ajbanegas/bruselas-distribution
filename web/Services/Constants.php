<?php
define('SW_DESCRIPTOR', 1);
define('SW_SIMILARITY', 2);
define('SW_PREFILTER', 3);
define('SW_PHARMACOPHORE', 4);

define('FN_MANHATTAN', 6);
define('FN_EUCLIDEAN', 7);
define('FN_RANGE', 8);
define('FN_CANBERRA', 10);

define('SIM_WEGA', 2);
define('SIM_LISICA', 3);
define('SIM_SCREEN3D', 4);
define('SIM_SHAFTS', 5);
define('SIM_OPTIPHARM', 9);
define('SIM_PHARMER', 12);

define('DESC_DRAGON', 1);

define('CONS_AVG', 'AVG');
define('CONS_WAVG', 'WAVG');
define('CONS_MAX', 'MAX');
?>
