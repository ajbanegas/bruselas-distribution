<?php
/**
 * Collect the input parameters from the UI.
 */
class SimilarityParams {
	public $similarity;	// array("1","2","3")
	public $queryPath;	// /c/opt/my-compounds/comp1.mol2
	public $smiles;		// CHc(O=2)
	public $libraryPath;	// /c/opt/my-libraries/lib1.mol2
	public $datasets;	// array("1","2","3") ***
	public $descriptorExtractor;	// 1
	public $descriptorList;	// array("MW","ALOGP","RBN","nHDon")
	public $distance;	// 7 (Euclidean)
	public $consensusFn;	// WAVG
	public $consensusWeights;	// array("3" => 0.1, "4" => 0.2, "1" => 0.7)
	public $cutoff;		// 0.7
	public $maxResults;	// 100
	public $description;	// blablabla
	public $email;		// user@mail.com
	public $calculationType; // SI, PH
	public $keywords;	// malaria, diabetes...
	public $filters;	// Lipinski's ro5, Jorgensen's ro3...

	public $isQueryFile = false;
	public $isQuerySmiles = false;
	public $isUserLibrary = false;
	public $isServerLibrary = false;
	public $isEnamineLibrary = false;
	public $isMolportLibrary = false;
}
?>
