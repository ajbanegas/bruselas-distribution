<?php
interface CacheService {

	// Check if a set of descriptors has already been processed and its results are still available.
	// Returns the list of conformers to use if any. Otherwise, NULL is returned.
	public function getCached($input);

	// Store a new entry in cache.
	public function cache($input, $output);

}
?>