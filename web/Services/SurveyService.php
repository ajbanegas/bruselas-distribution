<?php
interface SurveyService {
	
	public function start();

	public function save($responses);

	public function unlockTasksByUser($email);

	public function lockTasksByUser($email);

	public function isUserUnlocked($email);

}
?>
