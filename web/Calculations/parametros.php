<?php
include_once '../Services/impl/SoftwareServiceImpl.php';
include_once '../Services/impl/DescriptorServiceImpl.php';

$softSrv = new SoftwareServiceImpl();
$descSrv = new DescriptorServiceImpl();

$lista = isset($_GET["list"]) ? $_GET["list"] : ""; 

if ($lista === "descriptores") {
	$id = isset($_GET["key"]) ? $_GET["key"] : "";
	$descriptors = $descSrv->findBySoftware($id);
	echo json_encode($descriptors);
	
} else if ($lista === "similarity_sw") {
	$software_list = $softSrv->listSimilarity(true);
	echo json_encode($software_list);	
}
?>
