<?php
include_once '../Services/impl/ExperimentServiceImpl.php';
include_once '../Services/impl/SoftwareServiceImpl.php';

$pathTemplate="../../../template/";
$expSrv = new ExperimentServiceImpl();
$softSrv = new SoftwareServiceImpl();
?>
<link rel="stylesheet" type="text/css" href="../css/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="../css/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="../css/select2.min.css">
<link rel="stylesheet" type="text/css" href="./templateCalculations.css">
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery-ui.min.js"></script>
<script src="../js/modernizr.js"></script>
<script src="<?php echo $pathTemplate; ?>js/checkFields.js"></script>
<script src="../js/select2.min.js"></script>
<script>
function getDescriptors(code, obj) {
	$.ajax({ url: 'parametros.php',
		data: { list: 'descriptores', key: code },
		type: 'get',
		success: function(output) {
			var items = JSON.parse(output);
            
			$.each(items, function (i, item) {
				$('#descriptors_filter').append($('<option>', { 
					value: item.keyCode,
					text : item.description 
				}));
			});

            var proposed = items.filter(function(currentValue) {
                return currentValue.proposed === "1";
            }).map(function(currentValue) {
                return currentValue.code;
            });
            // pre-selected
            obj.val(proposed).trigger("change");
		}
	});
}
function createDescriptorHandler() {
	var obj = $("#descriptors_filter"); 

    	// check the first item, the first time
    	var radios = $("input[type=radio][name=descriptors]"); 
    	var checked = radios.find(":checked");
    	if (checked.length === 0) {
        	var first = radios.first();
        	first.attr("checked", true);
        	checked = first;
    	}
    	// record event change
    	radios.change(function() {
        	// remove previous options
        	obj.children("option").remove();
        	// get list of descriptos
        	getDescriptors($(this).val(), obj);
    	});

    	getDescriptors(checked.val(), obj);
    	// make input
    	obj.select2({
		maximumSelectionLength: 100
    	});
    	obj.on("select2:unselecting", function(e) {
		var n = obj.find(':selected').length;
		if (n === 1) {
			return false;
		}
    	});
}
function createResultsHandler() {
	var obj = $("#maxResultsSlider");
	
	obj.slider({
		range: "max",
		min: 10,
		max: 100,
		value: 100,
		slide: function( event, ui ) {
			$("#maxResultsText").val(ui.value);
			$("#maxResults").val(ui.value);
		}
	});
	$("#maxResultsText").val(obj.slider("value"));
	$("#maxResults").val(obj.slider("value"));
}
function initLibraries() {
	$("input[type=radio][name=libraryType]").eq(0).attr("checked","");
    	$("#libraryToUpload").prop("disabled", true);
    	$("input:checkbox[name^='dataset-']").attr("checked", true);

    	$("input[type=radio][name=libraryType]").change(function() {
        	if ($(this).val() === "bruselas") {        	
            		$("#libraryToUpload").prop("disabled", true);
            		$("input:checkbox[name^='dataset-']").prop("disabled", false);
            } else if ($(this).val() !== "custom") {
            		$("#libraryToUpload").prop("disabled", true);
            		$("input:checkbox[name^='dataset-']").prop("disabled", true);
        	} else {
            		$("#libraryToUpload").prop("disabled", false);
            		$("input:checkbox[name^='dataset-']").prop("disabled", true);
        	}
    	});
}
function showLigand(type) {
	$("#ligFile").css("display", type === "file" ? "" : "none");
	$("#ligSmiles").css("display", type === "smiles" ? "" : "none");
	$("#ligDraw").css("display", type === "draw" ? "" : "none");
}
function showWAVG(show) {
	$(".weights").hide();
	if (show) {
		var type = $('input[name=typeCalc]:checked').val();
		$("#" + type + "_weights").show();
	}
}
function setTypeCalcul(code) {
	$("#typeCalcul").val(code);
	
	/* hide all existing divs */
	$('div[id$="_software"]').hide()
	$('div[id$="_cutoff"]').hide()
	
	/* display only the selected one */
	$("#"+code+"_software").show();
	$("#"+code+"_cutoff").show();
	
	/* refresh the weighted mean div */
	if ($('input[name=consensus]:checked').val() == 'WAVG') {
		showWAVG(true);
	}
}
function enableLibraryBuilding(enable) {
	$("#tabs-3 :input").attr("disabled", !enable);	
}
$(function() {
	$("#tabs").tabs();
	$(".weights").hide();
	$("i.fa").tooltip({
		content: function() {
			return $(this).attr('title');
		}
	});
	$("input[type=radio][name=descriptors]").eq(0).attr("checked","");
    	$("input[type=radio][name=prefilters]").eq(0).attr("checked","");
	$("input[type=radio][name=typeCalc]").eq(0).attr("checked","");
	$("#PH_software").hide();
	$("#PH_cutoff").hide();

	createResultsHandler();
	createDescriptorHandler();
	initLibraries();
});
</script>

<form id="myForm" name="myForm" method="post" action="./calculos.php" enctype="multipart/form-data">
    <input type="hidden" id="typeCalcul">

    <div id="tabs">
      <ul>
		<li><a href="#tabs-0">Type of calculation</a></li>
		<li><a href="#tabs-1">Input structure</a></li>
        <li><a href="#tabs-2">Databases</a></li>
        <li><a href="#tabs-3">Pre-filtering</a></li>
		<li><a href="#tabs-4">Software and Consensus</a></li>
        <li><a href="#tabs-5">Results</a></li>
        <li><a href="#tabs-6">Submit</a></li>
      </ul>
	  
      <div id="tabs-0">
		<div>
			<b>Type of calculation</b>
			<br>
			<input type="radio" name="typeCalc" value="SI" onclick="setTypeCalcul(this.value)"> Shape Similarity Searching<br>
			<input type="radio" name="typeCalc" value="PH" onclick="setTypeCalcul(this.value)"> Pharmacophore Screening
		</div>
      </div>
	  <div id="tabs-1">
        <div>
            <b>Select query molecule:</b>
            <a href="#"><i class="fa fa-info-circle" aria-hidden="true" title="This option allows you to attach the query molecule for which you want to search for ligands. If a file is uploaded, its extension must be concordant with its format. Only one query is allowed on each experiment.<br>Maximum filesize is 20MB.<br>This parameter is required."></i></a>
            <br/>
            <input type="radio" name="ligandType" value="file" onChange="showLigand(this.value)"> Upload Ligand (mol2, sdf, smi)<br>
            <div id="ligFile" style="display:none;">
                <p>
                <input type="hidden" name="MAX_FILE_SIZE" value="20480000" />
                <input type="file" name="fileToUpload" id="fileToUpload" accept=".mol2,.sdf,.smi"/>  
                </br>
            </div>
            <input type="radio" name="ligandType" value="smiles" onChange="showLigand(this.value)"> Ligand (SMILES)<br>
            <div id="ligSmiles" style="display:none;">
                <?php 
                echo '<input type="hidden" name="pathTemplate" value='.$pathTemplate.' />'; // this must always be there
                // include $pathTemplate.'buttons/smiles.php' ;
                ?>
                <textarea id="smiles" name="smiles" cols="80" rows="4"></textarea>
                <input class="btn" type="button" onClick="$('#smiles').val('')" value="Clean" />
            </div>
            <input type="radio" name="ligandType" value="draw" onChange="showLigand(this.value)"> Draw Ligand<br>
            <div id="ligDraw" style="display:none;">
                Press the "Draw Molecule" button and design by hand your molecule of choice. Then, press "Submit Molecule" button in the editor window.<br>
                <?php
                include $pathTemplate.'buttons/paintMolecule.php' ;
                ?>
            </div>
        </div>
	  </div>
		<div id="tabs-2">
			<div>
			   <b>Select target library(ies):</b>
				<a href="#"><i class="fa fa-info-circle" aria-hidden="true" title="This option allows you to choose which libraries to screen. If you upload a custom one, it will not be treated (no descriptors calculated, no filtering...).<br>Maximum filesize is 20MB.<br>If you do not add any library, the program will calculate one for you.<br>If you select the server libraries but do not select any of them, then all of them will be screened by default.<br>This parameter is optional."></i></a>
				<br/>
				<div>
				<input type="radio" name="libraryType" value="bruselas" onChange="enableLibraryBuilding(true)"> BRUSELAS libraries<br>
				<?php
				$datasets = $expSrv->listDatasets();
				array_walk($datasets, function($elem) {
					echo '<input type="checkbox" checked name="dataset-'.$elem->id.'" value="'.$elem->id.'"> '.$elem->name;
				});
				?>
				</div>
				<div>
					<input type="radio" name="libraryType" value="enamine" onChange="enableLibraryBuilding(false)"> Enamine<br>
				</div>
				<div>
					<input type="radio" name="libraryType" value="molport" onChange="enableLibraryBuilding(false)"> MolPort<br>
				</div>
				<div>
					<input type="radio" name="libraryType" value="custom" onChange="enableLibraryBuilding(false)"> Custom library<br>
					<input type="file" name="libraryToUpload" id="libraryToUpload"/><br>
				</div>
			</div>
		</div>
      <div id="tabs-3">
		<div>
            <b>Descriptor extraction software:</b>
            <a href="#"><i class="fa fa-info-circle" aria-hidden="true" title="Software used to calculate descriptors from the query."></i></a>
            <br/>
            <?php
            $extractors = $softSrv->listDescriptors();
            array_walk($extractors, function($elem) {
                echo '<input type="radio" name="descriptors" value="'.$elem->idSoftware.'"> '.$elem->name;
            });
            ?>
        </div>
        <div>
            <b>Select descriptors (optional): </b> A number between 1 and 100 descriptors must be selected.
            <a href="#"><i class="fa fa-info-circle" aria-hidden="true" title="List of descriptors that will be used to build a numerical fingerprint to rank the database ligands and choose the most promising ones for the next stage.<br>The user can customize this set by selecting between 1 and 100 descriptors.<br>This is an optional parameter and will be ignored when screening custom libraries."></i></a>
            <br/>
            <select class="js-example-basic-multiple" multiple="multiple" id="descriptors_filter" name="descriptors_filter[]" style="width:60em;">
            </select>
        </div>
        <div>
            <b>Distance function:</b>
            <a href="#"><i class="fa fa-info-circle" aria-hidden="true" title="Function to rank ligands based on the descriptors selected above. The compounds with the lowest distance will be screened with the selected software."></i></a>
            <br/>
            <?php
            $filter_functions = $softSrv->listPrefilterFunctions();
            array_walk($filter_functions, function($elem) {
                echo '<input type="radio" name="prefilters" value="'.$elem->idSoftware.'"> '.$elem->name;
            });
            ?>
        </div>
		<div>
			<b>Compounds related with the following terms:</b>
			<a href="#"><i class="fa fa-info-circle" aria-hidden="true" title="Keywords may be used to choose those compounds (un)related to the given terms.<br>The use of double quotes is allowed to treat multiple words as a single term (e.g. breast cancer).<br>NOT operator is also allowed to exclude those compounds not related to the terms.<br>This parameter is optional."></i></a>&nbsp;<span class='alert'>The use of this option might slow down the calculation</span>
			<br/>
			<textarea id="keywords" name="keywords" placeholder='Ex.: malaria, "breast carcinoma", NOT MAPK, gp130' style="width:60em;"></textarea>
		</div>
		<div>
			<b>Compound filters:</b>
			<a href="#"><i class="fa fa-info-circle" aria-hidden="true" title="Filters and rules that all the ligands that will move forward to the next step must be compliant with."></i></a>
			<br/>
			<input type="checkbox" value="ro5_compliant" name="filter-ro5_compliant"> Lipinski's rule of five
			<input type="checkbox" value="ghose_compliant" name="filter-ghose_compliant"> Ghose's rule
			<input type="checkbox" value="veber_compliant" name="filter-veber_compliant"> Veber's rule
			<input type="checkbox" value="375_compliant" name="filter-375_compliant"> 3/75
		</div>
      </div>
      <div id="tabs-4">
        <div>
            <b>Software:</b>
            <a href="#"><i class="fa fa-info-circle" aria-hidden="true" title="Software packages you want to use to assess molecular similarity. Multiple programs can be selected. In such case, a consensus scoring function will be applied to the results obtained. This parameter is required."></i></a>
            <br/>
			<div id="SI_software">
            	<?php
            	$si_software_list = $softSrv->listSimilarity();
            	array_walk($si_software_list, function($elem) {
                	echo '<div class="miniPanel">';
                	echo '  <p><label title="'.$elem->name.'">'.$elem->name.'</label></p>';
                	echo '  <input type="checkbox" name="software-'.$elem->idSoftware.'" value="'.$elem->idSoftware.'" checked>';
                	echo '</div>';
            	});
            	?>
			</div>
	    <div id="SI_software_panel" class="miniPanel"></div>
	    <div id="PH_software">
			<?php 
			$ph_software_list = $softSrv->listPharmacophore();
			array_walk($ph_software_list, function($elem) {
				echo '<div class="miniPanel">';
				echo '  <p><label title="'.$elem->name.'">'.$elem->name.'</label></p>';
				echo '  <input type="checkbox" name="softwareph-'.$elem->idSoftware.'" value="'.$elem->idSoftware.'" checked>';
				echo '</div>';
			});
			?>
	    </div>
            <div id="PH_software_panel" class="miniPanel"></div>
        </div>
        <br><br>
        <div id="consensusDiv">
            <b>Consensus scoring function (optional):</b>
            <a href="#"><i class="fa fa-info-circle" aria-hidden="true" title="Consensus scoring function to merge the different individual scores into one single value per ligand. This option only has meaning when many software packages have been selected for screening.<br>If no consensus function is selected and it is required, then the arithmetic mean (AVG) is applied.<br>This parameter is optional."></i></a>
            <br/>
            <input type="radio" name="consensus" id="consensus" value="AVG" onChange="showWAVG(false)" checked> Arithmetic mean
            <input type="radio" name="consensus" id="consensus" value="WAVG" onChange="showWAVG(true)"> Weighted mean
            <input type="radio" name="consensus" id="consensus" value="MAX" onChange="showWAVG(false)"> Maximum value
            <br>
            <div id="SI_weights" class="weights">
            <?php
            array_walk($si_software_list, function($elem) {
            	echo '<div class="miniPanel">';
                echo '  <label title="'.$elem->name.'">'.$elem->name.'</label>';
                echo '  <input type="text" name="weight-'.$elem->idSoftware.'" value="" size="5em">';
                echo '</div>';
            });
            ?>
            </div>
            <div id="PH_weights" class="weights">
            <?php
            array_walk($ph_software_list, function($elem) {
            	echo '<div class="miniPanel">';
                echo '  <label title="'.$elem->name.'">'.$elem->name.'</label>';
                echo '  <input type="text" name="weight-'.$elem->idSoftware.'" value="" size="5em">';
                echo '</div>';
            });
            ?>
            </div>
        </div>
      </div>
      <div id="tabs-5">
        <div>
    	    <span id="SI_cutoff"><b>Similarity cutoff [0,1] (optional):</b></span>
	    <span id="PH_cutoff"><b>Similarity cutoff [0,1] (optional):</b></span>
            <a href="#"><i class="fa fa-info-circle" aria-hidden="true" title="Minimum score that all the results must reach to be displayed. If no cutoff is defined, then all the results will be considered.<br>This parameter is optional."></i></a>
            <br/><input type="text" id="threshold" name="threshold" size="10" maxlength="7"/><br/>
        </div>
        <div>
            <b>Maximum number of hits (optional):</b>
            <a href="#"><i class="fa fa-info-circle" aria-hidden="true" title="Maximum number of results to return. By default, up to 100 results are returned.<br>This parameter is optional."></i></a>
            <br>
            <input type="text" id="maxResultsText" name="maxResultsText" size="2" readonly />
            <input type="hidden" id="maxResults" name="maxResults" />
            <br>
           	<div id="maxResultsSlider" name="maxResultsSlider"></div>
        </div>
      </div>
      <div id="tabs-6">
        <div>
            <b>Job description (optional):</b>
            <a href="#"><i class="fa fa-info-circle" aria-hidden="true" title="A short descriptive text can be attached to the task to differentiate it from others. This text will be included in the confirmation mail.<br>This parameter is optional."></i></a>
            <br><input type="text" id="jobName" name="jobName" size="80"/></br>
        </div>
        <?php
        include $pathTemplate.'buttons/email.php';
        // require $pathTemplate.'php/captcha.php';
        ?>
        <div style="text-align:center;">
            <input class="btn" type="submit" name="send" id="send" value="SEND EXPERIMENT" />
        </div>
      </div>
    </div>
</form>
