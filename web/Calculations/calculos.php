<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once __DIR__.'/../Services/SimilarityParams.php';
require_once __DIR__.'/../Services/impl/SimilarityServiceImpl.php';
require_once __DIR__.'/../Services/impl/Exec.php';
require_once __DIR__.'/../Services/impl/SurveyServiceImpl.php';


// Message to display when the user must fill in the survey
function getSurveyMessage() {
	return "Dear user,<br>You have reached the limit of free tasks before filling in our satisfaction survey.<br>Please, take a minute to answer the questions and we promise not to bother you again.<br><br>Remind that BRUSELAS is totally free of charge, but your opinion is very important for us.<br><br>As soon as you send your answers, all your pending tasks will be automatically enabled and processed as soon as possible:<br><br><a href='http://bio-hpc.ucam.edu/Bruselas2/web/Survey/Survey.php'>http://bio-hpc.ucam.edu/Bruselas2/web/Survey/Survey.php</a><br><br>Thank you for help us improve the tool!";
}

// Validate format of the file.
function validateFormat($filename) {
	// count the number of molecules in the file: it must be 1
	$out = Exec::cmd("obabel $filename -onul -v 'FFFFF' 2>&1 | cut -d\" \" -f1 2> /dev/null | tail -1");
	if ($out !== "1") {
		throw new Exception("The extension of the file must be compliant with its format (see Open Babel documentation for further details).");
	}
}

// Validate a future conversion
function validateConversion($filename) {
	$fmt = pathinfo($filename, PATHINFO_EXTENSION);
	$out = Exec::cmd("obabel $filename -o $fmt -v 'FFFFF' 2>&1 | tail -1 | awk '{print $1}'");
	$n = intval($out[0]);

	if ($n == 0) {
        throw new Exception("The library cannot be converted by obabel. Please fix it an try again.");
	}	
}

// Verify that uploaded files are correct.
function validateFile($filesMap, $key, $element) {
	if (!isset($filesMap[$key])) {
		throw new Exception($element." file is required", 1);
	}
	if ($filesMap[$key]["size"] === 0 || $filesMap[$key]["size"] > 2000000) {
		$size = round($filesMap[$key]["size"]/(1024*1204), 2);
		throw new Exception("FileError: ".$element." file is empty or max size exceeded. Actual size: ".$size."MB (Maximum allowed: 2MB)");
	}
	if ($filesMap[$key]["error"] !== UPLOAD_ERR_OK) {
		throw new Exception("Error while uploading ".$element." file to the web server.");
	}
}

// Verify that a number between 1 and 100 descriptors has been selected.
function validateDescriptors($descriptorList) {
	if (is_null($descriptorList) || count($descriptorList) < 1) {
		throw new Exception("At least one descriptor must be selected. Please, select a number between 1 and 100 descriptors.");
	}
	if (count($descriptorList) > 100) {
		throw new Exception("More than 100 descriptors cannot be selected. Please, select a number between 1 and 100 descriptors.");	
	}
}

// Copy uploaded files to the server.
function copyFileToServer($prefix, $filename, $tempname) {
	$aux = tempnam(sys_get_temp_dir(), "$prefix");
	$foo = $aux.".".pathinfo($filename, PATHINFO_EXTENSION);
	rename($aux, $foo);
	move_uploaded_file($tempname, $foo);
	chmod("$foo", 0777);
	return $foo;
}

// Return the actual value of the variable or its default instead.
function getPostValue($key, $defaultValue) {
	return isset($_POST[$key]) ? $_POST[$key] : $defaultValue;
}

// Return an array containing all the variables whose name matches the pattern.
function getPostArray($arr, $pattern) {
	return array_values(array_map(function($item) use ($arr) {
			return $arr[$item]; 
		}, 
		array_filter(array_keys($arr), function($key) use($pattern) { 
			return strpos($key, $pattern) !== false; 
		})
	));	
}

// Collect POST variables
$pesos = array();
foreach ($_POST as $var => $value) {
	$pos = strpos($var, "weight-");
	if ($pos !== false) {
		$aux = explode("-", $var);
		$pesos[$aux[1]] = $value;
	}
}

// LBVS
try {
	$params = new SimilarityParams();

	// scalar properties
	$params->smiles = getPostValue("smiles", null);
	$params->descriptorExtractor = getPostValue("descriptors", null);
	$params->descriptorList = getPostValue("descriptors_filter", array());
	$params->distance = getPostValue("prefilters", null);
	$params->cutoff = getPostValue("threshold", null);
	$params->maxResults = getPostValue("maxResults", 100);
	$params->description = getPostValue("jobName", null);
	$params->email = getPostValue("email", null);
	$params->calculationType = getPostValue("typeCalc", null);
	$params->keywords = getPostValue("keywords", null);
	$params->isUserLibrary = isset($_FILES["libraryToUpload"]);
	$params->isEnamineLibrary = getPostValue("libraryType", "") == "enamine";
	$params->isMolportLibrary = getPostValue("libraryType", "") == "molport";
	$params->isServerLibrary = !$params->isUserLibrary && !$params->isEnamineLibrary && !$params->isMolportLibrary;
	$params->isQuerySmiles = strlen($params->smiles) > 0;
	$params->isQueryFile = strlen($params->smiles) === 0 && strlen($_FILES["fileToUpload"]["name"]) > 0;	

	// arrays
	if ($params->calculationType === "SI") {
		$params->similarity = getPostArray($_POST, "software-");
	} else if ($params->calculationType === "PH") {
		$params->similarity = getPostArray($_POST, "softwareph-");
	}
	$params->consensusWeights = $pesos;
	$params->filters = getPostArray($_POST, "filter-");

	// patch consensus AVG to WAVG
	$params->consensusFn = getPostValue("consensus", null);
	if ($params->consensusFn === "AVG") {
		$value = 1.0/count( $params->similarity );
		foreach ($params->similarity as $id) {
			$params->consensusWeights[$id] = strval($value);
		}
	}

	// properties requiring a file to be uploaded
	if ($params->isQueryFile) {
		validateFile($_FILES, "fileToUpload", "Query");	
		$params->queryPath = copyFileToServer("bru", $_FILES["fileToUpload"]["name"], $_FILES["fileToUpload"]["tmp_name"]);
		validateFormat($params->queryPath);
	}

	if ( $params->isUserLibrary && $_FILES["libraryToUpload"]["error"] !== UPLOAD_ERR_NO_FILE ) {
		validateFile($_FILES, "libraryToUpload", "Library");
		$params->libraryPath = copyFileToServer("bru-lib", $_FILES["libraryToUpload"]["name"], $_FILES["libraryToUpload"]["tmp_name"]);
		$params->datasets = array();
		validateConversion($params->libraryPath);
	} else {
		$params->libraryPath = null;
		if ($params->isServerLibrary) {
			$params->datasets = getPostArray($_POST, "dataset-");
			validateDescriptors($params->descriptorList);			
		}
	}

	$srv = new SimilarityServiceImpl();	
	$idExp = $srv->executeLBVS($params);
	
	// check if the user is really allowed to launch more tasks
	$survey_srv = new SurveyServiceImpl();
	if (!$survey_srv->isUserUnlocked($params->email)) {
		$survey_srv->lockTasksByUser($params->email);
		header("Location: message.php?idExp=".$idExp."&error=".getSurveyMessage());
		die();
	}	

	header("Location: message.php?idExp=".$idExp."&error=");

} catch (Exception $e) {
	header("Location: message.php?idExp=".$idExp."&error=".$e->getMessage());
	die(1);	
}
?>
