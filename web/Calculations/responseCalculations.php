<?php
$error = isset($_GET['error']) ? $_GET['error'] : "";
$idExp = isset($_GET['idExp']) ? $_GET['idExp'] : NULL;

if ($error == "")
{
	echo '<p>Job has been successfully submitted.<p>';
	echo '<p>ID_Job: '.$idExp.'</p>';         
	echo '<br/>';
	echo '<br/>';
	echo '<p> After job termination you will receive an email notification. <b>Please verify your spam mail.</b></p>';
	echo '<p>Waiting time will depend on cluster occupation. Sit down, grab a cup of coffee, and relax ... It can take up to 24 hours maximun.</p>';
	echo '<br/>';
	echo '<br/>';
} else {
  echo '<p>'.$error.'</p>';
}
?>