<?php
$pathTemplate = "../../../template/";

require_once __DIR__."/".$pathTemplate."php/conexion.php";

// conexion actual
$__conn = NULL;

//error handler function
function errorHandler($errno, $errstr) {
	global $__conn;

	if (isset($__conn)) {
		mysql_free_result($__conn);
		mysql_close($__conn);
	}
	$__conn = getConexion('Antonio_2');
}

// Conectar a la base de datos (privado).
function __connect() {
	global $__conn;

	//set error handler
	set_error_handler("errorHandler");

	if (is_null($__conn) || !mysql_ping($__conn)) {
		$__conn = getConexion('Antonio_2');
	}
}

// Ejecutar actualizaciones (insert, update).
// El valor devuelto puede ser ignorado.
function update($sql) {
	global $__conn;
	
	__connect();
	$res = mysql_query($sql, $__conn);
	$last_id = mysql_insert_id();
	return $last_id;
}

// Ejecutar consultas.
function query($sql) {
	global $__conn;

	__connect();
	$res = mysql_query($sql, $__conn);
	return $res;
}

// Devuelve la fila actual de un resultset.
function fetch_array($res) {
	return mysql_fetch_array($res);
}

// Comprueba si un resultset está vacío.
function isEmpty($res) {
	return (!$res || mysql_num_rows($res) == 0);
}

// Desconectar de la base de datos.
function disconnect() {
	global $__conn;
	
	if (!is_null($__conn)) {
		mysql_close($__conn);
		$__conn = NULL;
	}
}
?>
