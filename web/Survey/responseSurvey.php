<?php
$error = isset($_GET['error']) ? $_GET['error'] : "";

if ($error == "") {
	echo '<p>Your answers have been successfully submitted.<p>';
	echo '<p>If have any pending task, it will be processed as soon as possible.</p>';
	echo '<p>Thank you for your collaboration.</p>';
} else {
	echo '<p>'.$error.'</p>';
	echo '<p><a href="http://bio-hpc.ucam.edu/Bruselas2/web/Survey/Survey.php">Try again.</a></p>';
}
?>
