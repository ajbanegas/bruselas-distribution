<h1 style='font-size:24px;font-weight:bold;text-align:center'>BRUSELAS Server Survey</h1>
<br>
Please, help us improve BRUSELAS Server by answering this survey. It should not take more than a few minutes of your time.<br>
All the questions are mandatory.
<br><br>
<form name="survey" action="sendSurvey.php" method="post">
        <div>
        <b>Please, enter your email address. Make sure it is the same that you gave when launched the task(s)</b>
        <p/>
        <input type="text" name="email" maxlength="80" size="50em" placeholder="email">
        </div>
        <br>

	<div>
	<b>How would you rate your expertise in the following areas: (0 = no knowledge/skills, 5 = expert)</b>
	<p/>
		<table>
			<tr>
			<td>Chemistry</td>
			<td>
        <input type="radio" name="exp_chemistry" value="0">0
        <input type="radio" name="exp_chemistry" value="1">1
        <input type="radio" name="exp_chemistry" value="2">2
        <input type="radio" name="exp_chemistry" value="3">3
        <input type="radio" name="exp_chemistry" value="4">4
        <input type="radio" name="exp_chemistry" value="5">5
			</td>
			</tr>
                       <tr>
                        <td>Virtual Screening</td>
                        <td>
        <input type="radio" name="exp_vs" value="0">0
        <input type="radio" name="exp_vs" value="1">1
        <input type="radio" name="exp_vs" value="2">2
        <input type="radio" name="exp_vs" value="3">3
        <input type="radio" name="exp_vs" value="4">4
        <input type="radio" name="exp_vs" value="5">5
                        </td>
                        </tr>
                       <tr>
                        <td>Informatics</td>
                        <td>
        <input type="radio" name="exp_informatics" value="0">0
        <input type="radio" name="exp_informatics" value="1">1
        <input type="radio" name="exp_informatics" value="2">2
        <input type="radio" name="exp_informatics" value="3">3
        <input type="radio" name="exp_informatics" value="4">4
        <input type="radio" name="exp_informatics" value="5">5
                        </td>
                        </tr>
                       <tr>
                        <td>Pharmacy</td>
                        <td>
        <input type="radio" name="exp_pharmacy" value="0">0
        <input type="radio" name="exp_pharmacy" value="1">1
        <input type="radio" name="exp_pharmacy" value="2">2
        <input type="radio" name="exp_pharmacy" value="3">3
        <input type="radio" name="exp_pharmacy" value="4">4
        <input type="radio" name="exp_pharmacy" value="5">5
                        </td>
                        </tr>
		</table>
	<div>
	<br>

	<div>
	<b>Do you think is sufficiently clear that our server only performs similarity and pharmacophoric searches and not molecular docking?</b>
	<p/>
	<input type="radio" name="clear_enough" value="1">Yes<br>
	<input type="radio" name="clear_enough" value="0">No
	</div>
	<br>

	<div>
	<b>How accurate the results were according your expectations? (0 = totally wrong, 5 = totally accurate)</b>
	<p/>
	<input type="radio" name="accuracy" value="0">0
	<input type="radio" name="accuracy" value="1">1
	<input type="radio" name="accuracy" value="2">2
        <input type="radio" name="accuracy" value="3">3
        <input type="radio" name="accuracy" value="4">4
        <input type="radio" name="accuracy" value="5">5
	</div>
	<br>

        <div>
        <b>What feature(s) would you like to add to the server?</b>
        <p/>
        <input type="checkbox" name="feat_More similarity methods">More similarity methods<br>
	<input type="checkbox" name="feat_More consensus functions">More consensus functions<br>
	<input type="checkbox" name="feat_Additional databases to screen">Additional databases to screen<br>
	<input type="checkbox" name="feat_Docking calculations">Docking calculations<br>
	<input type="checkbox" name="feat_A private profile to store your history of searches">A private profile to store your history of searches<br>
	<input type="checkbox" name="feat_Other way of notification (other than email)">Other way of notification (other than email)<br>
	<input type="text" name="feat_Other" size="50em" maxlength="200" placeholder="Other">
        <div>
        <br>

        <div>
        <b>Do you think the server is easy to use?</b>
        <p/>
        <input type="radio" name="easiness" value="0">Yes, it is easy to use for every kind of user<br>
        <input type="radio" name="easiness" value="1">Yes, but only for experts in informatics<br>
        <input type="radio" name="easiness" value="2">No, it is difficult to work with
        </div>
        <br>

        <div>
        <b>How would you improve the user interface?</b>
        <p/>
        <textarea name="improve_ui" placeholder="your suggestions and comments" cols="80"></textarea>
        </div>
        <br>

        <div>
        <b>Evaluate your overall level of satisfaction with the tool (0 = totally unsatisfied, 5 = totally satisfied)</b>
        <p/>
        <input type="radio" name="satisfaction" value="0">0
        <input type="radio" name="satisfaction" value="1">1
        <input type="radio" name="satisfaction" value="2">2
        <input type="radio" name="satisfaction" value="3">3
        <input type="radio" name="satisfaction" value="4">4
        <input type="radio" name="satisfaction" value="5">5
        <div>
        <br>

        <div>
        <b>Indicate your position category in your institution/company (multiple choice)</b>
        <p/>
        <input type="checkbox" name="cat_Undergraduate Student">Undergraduate Student<br>
	<input type="checkbox" name="cat_PhD Student">PhD Student<br>
	<input type="checkbox" name="cat_Postdoc">Postdoc<br>
	<input type="checkbox" name="cat_Researcher Fellow">Researcher Fellow<br>
	<input type="checkbox" name="cat_Principal Investigator">Principal Investigator<br>
	<input type="checkbox" name="cat_Professor">Professor<br>
	<input type="checkbox" name="cat_Industry">Industry<br>
        <input type="text" name="cat_Other" size="50em" maxlength="200" placeholder="Other">
        </div>
        <br>

	<div style="text-align:center">
		<input class="btn" type="submit" name="send" id="send" value="SEND SURVEY" />
	</div>
</form>
