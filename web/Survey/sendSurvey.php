<?php
require_once __DIR__.'/../Services/impl/SurveyServiceImpl.php';

# Validate the answers
$success = TRUE;
$responses = array();

function is_empty($str) {
	return strlen(trim($str)) <= 0;
}

function check_variable($varName, $question, $option) {
	global $responses;

	if (!isset($_POST[$varName]) || is_empty($_POST[$varName])) {
		return FALSE;
	}
	$responses[$question][$option] = $_POST[$varName];
	return TRUE;
}

function check_multioption($prefix, $question, $options, $default) {
	global $responses;

	$any = FALSE;
	$i = 1;
	foreach ($options as $opt) {
		$key = str_replace(" ", "_", $prefix.$opt);
		if (isset($_POST[$key]) && !is_empty($_POST[$key])) {
			$any = TRUE;
			$value = ($opt == $default) ? $_POST[$prefix.$default] : $key;
			$responses[$question][$i] = str_replace("_", " ", str_replace($prefix, "", $value));
		}
		$i++;
	}
	return $any;
}

$success = $success && check_variable('email', '1', '1');
$success = $success && check_variable('exp_chemistry', '2', '1');
$success = $success && check_variable('exp_vs', '2', '2');
$success = $success && check_variable('exp_informatics', '2', '3');
$success = $success && check_variable('exp_pharmacy', '2', '4');
$success = $success && check_variable('clear_enough', '3', '1');
$success = $success && check_variable('accuracy', '4', '1');
$success = $success && check_multioption('feat_', '5', array('More similarity methods','More consensus functions','Additional databases to screen','Docking calculations','A private profile to store your history of searches','Other way of notification (other than email)','Other'), 'Other');
$success = $success && check_variable('easiness', '6', '1');
$success = $success && check_variable('improve_ui', '7', '1');
$success = $success && check_variable('satisfaction', '8', '1');
$success = $success && check_multioption('cat_', '9', array('Undergraduate Student','PhD Student','Postdoc','Researcher Fellow','Principal Investigator','Professor','Industry','Other'), 'Other');

if (!$success) {
        # Redirect to the error screen
        header("Location: message.php?error=".urlencode("Some questions were not answered.<br>Please, fill them all and submit the survey again."));
        die();
}

# Record the answers in the database
$srv = new SurveyServiceImpl();
$srv->save($responses);

# Unlock all the pending tasks of the user
$srv->unlockTasksByUser($_POST['email']);

# Redirect to success screen
header("Location: message.php");
?>
