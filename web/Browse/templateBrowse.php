<style type="text/css">
.miniPanel {
	float:left;
	margin: 0 0.5em;
	text-align: center;
}
#options {
	width: 100%;
	margin:0 auto 1em auto;	
}
#basic-addon2 > button {
  background-color: Transparent;
  background-repeat:no-repeat;
  border: none;
  overflow: hidden;  
}
.table-results {
  font-size: small;
  table-layout: fixed;
}
.table-results th {
  font-weight: bold;
}
.table-row-default {
  table-layout: fixed; 
  word-wrap: break-word;
}
.table-row-small {
  table-layout: fixed; 
  word-wrap: break-word; 
}
</style> 
<script src="../js/jquery.min.js"></script>
<script type="text/javascript">
function createTable(data) {
  table = $('<table></table>').attr({ class: ["table", "table-bordered", "table-hover", "table-condensed", "table-results"].join(' ') });

  var header = $('<tr></tr>').appendTo(table); 
  $('<th>Name</th>').css('width','29%').appendTo(header);
  $('<th>Formula</th>').css('width','9%').appendTo(header);
  $('<th>Smiles</th>').css('width','60%').appendTo(header);

  var rows = data.length;
  var cols = 3;
  var tr = [];
  for (var i = 0; i < rows; i++) {
    var row = $('<tr></tr>').appendTo(table);
    var link = 'consulta.php?idDrug='+data[i].id;

    $('<td></td>').html("<a class='simple' href='"+link+"' target='_blank'>"+data[i].name+"</a>").attr({ class: "table-row-default" }).appendTo(row); 
    $('<td></td>').text(data[i].formula).attr({ class: "table-row-small" }).appendTo(row); 
    $('<td></td>').text(data[i].smiles).attr({ class: "table-row-small" }).appendTo(row); 
  }
  $("#resultsDiv").html(table);
}

$(function() {
	var delayTimer = null;

  	$('#bus').focus();
  	$('#bus').on('input', function() {
  		if (delayTimer) {
 			clearTimeout(delayTimer);
 		}

    	delayTimer = setTimeout(function() {
    		var pattern = $('#bus').val();
	        // Do the ajax stuff
		    if (pattern && pattern.length > 1) {
		        $.post("searcher.php", { table: "drug", query: $("input[name=op2]:checked").val(), pattern: pattern })
		          .done(function(data) {
		            if (data) {
		              var arr = JSON.parse(data);
		              if (arr.length > 0) {
		                createTable(arr);
		              } else {
		                $("#resultsDiv").html("<label>No results found</label>");
		              }
		            } else {
		              $("#resultsDiv").html("<label>No results found</label>");
		            }
		        });
		    } else {
				$("#resultsDiv").html("");    	
		    }
    	}, 1000); // Will do the ajax stuff after 1000 ms, or 1 s
    	$("#resultsDiv").html("<label>Searching...</label>");
  	});
});
</script>

<div id="options">
    <div class="greyPanelL container-fluid">
        <p>Type molecule name, smiles or datasource identifier to carry out database searches. You can click on the desired molecule for further information.</p>
        <form name="test" class="form-horizontal" onsubmit="return doSearch()">
            <div class="form-group">
                <div class="col-sm-5">
                    <div class="input-group">
                      <input type="text" class="form-control" id="bus" placeholder="Chemical name, IUPAC name, InChI Key, SMILES or database ID" aria-describedby="basic-addon2" required size="50">
                    </div>
                </div>
            </div>
        </form>
	</div>
</div>

<div id="resultsDiv"></div>
