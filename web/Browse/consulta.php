<?php
require_once '../Services/impl/DrugServiceImpl.php';

function downloadFile($id) {
	$srv = new DrugServiceImpl();
	$drug = $srv->findDrugById($id);

	header('Content-Type: chemical/x-mol2');
	header('Content-Disposition: attachment; filename=BRUSELAS'.$id.'.mol2');
	header('Pragma: no-cache');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

	$drug_name = pathinfo(pathinfo($drug->filename)['filename'])['filename']."_1.mol2";
	exec("tar -axf ".$drug->filename." $drug_name -O", $output);
	echo implode(PHP_EOL, $output);
}

if (isset($_GET['action']) && $_GET['action'] === "download") {
	downloadFile($_GET['idDrug']);
} else {
	$pathTemplate="../../../template/";
	#
	# apaño donde se le indica el templae  con una pagina extra
	#
	$pagExtra="ResumenMolecule.php";
	require '../../templateWeb.php';
}
?>
