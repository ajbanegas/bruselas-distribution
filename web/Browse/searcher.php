<?php
session_start();
ob_start();
$table = isset($_POST['table']) ? $_POST['table'] : null ; 
$query = isset($_POST['query']) ? $_POST['query'] : null ; 
$inputValue = isset($_POST['pattern']) ? $_POST['pattern'] : null ; 
$MAX = 10;

$pathTemplate = "../../../template/";
require_once '../Services/impl/DrugServiceImpl.php';

$srv = new DrugServiceImpl();
$list = $srv->findDrugByName($table, $query, $inputValue, $MAX);
echo json_encode($list);
?>
