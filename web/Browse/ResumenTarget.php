<style type="text/css">
 
	#tabbli
	{

		width: 78%;
		float: left;
		margin-top: 1.7em;
	}
	#title
	{
		text-align: center;
		float: left;
		width: 75%;
		height:6em;
		font-size: 1.3em;
	
	}
	#titleRef
	{
		float: right;
		width: 20%;
	}
	#imagenSturctCapa
	{
		width: 100%;
		height: 14em;
 		float: left;
	}
	#latIzq{

			float: right;
			width: 20%;
	/*		margin-top: 0.3em;*/
	}
	#fila
	{
		float:left;
		width: 100%;
	}
	table{
	width: 100%;
	word-wrap:break-word; 
	table-layout: fixed;
	}
</style>
<?php
#
#	Para estas ventanas se hace un apaño
#	y se llama staticamente a la plantilla (no se mecourre otra cosa)
#
$uniProt = $_GET['idDrug'];#id del farmaco
#
#	Ampliar imagenes
#
$pathTemplate="../../../template/";
require $pathTemplate.'php/references.php';
require $pathTemplate."php/conexion.php";
require $pathTemplate.'js/ampliarImagenes.php';
$con=getConexion("Antonio");


#$sql="select * from TargetsNew where uniProt= '".$idDrug."'";
#select * from TargetsDrugsNew LEFT OUTER JOIN DrugsNew on (TargetsDrugsNew.idDrug=DrugsNew.idDrug) LEFT OUTER JOIN  Targets on (TargetsDrugsNew.uniProt=Targets.uniProt) and TargetsDrugsNew.uniProt='O00482' WHERE TargetsDrugsNew.idDrug IS NOT NULL 
$sql="SELECT DrugsNew.*, TargetsDrugsNew.*, TargetsNew.* FROM DrugsNew     JOIN TargetsDrugsNew  ON DrugsNew.idDrug = TargetsDrugsNew.idDrug     JOIN TargetsNew         ON TargetsDrugsNew.uniProt = TargetsNew.uniProt WHERE TargetsNew.uniProt='".$uniProt."';";


$res=mysql_query($sql,$con);
if(mysql_num_rows($res)==0){
	echo '<b>No hay sugerencias</b>';
}else 
{ 
	$contador=0;
	while($fila=mysql_fetch_array($res))
	{	
		if ($contador==0) #aparño apra pnoner el titulo
		{
			echo '<div id=title>';
			echo '<p><b>'.$fila["proteinName"]."</p></b>";
			echo '<p>';
			echo '<a href="http://www.uniprot.org/uniprot/'.$fila["uniProt"].'">'.$fila["uniProt"].'</a></p></b></br>';
			echo '<p>Know Ligands for this target</p>';
			echo '</div>';
			echo '<div id=titleRef>';
			putReferences("9,10,11,12");
			echo '</div>';
			$contador+=1;

		}

		echo '<div id=fila>';
			echo '<div id=latIzq>';
				echo '<br>';
				echo "<div id=imagenSturctCapa>";
					echo '<div class="greyPanelC">';	
					
						$urlImg='https://pubchem.ncbi.nlm.nih.gov/image/imgsrv.fcgi?cid='.$fila["pubChemLid"].'&t=l';
						echo '<img src="'.$urlImg.'" alt="Image Struct">';
						#echo '<p>Image src:</p><p><FONT SIZE=2>https://pubchem.ncbi.nlm.nih.gov/image/imgsrv.fcgi</FONT> </p><br>';
						echo '<br>';	
					echo '</div>';
				echo '</div>';
			echo '</div>';
			echo '<div id="tabbli">';
				echo '<div class="greyPanelL">';
	        	echo '<table style="margin:0">';
	        		if (!empty($fila["nameCH"])) {
		            	echo '<tr>';
	    	        	echo '<td width=150px><b>Drug name:</b> </td>';
	            		echo '<td>'.$fila["nameCH"].'</td>';
	            		echo '</tr>';
					}	
	           		if (!empty($fila["iuPackName"])) {
	   					echo '<tr>';
		            	echo '<td><b>IUPACName:</b>     </td>';
		            	echo '<td>'. $fila["iuPackName"].' </td>';
		            	echo '</tr>';
					}

					if (!empty($fila["smiles"])) {
			            echo '<tr>';
			            echo '<td><b>Smiles:</b>     </td>';
			            echo '<td>'. $fila["smiles"].' </td>';
			            echo '</tr>';
					}

					if (!empty($fila["diabetesType"])) {

					    echo '<tr>';
					    echo '<td><b>Diabetes Type: </b></td>';
					    echo '<td>'.$fila["diabetesType"].'</td>';
					    echo '</tr>';
					 }
					 if (!empty($fila["drugFamily"])) {
					 	$sql2='select * from DrugsNew join Drug_family where id_Drug_family=drugFamily AND idDrug='.$idDrug ;
					 	$res2=mysql_query($sql2,$con);
					 	$fila2=mysql_fetch_array($res2);
					    echo '<tr>';
					    echo '<td><b>Drug family:</b></td>';
					    echo '<td>'. ucfirst ($fila2["description"]).'</td>';
					    echo '</tr>';
					}
					if (!empty($fila["state"])) {
					    echo '<tr>';
					    echo '<td><b>state: </b> </td>';
					    echo '<td>'.$fila["state"].'</td>';
					    echo '</tr>';	    
					}
					if (!empty($fila["codZinc"])) {
			            echo '<tr>';
					    echo '<td><b>ZINC ID: </b></td>';
					    echo '<td><a href="http://zinc15.docking.org/substances/'.$fila["codZinc"].'">'.$fila["codZinc"].'</a></td>';
					    echo '</tr>';
					}
					if (!empty($fila["codChembl"])) {
			            echo '<tr>';
					    echo '<td><b>ChemBL ID: </b></td>';
					    #https://www.ebi.ac.uk/chembl/compound/inspect/CHEMBL268624
					    echo '<td><a href="https://www.ebi.ac.uk/chembl/compound/inspect/'.$fila["codChembl"].'"> '.$fila["codChembl"].'</a></td>';
					    echo '</tr>';
					}
					if (!empty($fila["codDrugBank"])) {
					    echo '<tr>';
					    echo '<td><b>DrugBank ID:</b></td>';
					   # #http://www.drugbank.ca/drugs/DB01050
					     echo '<td><a href="http://www.drugbank.ca/drugs/'.$fila["codDrugBank"].'"">'.$fila["codDrugBank"].'</a></td>';
					    echo '</tr>';
					}

				    if (!empty($fila["pubChemLid"])) {
					    echo '<tr>';
					    #https://pubchem.ncbi.nlm.nih.gov/compound/
					    echo '<td><b>PubChem CID: </b></td>';
					    echo '<td><a href="https://pubchem.ncbi.nlm.nih.gov/compound/'.$fila["pubChemLid"].'"> '.$fila["pubChemLid"].'</a></td>';
					    echo '</tr>';
				    }	
			    echo '</table>';
			echo '</div>';
			echo '</div>';
		echo '</div>';
       }
     } 

?>