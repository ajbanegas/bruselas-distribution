<link rel="stylesheet" type="text/css" href="../css/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="../css/jquery.fancybox.min.css">
<link rel="stylesheet" type="text/css" href="ResumenMolecule.css">
<link rel="stylesheet" type="text/css" href="../css/font-awesome-4.7.0/css/font-awesome.min.css">
<?php
$pathTemplate="../../../template/";
$idDrug = $_GET['idDrug'];	// drug id

require_once $pathTemplate.'php/references.php';
require_once '../Services/impl/DrugServiceImpl.php';
require_once '../Services/impl/DescriptorServiceImpl.php';
require_once $pathTemplate.'jsmol/jsmol.php';
?>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery-ui.min.js"></script>
<script src="../js/jquery.fancybox.min.js"></script>
<script src="../js/bruselas.js"></script>
<script type="text/javascript">
$(function(){
	$("a#query-img").fancybox({
		'overlayShow'	: false,
		'transitionIn'	: 'elastic',
		'transitionOut'	: 'elastic'
	});
});	
</script>
<?php
function loadSurface($filename) {
	$temp = "../../imagenesWeb/temp3d.mol2";
	$drug_name = pathinfo(pathinfo($filename)['filename'])['filename']."_1.mol2";
	exec("tar -axf $filename $drug_name -O", $output);
	$content = implode(PHP_EOL, $output);
	file_put_contents($temp, $content);

	$html = "<div id='appdiv1' class='surface-applet'></div>";
	$html .= "<script type='text/javascript'>";
	$html .= "	var info1 = getInfo('".$temp."','".$temp."');";
	$html .= "	Jmol.setDocument(false);";
	$html .= "	var applet = Jmol.getApplet('jmolApplet1', info1);";
	$html .= "	$('#appdiv1').html(Jmol.getAppletHtml(jmolApplet1));";
	$html .= "	$('#appdiv1').find('div').css('z-index',0);";
	$html .= "	Jmol.resizeApplet(jmolApplet1, [200,200]);";
	$html .= "	Jmol.script(applet, 'rotate best;center all;dots on;');";
	$html .= "</script>";
	return $html;
}

function print2D($urlImg) {
	$url = (empty($urlImg) ? '../Images/not_available.png' : $urlImg);

	echo "<div class='image-container'>";
	echo "	<a id='query-img' href='$url'>";
	echo "  	<img src='$url' alt='Image Struct' class='image2D' width='200' height='200'>";
	echo "	</a>";
	echo "</div>";
}

function print3D($filename) {
    echo '<div class="image-container">';
	echo loadSurface($filename);
	echo '</div>';
}

function printReferences($refs) {
    echo '<div class="ref-container">';
	echo '	<div><b>References</b></div>';
    echo '	<div id="ref-list"></div>';
    echo '	<script>showRef('.$refs.');</script>';
    echo '</div>';
}

function printInfo($drug, $alerts) {
	global $idDrug;

	echo "<div style='width:100%; display: flex;'>";
	echo "	<div style='float: left; width: 65%;'>";
	if (!empty($drug->name)) {
		echo "	<div style='float: left; width: 20%;'><b>Drug Name:</b></div>";
		echo "	<div>".$drug->name."</div>";
	}
	if (!empty($drug->chemicalName)) {
		echo "	<div style='float: left; width: 20%;'><b>IUPAC Name:</b></div>";
		echo "	<div>".$drug->chemicalName."</div>";
	}
	if (!empty($drug->inchiKey)) {
		echo "	<div style='float: left; width: 20%;'><b>InChI Key:</b></div>";
		echo "	<div>".$drug->inchiKey."</div>";
	}
	if (!empty($drug->formula)) {
		echo "	<div style='float: left; width: 20%;'><b>Formula:</b></div>";
		echo "	<div>".$drug->formula."</div>";
	}	
	if (!empty($drug->atcCode)) {
		echo "	<div style='float: left; width: 20%;'><b>ATC Code:</b></div>";
		echo "	<div>".$drug->atcCode."</div>";
	}
	if (!empty($drug->smiles)) {
		echo "	<div style='float: left; width: 20%;'><b>SMILES:</b></div>";
		echo "	<div>".$drug->smiles."</div>";
	}
	if (!empty($drug->familyName)) {
		echo "	<div style='float: left; width: 20%;'><b>Familiy Name:</b></div>";
		echo "	<div>".$drug->familyName."</div>";
	 }
	if (!empty($drug->state)) {
		echo "	<div style='float: left; width: 20%;'><b>State:</b></div>";
		echo "	<div>".$drug->state."</div>";
	}
	if (!empty($drug->type)) {
		echo "	<div style='float: left; width: 20%;'><b>Type:</b></div>";
		echo "	<div>".$drug->type."</div>";
	}
	if (!empty($drug->srcName)) {
		echo "	<div style='float: left; width: 20%;'><b>Source:</b></div>";
		echo "	<div><a href='".$drug->srcLink."' target='_blank'>".$drug->srcName."</a></div>";
	}
	if (!empty($drug->chemblId)) {
		echo "	<div style='float: left; width: 20%;'><b>ChEMBL ID:</b></div>";
		echo "	<div>".$drug->chemblId."</div>";
	}
	if (!empty($drug->drugbankId)) {
		echo "	<div style='float: left; width: 20%;'><b>DrugBank ID:</b></div>";
		echo "	<div>".$drug->drugbankId."</div>";
	}
	if (!empty($drug->keggId)) {
		echo "	<div style='float: left; width: 20%;'><b>KEGG ID:</b></div>";
		echo "	<div>".$drug->keggId."</div>";
	}
	if (!empty($drug->diadbId)) {
		echo "	<div style='float: left; width: 20%;'><b>DIA-DB ID:</b></div>";
		echo "	<div>".$drug->diadbId."</div>";
	}
	echo "	</div>";
	echo "	<div style='float: left; width: 35%;'>";
	echo "  	<fieldset class='alerts-fieldset'>";
	echo "			<legend><b>Alerts</b></legend>";
	foreach ($alerts as $alert) {
		if ($alert->compliant) {
			echo "<i class='fa fa-check' aria-hidden='true' style='color:green'></i>";
		} else {
			echo "<i class='fa fa-exclamation-triangle' aria-hidden='true' style='color:red'></i>";
		}
		echo $alert->name." (".$alert->violations." violations)"."<br>";
	}
	echo "		</fieldset>";
	echo "	</div>";
	echo "</div>";	
	echo "<div style='width: 100%;'>";
	echo '	<form id="downloadMolecule" action="consulta.php" method="GET">';
	echo '	<div style="text-align:center;">';
	echo '		<input type="button" class="btn" value="Download" onclick="submit()">';
	echo '		<input type="hidden" name="idDrug" id="idDrug" value="'.$idDrug.'">';
	echo '		<input type="hidden" name="action" id="action" value="download">';
	echo '	</div>';
	echo '	</form>';
	echo "</div>";
}

function printDescriptors($descriptors) {
	echo '<div><b>Calculated Descriptors:</b></div>';
	echo '<table class="descriptorsTable">';
	echo '	<tr>';
	echo '		<th><b>Mol.Weight</b></th>';
	echo '		<th><b>AlogP</b></th>';
	echo '		<th><b>HBA</b></th>';
	echo '		<th><b>HBD</b></th>';
	echo '		<th><b>Rotatable Bonds</b></th>';
	echo '		<th><b>Top.Polar Surf.Area</b></th>';
	echo '	</tr>';
	echo '	<tr>';
	echo '		<td>'.number_format($descriptors["MW"],3,',','.').'</td>';
	echo '		<td>'.number_format($descriptors["ALOGP"],3,',','.').'</td>';
	echo '		<td>'.number_format($descriptors["nHAcc"],0,',','.').'</td>';
	echo '		<td>'.number_format($descriptors["nHDon"],0,',','.').'</td>';
	echo '		<td>'.number_format($descriptors["RBN"],0,',','.').'</td>';
	echo '		<td>'.number_format($descriptors["TPSA(Tot)"],3,',','.').'</td>';
	echo '	</tr>';
	echo '	<tr>';
	echo '		<th><b>Average Mol.Weight</b></th>';
	echo '		<th><b>Mean Volume(van der Waals)</b></th>';
	echo '		<th><b>NºAtoms</b></th>';
	echo '		<th><b>NºAromatic Bonds</b></th>';
	echo '		<th><b>Heavy Atoms</b></th>';
	echo '		<th><b>Comp.Lipinski Alert Index</b></th>';
	echo '	</tr>';
	echo '	<tr>';
	echo '		<td>'.number_format($descriptors["AMW"],3,',','.').'</td>';
	echo '		<td>'.number_format($descriptors["Mv"],3,',','.').'</td>';
	echo '		<td>'.number_format($descriptors["nAT"],0,',','.').'</td>';
	echo '		<td>'.number_format($descriptors["nAB"],0,',','.').'</td>';
	echo '		<td>'.number_format($descriptors["nHM"],0,',','.').'</td>';
	echo '		<td>'.number_format($descriptors["cRo5"],3,',','.').'</td>';
	echo '	</tr>';
	echo '</table>';	
}

$drugSrv = new DrugServiceImpl();
$descSrv = new DescriptorServiceImpl();
$drug = $drugSrv->findDrugById($idDrug);

if(count($drug) === 0) {
	echo '<b>Your search did not match any compound</b>';
	return;
}

$descriptors = array(
	"MW" => $descSrv->findByDrugAndCode($idDrug, 1, 'MW'),
	"ALOGP" => $descSrv->findByDrugAndCode($idDrug, 1, 'ALOGP'),
	"nHAcc" => $descSrv->findByDrugAndCode($idDrug, 1, 'nHAcc'),
	"nHDon" => $descSrv->findByDrugAndCode($idDrug, 1, 'nHDon'),
	"RBN" => $descSrv->findByDrugAndCode($idDrug, 1, 'RBN'),
	"TPSA(Tot)" => $descSrv->findByDrugAndCode($idDrug, 1, 'TPSA(Tot)'),
	"AMW" => $descSrv->findByDrugAndCode($idDrug, 1, 'AMW'),
	"Mv" => $descSrv->findByDrugAndCode($idDrug, 1, 'Mv'),
	"nAT" => $descSrv->findByDrugAndCode($idDrug, 1, 'nAT'),
	"nAB" => $descSrv->findByDrugAndCode($idDrug, 1, 'nAB'),
	"nHM" => $descSrv->findByDrugAndCode($idDrug, 1, 'nHM'),
	"cRo5" => $descSrv->findByDrugAndCode($idDrug, 1, 'cRo5')
);

$alerts = array(
	"ro5" => $drugSrv->findAlert($idDrug, 'ro5'),
	"ghose" => $drugSrv->findAlert($idDrug, 'ghose'),
	"veber" => $drugSrv->findAlert($idDrug, 'veber'),
	"375" => $drugSrv->findAlert($idDrug, '375')
);

echo '<div>';
echo '	<div class="mainPane greyPanelL">';
printInfo($drug, $alerts);
printDescriptors($descriptors);
echo '	</div>';
echo '	<div class="rightPane">';
print2D($drug->imageLink);
print3D($drug->filename);
printReferences("10,15,17,18,19");
echo '	</div>';
echo '</div>';
?>
