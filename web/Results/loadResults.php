<?php
require __DIR__."/../Services/impl/ExperimentServiceImpl.php";

$idExp = isset($_GET["idexp"]) ? $_GET["idexp"] : 0;
$srv = new ExperimentServiceImpl();
$res = $srv->loadResults($idExp);

$out = array("data" => $res);
echo json_encode($out, JSON_PRETTY_PRINT);
?>
