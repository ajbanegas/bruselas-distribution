<?php
$pathTemplate = "../../../template/";

require_once $pathTemplate.'jsmol/jsmol.php';
require_once __DIR__."/../Services/impl/ExperimentServiceImpl.php";

// collect parameters
$idResult = isset($_GET["idResult"]) ? $_GET["idResult"] : "";
if (strlen($idResult) === 0) {
	die(1);
}
?>
<link rel="stylesheet" type="text/css" href="../css/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="./Results.css">
<link rel="stylesheet" type="text/css" href="./panel-jsmol.css">

<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript">
function downloadSmiles(path) {
	window.location = "downloader.php?type=ligand_smiles&path=" + encodeURI(path);
}
function downloadLigand(path) {
	window.location = "downloader.php?type=ligand&path=" + encodeURI(path);
}
</script>
<?php
// get data
function paintResultBox($item) {
	?>
	<div class="panel-container panel-container-extended">
		<div class="panel-header">
			<div class="panel-header-text">
				<b><?= $item->name ?>: <?= $item->score ?></b>
			</div>
			<div class="panel-header-icon">
				<a onclick="downloadSmiles('<?= $item->ligand_path ?>')">
					<i class="fa fa-smile-o" aria-hidden="true"></i>
				</a>
				<a onclick="downloadLigand('<?= $item->ligand_path ?>')">
					<i class="fa fa-download" aria-hidden="true"></i>
				</a>
			</div>
		</div>
		<div class="panel-mainbody" id="panel<?= $item->experiment_result_detail_id ?>">
			<div id='appdiv<?= $item->experiment_result_detail_id ?>'></div>
			<script type='text/javascript'>
				var info<?= $item->experiment_result_detail_id ?> = getInfo('<?= $item->query_path ?>','<?= $item->ligand_path ?>');
				Jmol.setDocument(false);
				var applet<?= $item->experiment_result_detail_id ?> = Jmol.getApplet('jmolApplet<?= $item->experiment_result_detail_id ?>', info<?= $item->experiment_result_detail_id ?>);
				$('#appdiv<?= $item->experiment_result_detail_id ?>').html(Jmol.getAppletHtml(jmolApplet<?= $item->experiment_result_detail_id ?>));
				$('#appdiv<?= $item->experiment_result_detail_id ?>').find('div').css('z-index', 0);
				$('#jmolApplet<?= $item->experiment_result_detail_id ?>_appletinfotablediv').css('width', '100%');
				$('#jmolApplet<?= $item->experiment_result_detail_id ?>_appletinfotablediv').css('text-align', 'center');
				Jmol.script(applet<?= $item->experiment_result_detail_id ?>, 'rotate best;center all;');
				// dots on; -> superficie de puntos
				// wireframe 0.15; spacefill 23%; -> bolas y varillas
				// wireframe 0.3; spacefill off; -> sticks
				// wireframe only; -> wireframe
				//Jmol.resizeApplet(jmolApplet<?= $item->experiment_result_detail_id ?>, [570,300]);
			</script>
		</div>
		<div class="panel-footer">
			<div class='legendQuery'>&nbsp;</div>
			<div class='legendText'>Query</div>
			<div class='legendHit'>&nbsp;</div>
			<div class='legendText'>Hit</div>
		</div>
	</div>
<?php
}

$expSrv = new ExperimentServiceImpl();
$results = $expSrv->findResultDetails($idResult);
if (count($results) === 0) {
	echo "There is no results for this compound in the experiment.";
	die(1);
}
$idExp = $results[0]->experiment_id;
$compoundName = $results[0]->chemical_name;
$score = $results[0]->final_score;
?>
<div style='float:left; width:100%;'>
	<div class='greyPanelL parameters-box' style="width: 100%;">
		<h1>
			Alignments of <b><?php echo $compoundName; ?></b> in experiment <b><?php echo $idExp; ?>: <?php echo $score; ?></b>
		</h1>

		<?php
		array_walk($results, function($item) {
			paintResultBox($item);
		});
		?>
	</div>
</div>
