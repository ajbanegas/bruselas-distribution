<?php
require_once __DIR__.'/../Services/impl/Exec.php';
require_once __DIR__.'/../Services/impl/ExperimentServiceImpl.php';
require_once __DIR__.'/../Services/impl/Excel.php';


function headers($filename, $contenttype) {
	header("Cache-Control: public");
	header("Content-Description: File Transfer");
	header("Content-Disposition: attachment; filename=".$filename);
	header("Content-Type: ".$contenttype);
}

function downloadPyMOL($idExp) {
	$sessionFile = __DIR__."/../../imagenesWeb/$idExp/BRUSELAS-$idExp.pse";
	// set response headers
	headers(basename($sessionFile), "chemical/x-mol2");
	// read file
	readfile("$sessionFile");
}

function downloadMolecules($idExp) {
	$moleculesFile = __DIR__."/../../imagenesWeb/$idExp/BRUSELAS-$idExp.tar.gz";
	// set response headers
	headers(basename($moleculesFile), "application/gzip");
	// read file
	readfile("$moleculesFile");
}

function downloadLigandSmiles($path) {
	$smiles = Exec::babel($path, "", "-osmi");
	$file = pathinfo(basename($path), PATHINFO_FILENAME).".smi";
	// set response headers
	headers($file, "x-application/text");
	// send output
	echo $smiles;
}

function downloadLigand($path) {
	// set response headers
	headers(basename($path), "chemical/x-mol2");
	// read file
	readfile($path);
}

function downloadExcel($idExp) {
	$srv = new ExperimentServiceImpl();

	$columns = array("Rank", "Name", "Code", "Total Score", "LiSiCA", "WEGA", "OptiPharm", "Screen3D", "SHAFTS", "Pharmer");
	$rows = array_map(function($item) {
		$rank = $item[0];
		$name = $item[3];
		$code = $item[4];
		$score = $item[5];
		$lisica = $item[6];
		$optipharm = $item[7];
		$screen3d = $item[8];
		$shafts = $item[9];
		$wega = $item[10];
		$pharmer = $item[11];
		
		return array($rank, $name, $code, $score, $lisica, $wega, $optipharm, $screen3d, $shafts, $pharmer);
	}, $srv->loadResults($idExp));

	// download file
	$path = Excel::build($idExp, $columns, $rows);
	headers(basename($path), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	readfile($path);
}

function string2float($value) {
	if (is_null($value) || strlen($value) === 0) {
		return 0.0;
	}
	$str = str_replace(",", ".", $value);
	return floatval($str);
}

$type = isset($_GET["type"]) ? $_GET["type"] : "";
if (strlen($type) === 0) {
	die(1);
}

if ($type === "pymol") {
	// download all the hits in a PyMOL session
	$idExp = isset($_GET["idExp"]) ? $_GET["idExp"] : "";
	if (strlen($idExp) === 0) {
		die(1);
	}
	downloadPyMOL($idExp);
} else if ($type === "ligand_smiles") {
	// download the ligand in format SMILES
	$path = isset($_GET["path"]) ? $_GET["path"] : "";
	if (strlen($path) === 0) {
		die(1);
	}
	downloadLigandSmiles(urldecode($path));
} else if ($type === "ligand") {
	// download the aligned ligand
	$path = isset($_GET["path"]) ? $_GET["path"] : "";
	if (strlen($path) === 0) {
		die(1);
	}
	downloadLigand($path);
} else if ($type === "molecules") {
	// download the aligned results in a compressed file
	$idExp = isset($_GET["idExp"]) ? $_GET["idExp"] : "";
	if (strlen($idExp) === 0) {
		die(1);
	}
	downloadMolecules($idExp);
} else if ($type === "excel") {
	// download the results in Excel format
	$idExp = isset($_GET["idExp"]) ? $_GET["idExp"] : "";
	if (strlen($idExp) === 0) {
		die(1);
	}
	downloadExcel($idExp);
}
?>
