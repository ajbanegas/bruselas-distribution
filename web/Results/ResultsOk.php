<?php
require $pathTemplate.'jsmol/jsmol.php';
require $pathTemplate.'js/ampliarImagenes.php';
require __DIR__."/../Services/impl/ExperimentServiceImpl.php";

$idExp = $_GET['idExp'];
$email = $_GET['email'];

function isActive($idExp) {
	$ligand = "../../imagenesWeb/$idExp";
	return file_exists($ligand);
}
?>
<link rel="stylesheet" type="text/css" href="../css/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="./Results.css">
<link rel="stylesheet" type="text/css" href="../css/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="../css/jquery.fancybox.min.css">
<style type="text/css">
th {
	width: 25%;
	padding-left: 10px;
}
.parameters-container {
	width: 100%; 
	margin-bottom: 10px;
}
.parameters-data {
	float: left; 
	width: 70%;
}
.parameters-query {
	float: left; 
	width: 30%; 
	text-align: center;
}
.dataTables_wrapper {
	font-size: 12px;
}
.results-table {
	width: 100%; 
	float: left;
}
.icon {
	cursor: pointer;
}
.references, .citation {
	width: 100%; 
	float: left; 
	text-align: center;
}
#ref-list > a {
	font-size: 16px;
}
</style>

<script src="../js/jquery.min.js"></script>
<script src="../js/jquery-ui.min.js"></script>
<script src="../js/jquery.fancybox.min.js"></script>
<script src="../js/bruselas.js"></script>
<script type="text/javascript">
function shareResults(user, idExp) {
	var url = "Please, find below a link to the results of experiment "+idExp+" performed on BRUSELAS server.\n\nhttp://bio-hpc.ucam.edu/Bruselas/web/Results/showResults.php?email="+user+"&idExp="+idExp+"&send=SUBMIT+DATA\n\nKind regards";
	var link = "mailto:" + escape("Enter email address")
             + "?subject=BRUSELAS"
             + "&body=" + escape(url);

	window.location.href = link;	
	return false;
}

function downloadSmiles(obj, name, smiles) {
	$(obj).attr("href", "data:x-application/text,"+smiles).attr("download", name);
}

function downloadExcel(id) {
	window.location = "downloader.php?type=excel&idExp=" + id;
}

function downloadPyMOL(id) {
	window.location = "downloader.php?type=pymol&idExp=" + id;
}

function downloadMolecules(id) {
	window.location = "downloader.php?type=molecules&idExp=" + id;
}

function openAlignment(id) {
	w = window.open("templateResults.php?idResult=" + id + "&alignment=1", "BRUSELAS" + Date.now());
	w.document.title = "BRUSELAS. Detailed results";
	if (window.focus()) { w.focus(); }
	return false;
}

function showAllParams(id) {
	$("#dialog-params").dialog({
		modal: true,
		minWidth: 500,
		buttons: {
			Ok: function() {
				$(this).dialog("close");
			}
		},
		classes: {
  			"ui-dialog": "parameters-popup",
  			"ui-dialog-titlebar": "parameters-popup"
		}
	});
}

$(function() {
	$(".hidden_row").hide();
	$("#dialog-params").hide();
	$("a#query-img").fancybox({
		'overlayShow'	: false,
		'transitionIn'	: 'elastic',
		'transitionOut'	: 'elastic'
	});
	var dataTable = $("#resultTable").DataTable({
        	"ajax": "loadResults.php?idexp=<?php echo $_GET['idExp']; ?>",
		"paging": true,
		"ordering": true,
		"pageLength": 25,
		"columnDefs": [
			{ "title": "Ranking", "visible": true, "targets": 0, "width": "1%" },
			{ "visible": false, "targets": 1 },
			{ "visible": false, "targets": 2 },
			{ "title": "Compound", "visible": true, "targets": 3,
				"render": function ( data, type, full, meta) {
					if (full[2] && full[2] != "-1") {
						return "<a href='../Browse/consulta.php?idDrug="+full[2]+"' target='_blank''>"+full[3]+"</a>";
					} else {
						return full[3];
					}
				} 
			},
			{ "title": "Score[0,1]", "visible": true, "targets": 5, "width": "5%" },
			{ "title": "LiSiCA", "visible": false, "targets": 6, "width": "1%" },
			{ "title": "Pharmer", "visible": false, "targets": 11, "width": "1%" },
			{ "title": "OptiPharm", "visible": false, "targets": 7, "width": "1%" },
			{ "title": "Screen3D", "visible": false, "targets": 8, "width": "1%" },
			{ "title": "SHAFTS", "visible": false, "targets": 9, "width": "1%" },
			{ "title": "WEGA", "visible": false, "targets": 10, "width": "1%" },
			{ "title": "Alignment", "visible": <?= isActive($idExp) ? "true" : "false" ?>, "targets": 12, "width": "5%", 
				"render": function ( data, type, full, meta ) {
                     			return "<a class='icon' title='View 3D alignment' onClick='openAlignment("+full[1]+")'><i class='fa fa-object-ungroup' arial-hidden='true'></i></a>";
        			} 
			}
		]
    });
	$("#resultTable_filter").append("&nbsp;<label>Extended view <input type='checkbox' id='extendedView_filter'></label>");
	$("#extendedView_filter").change(function() {
		var checked = $(this).prop("checked");
		for (i = 6; i < 12; i++) {
			var column = dataTable.column(i);
			var columnData = column.data().join('');
			if (columnData.length < 1) {
				column.visible(false);
			} else {
				column.visible(checked);
			}
		}
	});
});
</script>

<?php
function printFullParameters($config) {
?>
	<div id="dialog-params" title="Parameters of experiment <?= $config->idExp ?>">
		<div><b>Submitted by: </b><?= $config->user ?></div>
		<div><b>Start/End date: </b><?= $config->start." - ".$config->end ?></div>
		<div><b>Type of calculation: </b><?= $config->calculationType ?></div>
		<div><b>Screening software: </b><?= $config->similarity ?></div>
		<div><b>Databases: </b><?= $config->datasets ?></div>
		<div><b>Descriptor: </b><?= $config->extractor ?></div>
		<div><b>Scoring cutoff: </b><?= $config->threshold ?></div>
		<div><b>No.Results: </b><?= $config->maxResults ?></div>
		<div><b>Distance function: </b><?= $config->prefilterFunc ?></div>
		<div><b>Consensus function: </b><?= $config->consensus ?></div>
		<div><b>Key terms: </b><?= $config->keywords ?></div>
		<div><b>Filters: </b><?= $config->filters ?></div>
		<div><b>Description: </b><?= $config->comments ?></div>
	</div>
<?php
}

function printRunParameters($config) {
	printFullParameters($config);
?>
	<table style="text-align: left; font-size: 12px;">
		<caption style="font-weight: bold;">Parameters of experiment <?= $config->idExp ?></caption>
		<tbody>
			<tr>
				<th>Submitted by</th>
				<td><?= $config->user ?></td>
				<th>Start/End dates</th>
				<td><?= $config->start." - ".$config->end ?></td>
			</tr>
			<tr>
				<th>Type of calculation</th>
				<td><?= $config->calculationType ?></td>
				<th>Screening software</th>
				<td><?= $config->similarity ?></td>
			</tr>
			<tr>
				<th>Scoring cutoff</th>
				<td><?= $config->threshold ?></td>
				<th>No.Results requested</th>
				<td><?= $config->maxResults ?></td>
			</tr>
			<tr>
				<th>Distance function</th>
				<td><?= $config->prefilterFunc ?></td>
				<th>Consensus function</th>
				<td><?= $config->consensus ?></td>
			</tr>
			<tr>
				<th>Description</th>
				<td colspan="3"><?= $config->comments ?></td>
			</tr>
		</tbody>
		<tfoot style="text-align: center;">
			<tr>
				<td colspan="4">
					<input type="button" class="btn" value="View more" onclick="showAllParams(<?= $config->idExp ?>)">
				</td>
			</tr>
		</tfoot>
	</table>
<?php
}

function printQuery($config) {
	$idExp = $config->idExp;
	$smiles = $config->smiles;
	$user = $config->user;

	if (isActive($idExp)) {
		echo "<h6>Query Molecule</h6>";
		echo "<a class='icon' id='query-img' href='../../imagenesWeb/$idExp/ligand.png'>";
		echo "	<img class='query-img' src='../../imagenesWeb/$idExp/ligand.png' /><br>";
		echo "</a>";
		echo "<a class='icon' onClick='downloadSmiles(this,\"BRUSELAS-".$idExp.".smi\",\"".$smiles."\")'>";
		echo "	<i class='fa fa-smile-o' aria-hidden='true' title='Get query as SMILES'></i>";
		echo "</a>";
	} else {
		echo "<h6>Actions</h6>";
	}
	echo "<a class='icon' onClick='downloadExcel(\"".$idExp."\")'>";
	echo "	<i class='fa fa-file-excel-o' aria-hidden='true' title='Download results as Excel'></i>";
	echo "</a>";
	echo "<a class='icon' onClick='shareResults(\"".$user."\",".$idExp.")'>";
	echo "	<i class='fa fa-envelope-o' aria-hidden='true' title='Share results with a friend'></i>";
	echo "</a>";
	if (isActive($idExp)) {
		echo "<a class='icon' onClick='downloadPyMOL(\"".$idExp."\")'>";
		echo "	<img src='pymol.png' width='43px' height='12px' title='Download alignments as a PyMOL session'>";
		echo "</a>";
		echo "<a class='icon' onClick='downloadMolecules(\"".$idExp."\")'>";
		echo "	<img src='mol2.png' width='36px' height='10px' style='padding-bottom:2px;' title='Download aligned molecules'>";
		echo "</a>";
	}
}

function printCitation() {
	echo "<a href='https://pubs.acs.org/doi/10.1021/acs.jcim.9b00279' target='_blank'>If you use this results, please cite: Banegas-Luna AJ, Cerón-Carrasco JP, Puertas-Martín S, Pérez-Sánchez H. <b>\"BRUSELAS: HPC generic and customizable software architecture for 3D ligand-based virtual screening of large molecular databases\"</b>. Journal of Chemical Information and Modeling. 2019; 59(6):2805-2817.DOI: 10.1021/acs.jcim.9b00279</a>";
}

function printReferences($service, $idExp) {
	$similarity = $service->findExperimentSoftware($idExp);
	$references = array("3","4","5","6","7","10","12");

	if (in_array("WG", $similarity)) {
		array_push($references,"1");
	}
	if (in_array("LI", $similarity)) {
		array_push($references,"8");
	}
	if (in_array("S3", $similarity)) {
		array_push($references,"9");
	}
	if (in_array("SF", $similarity)) {
		array_push($references,"11");
	}
	if (in_array("OP", $similarity)) {
		array_push($references,"13");
	}
	if (in_array("PH", $similarity)) {
		array_push($references,"16");
	}
	sort($references);

	echo "<h6>References</h6>";
    echo "<div id='ref-list'></div>";
    echo "<script>showRef(".implode(",", $references).");</script>";
}

function printResultTable($service, $config) {
	$results = $service->findResults($config->idExp);
	$typeCalcul = $config->calculationType;

	echo "<input id='id_exp' type='hidden' value='".$config->idExp."'>";

	if (count($results) === 0) {
		echo "There is no results for this experiment.";
		return;
	}
	if (!isActive($config->idExp)) {
		echo "<span class='alertMessage'>(Alignments are no longer available because the experiment has expired)</span>";
	}
	echo "<table id='resultTable' class='display' style='width:100%;'></table>";
}

// services
$expSrv = new ExperimentServiceImpl();

// check if still ongoing
$config = $expSrv->findConfig($idExp, $email);

if (is_null($config)) {
	echo "<div class='greyPanelL'>";
	echo "	<i class='fa fa-exclamation-triangle' aria-hidden='true'></i> You are not allowed to see the results of this experiment.";
	echo "</div>";
	exit();
} else if ($config->status === "in_progress") {
	echo "<div class='greyPanelL'>";
	echo "	<i class='fa fa-exclamation-triangle' aria-hidden='true'></i> Experiment $idExp is still in progress.<br>Please, wait to receive the notification mail when the task is finished.";
	echo "</div>";
	exit();
} else if ($config->status === "queued") {
	echo "<div class='greyPanelL'>";
	echo "	<i class='fa fa-exclamation-triangle' aria-hidden='true'></i> Experiment $idExp is still in queue. It will be started in a few minutes.<br>Please, wait to receive the notification mail when the task is finished.";
	echo "</div>";
	exit();
}
?>
<div class="greyPanelL parameters-box parameters-container">
	<div class="parameters-data">
		<?php printRunParameters($config); ?>
	</div>
	<div class="parameters-query">
		<?php printQuery($config); ?>
	</div>
</div>

<div class="greyPanelL parameters-box results-table">
	<?php printResultTable($expSrv, $config); ?>
</div>
<br>
<div class="citation">
	<?php printCitation(); ?>
</div>
<div class="references">
	<?php printReferences($expSrv, $idExp); ?>
</div>
