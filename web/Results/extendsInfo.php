<?php
// DEPRECATED, NOT USED AND NOT DOCUMENTED
?>
<style type="text/css">
	#referen{
		width: 200px;
		padding-right: 5em;
	/*	margin-right: 3em;*/

	}
</style>
<?php
$pathTemplate='../../../template/';
echo '<link rel="stylesheet" href="'.$pathTemplate.'assets/css/main.css" />';
echo '<link rel="stylesheet" href="'.$pathTemplate.'style.css"/>';

require $pathTemplate.'jsmol/jsmol.php';
require $pathTemplate.'js/ampliarImagenes.php';
require $pathTemplate.'php/references.php';
require $pathTemplate."php/conexion.php";
require __DIR__."/../Services/impl/Exec.php";

$con=getConexion("Antonio");
$pdbCode=$_GET['pdbCode'];
$imgE=$_GET['imgE'];
$img=substr($imgE,0,-4);
$imgPoseView=$img.'.png';
$imaEnergies=$img.'E.png';
$imgPdbCode=$img.'P.png';
$imaEnergiesByAtom=$img.'-Atom.png';
$ligando=$img."-out.pdbqt";
$sessionPymol=$img.'.pdb';
$imagePlip=$img.'-Plip.png';
$textPlib=$img.'.Plip.txt';
$contador=0;
$tablaResults=[];

function readFiles()
{
	Global $textPlib;
	Global $tablaResults;
	//Output a line of the file until the end is reached
	$tokkenInicio="+=======+";
	$tokkenIntermedio="+-------+";
	$tokkenTitle="**";
	$escribir=false;
	$file = fopen($textPlib, "r") or exit("Unable to open file!");
	while(!feof($file))
	{
		$linea=fgets($file);
		
		if ($escribir==true)
		{
			$linea = str_replace(' ', '', $linea);
			$linea=substr(trim($linea),1,-1);
			$tablaResults[]=split('\|',$linea  );

		}
		if ( strpos($linea, $tokkenTitle)!== false ) //titulo
		{	
			$linea=str_replace($tokkenTitle, "",$linea);
			$tablaResults[]=$linea;

			#$tablaResults[]=str_replace($tokkenTitle, "",$linea);
			#echo '<tr><td colspan="4">'.$linea. "</td></tr>";

		}
		elseif (strpos($linea, $tokkenInicio)!== false || strpos($linea, $tokkenIntermedio)!== false)
			$escribir=true;
		else
			$escribir=false;
	}
	
	fclose($file);
}
	#por si se pone toda la proteina 
	#$textoScript='set antialiasDisplay;;hide all;display  lig or ('.substr($resis, 0, -2).');cartoons off;spacefill 23;wireframe 0.2;center (lig);select *;color cpk;select ('.substr($resis, 0, -2).') AND _C;color [253, 174, 97]';
	#$textoScript=$textoScript.';select ('.substr($resis, 0, -2).') AND _C;color [43, 131, 186];spacefill 70;select *.ca and visible;font label 10;color label grey;label %n%[resno]%[chain];;javascript jsmol_3._displayCoverImage(false);;refresh;javascript jsmol_3._displayCoverImage(false)';
	#$textoScript=$linea.";".$textoScript;
#
#	Las coordenadas no siempre estan en la misma posicion del array (pero si contiguas) por eso tomo la determinacion de usar la como como delimitador
#
function finCoords($array)
{
	$contador=0;
	foreach ($array as $valor)
	{
		 if (strpos($valor, ",")!== false and strpos($valor, ".")!== false)
		 	return $contador;
		 $contador=$contador+1;
	}
}
function tableTarget($pdbCode,$con)
{
	$sql="SELECT DrugsNew.*, TargetsDrugsNew.*, TargetsNew.* FROM DrugsNew     JOIN TargetsDrugsNew         ON DrugsNew.idDrug = TargetsDrugsNew.idDrug     JOIN TargetsNew         ON TargetsDrugsNew.uniProt = TargetsNew.uniProt  where pdbCode='".$pdbCode."';";
	#$sql="select * from TargetsNew where pdbCode='".$pdbCode."'";
	echo '<tr><td colspan=2><br><table   width=90% style="background: white; border:1px solid #cccccc">';
	echo '<tr><th bgcolor=#339DDA colspan="10"><b>Know Ligands for this Protein<b></th></tr>';
	#echo '<tr><td><br><td><tr>';
	$res=mysql_query($sql,$con);
	$contador=0;
	if(mysql_num_rows($res)==0){
		echo '<b>No hay sugerencias</b>';
	}else
	{
		while($fila=mysql_fetch_array($res))
		{
			if ($contador% 6 == 0 )
				echo '<tr>';
			if  (empty($fila["nameCH"]) ==false)
				echo '<td align=center  style="text-align: left; padding:0em 0.5em 0 0.5em;"><a href="../Search/consulta.php?idDrug='.$fila["idDrug"].'" target="_blank">'.$fila["nameCH"].'</a></td>';
			else
				echo '<td align=center  style="text-align: left;padding:0.5em;"><a href="../Search/consulta.php?idDrug='.$fila["idDrug"].'" target="_blank">'.$fila["codZinc"].'</a></td>';
			if ($contador%6==5 && $contador!=0) 
			#if ($i%4==3 && $i!=0)
				echo '</tr>';
			$contador+=1;
		}

	} 
	
	echo '</table><br></td></tr>';
}
#
#	Tabla con los datos importantes de plip
#
function contains($needle, $haystack)
{
    return strpos($haystack, $needle) !== false;
}
function tablePlip()
{
	global $tablaResults;
	$contador=0;
	echo '<tr><td colspan=2><table width="90%" style="background: white; border:1px solid #cccccc;">';
	echo '<tr><th bgcolor=#339DDA colspan="10"><b>Interactions Plip<b></th></tr>';
	
	$index=[]; #indice para los campos que hayq que poner
	foreach ($tablaResults as $valor)
	 {	if (gettype($valor) == "string") #strin es el titulo hyfdrofobic etc
		{
			echo "</table>";
			echo '<tr><td colspan=2><table style="background: white; border:1px solid #339DDA;width:90%">';
			echo '<tr><td style="padding-left:20px;" colspan=5><b><br>'.$valor.'</b></td></tr>';
			$contador+=1;
			$index=[];
		}
		if (gettype($valor) == "array")
		{	

			if ($contador==1)
			{
				echo '<tr>';
				for ($i=0; $i< count($valor); $i++)
				{	
					if (strrpos("LIGCOO PROTCOO", trim($valor[$i]))  === false)
					{	
						echo '<td style="padding-left:10px; border-bottom:1px solid #cccccc">'.trim($valor[$i]).'</td>';
						$index[]=$i;
					}
					
				}
				$contador=0;
				echo '</tr>';
			}else
			{
				echo "<tr>";
				for ($z=0; $z< count($index); $z++)
				{
					
					if ( !empty($valor[$index[$z]] ) )
					{
						//echo $valor[$index[$z]]." ".  $z." <BR>";
						echo '<td  style="padding-left:10px"> '.$valor[$index[$z]].'</td>';
					}
				}
				echo '</tr>';
			}
		}
	}
	echo '</table></td></tr>';
	

}
function putImage($imagen,$height)
{
	if (url_exists($imagen))
	   	echo '<td  align=center style="vertical-align:middle" bgcolor=#FFFFFF><a id="example2" href="'.$imagen.'" target="_blank"><img src="'.$imagen.'" width=60% height='.$height.'% /> </a>';
	else
	  	echo "<td   bgcolor=#FFFFFF align=center>The image could not be generated</td>";	
}
#
#	Colores para los enlaces
#
$colores=["green","blue","red","yellow","black","cyan"];
function cretateJmolScript()
{
	Global $colores;
	Global $tablaResults;
	$contadorHidrogen=1;
	$color=-1;
	$linea="";
	$residuos=[]; #residuos de la prot;
	foreach ($tablaResults as $valor)
	{
		if (gettype($valor) == "string") #strin es el titulo hyfdrofobic etc
			$color=$color+1;
		if ( gettype($valor) == "array" )
		{

			if  ( is_numeric($valor[0]) )
			{	#echo $valor[1].$valor[2].$valor[8]."<br>";
				$coords=finCoords($valor);
				$residuos[]=$valor[1].$valor[0];
				$linea=$linea.'draw hydrogen_bond'.$contadorHidrogen.' DIAMETER 0.05 color '.$colores[$color].' line {'.$valor[$coords].'} {'.$valor[$coords+1].'};set echo hydrogen_bond'.$contadorHidrogen.' $hydrogen_bond'.$contadorHidrogen.'; font echo 10; color echo '.$colores[$color].';echo '.$contadorHidrogen.';';
				$contadorHidrogen=$contadorHidrogen+1;
			}
		}
			    #draw hydrogen_bond4 DIAMETER 0.12 color [0, 0, 255] line {16.556 -6.381 12.896} {16.837 -3.130 14.931};set echo hydrogen_bond4 $hydrogen_bond4; font echo 10; color echo [0, 0, 255];echo "4"
	}
	$textoScript="select *.ca and visible;font label 15;color label grey;label %n%[resno]%[chain];select lig AND _C;color [253, 174, 97] ; select (";
	foreach ($residuos as $res)
		$textoScript=$textoScript." ".$res." OR";
	$textoScript=substr($textoScript, 0, -2).") AND _C;color [43, 131, 186];spacefill 70;wireframe 0.2;";
	
	$textoScript=$textoScript.";".$linea;
	return $textoScript;
}
#
#	Muesra la legenda en el jsMol
#
function showLegend()
{
	$color=0;
	Global $tablaResults ;
	Global $colores;
	
		echo '<table>';
		foreach ($tablaResults as $valor)
		{
			if (gettype($valor) == "string") #strin es el titulo hyfdrofobic etc
			{
				echo '<tr><td>';
				echo'<div style="background-color:'.$colores[$color].';border-radius:1em;height:1em;width:1em;float:left;margin-top:0.3em;margin-right:1em;"></div> <div style="float:left">  '.$valor.'</div>';
				echo '</td></tr>';
				$color+=1;
			}
		}
		echo '</table>';

}
##
##
# select *.ca and visible;font label 10;color label grey;label %n%[resno]%[chain] -->todos los resn de los CA en gris
# select lig AND _C;color [253, 174, 97] --> colo liganod naranjita
function url_exists($url) { //antes se utilizaba para urls ahora para ficheros
    return file_exists ( $url);
}
	#echo '<div class=contenido>';
	echo '<div class=contenidoPadding>';
	echo '<table width="80%"> ';
	echo '<tr><th colspan=1 align="right"><h1 style="font-size: 25px;"><b>'.$pdbCode.'</b></h1><br></th>';
	echo '<th align="right" colspan=1>';
		echo '<div id="referen">';
			putReferences("1,4,7,8,13");
		echo '</div>';
	echo '</th></tr>';
	#echo '<th>';
	echo '<tr><td colspan=4 >';
		readFiles();
		$linea=cretateJmolScript();
		echo '<table width="100%" >';
			echo '<tr><td >';
				echo '<div style="float:right;paddin-right:2em;" id="appdiv'.$contador.'""></div>';
				echo '<script type="text/javascript"> $("#appdiv'.$contador.'").html(Jmol.getAppletHtml("jmolApplet'.$contador.'", getInfoComplex("'.$sessionPymol.'","'.$linea.'" )))  </script>';
			echo '</td><td style="vertical-align:top;" >';
			showLegend();
		echo '</td ></tr></table><br>';
	echo '</td>';
	echo '</tr>';
	tablePlip();
	tableTarget($pdbCode,$con);
	echo "<tr>";
		echo "<th colspan=1 bgcolor=#339DDA><b>Protein</b></td>";
		echo "<th colspan=1 bgcolor=#339DDA><b>PDB Code</b></td>";	
	echo '</tr>';//<tr>';
	
	$er=Exec::cmd('identify -verbose '.$imgPoseView.' |grep Colors:| awk \'{print $2}\'');
	if ($er != 3 )
		putImage($imgPoseView,75);
	else
		echo "<td  bgcolor=#FFFFFF align=center>The image could not be generated</td>";
	putImage($imgPdbCode,75);
	echo '</tr><tr>';
	echo "<th  bgcolor=#339DDA><b>Score (Kcal/Mol)</b></th>";
	echo "<th  bgcolor=#339DDA><b>Score Atom(Kcal/Mol)</b></th>";
	echo '</tr><tr>';
	putImage($imaEnergies,10);
	putImage($imaEnergiesByAtom,10);
	echo '</tr>';






echo '</table></table>';
echo '</div></div>';


?>




<?php
//$img='/srv/www/dia-db-test/'.substr($fila["img_energy"], 6); #ejemplo ../../imagenesWeb/1009/0-VS-AD-2WR6_rec-ligand--16--4--2.png -6 /imagenesWeb/1009/0-VS-AD-2WR6_rec-ligand--16--4--2.png
#readFiles();
#cretateJmolScript();
#draw hydrogen_bond0 DIAMETER 0.12 color red line {16.556,-6.381,12.896} {16.837,-3.130,14.931};set echo $hydrogen_bond0 font echo 10; color echo red;echo 0;
#draw hydrogen_bond1 DIAMETER 0.12 color red line {16.556,-6.381,12.896} {15.090,-9.318,11.221};set echo $hydrogen_bond1 font echo 10; color echo red;echo 1;
#draw hydrogen_bond2 DIAMETER 0.12 color red line {17.935,-8.147,14.195} {14.969,-9.839,12.502};set echo $hydrogen_bond2 font echo 10; color echo red;echo 2
?>