<?php
$pathTemplate="../../../template/";
// tell the template that an extra page is provided
$pagExtra="ResultsOk.php";
require '../../templateWeb.php';
?>
<link rel="stylesheet" type="text/css" href="../css/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="../css/jquery.fancybox.min.css">
<link rel="stylesheet" type="text/css" href="../css/jquery.dataTables.min.css">

<script src="../js/jquery.min.js"></script>
<script src="../js/jquery-ui.min.js"></script>
<script src="../js/jquery.fancybox.min.js"></script>
<script src="../js/jquery.dataTables.min.js"></script>
