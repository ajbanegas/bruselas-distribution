function openWindowRef(id) {
	window.open('../References/References.php#ref' + id, 'BruselasReferences', 
		'height=300,width=800,menubar=no,status=no,titlebar=no,toolbar=no');
}

function showRef(...args) {
	for (i = 0; i < args.length; i++) {
		var ref = "[<a href='#' onclick='openWindowRef("+args[i]+")'>"+args[i]+"</a>] ";
		$("#ref-list").html($("#ref-list").html() + ref);
	}
}
