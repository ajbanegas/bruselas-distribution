<?php
require_once __DIR__.'/../../bruselas/autoload.php';

use UCAM\BioHpc\Bruselas as Bruselas;

?>
<ol>
	<li>
			<b>I have not received the email with the results link.</b>
	</li>
	
	<p style="margin-left: 40px;">
	First check the email account you used to submit calculations is valid. If it is a valid account, then check the spam inbox of your email account. Be patient, calculations can take time. If it has been more than 1 day since you submitted your calculations and you still have no results, please contact us, an error may have happened.
	</p>
	<br>
	<li>
			<b>How long will my calculations take? Can't you show me an estimation?</b>
	</li>
	<p style="margin-left: 40px;">
	That is difficult to foresee. Computation time depends on many factors such as the datasets to screen, the workload of the cluster or the number of results requested. Screening all the datasets proposed with one similarity method may be completed in less than 15 minutes. However, other configurations can take up to 24 hours.
	</p>
	<br>
	<li>
			<b>The JME applet for sketching molecules does not load.</b>
	</li>
	<p style="margin-left: 40px;">
	You may have a Java problem. Update Java to the latest release and add security exceptions for BRUSELAS site.
	</p>
	<br>
	<li>
			<b>For how long are my results available?</b>
	</li>
	<p style="margin-left: 40px;">
	Results remain in our server for 30 days. They will be removed afterwards and you will see the message "Not available" in the results screen.
	</p>
	<br>
	<li>
			<b>Can I share my results with anyone else?</b>
	</li>
	<p style="margin-left: 40px;">
	Yes, you can share your results in two different ways:<br>
	1) Send a link by email using the "Share" icon under the query image on the results screen. Other people will receive the link to access the dashboard.<br>
	2) Download the list of hits (names and scores) in an Excel file. Since the Excel file is downloaded on your computer, you are free to manipulate or share as you wish. This option will be available even after the 30 days period of availability of the results.<br>
	</p>
	<br>
	<li>
			<b>What libraries can I select for screening?</b>
	</li>
	<p style="margin-left: 40px;">
	By default, the server provides three libraries: ChEMBL (version 21), DrugBank (approved compounds), KEGG (compounds and drugs subsets) and DIA-DB which is an <i>ad-hoc</i> library of anti-diabetics. You can combine them as you wish. If none is suitable for your experiment or you want to screen you own library, you have the chance to upload a custom one in one single file of up to 20MB. In that case, the filter to select the most promising ligands is not applied to preserve the library exactly as it was uploaded.
	</p>
	<br>
	<li>
			<b>What molecular formats are supported by BRUSELAS?</b>
	</li>
	<p style="margin-left: 40px;">
	The server relies on Open Babel to perform format conversion, so that, any format accepted by babel/obabel is admitted (mol2, pdb, sdf, mol...). Smiles chains are admitted as well and converted to the format requested by each similarity software. BRUSELAS carries out the minimum amount of conversions because each transformation may result in a loss of accuracy. In any case, if you can choose, <b>mol2</b> is the default and more suitable format.
	</p>
	<br>
	<li>
			<b>I get the message <i>"The extension of the file must be compliant with its format (see Open Babel documentation for further details)"</i> when I upload a query file. What does it mean?</b>
	</li>
	<p style="margin-left: 40px;">
	BRUSELAS relies on Open Babel to perform file format conversions. Since Open Babel figures out the format of the molecule from the file extension, it must be compatible with the actual format. E.g. if you submit a molecule in SDF format but in a file with TXT extension, you will get this error. To fix it, just rename your file to be compliant with the real format you use (e.g. mv foo.txt foo.sdf). You can find detailed information in the official documentation:<br>
	<a href="https://openbabel.org/docs/dev/FileFormats/Overview.html" target="_blank">https://openbabel.org/docs/dev/FileFormats/Overview.html</a><br>
	<a href="https://openbabel.org/docs/dev/Command-line_tools/babel.html" target="_blank">https://openbabel.org/docs/dev/Command-line_tools/babel.html</a>
	</p>
	<br>
	<li>
			<b>Is it possible to run docking calculations from this website?</b>
	</li>
	<p style="margin-left: 40px;">
	No. This server is mainly focused on ligand-based virtual screening methods. Molecular docking is a structure-based approach which is not supported by this tool yet. If you need to run docking calculations, we invite you to use <a href="http://bio-hpc.ucam.edu/aquiles/" target="_blank">Achilles</a>.
	</p>
	<br>
	<li>
			<b>How large can a user library be?</b>
	</li>
	<p style="margin-left: 40px;">
	For technical reasons the size of uploaded files is limited to 2MB, so that is the maximum size allowed for files. The number of compounds included in the library is not restricted, however you should keep in mind two points:<br>
	1/ Some screening programs do not generate the alignment for a certain number of ligands. It is strongly recommended not to upload libraries with more than 300 compounds. If you do it, it is likely that you will only obtain the scores but not the alignments.<br>
	2/ BRUSELAS does not take care of conformer generation while processing user libraries. Thus, if you want to handle different posses of a compound, you must do it yourself and include it in the uploaded file.
	</p>
	<br>
	<li>
			<b>Will my custom library suffer any modification?</b>
	</li>
	<p style="margin-left: 40px;">
	The only modification that user libraries suffer is the deletion of duplicate ligands on the basis of their title. The sofware tools used to assess similarity strongly use the ligand name to generate the output, thus if two ligands have exactly the same name, the interpretation of the output by BRUSELAS may be wrong. For this reason, we first remove the duplicate ligands from user libraries. No other transformation is performed on such a library.
	</p>
	<br>
	<li>
			<b>Why sometimes I only see one compound in the alignment screen?</b>
	</li>
	<p style="margin-left: 40px;">
	The alignment screen displays the alignment generated by the screening programs. For a given query, not all the algorithms are able to assess similarity with all the ligands. Therefore, it is possible that certain algorithms do not generate an alignment for a given ligand. In that case, BRUSELAS is not able to display the alignment of query and ligand with some algorithms. 
	</p>
	<br>
	<li>
			<b>When I download the results of the screening of a library that I uploaded, why are files renamed?</b>
	</li>
	<p style="margin-left: 40px;">
	Chemical compounds may have special characters in their names and this can cause errors while interpreting the output of the screening programs. To overcome this, the server encodes the ligand names in user libraries using base 16 encoding which has no special characters. When the calculations are finished, the original ligand names are restored inside the files, however the filenames remain encoded. Thus, you can figure out the name of a ligand from the filename by decoding the long sequence of characters encoded in the name.<br>
	E.g.: VS-WG-6c2d56617a6963696e6f6e-aligned.sd => take <b>6c2d56617a6963696e6f6e</b> and decode it by using base 16. The ligand name obtained is <b>l-Vazicinon</b>.
	</p>
	<br>
	<li>
			<b>What is included in the downloadable results file?</b>
	</li>
	<p style="margin-left: 40px;">
	You can download the results in a .pse file which can be loaded on PyMOL as a session. The PyMOL session is organized in a hierarchy where the similarity algorithms represent the root. You can profit from this structure to display only the molecules of your interest. By default, only the five best-ranked compounds are selected, but you can select or unselect whatever compound that you wish.
	</p>
	<br>
	<li>
			<b>How can I cite BRUSELAS?</b>
	</li>
	<p style="margin-left: 40px;">
	BRUSELAS is an open tool for academic use. Please cite our publication:<br>
	"Banegas-Luna AJ, Cerón-Carrasco JP, Puertas-Martín S, Pérez-Sánchez H. <b>BRUSELAS: HPC generic and customizable software architecture for 3D ligand-based virtual screening of large molecular databases.</b> Journal of Chemical Information and Modeling. <b>2019</b>; 59(6):2805-2817".	
	</p>
	<br>
	<li>
			<b>I have some problem or question that is not listed in this FAQ.</b>
	</li>
	<p style="margin-left: 40px;">
	Contact the administrator (<a href="mailto:<?php echo Bruselas\SystemConfig::get( "email_sender" ) ?>">
		<?php echo Bruselas\SystemConfig::get( "email_sender" ) ?></a>).
	</p>
</ol>
