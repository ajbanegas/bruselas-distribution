<?php
	echo '<br>';
	echo '<p><b>SENECA:</b> This work was partially supported by the Fundación Séneca del Centro de Coordinación de la Investigación de la Región de Murcia under Project 18946/JLI/13.</p>';
	echo '<div id=fundingInfo>';
	echo '<img src="'.$pathTemplate.'FundingImages/SENECA.png" alt="Seneca"/>';
	echo '<p>This research project has been co-financed by the European Union (European Regional Development Fund- ERDF) and Greek national funds through the Operational Program <b>“THESSALY- MAINLAND GREECE AND EPIRUS-2007-2013”</b> of the National Strategic Reference Framework (NSRF 2007-2013)</p>';
	echo '<img src="'.$pathTemplate.'FundingImages/IOANNINON2.png" alt="IOANNINON"/>';
	echo '<p><b>CHILE: Powered@NLHPC:</b> This research was partially supported by the supercomputing infrastructure of the NLHPC (ECM-02)</p>';
	echo '<img src="'.$pathTemplate.'FundingImages/NLHPC.png" alt="NLHPC" width="200" >';

        echo '<p><b>MINECO:</b> This work is supported by the Spanish MINECO under grant TIN2016-78799-P</p>';
        echo '<img src="../Images/mineco.jpg" alt="MINECO" width="200">';
        echo '</div>';
?>
