# README #

### 2.1.0 (04/03/2021) ###

Features:

* Improved disk storage performance.
* Normalized city-block and euclidean distance to enhance ligand selection.
* Users must fill in the survey after the submission of 5 tasks.

Bugfixes:

* Corrected PyMOL session to include ligand names.
* Added support for smiles files.
* Removed path from compressed kegg-drug compounds.

### 2.0.2 (23/10/2020) ###

Features:

Bugfixes:

* Enabled consensus when performing pharmacophore screening.
* Improved online help messages.

### 2.0.1 (06/04/2020) ###

Features:

* Remove deprecated users_user_id from experiment table.
* Identify query format and type of library (user/server).
* Added database code to Excel file.
* Use of XLSXWriter to create nice Excel files.
* Added pharmacological filters: Veber, Ghose and 3/75.
* Use of weighted scores in the implementation of AVG function.
* Launch only one task in every cron cycle.
* Implemented algorithm to choose the task to run in the next cron cycle.
* Added paper citation to notification email.

Bugfixes:

* Replaced MATCH AGAINST by LOCATE to fix the search by text.
* Fixed enable/disable library building with user databases.
* Display correctly score after extended view is used.
* Do not stop execution when no result files are generated.
* Set email bcc as the only survey replay address.

### 2.0.0 (01/11/2019) ###

Features:

* Enabled Pharmer as a second pharmacophore screening software.
* Reworked input tabs.
* Added exclude option (use of NOT operator) and double quotes in text search.
* Added citation in landing screen.
* Upgraded tutorial.
* All software packages selected by default.

Bugfixes:

* Use of relative path in templateCalculations.php to avoid the usage of wrong URIs.
* Fixed alignment of OptiPharm.
* Group OR clauses with parenthesis in the text-based search.
* Validate whether the library can be converted by obabel or not.
* Verify empty library when retrieving ligands from the cache.

### 1.9.1 (01/09/2019) ###

Features:

* Added citation in the results screen and FAQ section.
* Re-organisation of the input into tabs.

Bugfixes:

### 1.9.0 (22/06/2019) ###

Features:

* Added Lipinski's rule of five as a pre-screening filter.
* Improved cache system to store serialized data.
* Display reference in a new window when clicking on the number.
* Added alert message informing that searching by keywords may slow down the calculation.
* Reworked results screen to make the main table larger.
* Indexed search by text (using FULLTEXT indexes of MySQL).
* Explain meaning of the error thrown when the file extension doesn't match the actual format.
* The formula is shown in the detail of a compound.
* Added missing formulas in the database.
* Search menu renamed to Browse.
* Remove field selector on the search screen. One single input box does the job for all the fields.
* Added Pharmer as a second pharmacophore screening approach.
* Duplicate ligands (based on the title) are removed from user libraries.

Bugfixes:

* Many keywords are combined in OR mode.
* Extra validation on the list of descriptors to avoid wrong elements.
* Verified that the extension of the query file matches its format.
* All the descriptors related to the number of atoms are displayed without decimal digits.
* Fixed search by pattern engine to support multiple words.
* Mean function must be based on the number of algorithms selected not the ones that give a result.
* Ligand names are encoded in base 16 when screening external libraries.
* Thrown EmptyLibraryException when no compounds are found to build a library.

### 1.8.3 (02/02/2019) ###

Features:

* Show database ID on details screen.
* Added partial scores to Excel file.
* Usage of Spout 2.7.3 to create full xlsx files.
* Implementation of a new consensus scoring function: Maximum score.
* Size of the library defined in configuration file.
* Added citation to OptiPharm.
* Record creation date and start date. The latter is set when the task is sent to the cluster.
* Configuration moved to JSON file.

Bugfixes:

* Improved command-line client to handle different variant properties.

### 1.8.2 (27/11/2018) ###

Features:

* Updated FAQ and References sections.
* Added download of all results in a single .tar.gz file.
* Designed new icon for PyMol session.
* Improved home screen.
* Improved tutorial with a step by step guide.

Bugfixes:

* Fixed titles on the results screen.
* Normalized downloadable files to be named BRUSELAS-XXX.zzz.
* Fixed Excel download to make it Opera compliant.
* Fixed email + ID authentication to access results.
* Fixed OptiPharm script (cluster) to handle especial characters in ligand names.
* Fixed LiSiCA script (cluster) to handle especial characters in ligand names.
* Fixed Screen3D script (cluster) to handle especial characters in ligand names.
* Fixed WEGA script (cluster) to handle especial characters in ligand names.
* Fixed alignment scripts to avoid infinite recursion when there's an error.
* Fixed generation of PyMol session for custom libraries.
* Limited size of the library to 2MB to match server's configuration.

### 1.8.1 (26/10/2018) ###

Features:

* Implementation of new distance functions: Consine and Canberra. Not published because they haven't been proved as better than Manhattan.
* Creation of PyMol session is moved to the transformer component.
* Non polar hydrogens are hidden in the PyMol session.
* Remove exceeding files from the server.
* Highlight only the first best-ranked compounds in PyMol.
* Optimized 3D view by displaying molecule's surface.
* Hide link to alignments when the task is expired.
* Enable Excel download and share by mail even when the task is expired.

Bugfixes:

* Reopen database connection when expired.
* Better error handling when calculating alignments with LiSiCA.

### 1.8.0 (15/08/2018) ###

Features:

* Refactorization of services to make them OO compliant.
* Alignment screen is arranged in boxes.
* Download scores in Excel format better than CSV.
* Removed deprecated database table to store queued experiments. Column EXPERIMENT.STATE is enough.
* Added feature to download individual aligned ligands (mol2/sdf).
* Added feature to download individual aligned ligands (SMILES).

Bugfixes:

* All emails are sent through MailService.

### 1.7.4 (05/08/2018) ###

Features:

* Improved results table to show partial scores.
* Improved autocomplete feature on the search screen.
* Normalized output filenames to VS-XX-YYYYYY-aligned.zzz.
* Download results in a PyMol session with menus.
* Results are available for download only for 30 days.
* Implemented more exceptions for a better error handling.
* Copy remote files with rsync rather than scp.
* Insert result details in one shot in the database.
* TaskRunner class renamed to Core.
* Added extra method to ITransformer interface.

Bugfixes:

* Fixed euclidean distance calculation.
* Ligand names are registered when using user libraries.
* Removed deprecated code.

### 1.7.3 (02/06/2018) ###

Features:

* Updated tutorial.
* Added OptiPharm algorithm.
* Files from cluster are copied in one go.
* Alert emails are only sent to my professional email address.

Bugfixes:

* The image of the query and alignment icon are hidden when the experiment is expired.
* An alert message is displayed when alignments aren't available.
* Escaped SMILES codes.

### 1.7.2 (10/04/2018) ###

Features:

* Added KEGG compounds => total 1.603.744 compounds and 7.473.066 conformers.

Bugfixes:

* Fixed search by name to make it case insensitive.
* Filters were reworked for a better selection of compounds.

### 1.7.1 (06/04/2018) ###

Features:

* Creation of conformers per compound => 1.580.077 compounds and 7.310.814 conformers.
* Added pharmacophore screening with SHAFTS.
* Added new status to know when the task is already ongoing or still in queue.
* Added pop-up to display the full list of input parameters.
* Added legend below the alignments to identify the query and the ligand.
* Added new input box to select compounds for screening on the basis of keywords provided by the user.

Bugfixes:

* Send mails through SMTP server.
* The on-fly library has a static size of 200 compounds.
* Removed the amount of compounds to include in the library. This feature is deprecated since the size of the library is fixed.
* Users can choose between 1 and 100 descriptors. Otherwise database queries are too long and crash.
* References section was updated and formatted.
* Fixed error when getting the conformers to include in the library.

### 1.6.0 (02/01/2018) ###

Features:

* Alignment scripts for LiSiCA and Screen3D were installed on the server.
* Added link to websites on the references screen (when needed).
* Output files are read from the cluster when we are sure that they are available.
* Display alignments on a new screen/tab.
* Results screen was redesigned to improve its performance.

Bugfixes:

* Fixed alignment script for LiSiCA.
* Fixed alignment script for Screen3D.
* The query is stored for every ligand and algorithm.

### 1.5.0 (10/12/2017) ###

Features:

* Added filtering by InChI and IUPAC in the compound explorer.
* Added button to clean the input box.
* Removed statistics from the menu.
* A survey is sent to new users (checked everyday at 11:00 AM).
* Updated tutorial.
* Tab "Descriptors" was renamed to "Library building".

Bugfixes:

* EXPERIMENT.IUPAC_NAME was extended to BLOB to hold longer names.
* Updated IUPAC names due to the bugfix above.
* Fixed issue while sending multiple files to the cluster.
* Setup the cluster to spend up to 24 hours per task.
* Scores outputed by Screen3D are formatted in a SQL-compliant way to build correct insert statements.
* Developed new scripts to generate alignments with LiSiCA and Screen3D.
* Temporary files and folders are removed from the cluster in case of error.

### 1.4.0 (21/11/2017) ###

Features:

* Users have to chance to choose the database to screen.
* Task parameters are stored as a JSON string bettern than separate columns. This is a way more flexible than it was.
* When an error is thrown during the offline process, it is catched and notified to the user by email.
* Update of FAQ section.

Bugfixes:

* EXPERIMENT.START_DATE and EXPERIMENT.END_DATE columns moved to datetime to store the timestamp.
* Formatting of citations on the results screen.
* Improved error messages when validating input parameters.
* SimilarityHelper class renamed to InputValidator.
* Deal with NsOH and nSOH descriptors. As MySQL ignores upper and lower case, an alias is needed to make a difference between them.
* Usage of an alias for the cluster. This way we keep a permanent connection relying on Linux features and avoid issues with timeouts which are present in any PHP third-party library.

### 1.3.0 (08/11/2017) ###

Features:

* All compounds are linked to one single source.
* Descriptors were moved to individual columns in the database.
* Temporary files are prefixed as 'bru'.
* Added 3D view of compounds in the detail screen.
* Details message on the results screen when the task is still ongoing.
* Added button to download all the results in one single mol2 file.

Bugfixes:

* All descriptors from DRAGON (4.885) recorded in the database.
* Conversion from SMILES to 3D format using molconvert + babel to avoid different outputs with the same molecule.
* Text in the email says that results are available for 30 days. It said 2 weeks instead.
* Fixed error while downloading results in CSV format.

### 1.2.0 (06/11/2017) ###

Features:

* Full list of compounds imported from ChEMBL, DrugBank and DIA-DB. It contains 1.580.077 compounds.
* Added link to download compounds from the detail screen.
* Command-line utility (only for internal usage, non distributable).
* Descriptors are stored as JSON strings in the database.

### 1.0.0 (29/06/2017) ###

* Usage of BRUSELAS' API.
* Added Screen3D as a shape similarity algorithm.
* New calculation parameters:
	- Support for custom libraries (optional).
	- Consensus scoring functions (optional).
	- Selection of many similarity algorithms in the same task.
* Selection of one single conformer per compound for screening.

### What is this repository for? ###

* Clean code, shell scripts and database scripts to run BRUSELAS server
* 1.8.3

### How do I get access? ###

* Access: http://bio-hpc.ucam.edu/Bruselas
