<?php

namespace UCAM\BioHpc\Bruselas;

/**
 * Implementation of a log system.
 * Logs are stored in separete files per date.
 *
 * @author Antonio-Jesús Banegas-Luna <ajbanegas@alu.ucam.edu>
 * @version 1.0.0
 * @package UCAM\BioHpc\Bruselas
 */
class Log {
	/**
	 * @var boolean $LOGON Enable or disable logs.
	 */
	const LOGON = true;

	/**
	 * Trace a general information message.
	 *
	 * @param string $message Message to save.
	 */
	public static function info($message) {
		self::__log ( "INFO", $message );
	}

	/**
	 * Trace a debugging information.
	 *
	 * @param string $message Message to save.
	 */
	public static function debug($message) {
		self::__log ( "DEBUG", $message );
	}

	/**
	 * Trace an error message.
	 *
	 * @param string $message Message to save.
	 */
	public static function error($message) {
		self::__log ( "ERROR", $message );
	}

	/**
	 * Save a message with the given logging level.
	 *
	 * @param string $level Log level (info, debug, error).
	 * @param string $message Message to save.
	 */
	private static function __log($level, $message) {
		if (! self::LOGON ) { 
			return;
		}
               	$msg = sprintf("[$level] %s - %s".PHP_EOL, self::__date("d-m-Y H:i:s"), $message);
                $foo = sprintf("%s/BRUSELAS-%s.log", __DIR__."/../logs", self::__date("Y-m-d"));
                $fd = fopen("$foo", "a+");
                fwrite($fd, $msg);
                fclose($fd);
	}

	/**
	 * Return the current date in a given format.
	 *
	 * @param string $mask Formatting pattern.
	 * @return string Formatted date.
	 */
	private static function __date($mask) {
		date_default_timezone_set ( "Europe/Madrid" );
		return date ( $mask );
	}
}
?>
