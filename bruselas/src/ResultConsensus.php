<?php

namespace UCAM\BioHpc\Bruselas;

/**
 * Represent the result of each hit in an experiment while the consensus is being applied.
 *
 * @author Antonio-Jesús Banegas-Luna <ajbanegas@alu.ucam.edu>
 * @version 1.0.0
 * @package UCAM\BioHpc\Bruselas
 */
class ResultConsensus {
	/**
	 * @var string $ligand Ligand name.
	 */
	protected $ligand;
	/**
	 * @var float $score Score assigned to the ligand.
	 */
	protected $score;
	
	function __construct($ligand, $score) {
		$this->ligand = $ligand;
		$this->score = $score;
	}
	
	/**
	 * Return the name of the ligand.
	 *
	 * @return string Name of the ligand.
	 */
	public function getLigand() {
		return $this->ligand;
	}

	/**
	 * Return the score of the ligand.
	 *
	 * @return float Score of the ligand.
	 */
	public function getScore() {
		return $this->score;
	}

}
?>
