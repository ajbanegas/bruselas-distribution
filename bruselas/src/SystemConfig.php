<?php

namespace UCAM\BioHpc\Bruselas;

define("STORE", __DIR__."/../store/bruselas-config.json");

/**
 * Store, set and get system's general configuration parameters.
 *
 * @author Antonio-Jesús Banegas-Luna <ajbanegas@alu.ucam.edu>
 * @version 1.0.0
 * @package UCAM\BioHpc\Bruselas
 */
class SystemConfig {

	/**
	 * @var array $config Associative array keeping parameters' names and values.
	 */
	private static $config = NULL;

	/**
	 * Return the value of a system parameter.
	 *
	 * @param string $name Name of the parameter whose value is to be returned.
	 * @return Object Parameter's value.
	 */
	public static function get($name) {
		if ( is_null ( self::$config ) ) {
			self::__load();
		}
		return self::$config ["$name"];
	}

	/**
	 * Set the value of a parameter.
	 *
	 * @param string $name Parameter name.
	 * @param Object $value Parameter value.
	 */
	public static function set($name, $value) {
		if ( is_null ( self::$config ) ) {
			self::__load();
		}
		self::$config["$name"] = $value;
		self::__save();
	}

	/**
	 * Load all the system parameters in the store.
	 */
	private static function __load() {
		if ( !file_exists ( STORE ) ) {
			echo "file does not exist\n";
			return;
		}
		self::$config = json_decode ( file_get_contents( STORE ), true );
	}

	/**
	 * Persist the current parameters.
	 */
	private static function __save() {
		file_put_contents( STORE, json_encode( self::$config, JSON_PRETTY_PRINT ) );
	}

}
?>
