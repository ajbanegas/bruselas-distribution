<?php

namespace UCAM\BioHpc\Bruselas;

/**
 * Represent an experiment.
 * It contains the input parameters, the transformers to manipulate the input
 * and the output data.
 *
 * @author Antonio-Jesús Banegas-Luna <ajbanegas@alu.ucam.edu>
 * @version 1.0.0
 * @package UCAM\BioHpc\Bruselas
 */
class Experiment {
	/**
	 * @var ExperimentParams $params Input parameters.
	 */
	private $params;
	/**
	 * @var ExperimentObjects $objects Objects to transform the input parameters.
	 */
	private $objects;
	/**
	 * @var ExperimentOutput $output Global and partial output results.
	 */
	private $output;
	
	function __construct() {
		$this->params = new ExperimentParams ();
		$this->objects = new ExperimentObjects ();
		$this->output = new ExperimentOutput ();
	}
		
	/**
	 * Return the input parameters.
	 *
	 * @return ExperimentParams Input parameters.
	 */
	public function getParams() {
		return $this->params;
	}
	
	/**
	 * Return the data transformers.
	 *
	 * @return ExperimentObjects Data transformers.
	 */
	public function getObjects() {
		return $this->objects;
	}
	
	/**
	 * Return the experiment output.
	 *
	 * @return ExperimentOutput Experiment output.
	 */
	public function getOutput() {
		return $this->output;
	}

	/**
	 * Set the input parameters.
	 *
	 * @param ExperimentParams $params Input parameters.
	 */
	public function setParams($params) {
		$this->params = $params;
	}

	/**
	 * Set the data transformers.
	 *
	 * @param ExperimentObjects $objects Data transformers.
	 */
	public function setObjects($objects) {
		$this->objects = $objects;
	}

	/**
	 * Set the output results.
	 *
	 * @param ExperimentOutput $output Output results.
	 */
	public function setOutput($output) {
		$this->output = $output;
	}
	
	/**
	 * Validate whether an experiment is completely defined or not.
	 *
	 * @return void
	 * @throws BruselasException If the experiment is invalid.
	 */
	public function validate() {
		$this->params->validate();
		$this->objects->validate();
	}

}
?>
