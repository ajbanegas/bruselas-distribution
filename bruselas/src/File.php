<?php

namespace UCAM\BioHpc\Bruselas;

/**
 * Represent the information of a file stored in disk.
 *
 * @author Antonio-Jesús Banegas-Luna <ajbanegas@alu.ucam.edu>
 * @version 1.0.0
 * @package UCAM\BioHpc\Bruselas
 */
class File {
	/**
	 * @var String $name Name of file.
	 */	
	private $name;
	/**
	 * @var String $content Serialized content file.
	 */
	private $content;
	/**
	 * @var String $path Path where the file is located.
	 */
	private $path;
	/**
	 * @var Long $size Number of bytes of the file.
	 */
	private $size;
	/**
	 * @var String $format Format of file (sdf, mol2...).
	 */
	private $format;
	
	function __construct($name, $content, $path = "") {
		$this->name = $name;
		$this->content = $content;
		$this->path = $path;
		$this->size = ($content != null) ? strlen ( $content ) : 0;
		$this->format = pathinfo ( $name, PATHINFO_EXTENSION );
	}

	/**
	 * Return the file name.
	 *
	 * @return String Name of file.
	 */	
	public function getName() {
		return $this->name;
	}

	/**
	 * Return content of the file.
	 *
	 * @return String Serialized content of the file.
	 */	
	public function getContent() {
		return $this->content;
	}

	/**
	 * Return the file size.
	 *
	 * @return Long Number of bytes of the file.
	 */
	public function getSize() {
		return $this->size;
	}

	/**
	 * Return the format of the file.
	 *
	 * @return String Format of the file.
	 */	
	public function getFormat() {
		return $this->format;
	}

	/**
	 * Return the path to the file.
	 *
	 * @return String Path where to find the file.
	 */	
	public function getPath() {
		return $this->path;
	}

}
?>
