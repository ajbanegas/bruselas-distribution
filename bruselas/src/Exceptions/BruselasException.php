<?php

namespace UCAM\BioHpc\Bruselas;

use Exception;

/**
 * Custom exception to handle errors.
 *
 * @author Antonio-Jesús Banegas-Luna <ajbanegas@alu.ucam.edu>
 * @version 1.0.0
 * @package UCAM\BioHpc\Bruselas
 */
class BruselasException extends Exception {
	/**
	 * Constructor.
	 *
	 * @param string $message Error message.
	 * @param int $code Error code. Default 0.
	 * @param Exception $previous Exception being extended. Default null.
	 */
	public function __construct($message, $code = 0, Exception $previous = null) {
		// make sure everything is assigned properly
		parent::__construct ( $message, $code, $previous );
	}
}
?>
