<?php

namespace UCAM\BioHpc\Bruselas;

/**
 * Custom exception to handle errors when the library is empty.
 *
 * @author Antonio-Jesús Banegas-Luna <ajbanegas@alu.ucam.edu>
 * @version 1.0.0
 * @package UCAM\BioHpc\Bruselas
 */
class EmptyLibraryException extends BruselasException {
	/**
	 * Constructor.
	 *
	 * @param string $message Error message.
	 * @param int $code Error code. Default 0.
	 * @param Exception $previous Exception being extended. Default null.
	 */
	public function __construct($message, $code = 0, Exception $previous = null) {
		// make sure everything is assigned properly
		parent::__construct ( $message, $code, $previous );
	}
}
?>
