<?php

namespace UCAM\BioHpc\Bruselas;

/**
 * Represent the transformation objects of an experiment.
 * It contains the object to transform the input of the experiment.
 *
 * @author Antonio-Jesús Banegas-Luna <ajbanegas@alu.ucam.edu>
 * @version 1.0.0
 * @package UCAM\BioHpc\Bruselas
 */
class ExperimentObjects {
	/**
	 * @var IPrefilter $prefilter Implementation of descriptor-based filter.
	 */
	private $prefilter;
	/**
	 * @var IFormatConverter $converter Implementation of conversion algorithm.
	 */
	private $converter;
	/**
	 * @var IDescriptorExtractor $extractor Implementation of descriptor extraction.
	 */
	private $extractor;
	/**
	 * @var IMolecularComparator[] $comparators Array of comparison objects.
	 */
	private $comparators;
	/**
	 * @var IConsensusCalculator $consensuator Implementation of consensus function.
	 */
	private $consensuator;
	/**
	 * @var ITransformer $transformer Implmentation of transformer.
	 */
	private $transformer;
	/**
	 * @var IResultNotifier $notifier Implementation of results notifier.
	 */
	private $notifier;
	
	function __construct() {
		$this->comparators = array ();
	}

	/**
	 * Set the pre-filter implementation object.
	 *
	 * @param IPrefilter $prefilter Implementation of descriptor-based filter.
	 */
	public function setPrefilter($prefilter) {
		$this->prefilter = $prefilter;
	}

	/**
	 * Set the conversion implementation object.
	 *
	 * @param IFormatConverter $converter Implementation of conversion algorithm.
	 */
	public function setConverter($converter) {
		$this->converter = $converter;
	}

	/**
	 * Set the descriptor extractor object.
	 *
	 * @param IDescriptorExtractor $extractor Implementation of descriptor extraction.
	 */
	public function setExtractor($extractor) {
		$this->extractor = $extractor;
	}

	/**
	 * Add a new comparator object.
	 *
	 * @param IMolecularComparator $comparator Comparison object.
	 */
	public function addComparator($comparator) {
		array_push( $this->comparators, $comparator );
	}
	
	/**
	 * Set the the array of comparators.
	 *
	 * @param IMolecularComparator[] $comparators Array of comparison objects.
	 */
	public function setComparators($comparators) {
		$this->comparators = $comparators;
	}
	
	/**
	 * Set the consensus function implementation object.
	 *
	 * @param IConsensusCalculator $consensuator Implementation of consensus function.
	 */
	public function setConsensuator($consensuator) {
		$this->consensuator = $consensuator;
	}
	
	/**
	 * Set the client-adapter object.
	 *
	 * @param ITransformer $transformer Implmentation of transformer.
	 */
	public function setTransformer($transformer) {
		$this->transformer = $transformer;
	}
	
	/**
	 * Set the results notifier object.
	 *
	 * @param IResultNotifier $notifier Implementation of results notifier.
	 */
	public function setNotifier($notifier) {
		$this->notifier = $notifier;
	}
	
	/**
	 * Return the prefilter object.
	 *
	 * @return IPrefilter Implementation of descriptor-based filter.
	 */
	public function getPrefilter() {
		return $this->prefilter;
	}

	/**
	 * Return the conversion object.
	 *
	 * @return IFormatConverter Implementation of conversion algorithm.
	 */
	public function getConverter() {
		return $this->converter;
	}

	/**
	 * Return the descriptor extractor object.
	 *
	 * @return IDescriptorExtractor Implementation of descriptor extraction.
	 */
	public function getExtractor() {
		return $this->extractor;
	}

	/**
	 * Return the array of comparator objects.
	 *
	 * @return IMolecularComparator[] Array of comparison objects.
	 */
	public function getComparators() {
		return $this->comparators;
	}
	
	/**
	 * Return the scoring consensus function.
	 *
	 * @return IConsensusCalculator Implementation of consensus function.
	 */
	public function getConsensuator() {
		return $this->consensuator;
	}
	
	/**
	 * Return the client-adapter object.
	 *
	 * @return ITransformer Implmentation of transformer.
	 */
	public function getTransformer() {
		return $this->transformer;
	}
	
	/**
	 * Return the results notifier object.
	 *
	 * @return IResultNotifier Implementation of results notifier.
	 */
	public function getNotifier() {
		return $this->notifier;
	}
	
	/**
	 * Validate whether an experiment is completely defined or not.
	 *
	 * @return True is the experiment is well defined.
	 * @throws BruselasException if the experiment is invalid.
	 */
	public function validate() {
		if ($this->prefilter !== null && ! $this->prefilter instanceof IPrefilter) {
			throw new InvalidArgumentException ( "Argument 'prefilter' must be an instance of IPrefilter." );
		}
		if ($this->comparators === null || ! is_array ($this->comparators) || count ($this->comparators) === 0) {
			throw new RequiredPropertyException ( "At least one comparator is required." );
		}
		if (count ( $this->comparators ) > 0) {
			foreach ($this->comparators as $comp) {
				if (! $comp instanceof IMolecularComparator) {
					throw new InvalidArgumentException ( "All comparators must implement IMolecularComparator." );
				}
			}
		}
		if ($this->converter !== null && ! $this->converter instanceof IFormatConverter) {
			throw new InvalidArgumentException ( "Argument 'converter' must be an instance of IFormatConverter." );
		}
		if ($this->consensuator !== null && ! $this->consensuator instanceof IConsensusCalculator) {
			throw new InvalidArgumentException ( "Argument 'consensuator' must be an instance of IConsensusCalculator." );
		}
		if ($this->transformer !== null && ! $this->transformer instanceof ITransformer) {
			throw new InvalidArgumentException ( "Argument 'transformer' must be an instance of ITransformer." );
		}
		if ($this->notifier !== null && ! $this->notifier instanceof IResultNotifier) {
			throw new InvalidArgumentException ( "Argument 'notifier' must be an instance of IResultNotifier." );
		}
		return true;
	}
	
}
?>
