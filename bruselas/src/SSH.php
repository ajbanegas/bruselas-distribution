<?php

namespace UCAM\BioHpc\Bruselas;

/**
 * Implementation of a SSH wrapper. SSH connections are actually handled by
 * an external library.
 *
 * @author Antonio-Jesús Banegas-Luna <ajbanegas@alu.ucam.edu>
 * @version 1.0.0
 * @package UCAM\BioHpc\Bruselas
 */
class SSH {	
	/**
	 * Open a SSH connection on the remote server.
	 *
	 * @throws BruselasException when connecting is not possible.
	 */
	public function connect() {
		// nothing to do
	}

	/**
	 * Close a previously opened connection.
	 */
	public function close() {
		// nothing to do
	}

	/**
	 * Run a command on the SSH server.
	 *
	 * @param string $command Command to execute.
	 * @return string Command response.
	 */
	public function exec($command) {
		$ip = SystemConfig::get( "cluster" );

		Log::debug("ssh $ip \"$command\"");
		
		exec("ssh $ip \"$command\"", $output, $retvar);
		if ($retvar !== 0) {
			throw new SSHException ( "Error executing remote command: $command" );
		}
		return implode("\n", $output);
	}

	/**
	 * Copy a file to the SSH server with the given name and permissions.
	 *
	 * @param string $srcfile Source filename.
	 * @param string $targetfile Target filename.
	 * @param int $mask Mask indicating the remote file permissions.
	 * @throws BruselasException when something fails.
	 */
	public function copy($srcfile, $targetfile, $mask = 0644) {
		$ip = SystemConfig::get( "cluster" );

		Log::debug("rsync -azq $srcfile $ip:$targetfile");

		exec("rsync -azq $srcfile $ip:$targetfile", $output, $retvar);
		if ($retvar !== 0) {
			throw new SSHException ( "Failed to send file: $srcfile -> $targetfile" );
		}
	}

}
?>
