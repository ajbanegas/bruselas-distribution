<?php

namespace UCAM\BioHpc\Bruselas;

/**
 * Represent the input parameters of an experiment.
 *
 * @author Antonio-Jesús Banegas-Luna <ajbanegas@alu.ucam.edu>
 * @version 1.0.0
 * @package UCAM\BioHpc\Bruselas
 */
class ExperimentParams {

	/**
	 * @var int MAXRESULTS Maximum number of hits to return. Default, 100.
	 */
	const MAXRESULTS = 100;
	
	/**
	 * @var string $query Path to the file containing the query molecule (if any).
	 */
	private $query;
	/**
	 * @var string $library Path to the file containing the library.
	 */
	private $library;
	/**
	 * @var string $user User that will receive the results.
	 */
	private $user;
	/**
	 * @var float $cutoff Minimum score required to consider a result as returnable.
	 */
	private $cutoff;
	/**
	 * @var int $id Unique experiment's identifier.
	 */
	private $id;
	/**
	 * @var string $description Text describing the experiment briefly.
	 */
	private $description;
	/**
	 * @var int $result_size Maximum number of results requested.
	 */
	private $result_size;
	/**
	 * @var array $descriptors List of descriptors used to select the ligands to include in the library.
	 */
	private $descriptors;

	
	function __construct() {
		$this->result_size = self::MAXRESULTS;
	}
	
	/**
	 * Set the query molecule.
	 *
	 * @param string $query Path to the file containing the query molecule.
	 */
	public function setQuery($query) {
		$this->query = $query;
	}

	/**
	 * Set the library.
	 *
	 * @param string $library Path to the file containing the library.
	 */
	public function setLibrary($library) {
		$this->library = $library;
	}

	/**
	 * Set the user to notify.
	 *
	 * @param string $user User that will be notified when the experiment is finished.
	 */
	public function setUser($user) {
		$this->user = $user;
	}

	/**
	 * Set the scoring cutoff.
	 *
	 * @param float $cutoff Minimum score requested to the results. Default, 0.0.
	 */
	public function setCutoff($cutoff = 0.0) {
		if ( is_null ( $cutoff ) ) {
			$cutoff = 0.0;
		}
		$this->cutoff = $cutoff;
	}

	/**
	 * Set the experiment identifier.
	 *
	 * @param int $id Unique experiment identifier. Default, -1.
	 */
	public function setId($id = -1) {
		if ( is_null ( $id ) ) {
			$id = -1;
		}
		$this->id = $id;
	}

	/**
	 * Set a descriptive text concerning the experiment.
	 *
	 * @param string $description Text describing the experiment.
	 */
	public function setDescription($description = "") {
		$this->description = $description;
	}

	/**
	 * Set the maximum number of hits to return.
	 *
	 * @param int $result_size Maximum number of hits requested. Default, MAXRESULTS.
	 */
	public function setResultSize($result_size = self::MAXRESULTS) {
		if ( is_null( $result_size ) ) {
			$result_size = self::MAXRESULTS;
		}
		$this->result_size = $result_size;
	}

	/**
	 * Set the list of descriptors to use in the pre-filter.
	 *
	 * @param array $descriptors Collection of descriptor to pre-filter with.
	 */
	public function setDescriptors($descriptors = array()) {
		$this->descriptors = $descriptors;
	}

	/**
	 * Return the path to the file containing the query molecule (if any).
	 *
	 * @return string Path to the file containing the query molecule (if any).
	 */
	public function getQuery() {
		return $this->query;
	}
	
	/**
	 * Return the path to the file containing the library.
	 *
	 * @return string Path to the file containing the library.
	 */
	public function getLibrary() {
		return $this->library;
	}
	
	/**
	 * Return the user that will receive the results.
	 *
	 * @return string User that will receive the results.
	 */
	public function getUser() {
		return $this->user;
	}
	
	/**
	 * Return the minimum score required to consider a result as returnable.
	 *
	 * @return float Minimum score required to consider a result as returnable.
	 */
	public function getCutoff() {
		return $this->cutoff;
	}
	
	/**
	 * Return the unique experiment's identifier.
	 *
	 * @return int Unique experiment's identifier.
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * Return a text describing the experiment briefly.
	 *
	 * @return string Text describing the experiment briefly.
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Return the maximum number of results requested.
	 *
	 * @return int Maximum number of results requested.
	 */
	public function getResultSize() {
		return $this->result_size;
	}

	/**
	 * Return the list of descriptors used to select the ligands to include in the library.
	 *
	 * @return array List of descriptors used to select the ligands to include in the library.
	 */
	public function getDescriptors() {
		return $this->descriptors;
	}

	/**
	 * Validates whether an experiment is completely defined or not.
	 *
	 * @return True is the experiment is well defined.
	 * @throws BruselasException if the experiment is invalid.
	 */
	public function validate() {
		if (strlen ( $this->query ) <= 0) {
			throw new RequiredPropertyException ( "Argument 'query' is required." );
		}
		if (! file_exists ( $this->query )) {
			throw new FileNotFoundException ( "File '" . $this->query . "' does not exist." );
		}
		if (strlen ( $this->library ) <= 0) {
			throw new RequiredPropertyException ( "Argument 'library' is required." );
		}
		if (! file_exists ( $this->library )) {
			throw new FileNotFoundException ( "File '" . $this->library . "' does not exist." );
		}
		if ($this->cutoff < 0 || $this->cutoff > 1) {
			throw new ValidationException ( "Cutoff must be in range [0,1]." );
		}
		return true;
	}
	
}
?>
