<?php

namespace UCAM\BioHpc\Bruselas;

/**
 * Implementation of the general operations on queues.
 * 
 * @implements IQueueManager
 *
 * @author Antonio-Jesús Banegas-Luna <ajbanegas@alu.ucam.edu>
 * @version 1.0.0
 * @package UCAM\BioHpc\Bruselas 
 */
class QueueManager implements IQueueManager {

	/**
	 * STORE_FILE File where general settings are persisted.
	 */
	const STORE_FILE = "queue-store.json";

	/**
	 * @var string $store Path to the store file.
	 */
	private $store;

	/**
	 * Constructor.
	 */
	function __construct() {
		$this->store = __DIR__."/../../store/".self::STORE_FILE;
		$this->createStore ();
	}

    /**
     * @inherit
     * {@inherit}
     * {@inheritdoc}
     */
	public function addMessage($message) {
		Log::debug( "queueing new message" );

		if (!$message instanceof IQueueMessage) {
			throw new InvalidArgumentException("Expected type for message is wrong (IQueueMessage expected)");
		}

		$queue = $this->fillQueue();
		array_push($queue, $message);
		$this->saveQueue($queue);

		Log::debug( count( $queue ). " messages in queue" );

		return $message;
	}

    /**
     * @inherit
     * {@inherit}
     * {@inheritdoc}
     */
	public function removeMessage($message) {
		Log::debug( "removing message from queue" );

		if (is_null($message) || !$message instanceof IQueueMessage) {
			return;
		}
		$queue = $this->fillQueue();
		$i = 0;
		foreach($queue as $msg) {
			if ($msg->equals($message)) {
				$this->deleteMessage($queue, $i);
				return;
			}
			$i++;
		}
	}

    /**
     * @inherit
     * {@inherit}
     * {@inheritdoc}
     */
	public function listMessages() {
		return $this->fillQueue();
	}

	/**
	 * Create a new store.
	 */
	private function createStore() {
		if ( file_exists( $this->store ) ) {
			return;
		}
		if ( !touch ( $this->store ) ) {
			throw new FileCreationException ( "Error creating queue file.".$this->store );
		}
		chmod ( $this->store, 0766 );
	}

	/**
	 * Populate a queue with the information stored in the temporary file.
	 *
	 * @return array $queue Array of messages.
	 */
	private function fillQueue() {
		if ( !file_exists( $this->store ) ) {
			return array();
		}

		$json = json_decode( file_get_contents( $this->store ) );
		$queue = array();

		if ( count ($json) > 0 ) {
			foreach ($json as $item) {
				$msg = new QueueMessage( $item->experiment, $item->similarity, $item->folder );
				array_push($queue, $msg);
			}
		}
		return $queue;
	}

	/**
	 * Persist a queue of messages into a file.
	 *
	 * @param object $data Queued data.
	 */
	private function saveQueue($data) {
		file_put_contents($this->store, json_encode($data, JSON_PRETTY_PRINT));
	}

	/**
	 * Remove a message from the queue.
	 *
	 * @param array $lines Array of messages.
	 * @param int $index Index of the message to remove.
	 */
	private function deleteMessage($lines, $index) {
		array_splice($lines, $index, 1);
		$this->saveQueue($lines);
	}

}
?>
