<?php

namespace UCAM\BioHpc\Bruselas;

/**
 * Interface representing a storable message.
 *
 * @author Antonio-Jesús Banegas-Luna <ajbanegas@alu.ucam.edu>
 * @version 1.0.0
 * @package UCAM\BioHpc\Bruselas
 */
interface IQueueMessage {

	/**
	 * Return the experiment identifier.
	 *
	 * @return int Experiment identifier.
	 */	
	public function getExperiment();

	/**
	 * Return the similarity method used.
	 *
	 * @return string Similarity method's code.
	 */
	public function getSimilarity();

	/**
	 * Return the output folder generated.
	 *
	 * @return string Directory where the comparison output is stored.
	 */
	public function getFolder();

	/**
	 * Return true if both messages are the same.
	 *
	 * @param string $message Message to compare with.
	 * @return boolean True if both messages are the same. False otherwise.
	 */
	public function equals($message);

}
?>
