<?php

namespace UCAM\BioHpc\Bruselas;

/**
 * Interface that defines the operations available on a queue.
 *
 * @author Antonio-Jesús Banegas-Luna <ajbanegas@alu.ucam.edu>
 * @version 1.0.0
 * @package UCAM\BioHpc\Bruselas
 */
interface IQueueManager {

	/**
	 * Add a new message to end of the queue.
	 *
	 * @param string $message 	Message to add to the queue.
	 * @return string Newly added message.
	 */
	public function addMessage($message);

	/**
	 * Remove a message identified from the queue.
	 *
	 * @param string $message 	Message to remove from the queue.
	 */
	public function removeMessage($message);

	/**
	 * List all the messages available in the queue.
	 *
	 * @return string All the messages stored in the queue.
	 */
	public function listMessages();

}
?>
