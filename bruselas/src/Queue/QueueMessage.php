<?php

namespace UCAM\BioHpc\Bruselas;

/**
 * Serializable queue implementation.
 * 
 * @implements IQueueMessage
 * @implements JsonSerializable
 *
 * @author Antonio-Jesús Banegas-Luna <ajbanegas@alu.ucam.edu>
 * @version 1.0.0
 * @package UCAM\BioHpc\Bruselas 
 */
class QueueMessage implements IQueueMessage, \JsonSerializable {

	/**
	 * @var int $experiment Experiment identifier.
	 */
	private $experiment;
	/**
	 * @var string $similarity Similarity algorithm's code.
	 */
	private $similarity;
	/**
	 * @var string $folder Folder where the output was stored.
	 */
	private $folder;

	/**
	 * Constructor
	 *
	 * @param int $experiment Experiment identifier.
	 * @param string $similarity Similarity algorithm's code.
	 * @param string $folder Folder where the output was stored.
	 */
	function __construct($experiment, $similarity, $folder) {
		$this->experiment = $experiment;
		$this->similarity = $similarity;
		$this->folder = $folder;
	}

    /**
     * @inherit
     * {@inherit}
     * {@inheritdoc}
     */
     public function getExperiment() {
		return $this->experiment;
	}

    /**
     * @inherit
     * {@inherit}
     * {@inheritdoc}
     */
	public function getSimilarity() {
		return $this->similarity;
	}

    /**
     * @inherit
     * {@inherit}
     * {@inheritdoc}
     */
	public function getFolder() {
		return $this->folder;
	}

    /**
     * @inherit
     * {@inherit}
     * {@inheritdoc}
     */
	public function equals($message) {
		if ($this->experiment !== $message->getExperiment()) {
			return false;
		}
		if ($this->similarity !== $message->getSimilarity()) {
			return false;
		}
		if ($this->folder !== $message->getFolder()) {
			return false;
		}
		return true;
	}

    /**
     * @inherit
     * {@inherit}
     * {@inheritdoc}
     */
	public function jsonSerialize() {
 		$vars = get_object_vars($this);
        	return $vars;
    	}

}
?>
