<?php

namespace UCAM\BioHpc\Bruselas;

/**
 * Final result of an experiment including the final score and the partial scores returned by each similarity algorithm.
 *
 * @author Antonio-Jesús Banegas-Luna <ajbanegas@alu.ucam.edu>
 * @version 1.0.0
 * @package UCAM\BioHpc\Bruselas
 */
class Result {
	/**
	 * @var string $ligand Ligand name.
	 */
	protected $ligand;
	/**
	 * @var float $score Final score after consensus (if needed).
	 */
	protected $score;
	/**
	 * @var array $partials Individual scores returned by each similarity algorithm.
	 */
	protected $partials;
	
	function __construct($ligand, $score) {
		$this->ligand = $ligand;
		$this->score = $score;
		$this->partials = array();
	}
	
	/**
	 * Return the name of the ligand.
	 *
	 * @return string Ligand name.
	 */
	public function getLigand() {
		return $this->ligand;
	}

	/**
	 * Return the final score for the ligand.
	 *
	 * @return float Final score after consensus (if needed).
	 */
	public function getScore() {
		return $this->score;
	}

	/**
	 * Return the results returned by each similarity algorithm.
	 *
	 * @return array Results returned by each similarity algorithm.
	 */
	public function getPartials() {
		return $this->partials;
	}

	/**
	 * Set the ligand name.
	 *
	 * @param string $ligand Ligand name to set.
	 */
	public function setLigand( $ligand ) {
		$this->ligand = $ligand;
	}

	/**
	 * Set the ligand score.
	 *
	 * @param float $score Final score assigned to the ligand.
	 */
	public function setScore( $score ) {
		$this->score = $score;
	}

	/**
	 * Add a ResultComparison coming from an individual comparison.
	 *
	 * @param ResultComparison $partial Partial result.
	 */
	public function addPartial( $partial ) {
		array_push ( $this->partials, $partial );
	}

}
?>
