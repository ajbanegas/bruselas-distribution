<?php
namespace UCAM\BioHpc\Bruselas;

include_once __DIR__.'/../../../vendor/PHPMailer_5.2.0/class.phpmailer.php';

class SMTPNotifier implements IResultNotifier {
	
	public function notifyUser( $message ) {
		if (! $message instanceof IMessage ) {
			throw new InvalidArgumentException( "Argument must be of type IMessage." );
		}

		$this->__sendEmail ( $message->getFrom(), $message->getTo(),
				$message->getTitle(), $message->getContent() );
	}
	
	private function __sendEmail( $from, $to, $subject, $body ) {
		$mail = new \PHPMailer(true);                            // Passing `true` enables exceptions
		try {
			//Server settings
			$mail->SMTPDebug = 1;                           // Enable verbose debug output
			//$mail->isSMTP();                              // Set mailer to use SMTP
			$mail->Host = 'smtp.gmail.com';                 // Specify main and backup SMTP servers
			$mail->SMTPAuth = true;                         // Enable SMTP authentication
			$mail->Username = SystemConfig::get( "smtp_user" );		// SMTP username
			$mail->Password = SystemConfig::get( "smtp_password" ); // SMTP password
			$mail->SMTPSecure = 'ssl';                      // Enable TLS encryption, `ssl` also accepted
			$mail->Port = 465;                              // TCP port to connect to

			//Recipients
			$mail->addAddress( $to );               // Name is optional
			$mail->addReplyTo(SystemConfig::get( "email_bcc" ), 'BRUSELAS Server Administrator');
			$mail->addBCC(SystemConfig::get( "email_bcc" ));
			$mail->addBCC(SystemConfig::get( "email_sender" ));
			$mail->setFrom(SystemConfig::get( "smtp_user" ), 'BRUSELAS Server');

			//Content
			$mail->CharSet = 'UTF-8';
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = $subject;
			$mail->Body    = $body;
			$mail->AltBody = $body;
			$mail->send();

		} catch (Exception $e) {
			Log::error( "Error sending mail to $to: " . print_r($mail->ErrorInfo, true) );
			throw new EmailSendingException ( "User '$user' could not be notified due to an error sending the notification email." );
		}
	}
	
}
?>
