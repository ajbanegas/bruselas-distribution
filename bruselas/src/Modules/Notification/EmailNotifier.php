<?php

namespace UCAM\BioHpc\Bruselas;

class EmailNotifier implements IResultNotifier {
	
	public function notifyUser( $message ) {
		if (! $message instanceof IMessage ) {
			throw new InvalidArgumentException( "Argument must be of type IMessage." );
		}

		$this->__sendEmail ( $message->getFrom(), $message->getTo(),
				$message->getTitle(), $message->getContent() );
	}
	
	private function __sendEmail( $from, $to, $subject, $body ) {
		$headers = "From: " . $from . "\r\n" . 
			"Reply-To: " . $from . "\r\n" . 
			"Bcc: " . SystemConfig::get( "email_bcc" ) . "\r\n" .
			"MIME-Version: 1.0" . "\r\n" . 
			"Content-type: text/html; charset=UTF-8" . "\r\n";
				
		if (! mail ( $to, $subject, $body, $headers )) {
			Log::error( "Error sending mail to $to" );
			throw new EmailSendingException ( "User '$user' could not be notified due to an error sending the notification email." );
		}
	}
	
}
?>
