<?php

namespace UCAM\BioHpc\Bruselas;

/**
 * Defines the list of ways that a user can be notified of
 * the results availability.
 */
interface IResultNotifier {

	/**
	 * Sends a notification to the user advertising that the results
	 * are available.
	 * 
	 * @param $message	Message implementing IMessage interface.
	 * @throws BruselasException if anything goes wrong.
	 */
	public function notifyUser($message);
}
?>
