<?php

namespace UCAM\BioHpc\Bruselas;

class Message implements IMessage {
	private $from;
	private $to;
	private $title;
	private $content;

	function __construct($from, $to, $title, $content) {
		$this->from = $from;
		$this->to = $to;
		$this->title = $title;
		$this->content = $content;
	}
	
	public function getFrom() { return $this->from; }
	
	public function getTo() { return $this->to; }
	
	public function getTitle() { return $this->title; }
	
	public function getContent() { return $this->content; }
	
}
?>