<?php

namespace UCAM\BioHpc\Bruselas;

interface IMessage {
	
	public function getFrom();
	
	public function getTo();
	
	public function getTitle();
	
	public function getContent();
	
}
?>