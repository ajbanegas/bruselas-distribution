<?php

namespace UCAM\BioHpc\Bruselas;

class ConsensusAvg implements IConsensusCalculator {

	private $weights;

	function __construct() {
		$this->weights = array ();
	}	

	public function getParams() {
		return $this->weights;
	}
	
	public function setParams($params = array()) {
	    if (! is_array ( $params )) {
            throw new InvalidArgumentException ( "'".print_r($params,true)."' argument is not an array" );
        }
        $this->weights = $params;
	}

    public function calculate($result) {
        // validate arguments
        if (! $result instanceof Result) {
            throw new InvalidArgumentException ( "Argument is not a Result object" );
        }
        // apply algorithm
        $score = array_reduce ( $result->getPartials(), function ( $acc, $elem ) {
		$pct = $this->__findWeight ( $this->weights, $elem->getMethod() );
                return $acc + ( $elem->getScore() * $pct );
        } );
        return $score;
    }

	/**
	 * Finds the weight of a method.
	 *
	 * @param
	 *        	weights Array of weights.
	 * @param
	 *        	method Method to find.
	 * @return Weight assigned to the method. If not found, then 0 is returned.
	 */
	private function __findWeight($weights, $method) {
		return array_key_exists($method,$weights) ? $weights[$method] : 0.0;
	}

}	
?>
