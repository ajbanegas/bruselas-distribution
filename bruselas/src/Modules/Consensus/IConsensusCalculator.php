<?php

namespace UCAM\BioHpc\Bruselas;

/**
 * Provides a diversity of consensus algorithms.
 */
interface IConsensusCalculator {

	/**
	 * Returns the parameters attached to the object (if any).
	 *
	 * @return Array of parameters.
	 */
	public function getParams();

	/**
	 * Sets any param required by the function.
	 *
	 * @param params	Associative array of parameters.
	 */
	public function setParams($params = array());
	
	/**
	 * Reduces the full set of scores to one single descriptive value.
	 *
	 * @param
	 *        	result Result object which contains a list of partials scores.
	 * @return Unified score calculated from the original scores.
	 */
	public function calculate($result);

}
?>
