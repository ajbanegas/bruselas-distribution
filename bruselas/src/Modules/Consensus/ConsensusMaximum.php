<?php

namespace UCAM\BioHpc\Bruselas;

class ConsensusMaximum implements IConsensusCalculator {

	public function getParams() {
		return null;
	}
	
        public function setParams($params = array()) {
                // do nothing
        }

        public function calculate($result) {
                // validate arguments
                if (! $result instanceof Result) {
                        throw new BruselasException ( "Argument is not a Result object" );
                }
                // apply algorithm
				return max ( array_map( function ( $elem ) {
					return $elem->getScore();
				}, $result->getPartials() ) );
        }

}	
?>
