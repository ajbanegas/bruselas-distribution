<?php

namespace UCAM\BioHpc\Bruselas;

class DefaultConsensus implements IConsensusCalculator {

	public function getParams() {
		return null;
	}

	public function setParams($params = array()) {
		// do nothing
	}
	
    public function calculate($result) {
        // validate arguments
        if (! $result instanceof Result) {
                throw new InvalidArgumentException ( "Argument is not a Result object" );
        }
        // apply algorithm
        $n = count ( $result->getPartials () );
		$score = array_reduce ( $result->getPartials(), function ( $acc, $elem ) use ( &$n ) {
			return $acc + ( $elem->getScore() / $n );
		} );
		return $score;
    }
	
}
?>
