<?php

namespace UCAM\BioHpc\Bruselas;

class StdOutputTransformer implements ITransformer {

	public function saveResults($params, $results, $objects) {
		usort ( $results, array ( $this, "sortByScore" ) );
		foreach ($results as $result) {
			echo $result->getLigand().",".$result->getScore().PHP_EOL;
		}
	}

	public function clean($params) {
		// Nothing to do
	}

	private function sortByScore($a, $b) {
		if ( $a->getScore () < $b->getScore () ) return 1;
		if ( $a->getScore () > $b->getscore () ) return -1;
		return 0;
	}
}
?>
