<?php

namespace UCAM\BioHpc\Bruselas;

/**
 * Exposes a list of transformations on the data.
 */
interface ITransformer {
	
	/**
	 * Performs any type of transformation with the results.
	 *
	 * @param
	 *        	params Experiment's params.
	 * @param
	 *        	results Array of Result objects.
	 * @param
	 *		objects Transformers attached to the experiment.
	 */
	public function saveResults($params, $results, $objects);

	/**
	 * Clean all the temporary data from the experiment.
	 *
	 * @param
	 *        	params Experiment's params.
	 */
	public function clean($params);

}
?>
