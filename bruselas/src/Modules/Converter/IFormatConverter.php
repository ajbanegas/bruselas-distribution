<?php

namespace UCAM\BioHpc\Bruselas;

/**
 * Defines the molecular conversion operation.
 */
interface IFormatConverter {
	
	/**
	 * Converts a molecule from one format to another.
	 *
	 * @param	fromFile	File containing the source molecule.
	 * @param      	toFile		File where the converted molecule will be.
	 * @throws BruselasException if any error happens.
	 */
	public function convertToFile($fromFile, $toFile);

	public function convertToOutput($fromFile, $outFormat);

}
?>
