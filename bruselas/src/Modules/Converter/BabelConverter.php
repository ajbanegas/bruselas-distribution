<?php

namespace UCAM\BioHpc\Bruselas;

/**
 * Converts molecules using Babel software.
 */
class BabelConverter implements IFormatConverter {
	
	public function convertToFile( $fromFile, $toFile ) {
		if ( !file_exists( $fromFile ) ) {
			throw new FileNotFoundException ( "Source file '$file' does not exists" );
		}
		$outFormat = $this->__getFormat ( $toFile );
		$cmd = "obabel $fromFile -o $outFormat -O $toFile --gen3D";
		$line = exec( $cmd, $output, $retvar );
		$this->__validateOutput ( $line, $output, $retvar );
	}

	public function convertToOutput( $fromFile, $outFormat ) {
		Log::debug ( "$fromFile -> $outFormat" );

        if ( !file_exists( $fromFile ) ) {
            throw new FileNotFoundException ( "Source file '$fromFile' does not exists" );
        }
		if ( $outFormat === "smi" || $outFormat === "smiles" ) {
            $cmd = "obabel $fromFile -o $outFormat 2> /dev/null | tr '\t' ' ' | cut -d' ' -f1";
        } else {
            $cmd = "obabel $fromFile -o $outFormat --gen3D";
        }
	
		Log::debug ( "cmd: $cmd" );

        $line = exec( $cmd, $output, $retvar );

		Log::debug ( "line: $line" );		

        $this->__validateOutput ( $line, $output, $retvar );
		return $this->__print ( $output );
	}

	private function __getFormat( $foo ) {
		return pathinfo ( $foo, PATHINFO_EXTENSION );
	}

	private function __validateOutput( $line, $output, $retvar ) {
		if ( $retvar !== 0 ) {
			throw new MolConversionException ( "Unexpected error while converting molecule" );
		}
	}

    private function __print( $output ) {
            return implode ( PHP_EOL, $output );
    }

}
?>
