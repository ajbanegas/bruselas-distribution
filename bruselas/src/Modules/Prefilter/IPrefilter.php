<?php

namespace UCAM\BioHpc\Bruselas;

/**
 * Interface defining the operations to perform the pre-filter step.
 *
 * @author Antonio-Jesús Banegas-Luna <ajbanegas@alu.ucam.edu>
 * @version 1.0.0
 * @package UCAM\BioHpc\Bruselas
 */
interface IPrefilter {
	
	/**
	 * Build a query to select the ligands to include in the library.
	 *
	 * @param array List of descriptors used in the query.
	 * @param int Number of ligands to include in the library.
	 * @param array List of datasets to screen.
	 * @param string Terms to filter with.
	 * @param array List of drug-likeness filters to apply.
	 * @return string Query to select the ligands to include in the library.
	 */
	public function getLigandFiles($descriptors, $datasets, $keywords = null, $filters = null);

}
?>
