<?php

namespace UCAM\BioHpc\Bruselas;

class ExtractorDragon implements IDescriptorExtractor {

	private $converter;

	function __construct() {
		$this->converter = new BabelConverter ();
	}

	public function extract($molecule, array $descriptorList = array()) {
		// validate input types
		$this->__validate ( $molecule );

		// prepare config file
        $home_dir = SystemConfig::get ( "home_dir" );

		$sw_path = "$home_dir/scriptsDocking/externalSw/dragon6";
		$target = rand ();
		$ext = pathinfo ( $molecule, PATHINFO_EXTENSION );

		if ( !$this->__isAllowedFormat ( $ext ) ) {
			$outmolecule = $this->__convert ( $molecule, "mol2" );
		} else {
			$outmolecule = $molecule;
		}

		$remote_drs_file = "$sw_path/adeno_$target.drs";
		$remote_molecule = "$sw_path/$target.$ext";

		$drs_file = $this->__template ( $target, $remote_molecule );

		// copy files
		$ssh = new SSH ();
		$ssh->connect ();
		$ssh->copy ( $drs_file, $remote_drs_file );
		$ssh->copy ( $outmolecule, $remote_molecule );

		// perform extraction
		$out = $ssh->exec( "cd $sw_path && ./dragon6shell -ig drg_license.txt -s $remote_drs_file -n 2> /dev/null" );

		// transform output
		$descriptors = $this->__parseOutput ( $out );
		$descriptors = $this->__filter ( $descriptors, $descriptorList );

		// remove temporary files
		unlink( $drs_file );
		$ssh->exec ( "rm $remote_molecule" );
		$ssh->exec ( "rm $remote_drs_file" );
		$ssh->close ();

		return $descriptors;
	}

	/**
	 *
	 *
	 */
	private function __validate($molecule) {
		if (! is_string ( $molecule )) {
			throw new InvalidArgumentException ( "'$molecule' argument is not a string" );
		}
		if (strlen ( $molecule ) <= 0) {
			throw new InvalidArgumentException ( "Argument 2 must be a string indicating the location of the query molecule" );
		}
		if (! file_exists ( $molecule )) {
			throw new FileNotFoundException ( "File '$molecule' does not exist" );
		}
	}

	/**
	 *
	 *
	 *
	 */
	private function __template($id, $target_path) {
        $scripts_path = __DIR__ . "/../../../scripts";
		$aux = tempnam ( sys_get_temp_dir(), "DRS-" );
		$foo = "$aux.drs";
		rename($aux, $foo);

        $content = file_get_contents ( "$scripts_path/adeno_query.drs" );
        $content = str_replace ( "MIFICHERO", $target_path, $content );
        if ( file_put_contents ( $foo, $content ) === FALSE ) {
			throw new FileCreationException("Error creating template file for DRAGON");
		}
		chmod ( $foo, 0644 );

		return $foo;
	}

	private function __parseOutput($output) {
		if ( strlen ( $output ) === 0 ) { 
			return;
		}
		$aux = explode ( "\n", $output );
		$names = explode ( "\t", $aux[0] );
		$values = explode ( "\t", $aux[1] );

		$result = array ();
		for ( $i = 2; $i < count ( $names ); $i++ ) {
			if ( is_null ( $values [$i] ) || $values [$i] === "NaN" ) {
				$values [$i] = 0;
			}
			$result [$i] = new Descriptor ( $names [$i], $values [$i] );
		}
		return $result;
	}

	/**
	 *
	 *
	 */
	private function __filter($calculated, $names) {
		$requested = count ( $names );
		$result = array ();
		foreach ($calculated as $desc) {
			if (in_array ( $desc->getName(), $names) || $requested === 0 ) {
				array_push ( $result, $desc );
			}
		}
		
		return $result;
	}

	/**
	 *
	 */
	private function __isAllowedFormat($format) {
		$allowed = array ( "cml", "hin", "mmd", "mol", "mol2", "sdf", "smi" );
		return in_array ( $format, $allowed );
	}

	/**
	 *
	 */
	private function __convert($infile, $format) {
		$dir = pathinfo ( $infile, PATHINFO_DIRNAME );
		$file = pathinfo ( $infile, PATHINFO_FILENAME );
		$outfile = "$dir/$file.$format";
		$this->converter->convertToFile ( $infile, $outfile );
		return $outfile;
	}

}
?>
