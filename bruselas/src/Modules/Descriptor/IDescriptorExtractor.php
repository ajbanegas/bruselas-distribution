<?php

namespace UCAM\BioHpc\Bruselas;

/**
 * Provides descriptor extraction functions on a molecule.
 */
interface IDescriptorExtractor {
	
	/**
	 * Extracts a set of descriptores from a molecule.
	 *
	 * @param
	 *        	molecule Path to the source molecule.
	 * @param
	 *        	descriptorList List of wished descriptors.
	 * @return A map of tuples (descriptor,value) representing molecular properties.
	 */
	public function extract($molecule, array $descriptorList = array());
}
?>
