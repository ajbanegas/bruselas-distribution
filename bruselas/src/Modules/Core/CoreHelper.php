<?php

namespace UCAM\BioHpc\Bruselas;

class CoreHelper {

	/**
	 * Creates the remote folders and copy the query and library files
	 * to the calculation server.
	 *
	 * @param
	 *        	experiment Experiment getting ready.
	 * @return Remote paths of query and library files.
	 */
	public function prepareCluster($experiment) {
		$id = $experiment->getParams ()->getId ();
		$query = $experiment->getParams ()->getQuery ();
		$library = $experiment->getParams ()->getLibrary ();
		
		// load parameters
		$home_dir = SystemConfig::get ( "home_dir" );
		$query_ext = pathinfo ( $query, PATHINFO_EXTENSION );
		$library_ext = pathinfo ( $library, PATHINFO_EXTENSION );
		$dir = "$home_dir/proteinas/$id";

		// remote paths
		$paths = array (
			"query" => "$dir/$id.$query_ext",
			"library" => "$dir/database.$library_ext" 
		);

		// prepare remote folder and copy files
		$ssh = new SSH ();
		$ssh->connect ();
		$ssh->exec ( "mkdir -p $dir" );
		$ssh->copy ( $query, $paths ["query"] );
		$ssh->copy ( $library, $paths ["library"] );
		$ssh->close ();

		return $paths;
	}
	
	/**
	 * Check whether an experiment is finished.
	 * 
	 * @param $experiment	Experiment to check.
	 * @return boolean	True if all comparisons have been done. False otherwise.
	 */
	public function isComplete( $experiment ) {
		$n_comp = count( $experiment->getObjects ()->getComparators () );
		$done = $this->__findMessages( $experiment->getParams ()->getId() );
		return ( $n_comp === count( $done ) );
	}

	/**
	 * Return the first $size elements of $list.
	 *
	 * @param $list	List to filter.
	 * @param $size	Number of elements to take from the list.
	 * @return	First $size elements of $list.
	 */
	public function sublist($list, $size) {
                if ( count ( $list ) > $size ) {
                        usort ( $list, function ( $a, $b ) {
                        	if ($a->getScore() < $b->getScore()) { return 1; }
                        	else if ($a->getScore() > $b->getScore()) { return -1; }
                        	return 0;
                        } );
                        return array_slice ( $list, 0, $size );
                }
		return $list;
	}

	/**
	 * Return a sublist containing only the elements whose score is greater than $cutoff
	 *
	 * @param $list		List to filter.
	 * @param $cutoff	Minimum score to remain in the list.
	 * @return	Sublist with the elements that reach the cutoff.
	 */
	public function applyCutoff($list, $cutoff) {
		if ( is_null ( $list ) || count ( $list) === 0 ) {
			return array();
		}
		$arr = array_filter ( $list, function ( $obj ) use ( &$cutoff ) {
			return $obj->getScore() >= $cutoff;
		});
		return $arr;
	}

        /**
    	 * Load the output of an experiment.
         *
         * @param $experiment   Experiment.
         * @return      Array of QueueMessage objects.
         */
        public function loadOutput( $experiment ) {
                $done = $this->__findMessages ( $experiment->getParams ()->getId () );
                $manager = new QueueManager ();

                foreach ($done as $msg) {
                        $comparator = $this->__findComparator ( $experiment, $msg->getSimilarity() );
                        $output = $comparator->getOutput ( $msg->getFolder() );
                        $experiment->getOutput ()->addOutput ( $output );
                }

		return $experiment;
        }

	/**
	 * Groups the results by ligand.
	 *
	 * @param experiment
	 * @return	Result[]
	 */
	public function mergeOutput( $experiment ) {
		if ( is_null ( $experiment ) ) {
			return array();
		}
		$output = $experiment->getOutput()->getOutput();
		$list = array ();	// Result[]

		foreach ( $output as $comp ) {
			$partial = new ResultComparison ( $comp->getLigand(), $comp->getScore(), $comp->getMethod(), $comp->getFolder(), $comp->getFile() );
			$element = $this->__findLigand ( $list, $comp->getLigand() );
			if ( is_null ( $element ) ) {
				$result = new Result ( $comp->getLigand(), $comp->getScore() );
				$result->addPartial ( $partial );
				array_push ( $list, $result );
			} else {
				$element->addPartial( $partial );
			}
		}
		return $list;
	}

	/**
	 * Extracts from the queue the messages linked to a given experiment.
	 *
	 * @param $id	Experiment identifier.
	 */
	public function popMessages( $id ) {
		$done = $this->__findMessages ( $id );
		$manager = new QueueManager ();
		return array_map(function( $msg ) use( $manager ) {
			$manager->removeMessage ( $msg );
			return $msg->getFolder();
		}, $done);
	}

	/**
	 * Creates a notification message.
	 * 
	 * @param $experiment	Experiment.
	 * @return	IMessage object.
	 */
	public function buildMessage( $experiment ) {
		Log::debug ( "building message" );

		$senderName = SystemConfig::get( "email_sender_name" );
		$senderEmail = SystemConfig::get( "email_sender" );

		$recipient = $experiment->getParams()->getUser();
		$query = $experiment->getParams()->getQuery();
		$id = $experiment->getParams()->getId();
		$description = $experiment->getParams()->getDescription();
		$converter = $experiment->getObjects()->getConverter();

		$from = "$senderName <$senderEmail>";
		$to = $recipient;
		$title = "Ligand similarity results from BRUSELAS Server";
		
		Log::debug ( "starting smiles extraction: ".print_r($converter, true) );

		$smiles = $converter->convertToOutput( $query, "smi" );
		$url = "http://bio-hpc.ucam.edu/Bruselas2/web/Results/Results.php?idExp=$id&email=$to";
		$text = ( strlen( $description ) > 0 ) ? $description : "";

		Log::debug ( "smiles: $smiles" );

		$content = "Your Similarity Search for '$smiles' on BRUSELAS Server finished successfully.<br><br>".
			(strlen($text) > 0 ? "<b>Description: </b>$text<br><br>" : "").
			"You can access the results through your dashboard or by using the following url:<br><br>".
			"<a href='$url'>Similarity Search No. $id</a><br><br>".
			"Results will expire in 30 days.<br><br>".
			"If you use BRUSELAS Server in your research, please add the following citation in the methods ".
			"section of your manuscript:<br>".
			"<a href='https://pubs.acs.org/doi/10.1021/acs.jcim.9b00279' target='_blank'>Banegas-Luna AJ, Cer&oacute;n-Carrasco JP, Puertas-Mart&iacute;n S, P&eacute;rez-S&aacute;nchez H. <b>\"BRUSELAS: HPC generic and customizable software architecture for 3D ligand-based virtual screening of large molecular databases\"</b>. Journal of Chemical Information and Modeling. 2019; 59(6):2805-2817.DOI: 10.1021/acs.jcim.9b00279</a><br><br>".
			"The server is available at:<br>".
			"<a href='http://bio-hpc.ucam.edu/Bruselas2'>http://bio-hpc.ucam.edu/Bruselas2</a><br><br>".
			"In case of questions please reply directly to this email.<br>";

		return new Message($from, $to, $title, $content);
	}

	// private methods

	private function __findMessages( $id ) {
                $manager = new QueueManager ();
                $messages = $manager->listMessages ();
                $done = array_filter( $messages, function ($msg) use ($id) {
                        return intval( $msg->getExperiment() ) === intval ( $id );
                } );
		return $done;
	}

	private function __findComparator( $experiment, $code ) {
		foreach ($experiment->getObjects()->getComparators() as $comp) {
			if ($comp->getCode() === $code) {
				return $comp;
			}
		}
		return NULL;
	}

	private function __findLigand( $result_list, $ligand ) {
		if ( is_null ( $result_list ) || count ( $result_list ) === 0 ) {
			return null;
		}
		$tmp = array_filter (
			$result_list,
			function ($e) use ( &$ligand ) {
				return boolval($e->getLigand() === $ligand);
			}
		);
		$arr = array_merge ( array(), $tmp );	// trick to get 0-based array (same as array_pop($arr))
		if ( count ( $arr ) > 0 ) {
			return $arr[0];
		}
		return null;
	}
	
}
?>
