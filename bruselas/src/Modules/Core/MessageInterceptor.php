<?php

namespace UCAM\BioHpc\Bruselas;

require_once __DIR__."/../../../autoload.php";

class MessageInterceptor {

	public function validate($args) {
		if ( ! is_array ( $args ) || count ( $args ) < 3 ) {
			return false;
		}
		return true;
	}

	public function notify($idExp, $dir, $code) {
		if ( strlen( $idExp ) === 0 || strlen( $dir ) === 0 || strlen( $code ) === 0 ) {
			return false;
		}
		$manager = new QueueManager ();
		$manager->addMessage( new QueueMessage( $idExp, $code, $dir ) );
		return true;
	}	

}

class Locker {

	const FILELOCK = "/tmp/bruselas-lock.txt";

	public function isLocked() {
		return file_exists(self::FILELOCK);
	}

	public function lock() {
		touch(self::FILELOCK);
	}

	public function unlock() {
		unlink(self::FILELOCK);
	}

	public function wait() {
		usleep(rand(1,1000));	// x1000000 -> seconds, x1000-> milliseconds
	}

}

/* Assure that the lock is released even when a fatal error happens */
function bruselas_error_handler() {
	global $locker;
	$locker->unlock();
}

register_shutdown_function('bruselas_error_handler');

if (!isset($argv) || is_null($argv)) {
	return;
}

/* Receive arguments */
$interceptor = new MessageInterceptor();
$locker = new Locker();

$locker->wait();
while ($locker->isLocked()) {
	$locker->wait();
}

$locker->lock();
try {
	if ( ! $interceptor->validate($argv) ) {
		return;
	}

	$idExp = isset ( $argv [1] ) ? $argv [1] : 0;
	$dir = isset ( $argv [2] ) ? $argv [2] : NULL;
	$code = isset ( $argv [3] ) ? $argv [3] : NULL;
	if ( $interceptor->notify($idExp, $dir, $code) ) {
		#echo "*** mensaje encolado\n";
	}
} catch (Exception $e) {
} finally {
	$locker->unlock();
}
?>
