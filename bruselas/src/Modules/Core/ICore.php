<?php

namespace UCAM\BioHpc\Bruselas;

interface ICore {
	
	/**
	 * Public function to extract descriptors from a query.
	 *
	 * @param
	 *        	query Query molecule whose descriptors will be calculated.
	 * @param
	 *        	extractor Extractor object to perform the calculation.
	 * @param
	 *        	filter List of desired descriptors.
	 * @return Array of Descriptor objects.
	 * @throws BruselasException if something goes wrong.
	 */
	public function extractDescriptors($query, $extractor, $filter = array());
	
	/**
	 * Public function to run experiments.
	 *
	 * @param
	 *        	experiment Object describing the experiment parameters.
	 * @return Newly created experiment's identifier.
	 * @throws BruselasException if something goes wrong.
	 */
	public function runExperiment($experiment);
	
}
?>
