<?php

namespace UCAM\BioHpc\Bruselas;

class Core implements ICore {
	
	private $helper; 	// helper class
	
	public function __construct() {
		$this->helper = new CoreHelper ();
	}
		
	public function extractDescriptors($query, $extractor, $filter = array()) {
		if ($extractor === null) {
			throw new InvalidArgumentException ( "Argument 2 must be an instance of IDescriptorExtractor." );
		}
		try {
			return $extractor->extract ( $query, $filter );
		} catch ( Exception $e ) {
			throw $e;
		}
	}
	
	public function runExperiment($experiment) {
		// input validation
		$experiment->validate();
		try {
			// copy files
			$remote_paths = $this->helper->prepareCluster ( $experiment );
			// extract params
			$id = $experiment->getParams ()->getId ();
			$query = $experiment->getParams ()->getQuery ();
			$library = $experiment->getParams ()->getLibrary ();
	 		$cutoff = $experiment->getParams ()->getCutoff ();
			$size = $experiment->getParams ()->getResultSize ();
			$converter = $experiment->getObjects ()->getConverter ();
			$comparators = $experiment->getObjects () ->getComparators ();
			$consensuator = $experiment->getObjects ()->getConsensuator ();
			$transformer = $experiment->getObjects ()->getTransformer ();
			$notifier = $experiment->getObjects ()->getNotifier ();

			if ( is_null ( $converter ) ) {
				$experiment->getObjects ()->setConverter( new BabelConverter () );
				$converter = $experiment->getObjects ()->getConverter ();
			}

			// run task
			foreach ( $comparators as $obj ) {
				$obj->compare ( $remote_paths ["query"], $remote_paths ["library"], $id );
			}
			// wait for all the comparations to be finished.
			// query the queue every minute to check whether everything is done
			while ( !$this->helper->isComplete( $experiment ) ) {
				Log::debug( "still incomplete job $id" );
				sleep( 60 );
			}
			// load files and scores. remove messages from the queue
			$result = $this->__getResults ( $experiment );
			$folders = $this->helper->popMessages ( $id );

            		// actions
			if ( count ( $comparators ) > 1 ) {
				if ( is_null ( $consensuator ) ) {
					$consensuator = new DefaultConsensus ();
				}
				/*if ( $consensuator instanceof ConsensusAvg ) {
					$avgParams = array( "nAlgorithms" => count ( $comparators ) );
					$consensuator->setParams ( $avgParams );
				}*/
				$result = $this->__applyConsensus ( $consensuator, $result );
			}

			$result = $this->helper->applyCutoff ( $result, $cutoff );
			$result = $this->helper->sublist ( $result, $size );
			$experiment->getOutput()->setResult ( $result );
			
			$notificationMessage = $this->helper->buildMessage ( $experiment );

        	if ( is_null ( $transformer ) ) {
				$transformer = new StdOutputTransformer ();
			}
            $transformer->saveResults ( $experiment->getParams(), $experiment->getOutput()->getResult(), $experiment->getObjects() );
            $transformer->clean ( $experiment->getParams() );

			Log::debug( "transformation completed ($id)" );
			Log::debug( "notifier: ".print_r($notifier,true) );

			if ( is_null ( $notifier ) ) {
				$notifier = new SMTPNotifier ();
			}
			$notifier->notifyUser ( $notificationMessage );

			Log::debug( "mail sent ($id)" );

                	// remove temporary files
			#$this->__clean ( $id, $folders );

        	Log::debug( "experiment $id finished." );
		} catch ( Exception $e ) {
			throw $e;
		}
	}

	/**
	 * Remove temporary files.
	 *
	 * @param $id		Experiment's identifier.
	 * @param $folders	Remote directories to delete.
	 */
	private function __clean ( $id, $folders ) {
        $home_dir = SystemConfig::get ( "home_dir" );
        $ssh = new SSH ();
        $ssh->exec ( "rm -r $home_dir/proteinas/$id" );
        array_walk($folders, function( $dir ) use( $ssh ) {
        	$ssh->exec( "rm -r $dir" );
        });
		$ssh->close ();
	}

	/**
	 * Collect the information generated.
	 *
	 * @param $experiment	Experiment.
	 * @return	Result[]
	 */
	private function __getResults ( $experiment ) {
 		$experiment = $this->helper->loadOutput ( $experiment );
        //$this->helper->popMessages ( $experiment->getParams()->getId () );
        $result = $this->helper->mergeOutput ( $experiment );
		return $result;
	}

	private function __applyConsensus( $consensuator, $result ) {
		$consensuated = array_map ( function ( $res ) use ( &$consensuator ) {
							$score = $consensuator->calculate ( $res );
							$res->setScore ( $score );
							return $res;
					}, $result );
		return array_merge ( array(), $consensuated );
	}

}
?>
