<?php

namespace UCAM\BioHpc\Bruselas;

abstract class DefaultComparator implements IMolecularComparator {

	protected $inputDir;
	
	abstract protected function getSwFormat();
	
	abstract public function getCode();
	
	public function compare($query, $library, $id = -1) {
		// validate input types
		if (! is_string ( $query )) {
			throw new InvalidArgumentException ( "'$query' argument is not a string" );
		}
		if (! is_string ( $library )) {
			throw new InvalidArgumentException ( "'$library' argument is not a string" );
		}
		// run comparison
		$home_dir = SystemConfig::get ( "home_dir" );

		$ssh = new SSH ();
		$ssh->connect ();
		$ssh->exec ( "nohup $home_dir/scriptsWeb/Bruselas.sh -l $query -d $library -s ". $this->getCode () ." -o VS & 2> /dev/null" );
		$ssh->close ();
		return $id;
	}
	
	protected function __readFile( $filepath ) {
		$ssh = new SSH ();
		$ssh->connect ();
		$out = $ssh->exec ( "cat $filepath" );
		$lines = explode ( "\n", $out );
		$ssh->close ();
		return $this->__parseContent( $lines );
	}
	
	protected function __parseContent($content) {
		$str = "";
		foreach ( $content as $line ) {
			$str .= $line . PHP_EOL;
		}
		return $str;
	}

    protected function __clean( $dir ) {
        $ssh = new SSH ();
        $ssh->exec ( "rm -rf $dir" );
        $ssh->close ();
    }
	
}
?>
