<?php

namespace UCAM\BioHpc\Bruselas;

class ComparatorShafts extends DefaultComparator {

        protected function getSwFormat() {
                return "mol2";
        }

        public function getCode() {
                return "SF";
        }

        public function getOutput( $dir ) {
		$this->inputDir = $dir;
		$out = $this->__load ( $dir );
		$results = $this->__parse ( $out, $dir );
		return $results;
        }

       public function copyFiles( $outputDir ) {
                $host = SystemConfig::get( "cluster" );
                $src = $this->inputDir . "VS-" . $this->getCode() . "-*-aligned." . $this->getSwFormat();

				Log::info("[ rsync -azq $host:$src $outputDir ]");
                exec( "rsync -azq $host:$src $outputDir", $output, $retvar );
                if ( $retvar != 0 ) {
                	Log::debug( "No result files created by SHAFTS" );
                }
        }

	private function __load( $dir ) {
		$ssh = new SSH ();
		$ssh->connect ();
		$out = $ssh->exec ( "cat $dir*Result.list" );
		$ssh->close ();
		return $out;
	}

	private function __parse( $out, $dir ) {
            	$list = explode ( "\n", $out );
		
		$results = array();             // array<ResultComparison>
		foreach ( $list as $elem ) {
			$items = explode ( "\t", $elem );
			if ( count($items) < 3 ) {
				continue;
			}
			$ligand = $items[1];
			$score = $items[4];
			$method = $this->getCode();

			if ( strlen ( $ligand ) > 0 ) {
				$name = "VS-" . $this->getCode() . "-" . $ligand . "-aligned." . $this->getSwFormat();
				$content = "";

				$file = new File($name, $content, $dir);
				$rc = new ResultComparison( $ligand, $score, $method, $dir, $file );

				array_push( $results, $rc );
			}
		}
		return $results;
	}
		
}
?>