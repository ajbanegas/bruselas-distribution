<?php

namespace UCAM\BioHpc\Bruselas;

/**
 * Defines the molecular comparison operation.
 */
interface IMolecularComparator {
	
	/**
	 * Compares a query molecule against a library or set of molecules.
	 *
	 * @param
	 *        	query Path to the query molecule.
	 * @param
	 *        	library Path to the library of ligands.
	 * @return A notification when the process is complete.
	 */
	public function compare($query, $library, $id = -1);
	
	/**
	 * Returns the unique code identifying the method.
	 *
	 * @return Comparison method identifier.
	 */
	public function getCode();
	
	/**
	 * Reads the scores and the aligment files.
	 *
	 * @return Array of ResultComparison objects.
	 */
	public function getOutput($dir);

	/**
	 * Copy the output files from the cluster.
	 *
	 * @param
	 *		outputDir Destination folder.
	 */
	public function copyFiles($outputDir);

}
?>
