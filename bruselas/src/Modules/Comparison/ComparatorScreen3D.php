<?php

namespace UCAM\BioHpc\Bruselas;

class ComparatorScreen3D extends DefaultComparator {

        protected function getSwFormat() {
                return "mol2";
        }

        public function getCode() {
                return "S3";
        }

        public function getOutput( $dir ) {
		$this->inputDir = $dir;
		$out = $this->__load ( $dir );
		$results = $this->__parse ( $out, $dir );
		return $results;
        }
	
       public function copyFiles( $outputDir ) {
            $host = SystemConfig::get( "cluster" );
            $src = $this->inputDir . "VS-" . $this->getCode() . "-*-aligned." . $this->getSwFormat();
			$srcQuery = $this->inputDir . "ligand_VS-" . $this->getCode() . "-*-aligned." . $this->getSwFormat();

            Log::info("[ rsync -azq $host:$src $outputDir ]");
            exec( "rsync -azq $host:$src $outputDir", $output, $retvar );
	        if ( $retvar != 0 ) {
	        	Log::debug( "No result files created by Screen3D (1)" );
	        }

            Log::info("[ rsync -azq $host:$srcQuery $outputDir ]");
			exec( "rsync -azq $host:$srcQuery $outputDir", $output, $retvar );
	        if ( $retvar != 0 ) {
	        	Log::debug( "No result files created by Screen3D (2)" );
	        }
        }
	
	private function __load( $dir ) {
		$ssh = new SSH ();
		$ssh->connect ();
		$out = $ssh->exec ( "find $dir -type f -name '*_screenOut.txt' -exec cat {} \;" );
		$ssh->close ();
		return $out;
	}
		
	private function __parse( $out, $dir ) {
        	$list = explode ( "\n", $out );
		
                $results = array();             // array<ResultComparison>
                foreach ( $list as $elem ) {
                        $items = explode ( "####", $elem );
                        if ( count ( $items ) < 2 ) {
                                continue;
                        }
                        $ligand = $items[0];
                        $score = str_replace(',', '.', $items[1]);
                        $method = $this->getCode();

                        if ( strlen ( $ligand ) > 0 ) {
                                $name = "VS-" . $this->getCode() . "-" . $ligand . "-aligned." . $this->getSwFormat();
                                $content = "";

                                $file = new File($name, $content, $dir);
                                $rc = new ResultComparison( $ligand, $score, $method, $dir, $file );

                                array_push( $results, $rc );
                        }
                }

		return $results;
	}

}
?>

