<?php

namespace UCAM\BioHpc\Bruselas;

class ComparatorPharmer extends DefaultComparator {

    protected function getSwFormat() {
        return "sdf";
    }

    public function getCode() {
        return "PH";
    }

    public function getOutput( $dir ) {
		$this->inputDir = $dir;
		$out = $this->__load ( $dir );
		$results = $this->__parse ( $out, $dir );
		return $results;
    }

   public function copyFiles( $outputDir ) {
        $host = SystemConfig::get( "cluster" );
        $src = $this->inputDir . "VS-" . $this->getCode() . "-*-aligned." . $this->getSwFormat();

		Log::info("[ rsync -azq $host:$src $outputDir ]");
        exec( "rsync -azq $host:$src $outputDir", $output, $retvar );
        if ( $retvar != 0 ) {
        	Log::debug( "No result files created by Pharmer (1)" );
        }

        $src = $this->inputDir . "model*." . $this->getSwFormat();
		Log::info("[ scp $host:$src $outputDir/model." . $this->getSwFormat() . " ]");
        exec( "scp $host:$src $outputDir/model." . $this->getSwFormat(), $output, $retvar );
        if ( $retvar != 0 ) {
        	Log::debug( "No result files created by Pharmer (2)" );
        }        
    }

	private function __load( $dir ) {
		$ssh = new SSH ();
		$ssh->connect ();
		$out = $ssh->exec ( "cat $dir*.txt" );
		$ssh->close ();
		return $out;
	}

	private function __parse( $out, $dir ) {
    	$list = explode ( "\n", $out );
		
		$results = array();             // array<ResultComparison>
		foreach ( $list as $elem ) {
			$items = explode ( "####", $elem );
			if ( count($items) < 2 ) {
				continue;
			}
			$ligand = $items[1];
			$score = $items[0];
			$method = $this->getCode();

			if ( strlen ( $ligand ) > 0 ) {
				$name = "VS-" . $this->getCode() . "-" . $ligand . "-aligned." . $this->getSwFormat();
				$content = "";

				$file = new File($name, $content, $dir);
				$rc = new ResultComparison( $ligand, $score, $method, $dir, $file );

				array_push( $results, $rc );
			}
		}
		return $results;
	}

}
?>