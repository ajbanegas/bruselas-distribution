<?php

namespace UCAM\BioHpc\Bruselas;

/**
 * Represent the output of a 3D shape similarity comparison.
 * It includes the code of the similarity algorithm and the folder where the output is stored.
 *
 * @author Antonio-Jesús Banegas-Luna <ajbanegas@alu.ucam.edu>
 * @version 1.0.0
 * @package UCAM\BioHpc\Bruselas
 */
class ResultComparison extends Result {
	/**
	 * @var string $method Similarity method's code.
	 */
	private $method;
	/**
	 * @var string $folder Directory where the output is stored.
	 */
	private $folder;
	
	function __construct($ligand, $score, $method, $folder, $file = null) {
		$this->ligand = $ligand;
		$this->score = $score;
		$this->method = $method;
		$this->folder = $folder;
		$this->file = $file;
	}

	/**
	 * Return the ligand name.
	 *
	 * @return string Name of the ligand.
	 */
	public function getLigand() {
		return $this->ligand;
	}

	/**
	 * Return the score assigned by the similarity algorithm.
	 *
	 * @return float Score returned by the similarity algorith.
	 */
	public function getScore() {
		return $this->score;
	}
	
	/**
	 * Return the code of the similaritu method used.
	 *
	 * @return string Similarity method's code.
	 */
	public function getMethod() {
		return $this->method;
	}
	
	/**
	 * Return the directory where the output is stored.
	 *
	 * @return string Directory where the output is stored.
	 */
	public function getFolder() {
		return $this->folder;
	}

	/**
	 * Return the file containing the compared ligand.
	 *
	 * @return File File containing the compared ligand.
	 */
	public function getFile() {
		return $this->file;
	}
	
}
?>
