2017-10-19	v1.0.0

# Bugfixes
- la conexión se cierra cuando la consulta tarda mucho
- TODO la consulta para obtener la librería tarda mucho a veces
- la biblioteca llega vacía a veces
- la búsqueda por nombre se queda parada la primera vez

# TODO
- leer solo los nombres y puntuaciones. copiar los ficheros despues del filtrado

2017-06-01	v1.0.0

# Features:
- released 1.0.0
